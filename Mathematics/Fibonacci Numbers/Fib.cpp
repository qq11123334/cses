#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
using namespace std;
struct matrix {
    ll a, b;
    ll c, d;
};
matrix muti(matrix x, matrix y) {
    matrix ans;
    ans.a = (x.a * y.a + x.b * y.c) % MOD;
    ans.b = (x.a * y.b + x.b * y.d) % MOD;
    ans.c = (x.c * y.a + x.d * y.c) % MOD;
    ans.d = (x.c * y.b + x.d * y.d) % MOD;
    return ans;
}
matrix fp(matrix x, ll y) {
    if(y == 0) {
        return {1, 0, 
                0, 1};
    }
    if(y % 2 == 0) {
        matrix ans = fp(x, y / 2);
        return muti(ans, ans);
    }
    if(y % 2 == 1) {
        return muti(x, fp(x, y - 1));
    }
}
int main() {
    ll n;
    cin >> n;
    matrix x = {0, 1, 
                1, 1};
    matrix ans = fp(x, n);
    cout << ans.b << endl;
    return 0;
}