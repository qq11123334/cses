#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
using namespace std;
struct matrix
{
    ll a, b;
    ll c, d;
};
matrix muti(matrix x, matrix y)
{
    matrix ans;
    ans.a = (x.a * y.a + x.b * y.c) % MOD;
    ans.b = (x.a * y.b + x.b * y.d) % MOD;
    ans.c = (x.c * y.a + x.d * y.c) % MOD;
    ans.d = (x.c * y.b + x.d * y.d) % MOD;
    return ans;
}
ll fib(ll n)
{
    matrix base;
    matrix ans;
    ans.a = 1, ans.b = 0, ans.c = 0,ans.d = 1;
    base.a = 0, base.b = 1, base.c = 1, base.d = 1;
    while(n > 0)
    {
        if(n & 1)
        {
            ans = muti(base, ans);
        }
        base = muti(base, base);
        n >>= 1;
    }
    return ans.b;
}
int main()
{
    ll n;
    cin >> n;
    cout << fib(n) << endl;
    return 0;
}