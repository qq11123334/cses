def fp(x, y):
    return (x ** y) % 1000000007
 
t = int(input())
for _ in range(t):
    a, b = map(int, input().split(' '))
    print(fp(a, b))
