#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
ll fp(ll x,ll y,ll p)
{
    ll res = 1;
    while(y)
    {
        if(y & 1) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD; 
    }
    return res;
}
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        ll a,b;
        scanf("%lld%lld",&a,&b);
        printf("%lld\n",fp(a,b,MOD));
    }
    return 0;
}
