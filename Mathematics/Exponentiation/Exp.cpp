#include <bits/stdc++.h>
#define ll long long int
using namespace std;
//假設2ㄉ100
ll Fac(ll n) {
    if(n == 0) return 1;
    return n * Fac(n - 1);
}
// Fac(5) = 5 * 24 = 120
// Fac(4) = 4 * 6 = 24
// Fac(3) = 3 * 2 = 6
// Fac(2) = 2 * 1 = 2
// Fac(1) = 1 * 1 = 1
// Fac(0) = 1

// 2 ^ 100 = 2 ^ 50 * 2 ^ 50
// 2 ^ 50 = 2 ^ 25 * 2 ^ 25
// 2 ^ 25 = 2 * 2 ^ 24
// 2 ^ 24 = 2 ^ 12 * 2 ^ 12
// 2 ^ 12 = 2 ^ 6 * 2 ^ 6
// 2 ^ 6 = 2 ^ 3 * 2 ^ 3
// 2 ^ 3 = 2 * 2 ^ 2
// 2 ^ 2 = 2 * 2
// 2 ^ 1 = 2 
// calculate the x ^ y % p
// fp(x, 1)
ll fp(ll x, ll y,ll p) {
    if(y==0)
    {
        return 1;
    }
    if(y%2==0)
    {
        ll res = fp(x, y/2, p)%p;
        return res*res%p;
    }
    if(y%2==1)
    {
        return (x * fp(x,y-1,p))%p;
    }
}
int main() {
    cin.tie(0), ios_base::sync_with_stdio(false);
    ll a;
    ll b;
    ll n;
    ll p=1e9+7;
    cin>>n;
    for(ll i=0;i<n;i++)
    {
        cin>>a>>b;
        cout<<fp(a,b,p)<<"\n";
    }
        
    return 0;
}