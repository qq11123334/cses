#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
using namespace std;
int n, m, k;
struct Matrix
{
    ll mat[105][105];
}adj;
Matrix Zero()
{
    Matrix zero;
    for(int i = 0;i <= n;i++)
    {
        for(int j = 0;j <= n;j++)
        {
            zero.mat[i][j] = 0;
        }
    }
    return zero;
}
Matrix I()
{
    Matrix a = Zero();
    for(int i = 0;i <= n;i++) a.mat[i][i] = 1;
    return a;
}
Matrix Matrix_muti(Matrix a, Matrix b, ll p)
{
    Matrix res = Zero();
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            for(int k = 1;k <= n;k++)
            {
                if(!a.mat[i][k] || !b.mat[k][j]) continue; // 沙小優化
                res.mat[i][j] = (res.mat[i][j] + a.mat[i][k] * b.mat[k][j]) % p;
            }
        }
    }
    return res;
}
Matrix fp(Matrix x, ll y, ll p)
{
    /*Matrix ans = I();
    Matrix base = x;
    while(y)
    {
        if(y & 1) ans = Matrix_muti(ans, base, p);
        y = y >> 1;
        base = Matrix_muti(base, base, p);
    }
    return ans;*/
    if(y == 0) return I();
    if(y % 2) return Matrix_muti(x, fp(x, y - 1, p), p);
    Matrix res = fp(x, y / 2, p);
    return Matrix_muti(res, res, p);
}
int main()
{
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj.mat[a][b]++;
    }

    Matrix res = fp(adj, k, MOD);
    printf("%lld", res.mat[1][n]);
    return 0;
}