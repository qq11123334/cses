#include <bits/stdc++.h>
#define MAX_N (1000000)
using namespace std;
bool lose[1223];
#if 0
int first_win[MAX_N + 5];
bool used[MAX_N + 5];
#endif
int main()
{
#if 0
    first_win[0] = 0;
    for(int i = 1;i < MAX_N;i++)
    {
        for(int j = 0;j < i;j++) {
            used[j] = 0;
        }
        for(int j = 1;j < i - j;j++) {
            if(j != i - j) {
                used[first_win[j] ^ first_win[i - j]] = 1;
            }
        }
        for(int j = 0;j < i;j++) {
            if(!used[j]) {
                first_win[i] = j;
                break;
            }
        }
        if(i % 10000 == 0) printf("Process %.2f %%\n", 100 * (double)i / (double)MAX_N);
    }
    ofstream output;
    output.open("result.txt" , ios::out | ios::trunc);
    for(int i = 0;i < MAX_N;i++) {
        if(first_win[i] == 0) {
            output << "lose[" << i << "] = 1;" << endl;
        }
    }
    output.close();
#endif
#if 1
    lose[0] = 1;
    lose[1] = 1;
    lose[2] = 1;
    lose[4] = 1;
    lose[7] = 1;
    lose[10] = 1;
    lose[20] = 1;
    lose[23] = 1;
    lose[26] = 1;
    lose[50] = 1;
    lose[53] = 1;
    lose[270] = 1;
    lose[273] = 1;
    lose[276] = 1;
    lose[282] = 1;
    lose[285] = 1;
    lose[288] = 1;
    lose[316] = 1;
    lose[334] = 1;
    lose[337] = 1;
    lose[340] = 1;
    lose[346] = 1;
    lose[359] = 1;
    lose[362] = 1;
    lose[365] = 1;
    lose[386] = 1;
    lose[389] = 1;
    lose[392] = 1;
    lose[566] = 1;
    lose[630] = 1;
    lose[633] = 1;
    lose[636] = 1;
    lose[639] = 1;
    lose[673] = 1;
    lose[676] = 1;
    lose[682] = 1;
    lose[685] = 1;
    lose[923] = 1;
    lose[926] = 1;
    lose[929] = 1;
    lose[932] = 1;
    lose[1222] = 1;
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        scanf("%d", &n);
        printf((n >= 1223 || !lose[n]) ? "first\n" : "second\n");
    }
#endif
    return 0;
}