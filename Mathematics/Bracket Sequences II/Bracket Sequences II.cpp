#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
using namespace std;
ll fp(ll x, ll y, ll p) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % p;
        y >>= 1;
        x = x * x % p;
    }
    return res;
}
ll fac[2000005];
ll mod_reverse(ll x, ll p) {
    return fp(x, p - 2, p);
}
ll C(ll a, ll b) {
    if(a < 0 || b < 0) return 0;
    ll res = fac[a] * mod_reverse(fac[b], MOD) % MOD;
    res *= mod_reverse(fac[a - b], MOD);
    return res % MOD;
}
string s;
ll n; 
void init() {
    fac[0] = 1;
    for(int i = 1;i <= 2 * n;i++) {
        fac[i] = i * fac[i - 1] % MOD;
    }
}
int main() {
    // freopen("input.txt", "r", stdin);
    cin >> n >> s;
    init();

    ll x = 0, y = 0;
    bool isable = 1;
    for(int i = 0;i < (int)s.size();i++) {
        if(s[i] == '(') {
            x++;
        } else {
            y++;
        }
        if(y > x) {
            isable = 0;
        }
    }
    if(n % 2 == 1 || !isable || (x - y) > n - (int)s.size()) {
        cout << 0 << endl;
        return 0;
    }
    n /= 2;
    ll ans = C(2 * n - (x + y), n - x) - C(2 * n - (x + y), n - 1 - x);
    if(ans < 0) {
        ans %= MOD;
        ans += MOD;
    }
    cout << ans << endl;
    return 0;
}