#include <bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d", &t);
    while(t--)
    {
        int ans = 0;
        int n;
        scanf("%d", &n);
        //printf("%d\n", (int)(sqrt(n)));
        for(int i = 1;i < sqrt(n);i++)
        {
            if(n % i == 0) ans += 2;
        }
        int sq = (int)sqrt(n);
        if(sq * sq == n && n % (int)sqrt(n) == 0) ans++;
        printf("%d\n", ans);
    }
    return 0;
}
