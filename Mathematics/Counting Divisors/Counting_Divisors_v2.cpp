#include <bits/stdc++.h>
using namespace std;
const int N = 1e6;
int divisor_cnt[N + 5];
int main() {

    for(int i = 1; i <= N; i++) {
        for(int j = i; j <= N; j += i) {
            divisor_cnt[j]++;
        }
    }
    
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", divisor_cnt[n]);
    }
}
