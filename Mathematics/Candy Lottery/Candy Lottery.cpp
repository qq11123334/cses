#include <bits/stdc++.h>
using namespace std;
double fp(double x, int y)
{
    double ans = 1;
    while(y)
    {
        if(y & 1) ans = ans * x;
        y >>= 1;
        x = x * x;
    }
    return ans;
}
int main()
{
    int n, k;
    double ans = 0;
    scanf("%d%d", &n, &k);
    for(int i = 1;i <= k;i++)
    {
        double rate = fp(i/(double)k, n) - fp((i - 1)/(double)k, n);
        //printf("%f\n", rate);
        ans = ans + i * rate;
    }

    printf("%f\n", ans);
    return 0;
}