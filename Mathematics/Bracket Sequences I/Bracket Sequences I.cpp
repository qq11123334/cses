#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
const int N = 2000005;
using namespace std;
ll fp(ll x, ll y, ll p) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % p;
        y >>= 1;
        x = x * x % p;
    }
    return res;
}
ll fac[N];
ll mod_reverse(ll x, ll p) {
    return fp(x, p - 2, p);
}
ll C(ll a, ll b) {
    ll res = fac[a] * mod_reverse(fac[b], MOD) % MOD;
    res *= mod_reverse(fac[a - b], MOD);
    return res % MOD;
}
int main() {
    ll n; cin >> n;
    fac[0] = 1;
    for(int i = 1;i <= 2 * n;i++) {
        fac[i] = i * fac[i - 1] % MOD;
    }
    if(n % 2 == 1) {
        cout << 0 << endl;
        return 0;
    }
    n /= 2;
    cout << ((C(2 * n, n) - C(2 * n, n - 1)) % MOD + MOD) % MOD;
    return 0;
}