#include <bits/stdc++.h>
#define ll long long int
#define ld long double
#define MOD 1000000007
using namespace std;
tuple<ll, ll, ll> exgcd(ll a, ll b)
{
    if(b == 0) return make_tuple(1, 0, a);
    ll x, y, z;
    tie(x, y, z) = exgcd(b, a % b);
    return make_tuple(y, x - (a / b) * y, z);
}
int main()
{
    ll n;
    scanf("%lld", &n);
    ll ans = 0;
    ll x, y, z;
    tie(x, y, z) = exgcd(2, MOD);
    if(x < 0) x += MOD;
    //printf("%lld\n", x);
    for(ll i = 1;i * i <= n;i++)
    {
        ans = (ans + i * (n / i)) % MOD;
        ll start = max((ll)ceil((ld)sqrt(n)), n / (i + 1) + 1), ending = (n / i);
        ll tmp_start = start, tmp_ending = ending;
        start %= MOD, ending %= MOD;
        ll num = (ending - start + 1) % MOD;
        ll sum = ((start + ending) % MOD * (num)) % MOD;
        sum = (sum * i) % MOD;
        sum = (sum * x) % MOD;
        if(tmp_ending >= tmp_start && i * i < n) ans = (ans + sum) % MOD;
        //printf("%lld %lld %lld %lld\n", start, ending, sum, i * (n / i));
    }
    printf("%lld", ans);
    return 0;
}