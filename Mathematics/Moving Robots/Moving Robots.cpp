#include <bits/stdc++.h>
using namespace std;
int k;
int x_dir[4] = {1, -1, 0, 0};
int y_dir[4] = {0, 0, 1, -1};
double prob[105][8][8];
double prob_emp[8][8][8][8];
bool is_valid(int x, int y)
{
    if (x >= 0 && x < 8 && y >= 0 && y < 8)
        return 1;
    return 0;
}
void init()
{
    for(int i = 0;i <= k;i++)
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                prob[i][x][y] = 0;
            }
        }
    }
}
int main()
{
    init();
    scanf("%d", &k);
    for (int a = 0; a < 8; a++)
    {
        for (int b = 0; b < 8; b++)
        {
            init();
            prob[0][a][b] = 1;
            for (int i = 1; i <= k; i++)
            {
                for (int x = 0; x < 8; x++)
                {
                    for (int y = 0; y < 8; y++)
                    {
                        int cnt = 0;
                        for (int d = 0; d < 4; d++)
                        {
                            if (is_valid(x + x_dir[d], y + y_dir[d])) cnt++;
                        }
                        double rate = 1.0 / cnt;
                        for (int d = 0; d < 4; d++)
                        {
                            int next_x = x + x_dir[d];
                            int next_y = y + y_dir[d];
                            if (is_valid(next_x, next_y))
                            {
                                prob[i][next_x][next_y] = prob[i][next_x][next_y] + prob[i - 1][x][y] * rate;
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    prob_emp[a][b][x][y] = prob[k][x][y];
                    //printf("%f\n", prob[k][x][y]);
                }
            }
        }
    }
    double ans = 0;
    for(int x = 0;x < 8;x++)
    {
        for(int y = 0;y < 8;y++)
        {
            double prob_empty = 1;
            for(int a = 0;a < 8;a++)
            {
                for(int b = 0;b < 8;b++)
                {
                    prob_empty = prob_empty * (1 - prob_emp[a][b][x][y]);
                }
            }
            ans += prob_empty;
        }
    }

    printf("%f\n", ans);
    return 0;
}