#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 1000000;
int mobius[N];
int LPF[N], cnt[N];
void Least_Prime_Factor() {
    for(int i = 2;i <= N;i++) {
        if(!LPF[i]) {
            for(int j = i;j <= N;j += i) {
                if(!LPF[j]) LPF[j] = i;
            }
        }
    }
}
void Mobius() {
    Least_Prime_Factor();
    mobius[1] = 1;
    for(int i = 2;i <= N;i++) {
        if(LPF[i / LPF[i]] == LPF[i]) {
            mobius[i] = 0;
        } else {
            mobius[i] = -1 * mobius[i / LPF[i]];
        }
    }
}
int main() {
    Mobius();
    ll n;
    scanf("%lld", &n);
    for(int i = 0;i < n;i++) {
        ll num;
        scanf("%lld", &num);
        cnt[num]++;
    }

    ll ans =  (n * (n - 1)) / 2;
    for(int i = 2;i <= N;i++) {
        ll cnt_muti = 0;
        for(int j = i;j <= N;j += i) {
            cnt_muti += cnt[j];
        }
        ans += (mobius[i] * (cnt_muti * (cnt_muti - 1) / 2));
    }
    printf("%lld\n", ans);
    return 0;
}