#include <bits/stdc++.h>
#define ll long long int
using namespace std;
inline ll death_this_rnd(ll n) {
    return n - (n / 2);
}
inline tuple<ll, ll, ll> calu_death_pos(ll n, ll k) {
    ll rnd = 0;
    while(k > death_this_rnd(n)) {
        k -= death_this_rnd(n);
        n -= death_this_rnd(n);
        rnd++;
    }
    if(n % 2 == 1 && k == death_this_rnd(n)) return make_tuple(n, rnd, 1);
    else return make_tuple(n, rnd, 2 * k);
}
inline bool is_even(ll n, ll rnd) {
    return (n & (1LL << (rnd - 1))) == 0LL;
}
inline ll Josephus(ll originalN, ll n, ll rnd, ll cur_pos) {
    if(rnd == 0) return cur_pos;
    if(is_even(originalN, rnd)) {
        return Josephus(originalN, 2 * n, rnd - 1, 2 * cur_pos - 1);
    } else {
        return Josephus(originalN, 2 * n + 1, rnd - 1, 2 * cur_pos + 1);
    }
}
int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        ll n, k;
        scanf("%lld%lld", &n, &k);
        auto [after_n, rnd, pos] = calu_death_pos(n, k);
        printf("%lld\n", Josephus(n, after_n, rnd, pos));
    }
    
    return 0;
}

/*

1 2 3 4 5 6 7
3 5 7


1 2 3 4 5 6
1 3 5

*/