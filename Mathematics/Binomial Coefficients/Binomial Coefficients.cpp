#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
#define MAX 1000000
#ifdef _WIN32
#define lld "%I64d"
#else
#define lld "%lld"
#endif
using namespace std;
ll fatorial[1000005];
ll muti(ll a, ll b, ll p)
{
    vector <int> dec;
    while(b > 0)
    {
        dec.push_back(b % 10);
        b = b / 10;
    }
    ll ans = 0;
    for(int i = (int)dec.size() - 1; i >= 0;i--)
    {
        ans = ( ans * 10 + a * dec[i] ) % p;
    }
    return ans;
}
tuple<ll, ll, ll> exgcd(ll a, ll b)
{
    if(b == 0) return make_tuple(1, 0, a);
    ll x, y, z;
    tie(x, y, z) = exgcd(b, a % b);
    return make_tuple(y, x - (a / b) * y, z);
}
void build()
{
    fatorial[0] = 1;
    for(int i = 1;i <= MAX;i++)
    {
        fatorial[i] = fatorial[i - 1] * i % MOD;
    }
}
int main()
{
    build();
    int q;
    scanf("%d",&q);
    while(q--)
    {
        int a, b;
        scanf("%d%d",&a,&b);
        ll ans = fatorial[a];
        ll product = fatorial[b] * fatorial[a - b] % MOD;
        ll x, y, z;
        tie(x, y, z) = exgcd(product, MOD);
        ans = ans * x % MOD;
        if(ans < 0) ans += MOD;
        printf(lld "\n", ans % MOD);
    }
    return 0;
}