#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 5005;
const ll MOD = (ll)1e9 + 7;
ll C[N][N];
int main() {
	for(int i = 0; i < N; i++) {
		C[i][0] = 1;
		for(int j = 1; j <= i; j++) {
			C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % MOD;
		}
	}
	int t;
	cin >> t;
	while(t--) {
		int a, b;
		cin >> a >> b;
		cout << C[a][b] << "\n";
	}
	return 0;
}