#include <bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        int n;
        scanf("%d",&n);
        int sum = 0;
        for(int i = 0;i < n;i++)
        {
            int heap;
            scanf("%d",&heap);
            sum = sum ^ heap;
        }
        if(sum) printf("first\n");
        else printf("second\n");
    }
    return 0;
}