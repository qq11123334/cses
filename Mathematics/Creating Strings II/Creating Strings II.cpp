#include <bits/stdc++.h>
#define ll long long int
#define MAX 1000000
#define MOD 1000000007
#define tp tuple<int,int,int>
#define mktp(x, y, z) make_tuple(x, y, z);
using namespace std;
int cnt[26];
ll factorial[MAX + 5];
ll ans;
void build()
{
    factorial[0] = 1;
    for(int i = 1;i <= MAX;i++)
    {
        factorial[i] = factorial[i - 1] * i % MOD;
    }
}
tp exgcd(ll a, ll b)
{
    if(b == 0) return mktp(1, 0, a);
    ll x, y, g;
    tie(x, y, g) = exgcd(b, a % b);
    return mktp(y, x - y * (a/b), g);
}
int main()
{
    string str;
    cin >> str;
    for(int i = 0;i < (int)str.size();i++)
    {
        cnt[str[i] - 'a']++;
    }
    
    build();

    ans = factorial[str.size()];

    for(int i = 0;i < 26;i++)
    {
        if(cnt[i] == 0 || cnt[i] == 1) continue;
        ll x, y, g;
        tie(x, y, g) = exgcd(factorial[cnt[i]], MOD);
        ans = ans * x % MOD;
    }
    if(ans < 0) ans += MOD;
    printf("%lld\n", ans);
    return 0;
}