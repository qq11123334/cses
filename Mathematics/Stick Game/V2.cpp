#include <bits/stdc++.h>
using namespace std;
int dp[1000005], p[105];
int main()
{
    int n, k;
    scanf("%d%d", &n, &k);
    dp[0] = 0;
    dp[1] = 1;

    for(int i = 0;i < k;i++)
    {
        scanf("%d", &p[i]);
    }

    for(int i = 2;i <= n;i++)
    {
        for(int j = 0;j < k;j++)
        {
            // if dp[i - p[j]] is lose, then dp[i] will win
            if(i - p[j] >= 0) dp[i] |= (!dp[i - p[j]]);
        }
    }

    for(int i = 1;i <= n;i++)
    {
        printf("%c", dp[i] ? 'W' : 'L');
    }
    return 0;
}