#include <bits/stdc++.h>
using namespace std;
int dp[1000005][2], p[105];
int main()
{
    int n, k;
    scanf("%d%d", &n, &k);
    for(int i = 0;i < k;i++)
    {
        scanf("%d", &p[i]);
    }

    dp[0][0] = -1;
    dp[0][1] = 1;
    for(int i = 1;i <= n;i++)
    {
        dp[i][0] = -1;
        dp[i][1] = 1;
        for(int j = 0;j < k;j++)
        {
            if(i - p[j] >= 0)
            {
                if(dp[i - p[j]][1] == 1) dp[i][0] = 1;
                if(dp[i - p[j]][0] == -1) dp[i][1] = -1;
            }
        }
    }

    for(int i = 1;i <= n;i++)
    {
        printf("%c", dp[i][0] == 1 ? 'W' : 'L');
    }
    return 0;
}