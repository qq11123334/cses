#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
ll fp(ll x,ll y,ll p)
{
    if(y == 1) return x;
    if(y == 0) return 1;
    if(y % 2) return fp(x,y - 1,p) * x % p;
    ll ans = fp(x,y/2,p);
    return ans * ans % p;
}
using namespace std;
int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        ll a,b,c;
        scanf("%lld%lld%lld",&a,&b,&c);
        printf("%lld\n",fp(a,fp(b,c,MOD - 1),MOD));
    }
    return 0;
}