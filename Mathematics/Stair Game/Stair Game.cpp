#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int main()
{
    int t;
    scanf("%d", &t);
    while(t--)
    {
        ll n;
        scanf("%lld", &n);
        ll xor_sum = 0;
        for(int i = 1;i <= n;i++)
        {
            ll ball;
            scanf("%lld", &ball);
            if(i % 2 == 0) {
                xor_sum ^= ball;
            }
        }
        if(xor_sum) printf("first\n");
        else printf("second\n");
    }
    return 0;
}