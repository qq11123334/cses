#include <bits/stdc++.h>
#define MAX 1000000
#define N 100000
#define ll long long int
using namespace std;
bool sieve[MAX + 1005];
vector <ll> prime_table;
void build()
{
    for(ll i = 2;i <= MAX + 1000;i++)
    {
        if(sieve[i]) continue;
        prime_table.push_back(i);
        for(ll u = i * i;u <= MAX + 1000;u += i)
        {
            sieve[u] = 1;
        }
    }
}
int main()
{
    //freopen("sample_1.in", "w", stdout);
    build();
    int cnt = 0;
    while(prime_table[cnt] < MAX)
    {
        cnt++;
    }
    printf("%d\n", cnt);
    for(int i = 0;i < cnt;i++)
    {
        if(i) printf(" ");
        printf("%d", (int)prime_table[i]);
    }
    printf("\n");
    return 0;
}