#include <bits/stdc++.h>
#define MAX 1000000
using namespace std;
int cnt[MAX + 5];
int main()
{
    freopen("sample_1.in", "r", stdin);
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++)
    {
        int num;
        scanf("%d", &num);
        cnt[num]++;
    }
    for(int i = MAX;i >= 0;i--)
    {
        int cur = 0;
        for(int j = i;j <= MAX;j += i)
        {
            cur += cnt[j];
        }
        if(cur > 1)
        {
            printf("%d\n", i);
            break;
        }
    }
    return 0;
}