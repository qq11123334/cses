#include <bits/stdc++.h>
using namespace std;
unordered_map<int,int> cnt_divisor;
int main()
{
    int ans = 1;
    int n;
    scanf("%d", &n);
    //printf("%d\n", (int)(sqrt(n)));
    for(int j = 0;j < n;j++)
    {
        int num;
        scanf("%d", &num);
        for (int i = 1; i < sqrt(num); i++)
        {
            //printf("%d\n", i);
            if(num / i < ans) break;  //重要回溯
            if (num % i == 0)
            {
                //printf("%d %d\n",i, num/i);
                if(++cnt_divisor[i] == 2) ans = max(ans, i);
                if(++cnt_divisor[num/i] == 2) ans = max(ans, num/i);
            }
        }
        int sq = (int)sqrt(num);
        if (sq * sq == num && num % sq == 0)
        {
            if(++cnt_divisor[sq] == 2) ans = max(ans, sq);
        }
    }
    printf("%d\n", ans);
    return 0;
}