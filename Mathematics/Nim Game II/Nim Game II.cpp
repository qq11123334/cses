#include <bits/stdc++.h>
using namespace std;
int main()
{
    int t;
    scanf("%d", &t);
    while(t--)
    {
        int n;
        scanf("%d", &n);
        int xor_sum = 0;
        for(int i = 0;i < n;i++)
        {
            int num;
            scanf("%d", &num);
            xor_sum ^= num;
        }
        if(xor_sum & 1 || xor_sum & (1 << 1)) printf("first\n");
        else printf("second\n");
    }
    return 0;
}