#include <bits/stdc++.h>
#define ll long long int
#define INF (ll)0x3f3f3f3f3f3f3f3f
using namespace std;
int n, m, k;
struct Matrix
{
    ll mat[105][105];
}adj;
Matrix Zero()
{
    Matrix zero;
    for(int i = 0;i <= n;i++)
    {
        for(int j = 0;j <= n;j++)
        {
            zero.mat[i][j] = INF;
        }
    }
    return zero;
}
Matrix I()
{
    Matrix a = Zero();
    for(int i = 0;i <= n;i++) a.mat[i][i] = 0;
    return a;
}
Matrix Matrix_muti(Matrix a, Matrix b)
{
    Matrix res;
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            res.mat[i][j] = INF;
            for(int k = 1;k <= n;k++)
            {
                res.mat[i][j] = min(res.mat[i][j], a.mat[i][k] + b.mat[k][j]);
            }
        }
    }
    return res;
}
Matrix fp(Matrix x, ll y)
{
    /*
    Matrix ans = I();
    Matrix base = x;
    while(y)
    {
        if(y & 1) ans = Matrix_muti(ans, base);
        y = y >> 1;
        base = Matrix_muti(base, base);
    }
    return ans;*/
    if(y == 0) return I();
    if(y % 2) return Matrix_muti(x, fp(x, y - 1));
    Matrix res = fp(x, y / 2);
    return Matrix_muti(res, res);
}
int main()
{
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < m;i++)
    {
        ll a, b, w;
        scanf("%lld%lld%lld", &a, &b, &w);
        if(adj.mat[a][b]) adj.mat[a][b] = min(adj.mat[a][b], w); // 被陰
        else adj.mat[a][b] = w;
    }

    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            if(adj.mat[i][j] == 0) adj.mat[i][j] = INF;
        }
    }

    Matrix res = fp(adj, k);
    printf("%lld", res.mat[1][n] == INF ? -1 : res.mat[1][n]);
    return 0;
}