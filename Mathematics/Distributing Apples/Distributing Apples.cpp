#include <bits/stdc++.h>
#define ll long long int
#define MAX 2000000
#define MOD 1000000007
using namespace std;
ll factorial[MAX + 5];
void build()
{
    factorial[0] = 1;
    for(int i = 1;i <= MAX;i++)
    {
        factorial[i] = factorial[i - 1] * i % MOD;
    }
}
tuple<int,int,int> exgcd(ll a, ll b)
{
    if(b == 0) 
    {
        return make_tuple(1, 0, a);
    }
    ll x, y, g;
    tie(x, y, g) = exgcd(b, a % b);
    return make_tuple(y, x - y * (a/b), g);
}
int main()
{
    build();
    int a, b;
    scanf("%d%d", &a, &b);
    ll ans = factorial[a + b - 1];
    ll x, y, g;
    tie(x, y, g) = exgcd(factorial[b], MOD);
    ans = ans * x % MOD;
    tie(x, y, g) = exgcd(factorial[a - 1], MOD);
    ans = ans * x % MOD;
    if(ans < 0) ans += MOD;
    printf("%d", (int)ans);
    return 0;
}