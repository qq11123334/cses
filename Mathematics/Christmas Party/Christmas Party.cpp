#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
#define tp tuple<int, int, int>
using namespace std;
ll fac[1000005];
tp exgcd(ll a, ll b)
{
    if(b == 0) return make_tuple(1, 0, a);
    int x, y, g;
    tie(x, y, g) = exgcd(b, a % b);
    return make_tuple(y, x - (a/b) * y, g);
}
ll C(int n, int m)
{
    // n! / ((n - m)! * m!)
    ll ans = fac[n];
    ll x, y, z;
    tie(x, y, z) = exgcd(fac[n - m], MOD);
    ans = ans * x % MOD;
    tie(x, y, z) = exgcd(fac[m], MOD);
    ans = ans * x % MOD;
    return ans;
}
int main()
{
    int n;
    scanf("%d", &n);
    fac[0] = 1;
    for(int i = 1;i <= n;i++)
    {
        fac[i] = fac[i - 1] * i % MOD;
    }
    ll sum = fac[n];

    for(int i = 1;i <= n;i++)
    {
        if(i % 2) sum = (sum - C(n, i) * fac[n - i]) % MOD;
        else sum = (sum + C(n, i) * fac[n - i]) % MOD;
    }

    if(sum < 0) sum += MOD;
    printf("%lld", sum);

    return 0;
}