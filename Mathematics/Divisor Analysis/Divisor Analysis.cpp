#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1000000007)
using namespace std;
const int N = 100005;
ll base[N], expon[N];
struct Node {
    int left, right, mid;
    ll val;
    Node *rc, *lc;
    void pull() {
        val = (lc->val * rc->val) % (MOD - 1);
    }
}node[2 * N], *last_node = node, *root_node;
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->val = expon[left] + 1;
        return;
    }
    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
ll qry(Node *r, int pos) {
    if(r->left == r->right) {
        return 1;
    }
    if(pos <= r->mid) {
        return (r->rc->val * qry(r->lc, pos)) % (MOD - 1);
    } else {
        return (r->lc->val * qry(r->rc, pos)) % (MOD - 1);
    }
}
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % MOD;
        x = x * x % MOD;
        y >>= 1;
    }
    return res;
}
tuple<ll, ll, ll> exgcd(ll a, ll b) {
    if(b == 0) {
        return make_tuple(1, 0, a);
    } else {
        ll x, y, z;
        tie(x, y, z) = exgcd(b, a % b);
        return make_tuple(y, x - (a / b) * y, z);
    }
}
ll mod_reverse(ll x, ll p) {
    ll a, b, c;
    tie(a, b, c) = exgcd(x, p);
    if(a < 0) {
        a %= p;
        a += p;
    }
    return a % p;
}
ll Geometric_sum(ll n, ll r) {
    // 1, r, r^2, r^3 .. r^(n - 1)
    return ((fp(r, n) - 1) * mod_reverse(r - 1, MOD)) % MOD;
}
ll arithmetic_sum(ll n) {
    // 1 + 2 + 3 + 4 .. + n
    return ((n * (n + 1) / 2));
}
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        scanf("%lld%lld", &base[i], &expon[i]);
    }
    build(root_node = last_node++, 0, n - 1);
    ll ans_num = 1;
    // ll ans_num_MOD1 = 1;
    for(int i = 0;i < n;i++) {
        ans_num *= (expon[i] + 1);
        ans_num %= MOD;
        // ans_num_MOD1 *= (expon[i] + 1);
        // ans_num_MOD1 %= (MOD - 1);
    }
    ll ans_sum = 1;
    for(int i = 0;i < n;i++) {
        ans_sum *= (Geometric_sum(expon[i] + 1, base[i]));
        ans_sum %= MOD;
    }
    ll ans_prod = 1;
    for(int i = 0;i < n;i++) {
        ll ans_num_MOD1 = qry(root_node, i);
        ans_prod *= fp(fp(base[i], arithmetic_sum(expon[i]) % (MOD - 1)), ans_num_MOD1);
        ans_prod %= MOD;
    }

    printf("%lld %lld %lld\n", ans_num, ans_sum, ans_prod);
    return 0;
}
// 2 3
// 3 3
// 5^1 * 5^2 * 5^3 .. 5^(500000002) = 5^(1 + 2 + 3 + 4 ... + 500000002)