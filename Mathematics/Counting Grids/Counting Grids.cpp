#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD;
    }
    return res;
}
using namespace std;
int main() {
    ll n;
    cin >> n;
    ll ans = 0;
    ll is_odd = (n % 2 == 1) + 1;
    ans += fp(2, n * n);
    ans %= MOD;
    ans += fp(2, ((n + 1) / 2) * (n / 2)) * is_odd;
    ans %= MOD;
    ans += fp(2, (n * n) / 2) * is_odd;
    ans %= MOD;
    ans += fp(2, ((n + 1) / 2) * (n / 2)) * is_odd;
    ans %= MOD;
    ans *= fp(4, MOD - 2);
    ans %= MOD;
    printf("%lld\n", ans);
    return 0;
}