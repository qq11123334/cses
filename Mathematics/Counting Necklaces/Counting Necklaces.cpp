#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
using namespace std;
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD;
    }
    return res;
}
ll gcd(ll a, ll b) {
    if(b == 0) {
        return a;
    } else {
        return gcd(b, a % b);
    }
}
int main() {
    ll n, m;
    scanf("%lld %lld", &n, &m);
    ll ans = 0;
    for(int i = 0;i < n;i++) {
        ans += fp(m, gcd(i, n));
        ans %= MOD;
    }

    ll RevN = fp(n, MOD - 2);
    ans *= RevN;
    ans %= MOD;
    
    printf("%lld\n", ans);
    return 0;
}