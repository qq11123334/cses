#include <bits/stdc++.h>
#define MAX 600
#define ll long long int
double dp[105][MAX + 5];
using namespace std;
int main()
{
    int n, a, b;
    scanf("%d%d%d", &n, &a, &b);
    dp[0][0] = 1;
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= 6;j++)
        {
            for(int k = 0;k + j <= MAX;k++)
            {
                if(k - j >= 0) dp[i][k] = dp[i][k] + dp[i - 1][k - j] / 6;
            }
        }
    }

    double ans = 0;
    for(int i = a;i <= b;i++) ans += dp[n][i];

    printf("%f\n", ans);
    return 0;
}