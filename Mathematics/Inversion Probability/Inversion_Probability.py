from decimal import *

getcontext().prec = 250

n = int(input())

arr = list(map(int, input().split(' ')))

ans1 = Decimal(0)
ans2 = Decimal(0)

for i in range(n):
    for j in range(i + 1, n):
        if arr[i] > arr[j]:
            ans1 += Decimal(arr[j] - 1) / Decimal(arr[i])
            ans2 += Decimal(arr[i] - arr[j]) / Decimal(arr[i])
        else:
            ans1 += Decimal(arr[i] - 1) / Decimal(arr[j])
print(format(ans1 / 2 + ans2, ".6f"))
