#include <bits/stdc++.h>
using namespace std;
const long double EPS = 1e-15;
long double prob_inver(long double a, long double b)
{
    /*
    long double ans = 0;
    for(int i = 1; i <= b;i++)
    {
        if(a > i) ans = ans + ((a - i) / a);
    }
    return ans / b;
    */

    long double i = min(a, b);
    return (a * i - (i * (i + 1)) / 2) / (a * b);
}
int arr[105];
int main()
{
    int n;
    long double ans = 0;

    scanf("%d", &n);
    for(int i = 0;i < n;i++)
    {
        scanf("%d", &arr[i]);
    }

    for(int i = 0;i < n - 1;i++)
    {
        for(int j = i + 1;j < n;j++)
        {
            ans = ans + prob_inver(arr[i], arr[j]);
        }
    }
    printf("%Lf\n", ans + EPS);
    fprintf(stderr, "%.20LF", ans + EPS);
    return 0;
}
