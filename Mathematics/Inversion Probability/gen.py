n = 100
f = open("input.txt", "w")

f.write(str(n) + "\n")

for i in range(n):
    if i % 2 == 0:
        f.write("1")
    else:
        f.write("100")

    if i != n - 1:
        f.write(" ")
    else:
        f.write("\n")
