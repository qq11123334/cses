#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll p[25];
ll x, n;
ll ans;
bitset<25> b;
int my_log10(ll x) {
    int cnt = 0;
    while(x >= 10) {
        cnt++;
        x /= 10;
    }
    return cnt;
}
ll cur_subset_lcm() {
    ll res = 1;
    for(int i = 0;i < n;i++) {
        if(b[i]) {
            if(my_log10(res) + my_log10(p[i]) >= 18)
                return -1;
            res *= p[i];
        }
    }
    return res;
}
void DFS(int level) {
    if(level == n) {
        ll lcm = cur_subset_lcm();
        if(lcm != -1) {
            if(b.count() % 2 == 1) {
                ans += x / lcm;
            } else {
                ans -= x / lcm;
            }
        }
        return;
    }

    b[level] = 0;
    DFS(level + 1);
    b[level] = 1;
    DFS(level + 1);
}
int main() {
    scanf("%lld%lld", &x, &n);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &p[i]);
    }

    DFS(0);
    printf("%lld\n", x + ans);
    return 0;
}