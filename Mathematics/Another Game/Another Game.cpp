#include <bits/stdc++.h>
using namespace std;
int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        bool first_win = 0;
        int n;
        scanf("%d", &n);
        for(int i = 0;i < n;i++) {
            int heap;
            scanf("%d", &heap);
            if(heap % 2 == 1) first_win = 1;
        }
        if(first_win) printf("first\n");
        else printf("second\n");
    }
    return 0;
}