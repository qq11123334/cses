n = int(input())
s = set()
for _ in range(n):
    s.add(_ + 1)

A = []
A = list(map(int, input().split(' ')))
for i in range(n - 1):
    s.remove(A[i])

print(s.pop())