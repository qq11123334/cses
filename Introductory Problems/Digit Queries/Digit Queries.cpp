#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll fp(ll x, ll y)
{
    ll res = 1;
    while(y)
    {
        if(y & 1) res *= x;
        x = x * x;
        y >>= 1;
    }
    return res;
}
vector<int> itov(ll x)
{
    vector<int> res;
    while(x)
    {
        res.push_back(x % 10);
        x /= 10;
    }
    reverse(res.begin(), res.end());
    return res;
}
int main()
{
    // freopen("input.txt", "r", stdin);
    // freopen("output.txt", "w", stdout);
    int t;
    scanf("%d", &t);
    while(t--)
    {
        ll k;
        scanf("%lld", &k);
        ll cur_cnt = 9;
        ll cur_digit = 1;
        ll sum_digit = 0;
        while(sum_digit + cur_cnt * cur_digit < k)
        {
            sum_digit = sum_digit + (cur_digit) * (cur_cnt);
            cur_digit++;
            cur_cnt *= 10;
        }
        // printf("sum_digit=%lld\n", cur_digit);
        ll ith_digit = (k - sum_digit) % (cur_digit);
        if(ith_digit == 0) ith_digit = cur_digit;
        ll ith_number = (k - sum_digit) / (cur_digit) + (ith_digit != cur_digit);
        ll the_number = fp(10, cur_digit - 1) + ith_number - 1;
        // printf("%lld %lld %lld\n", ith_digit, ith_number, the_number);
        vector<int> number = itov(the_number);
        printf("%d\n", number[ith_digit - 1]);
    }
    return 0;
}


// 1234567891011121314