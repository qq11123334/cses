#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int main()
{
    int t;
    cin >> t;
    while(t--)
    {
        ll x,y;
        cin >> x >> y;
        ll n = max(x,y);
        if(n % 2 == 0)
        {
            if(y == n)
            {
                printf("%lld\n",(n-1)*(n-1) + x);
            }
            else
            {
                printf("%lld\n",(n-1)*(n-1) + n + (n-y));
            }
        }
        else
        {
            if(x == n)
            {
                printf("%lld\n",(n-1)*(n-1) + y);
            }
            else
            {
                printf("%lld\n",(n-1)*(n-1) + n + (n-x));
            }
            
        }
    }
    return 0;
}