#include <iostream>
#include <cstdio>
#define ll long long
using namespace std;
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    ll n;
    cin >> n;
    while(n != 1)
    {
        printf("%lld ",n);
        if(n % 2 == 0)
        {
            n = n / 2;
        }else
        {
            n = 3*n + 1;
        }
    }
    printf("1");
    return 0;
}