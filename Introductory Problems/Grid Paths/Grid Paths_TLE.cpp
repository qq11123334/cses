// #pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
// #pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
char str[50];
int cnt = 0;
bool vis[50][50];
bool is_valid(int x, int y)
{
    if (x >= 0 && x < 7 && y >= 0 && y < 7 && !vis[x][y]) return 1; //改成 && 就會 AC
    return 0;
}
void search(int level, int x, int y)
{
    //printf("%d %d %d\n",level,x,y);

    // 若遇到只能走上下或左右就return 重要的回溯
    if(is_valid(x + 1, y) && is_valid(x - 1, y) && !is_valid(x, y - 1) && !is_valid(x, y + 1)) return;
    if(!is_valid(x + 1, y) && !is_valid(x - 1, y) && is_valid(x, y - 1) && is_valid(x, y + 1)) return;
    if (level == 48)
    {
        if(x == 6 && y == 0) cnt++;
        return;
    }
    if(x == 6 && y == 0) return;

    vis[x][y] = 1;
    if (str[level] == 'U')
    {
        if(is_valid(x - 1, y)) search(level + 1, x - 1, y);
    }
    else if (str[level] == 'D')
    {
        if(is_valid(x + 1, y)) search(level + 1, x + 1, y);
    }
    else if (str[level] == 'L')
    {
        if(is_valid(x, y - 1)) search(level + 1, x, y - 1);
    }
    else if (str[level] == 'R')
    {
        if(is_valid(x, y + 1)) search(level + 1, x, y + 1);
    }
    else if(str[level] == '?')
    {
        if(is_valid(x - 1, y)) search(level + 1, x - 1, y);
        if(is_valid(x + 1, y)) search(level + 1, x + 1, y);
        if(is_valid(x, y - 1)) search(level + 1, x, y - 1);
        if(is_valid(x, y + 1)) search(level + 1, x, y + 1);
    }
    vis[x][y] = 0;
}
int main()
{
    for (int i = 0; i < 48; i++)
    {
        cin >> str[i];
    }
    search(0,0,0);
    printf("%d",cnt);
    return 0;
}