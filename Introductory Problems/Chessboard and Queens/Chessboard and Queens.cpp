#include <bits/stdc++.h>
using namespace std;
bool grid[8][8];
int column[8], dia1[15], dia2[15];
int cnt = 0;
void search(int x)
{
    //printf("%d",x);
    if (x == 8)
    {
        cnt++;
        return;
    }
    for (int y = 0; y < 8; y++)
    {
        if (column[y] || dia1[8 + y - x - 1] || dia2[x + y] || grid[x][y])
            continue;
        column[y] = dia1[8 + y - x - 1] = dia2[x + y] = 1;
        search(x + 1);
        column[y] = dia1[8 + y - x - 1] = dia2[x + y] = 0;
    }
}
int main()
{
    memset(column, 0, sizeof(column));
    memset(dia1, 0, sizeof(dia1));
    memset(dia2, 0, sizeof(dia2));
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            char c;
            scanf(" %c", &c);
            if (c == '*')
            {
                grid[i][j] = 1;
            }
            else
            {
                grid[i][j] = 0;
            }
        }
    }
    search(0);
    printf("%d", cnt);
    return 0;
}