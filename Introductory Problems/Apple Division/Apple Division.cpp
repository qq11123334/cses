#include <bits/stdc++.h>
#define ll long long
#define INF 0x3f3f3f3f3f
using namespace std;
ll mi = INF;
ll all_sum = 0;
int n;
int arr[30] = {0};
void search(int level, ll sum)
{
    if (level == n)
    {
        mi = min(mi, abs(all_sum - 2 * sum));
        return;
    }
    search(level + 1, sum + arr[level]);
    search(level + 1, sum);
}
int main()
{
    memset(arr, 0, sizeof(arr));
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
        all_sum = all_sum + arr[i];
    }
    search(0, 0);
    printf("%lld", mi);
    return 0;
}