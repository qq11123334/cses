#include <iostream>
#include <cstdio>
using namespace std;
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int t;
    cin >> t;
    while(t--)
    {
        int a,b;
        cin >> a >> b;
        if(a > b)
        {
            swap(a,b);
        }
        if(a == 0 && b != 0)
        {
            printf("NO\n");
            continue;
        }
        if(a == 1 && b != 2)
        {
            printf("NO\n");
            continue;
        }
        if(a*2 < b)
        {
            printf("NO\n");
            continue;
        }
        int dif = b - a;
        b = b - 2 * dif;
        a = a - dif;
        if(a % 3 == 0 && b % 3 == 0)
        {
            printf("YES\n");
        }
        else
        {
            printf("NO\n");
        }
        
    }
    return 0;
}