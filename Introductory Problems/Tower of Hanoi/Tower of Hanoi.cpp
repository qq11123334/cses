#include <bits/stdc++.h>
using namespace std;
int dp[20];
int hanoi_times(int n)
{
    if(dp[n]) return dp[n];
    if(n == 1) return 1;
    return dp[n] = hanoi_times(n - 1) + 1 + hanoi_times(n - 1);
}
void hanoi_output(int n, int stack1, int stack2, int stack3)
{
    if(n == 1)
    {
        printf("%d %d\n", stack1, stack3);
        return;
    }
    hanoi_output(n - 1, stack1, stack3, stack2);
    hanoi_output(1, stack1, stack2, stack3);
    hanoi_output(n - 1, stack2, stack1, stack3);
}
int main()
{
    int n;
    scanf("%d", &n);
    printf("%d\n", hanoi_times(n));
    hanoi_output(n, 1, 2, 3);

    // h(3, a, b, c)
    //      h(2, a, c, b)
    //          h(1, a, b, c)
    //              a, c
    //          h(1, a, c, b)
    //              a, b
    //          h(1, c, a, b)
    //              c, b
    return 0;
}