#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int main()
{
    ll n;
    cin >> n;
    for (ll i = 1; i <= n; i++)
    {
        ll ans = (i * i) * (i * i - 1);
        ans = ans - (4 * 2 + 8 * 3 + 4 * (i - 4) * 4 + 4 * 4 + 4 * (i - 4) * 6 + (i - 4)*(i - 4) * 8);
        printf("%lld\n", ans/2);
    }
    return 0;
}