#include <bits/stdc++.h>
using namespace std;
vector<int> ans1, ans2;
int main()
{
    int n;
    cin >> n;
    if (n % 4 == 1 || n % 4 == 2)
    {
        printf("NO");
    }
    else
    {
        printf("YES\n");
        if (n % 4 == 3)
        {
            ans1.push_back(1);
            ans1.push_back(2);
            ans2.push_back(3);
            for (int i = 4; i <= n; i++)
            {
                if (i % 4 == 0 || i % 4 == 3)
                {
                    ans1.push_back(i);
                }
                else
                {
                    ans2.push_back(i);
                }
            }
        }
        else
        {
            for (int i = 1; i <= n; i++)
            {
                if (i % 4 == 1 || i % 4 == 0)
                {
                    ans1.push_back(i);
                }
                else
                {
                    ans2.push_back(i);
                }
            }
        }
        printf("%d\n", ans1.size());
        for (int i = 0; i < ans1.size(); i++)
        {
            printf("%d ", ans1[i]);
        }
        printf("\n");
        printf("%d\n", ans2.size());
        for (int i = 0; i < ans2.size(); i++)
        {
            printf("%d ", ans2[i]);
        }
    }
    return 0;
}