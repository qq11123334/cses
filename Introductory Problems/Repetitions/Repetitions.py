S = str(input())

length = len(S)
ans = 1
cur = 1
for i in range(length - 1):
    if S[i + 1] == S[i]:
        cur = cur + 1
    else:
        cur = 1
    ans = max(ans, cur)

print(ans)
