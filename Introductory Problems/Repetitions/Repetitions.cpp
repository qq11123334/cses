#include <iostream>
#include <cstdio>
using namespace std;
int main()
{
    char c, last = 'E';
    int count = 0, mx = 1;
    while ((c = getchar()) != EOF)
    {
        if (c == last)
        {
            count++;
            mx = max(mx, count);
        }else
        {
            count = 1;
        }
        last = c;
    }
    printf("%d",mx);
    return 0;
}