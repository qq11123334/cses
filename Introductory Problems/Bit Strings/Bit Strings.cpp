#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const ll MOD = 1000000007;
ll fp(ll a, ll b, ll c)
{
    if (b == 0)
    {
        return 1;
    }
    if(b == 1)
    {
        return a;
    }
    ll ans;
    if(b % 2 == 0)
    {
        ans = fp(a,b/2,c) %c;
        return ans*ans %c;
    }
    else
    {
        ans = fp(a,b/2,c) % c;
        return ans * ans * fp(a,1,c) % c;
    }
    
}
int main()
{
    ll n;
    cin >> n;
    printf("%lld",fp(2,n,MOD));
    return 0;
}