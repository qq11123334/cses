#include <bits/stdc++.h>
using namespace std;
bool bit[20];
int n;
void change(int level)
{
    bit[level] = !bit[level];
}
void print()
{
    for(int i = 0;i < n;i++)
        printf("%d", bit[i]);
    printf("\n");
}
void DFS(int level)
{
    if(level == n)
    {
        print();
        return;
    }

    DFS(level + 1);
    change(level);
    DFS(level + 1);
}
int main()
{
    scanf("%d", &n);
    DFS(0);
    return 0;
}