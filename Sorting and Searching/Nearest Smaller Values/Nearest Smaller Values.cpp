#include <bits/stdc++.h>
#define pii pair<int,int>
#define val first
#define id second
using namespace std;
stack <pii >arr;  
int main()
{
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n;i++)
    {
        int num;
        scanf("%d",&num);
        while(!arr.empty())
        {
            pii top = arr.top();
            if(top.val < num) break;
            arr.pop();
        }

        int x = 0;
        if(!arr.empty()) x = arr.top().id;
        printf("%d ",x);
        arr.push({num,i+1});
    }
    return 0;
}