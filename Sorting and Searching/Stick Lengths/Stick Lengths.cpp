#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int arr[200005];
int main()
{
    int n;
    scanf("%d",&n);
    for (int i = 0;i < n;i++)
    {
        scanf("%d",&arr[i]);
    }
    sort(arr,arr+n);
    int median = arr[n/2];
    ll cost = 0;
    for (int i = 0;i < n;i++)
    {
        cost = cost + abs(median - arr[i]);
    }
    printf("%lld",cost);
    return 0;
}