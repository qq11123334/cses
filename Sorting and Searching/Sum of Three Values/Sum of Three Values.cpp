#include <bits/stdc++.h>
using namespace std;
struct Num
{
    int val;
    int id;
}arr[5005];
bool num_compare(Num a,Num b)
{
    return a.val < b.val;
}
int main()
{
    int n,x;
    scanf("%d%d",&n,&x);

    for(int i = 0;i < n;i++)
    {
        scanf("%d",&arr[i].val);
        arr[i].id = i + 1;
    }

    sort(arr,arr+n,num_compare);

    for(int i = 0;i < n - 1;i++)
    {
        for(int j = i+1;j < n;j++)
        {
            int l = 0,r = n;
            while(l < r - 1)
            {
                int m = (r - l)/2 + l;
                if(arr[i].val + arr[j].val + arr[m].val > x)
                {
                    r = m;
                }
                else
                {
                    l = m;
                }                
            }
            if(i != l && j != l && arr[i].val + arr[j].val + arr[l].val == x)
            {
                printf("%d %d %d",arr[i].id,arr[j].id,arr[l].id);
                return 0;
            }
        }
    }
    printf("IMPOSSIBLE");
    return 0;
}