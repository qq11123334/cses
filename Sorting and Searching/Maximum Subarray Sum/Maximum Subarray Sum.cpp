#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
using namespace std;
ll arr[200005];
int main()
{
    int n;
    scanf("%d",&n);
    for (int i = 0;i < n;i++)
    {
        scanf("%lld",&arr[i]);
    }
    ll sum = -INF,mx = -INF;
    for (int i = 0;i < n;i++)
    {
        //printf("%lld %lld\n",sum + arr[i],arr[i]);
        sum = max(sum + arr[i],arr[i]);
        mx = max(sum,mx);
    }
    printf("%lld",mx);
    return 0;
}