#include <bits/stdc++.h>
using namespace std;
struct Range
{
    int l, r;
    int index;
}range[200005];
bool cmp_sb(Range a, Range b) // small to big
{
    if(a.l != b.l) return a.l < b.l;
    return a.r > b.r;
}
bool cmp_bs(Range a, Range b) // big to small
{
    if(a.l != b.l) return a.l > b.l;
    return a.r < b.r;
}
int ans1[200005];
int ans2[200005];
int n;
void print(int *arr)
{
    for(int i = 0;i < n;i++)
        printf("%d ", arr[i]);
    printf("\n");
}
int main()
{
    scanf("%d", &n);
    for(int i = 0;i < n;i++)
    {
        scanf("%d%d", &range[i].l, &range[i].r);
        range[i].index = i;
    }

    
    sort(range, range + n, cmp_bs);
    if(range[0].l == range[1].l && range[0].r == range[1].r)
        ans1[range[0].index] = 1;

    int cur_small = range[0].r;
    for(int i = 1;i < n;i++)
    {
        if(cur_small <= range[i].r)
            ans1[range[i].index] = 1;
        else
            ans1[range[i].index] = 0;
        
        cur_small = min(cur_small, range[i].r);
    }

    sort(range, range + n, cmp_sb);
    if(range[0].l == range[1].l && range[0].r == range[1].r)
        ans2[range[0].index] = 1;

    int cur_max = range[0].r;
    for(int i = 1;i < n;i++)
    {
        if(cur_max >= range[i].r)
            ans2[range[i].index] = 1;
        else
            ans2[range[i].index] = 0;
        
        cur_max = max(cur_max, range[i].r);
    }

    print(ans1);
    print(ans2);
    return 0;
}