#include <bits/stdc++.h>
using namespace std;
map<int, int> val_to_idx;
struct Range {
    int left, right;
    int idx;
}range[200005];
bool cmp_bs(Range a, Range b) {// big to samll

    if(a.left != b.left) return a.left > b.left;
    return a.right < b.right;
}
bool cmp_sb(Range a, Range b) {// samll to big
    if(a.left != b.left) return a.left < b.left;
    return a.right > b.right;
}
int ans_contained_by[200005], ans_contain_other[200005];
void build_reindex() {
    int idx = 1;
    for(auto &x : val_to_idx) {
        x.second = idx++;
    }
}
struct Node {
    int left, right;
    int mid;
    int cnt;
    Node *lc, *rc;
}node[400005], *last_node = node, *root_node;
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    root->cnt = 0;

    if(left == right) return;

    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
}
int qry(Node *root, int left, int right) {
    if(root->left == left && root->right == right)
        return root->cnt;
    if(right <= root->mid)
        return qry(root->lc, left, right);
    if(left > root->mid)
        return qry(root->rc, left, right);
    return qry(root->lc, left, root->mid) + qry(root->rc, root->mid + 1, right);
}
void upd(Node *root, int pos) {
    root->cnt++;
    if(root->left == root->right) return;
    if(pos <= root->mid)
        upd(root->lc, pos);
    else
        upd(root->rc, pos);
}
void print(int *arr, int n)
{
    for(int i = 0;i < n;i++)
        printf("%d ", arr[i]);
    printf("\n");
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        scanf("%d %d", &range[i].left, &range[i].right);
        range[i].idx = i;
        val_to_idx[range[i].right];
    }
    
    build_reindex();
    build(root_node = last_node++, 1, (int)val_to_idx.size());
    sort(range, range + n, cmp_sb);
    for(int i = 0;i < n;i++) {
        ans_contained_by[range[i].idx] = qry(root_node, val_to_idx[range[i].right], (int)val_to_idx.size());
        upd(root_node, val_to_idx[range[i].right]);
    }

    last_node = node;
    build(root_node = last_node++, 1, (int)val_to_idx.size());
    sort(range, range + n, cmp_bs);
    for(int i = 0;i < n;i++) {
        ans_contain_other[range[i].idx] = qry(root_node, 1, val_to_idx[range[i].right]);
        upd(root_node, val_to_idx[range[i].right]);
    }

    print(ans_contain_other, n);
    print(ans_contained_by, n);
    return 0;
}