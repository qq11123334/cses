#include <bits/stdc++.h>
#define ll long long int
using namespace std;
map<ll,int> sum_cnt;
int main()
{
    int n,x;
    scanf("%d%d",&n,&x);
    ll sum = 0;
    if(x) sum_cnt[0] = 1;
    ll ans = 0;
    for(int i = 0;i < n;i++)
    {
        int num;
        scanf("%d",&num);
        sum = sum + num;
        sum_cnt[sum]++;
        ans = ans + sum_cnt[sum - x];
    }
    printf("%lld",ans);
    return 0;
}