#include <bits/stdc++.h>
using namespace std;
int next_alive[200005];
int last_alive[200005];
int main()
{
    int n;
    scanf("%d", &n);

    for(int i = 0;i < n;i++)
    {
        next_alive[i] = (i + 1);
        last_alive[i + 1] = i;
    }
    next_alive[n] = 1;
    last_alive[1] = n;

    int times = 2;
    int cur_alive = 0;
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < times;j++)
            cur_alive = next_alive[cur_alive];
        
        printf("%d ", cur_alive);

        int last = last_alive[cur_alive];
        int next = next_alive[cur_alive];

        last_alive[next] = last;
        next_alive[last] = next;
    }
    return 0;
}