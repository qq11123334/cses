#include <bits/stdc++.h>
#define ed first
#define st second
using namespace std;
multiset<int> memb;
vector<pair<int, int>> movie; // (end, start)
int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    for(int i = 0;i < k;i++) {
        memb.insert(0);
    }

    for(int i = 0;i < n;i++) {
        int start, end;
        scanf("%d%d", &start, &end);
        movie.push_back({end, start});
    }

    sort(movie.begin(), movie.end());

    int ans = 0;
    for(int i = 0;i < n;i++) {
        auto cur_mov = movie[i];
        auto it = memb.upper_bound(cur_mov.st);
        if(it != memb.begin()) {
            ans++;
            memb.erase(--it);
            memb.insert(cur_mov.ed);
        }
    }
    printf("%d\n", ans);
    return 0;
}