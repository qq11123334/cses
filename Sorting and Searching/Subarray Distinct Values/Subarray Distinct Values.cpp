#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int arr[200005];
set<int> s;
map<int, int> m;
int main()
{
    int n, k;
    scanf("%d%d", &n, &k);
    for(int i = 0;i < n;i++)
    {
        scanf("%d", &arr[i]);
    }

    ll ans = 0;
    int left = 0;
    for(int right = 0;right < n;right++)
    {
        s.insert(arr[right]);
        m[arr[right]]++;

        while((int)s.size() > k)
        {
            m[arr[left]]--;
            if(m[arr[left]] == 0)
                s.erase(arr[left]);
            left++;
        }
        ans += (right - left + 1);
    }

    printf("%lld\n", ans);
    return 0;
}