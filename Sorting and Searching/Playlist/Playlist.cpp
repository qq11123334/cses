#include <bits/stdc++.h>
using namespace std;
int song[200005];
set<int> song_set;
int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &song[i]);
    }

    int i = 0, j = 0, mx = 1;
    while (j < n)
    {
        if (song_set.count(song[j]))
        {
            mx = max(mx, j - i);
            while (song[i] != song[j]) song_set.erase(song[i++]);
            i++;
        }
        else song_set.insert(song[j]);

        j++;
    }
    mx = max(mx, j - i);

    printf("%d", mx);
    return 0;
}