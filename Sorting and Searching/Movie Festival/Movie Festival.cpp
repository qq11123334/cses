#include <bits/stdc++.h>
using namespace std;
struct Time
{
    int start;
    int end;
}movie[200005];
bool t_compare(Time a,Time b)
{
    return a.end < b.end;
}
int main()
{
    memset(movie,0,sizeof(movie));
    int n;
    scanf("%d",&n);
    for (int i = 0;i < n;i++)
    {
        scanf("%d %d",&movie[i].start,&movie[i].end);
    }
    sort(movie,movie+n,t_compare);
    int cnt = 0,cur_time = 0;
    for(int i = 0;i < n;i++)
    {
        while(i < n && cur_time > movie[i].start) i++;
        if(i < n)
        {
            cnt++;
            cur_time = movie[i].end;
        }
    }
    printf("%d",cnt);
    return 0;
}