#include <bits/stdc++.h>
using namespace std;
struct number
{
    int val;
    int pos;
} arr[200005];
bool num_compare(number a, number b)
{
    if(a.val == b.val) return a.pos < b.pos;
    return a.val < b.val;
}
int main()
{
    int n, x;
    scanf("%d%d", &n, &x);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i].val);
        arr[i].pos = i + 1;
    }
    sort(arr, arr + n, num_compare);
    int flag = 0;
    for(int i = 0;i < n;i++)
    {
        int dif = x - arr[i].val;
        int l = 0, r = n;
        while(l < r - 1)
        {
            int m = (r - l) / 2 + l;
            if (arr[m].val >= dif) r = m;
            else l = m;
        }
        if (r != i && r != n && arr[r].val == dif)
        {
            printf("%d %d", arr[i].pos, arr[r].pos);
            flag = 1;
            break;
        }
    }
    if (flag == 0)
    {
        printf("IMPOSSIBLE");
    }
    return 0;
}