#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int book[200005];
int main()
{
    int n;
    scanf("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &book[i]);
    }

    sort(book, book + n);

    ll sum1 = 0,sum2 = 0;

    int idx_1 = 0,idx_2 = n - 1;

    for(int i = 0;i < n;i++)
    {
        if(sum1 < sum2) sum1 = sum1 + book[idx_1++];
        else sum2 = sum2 + book[idx_2--];
    }

    if(idx_2 == n - 2 && sum2 >= sum1) printf("%lld",2 * sum2);
    else printf("%lld",sum1 + sum2);
    return 0;
}