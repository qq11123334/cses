#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll book[200005];
int main()
{
    int n;
    scanf("%d", &n);
    ll sum = 0;
    ll mx = 0;
    for (int i = 0; i < n; i++)
    {
        scanf("%lld", &book[i]);
        sum = sum + book[i];
        mx = max(mx, book[i]);
    }

    if(sum - mx < mx) printf("%lld", 2 * mx);
    else printf("%lld",sum);
    return 0;
}