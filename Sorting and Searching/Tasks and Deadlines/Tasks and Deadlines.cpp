#include <bits/stdc++.h>
#define ll long long int
using namespace std;
struct Task
{
    int dur;
    int deadline;
}task[200005];
bool t_compare(Task a,Task b)
{
    return a.dur < b.dur;
}
int main()
{
    int n;
    scanf("%d",&n);
    for (int i = 0;i < n;i++)
    {
        scanf("%d %d",&task[i].dur,&task[i].deadline);
    }
    sort(task,task+n,t_compare);
    ll cur_time = 0,reward = 0;
    for (int i = 0;i < n;i++)
    {
        cur_time = cur_time + task[i].dur;
        reward = reward + (task[i].deadline - cur_time);
    }
    printf("%lld",reward);
    return 0;
}