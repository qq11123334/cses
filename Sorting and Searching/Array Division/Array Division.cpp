#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll arr[200005];
ll n,k;
bool isable(ll d)
{
    //cout << "d:" << d << endl;
    int cnt = k;
    int idx = 0;
    while(cnt > 0)
    {
        ll sum = 0;
        while(idx < n && sum + arr[idx] <= d)
        {
            sum = sum + arr[idx++];
        }
        //cout << sum << endl;
        cnt--;
        if(sum <= d && idx == n) return 1;
    }
    return 0;
}
int main()
{
    cin >> n >> k;
    ll sum = 0;
    for(int i = 0;i < n;i++)
    {
        cin >> arr[i];
        sum = sum + arr[i];
    }
    ll l = 0,r = sum + 1;
    while(l < r - 1)
    {
        ll m = (r - l)/2 + l;
        if(isable(m)) r = m;
        else l = m;
    }
    cout << r;
    return 0;
}