#include <bits/stdc++.h>
using namespace std;
multiset<int> street;
multiset<int> length;
int main()
{
    street.clear();
    length.clear();
    int x,n;
    scanf("%d %d",&x,&n);
    street.insert(0);
    street.insert(x);
    length.insert(x);
    while(n--)
    {
        int pos;
        scanf("%d",&pos);
        multiset<int>::iterator it;
        it = street.upper_bound(pos);
        int r = *it--;
        int l = *it;
        int old_len = r - l;
        multiset<int>::iterator tmp = length.lower_bound(old_len);
        int new_len1 = r - pos;
        int new_len2 = pos - l;
        length.erase(tmp);
        length.insert(new_len1);
        length.insert(new_len2);
        street.insert(pos);
        multiset<int>::iterator mx = length.end();
        printf("%d ",*(--mx));
    }
    return 0;
}