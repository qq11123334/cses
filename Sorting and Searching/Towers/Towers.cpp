#include <bits/stdc++.h>
using namespace std;
multiset <int> tower;
int main()
{
    //freopen("input.txt","r",stdin);
    tower.clear();
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n;i++)
    {
        int size;
        scanf("%d",&size);
        multiset<int>::iterator it;
        it = tower.upper_bound(size);
        if(it == tower.end())
        {
            tower.insert(size);
        }
        else
        {
            tower.insert(size);
            tower.erase(it);
        }
    }
    printf("%d",tower.size());
    return 0;
}