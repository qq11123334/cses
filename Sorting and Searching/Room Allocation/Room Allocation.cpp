#include <bits/stdc++.h>
#define time first
#define num second
#define pii pair<int,int>
using namespace std;
priority_queue <pii,vector<pii>,greater<pii> > all_room; //存第幾天離開 跟 房間的編號
vector <int> ans;
struct C 
{
    int num;
    int arrival;
    int departure;
    int ans;
}cus[200005];

bool compare1(C a, C b)
{
    return a.arrival < b.arrival;
}

bool compare2(C a,C b)
{
    return a.num < b.num;
}
int main()
{
    int n;
    cin >> n;
    int size = 0; //房間數量
    all_room.push({0,++size});
    for(int i = 0;i < n;i++)
    {
        cin >> cus[i].arrival >> cus[i].departure;
        cus[i].num = i;
    }

    sort(cus,cus + n,compare1);
    for(int i = 0;i < n;i++)
    {
        int arr = cus[i].arrival;
        int dep = cus[i].departure; //dep = dep_new
        pii top = all_room.top();   //top.time = dep_last, top.num = num_last
        if(top.time >= arr)
        {
            all_room.push({dep,++size});
            cus[i].ans = size;
        }
        else
        {
            all_room.pop();
            all_room.push({dep,top.num});
            cus[i].ans = top.num;
        }
    }

    sort(cus,cus + n,compare2);

    printf("%d\n", size);

    for(int i = 0;i < n;i++)
    {
        printf("%d ",cus[i].ans);
    }
    return 0;
}