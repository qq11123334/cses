#include <bits/stdc++.h>
using namespace std;
map<int,vector<pair<int,int>>> sum;
int a[1005];
int main()
{
    int n,x;
    scanf("%d%d",&n,&x);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&a[i]);
    }
    for(int i = 0;i < n - 1;i++)
    {
        for(int j = i + 1;j < n;j++)
        {
            sum[a[i] + a[j]].push_back({i,j});
        }
    }
    for(int i = 0;i < n - 1;i++)
    {
        for(int j = i + 1;j < n;j++)
        {
            int val = x - a[i] - a[j];
            if(val < 0) continue;
            if(!sum[val].empty())
            {
                for(auto k : sum[val])
                {
                    int v = k.first;
                    int u = k.second;
                    if(i == v || i == u || j == v || j == u) continue;
                    printf("%d %d %d %d",i + 1,j + 1,v + 1,u + 1);
                    return 0;
                }
            }
        }
    }
    printf("IMPOSSIBLE");
    return 0;
}