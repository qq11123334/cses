#include <bits/stdc++.h>
using namespace std;
struct Time
{
    int time;
    bool status;
}cus[400005];
bool t_compare(Time a,Time b)
{
    return a.time < b.time;
}
int main()
{
    int n,idx = 0;
    scanf("%d",&n);
    for(int i = 0;i < n;i++)
    {
        int arr,lev;
        scanf("%d %d",&arr,&lev);
        cus[idx].time = arr;
        cus[idx++].status = 1;
        cus[idx].time = lev;
        cus[idx++].status = 0;
    }
    sort(cus,cus + idx,t_compare);
    int cnt = 0,mx = 0;
    for(int i = 0;i < idx;i++)
    {
        if(cus[i].status)
        {
            cnt++;
            mx = max(mx,cnt);
        }
        else
        {
            cnt--;
        }
    }
    printf("%d",mx);
    return 0;
}