#include <bits/stdc++.h>
#define ll long long int
#define pii pair<ll,ll>
using namespace std;
priority_queue<pii, vector<pii>, greater<pii>> order;
ll mac[200005];
int main()
{
    int n, t;
    scanf("%d%d",&n, &t);
    for(int i = 0;i < n;i++)
    {
        scanf("%lld", &mac[i]);
        order.push({mac[i], i});
    }
    ll cur_time = 0;
    for(int i = 0;i < t;i++)
    {
        pii top = order.top();
        order.pop();
        cur_time = top.first;
        order.push({top.first + mac[top.second], top.second});
    }
    printf("%lld", cur_time);

    return 0;
}