#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define ll unsigned long long int
using namespace std;
int n;
ll mac[200005];
ll t;
bool is_able(ll time) //在time的時間內，做不做得完
{
    ll sum = 0;
    for (int i = 0;i < n;i++)
    {
        sum = sum + time/mac[i]; //若有time個時間，每個機器最多可做 time / mac[i] 個產品
    }
    //printf("%lld\n",time/mac[i]);
    return sum >= t;
}
int main()
{
    //freopen("input.txt","r",stdin);
    scanf("%d%lld",&n,&t);
    ll mi = INF;
    for(int i = 0;i < n;i++)
    {
        scanf("%lld",&mac[i]);
        mi = min(mi,mac[i]);
    }
    ll l = 0,r = t*mi;
    //printf("%lld %lld\n",l,r);
    while(l < r - 1)
    {
        ll m = (r + l)/2;
        if(is_able(m)) r = m;
        else l = m;
    }
    printf("%lu",r);
    return 0;
}