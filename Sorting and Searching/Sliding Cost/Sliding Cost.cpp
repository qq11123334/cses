#include <bits/stdc++.h>
#define ll long long int
using namespace std;
multiset<ll> small, big;
ll small_sum, big_sum;
int n, k;
ll arr[200005];
void refresh() {
    while(big.size() > small.size()) {
        auto big_bg = big.begin();
        small_sum += *big_bg;
        big_sum -= *big_bg;
        small.insert(*big_bg);
        big.erase(big_bg);
    }
    while(big.size() < small.size() - 1) {
        auto small_ed = prev(small.end());
        small_sum -= *small_ed;
        big_sum += *small_ed;
        big.insert(*small_ed);
        small.erase(small_ed);
    }
}
void add(ll a) {
    if(small.empty() || a <= *prev(small.end())) {
        small.insert(a);
        small_sum += a;
    } else {
        big.insert(a);
        big_sum += a;
    }
    refresh();
}
void remove(ll a) {
    if(small.find(a) != small.end()) {
        small_sum -= a;
        small.erase(small.find(a));
    } else {
        big_sum -= a;
        big.erase(big.find(a));
    }
    refresh();
}
ll medium() {
    if(k % 2 == 0) {
        return (*prev(small.end()) + *big.begin()) / 2;
    } else {
        return *prev(small.end());
    }
}
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &k);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &arr[i]);
    }
    for(int i = 0;i < k;i++) {
        add(arr[i]);
    }

    for(int i = k;i <= n;i++) {
        ll big_contri = big_sum - big.size() * medium();
        ll small_contri = small.size() * medium() - small_sum;
        printf("%lld ", big_contri + small_contri);
        if(i != n) add(arr[i]);
        if(i != n) remove(arr[i - k]);
    }
    return 0;
}