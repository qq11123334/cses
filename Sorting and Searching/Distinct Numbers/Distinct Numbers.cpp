#include <bits/stdc++.h>
using namespace std;
map <int,int> flag;
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    flag.clear();
    int n,cnt = 0;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int num;
        cin >> num;
        if(flag[num])
        {
            continue;
        }
        else
        {
            flag[num] = 1;
            cnt++;
        }
    }
    printf("%d",cnt);
    return 0;
}