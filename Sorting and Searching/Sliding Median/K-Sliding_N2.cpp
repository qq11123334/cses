#include <bits/stdc++.h>
using namespace std;
const int N = 500005;
int arr[N];
int n, m, k;
multiset<int> ms;
void add(int x) {
    ms.insert(x);
}
void remove(int x) {
    ms.erase(ms.find(x));
}
int K_th() {
    auto it = ms.begin();
    for(int i = 0; i < k - 1; i++) {
	it++;
    }
    return *it;
}
int main() {
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < n;i++) {
        scanf("%d",&arr[i]);
    }
    
    for(int i = 0; i < m; i++) {
	ms.insert(arr[i]);
    }
    printf("%d ", K_th());

    for(int i = m;i < n;i++) {
        add(arr[i]);
        remove(arr[i - m]);
        printf("%d ", K_th());
    }
}
