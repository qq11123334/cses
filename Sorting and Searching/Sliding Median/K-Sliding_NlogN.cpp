#include <bits/stdc++.h>
using namespace std;
const int N = 500005;
int arr[N];
int n, m, k;
multiset<int> small, big;
void refresh() {
    while((int)small.size() > k) {
        auto it = prev(small.end());
        big.insert(*it);
        small.erase(it);
    }
    while((int)small.size() < k) {
        auto it = big.begin();
        small.insert(*it);
        big.erase(it);
    }
}
void init() {
    vector<int> v;
    v.clear(); small.clear(); big.clear();
    for(int i = 0; i < m; i++) {
        v.push_back(arr[i]);
    }

    sort(v.begin(), v.end());
    
    for(int i = 0; i < m; i++) {
        if(i < k) small.insert(v[i]);
        else big.insert(v[i]);
    }
    refresh();
}
void add(int x) {
    int num = *big.begin();
    if(num < x) big.insert(x);
    else small.insert(x);
    refresh();
}
void remove(int x) {
    auto it = small.find(x);
    if(it != small.end()) small.erase(it);
    else big.erase(big.find(x));
    refresh();
}
int K_th() {
    return *prev(small.end());
}
int main() {
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < n;i++) {
        scanf("%d",&arr[i]);
    }

    init();
    printf("%d ", K_th());

    for(int i = m;i < n;i++) {
        add(arr[i]);
        remove(arr[i - m]);
        printf("%d ", K_th());
    }
}
