#include <bits/stdc++.h>
using namespace std;
int arr[200005];
int n, k;
multiset<int> small, big;
void refresh()
{
    while((int)(small.size() - big.size()) >= 2)
    {
        auto it = prev(small.end());
        big.insert(*it);
        small.erase(it);
    }
    while((int)(big.size() - small.size()) >= 1)
    {
        auto it = big.begin();
        small.insert(*it);
        big.erase(it);
    }
}
void init()
{
    vector<int> v;
    v.clear(); small.clear(); big.clear();
    for(int i = 0;i < k;i++)
    {
        v.push_back(arr[i]);
    }

    sort(v.begin(), v.end());
    
    for(int i = 0;i < k;i++)
    {
        if(i <= k / 2) small.insert(v[i]);
        else big.insert(v[i]);
    }
    refresh();
}
void add(int x)
{
    int num = *big.begin();
    if(num < x) big.insert(x);
    else small.insert(x);
    refresh();
}
void remove(int x)
{
    auto it = small.find(x);
    if(it != small.end()) small.erase(it);
    else big.erase(big.find(x));
    refresh();
}
int medium()
{
    return *prev(small.end());
}
int main()
{
    scanf("%d%d",&n,&k);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&arr[i]);
    }

    init();
    printf("%d ", medium());

    for(int i = k;i < n;i++)
    {
        add(arr[i]);
        remove(arr[i - k]);
        printf("%d ", medium());
    }
    return 0;
}