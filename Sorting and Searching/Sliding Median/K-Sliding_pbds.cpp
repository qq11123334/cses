#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
using namespace std;
using ll = long long int;
typedef pair<int, int> pii;
typedef tree<pii,null_type,less<pii>,rb_tree_tag, tree_order_statistics_node_update> indexed_set;
indexed_set s;
const int N = 500005;
int arr[N];
int main() {
    int n, m, k;
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0; i < n; i++) {
	scanf("%d", &arr[i]);
    }
    for(int i = 0; i < m - 1; i++) {
	s.insert({arr[i], i});
    }
    for(int i = m - 1; i < n; i++) {
	s.insert({arr[i], i});
	printf("%d ", (*s.find_by_order(k - 1)).first);
	s.erase({arr[i - m + 1], i - m + 1});
    }
    return 0;
}

