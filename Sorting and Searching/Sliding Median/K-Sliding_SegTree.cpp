#include <bits/stdc++.h>
using namespace std;
const int N = 500005;
int arr[N];
struct Node {
    int left, right, mid;
    Node *lc, *rc;
    int cnt;
    Node() {left = right = mid = cnt = 0, lc = rc = NULL;}
    void puint() {
        cnt = lc->cnt + rc->cnt;
    }
}*root_node[N]; // Store the history root
/* Use new_root = copy_node(old_root)
 * and Update new_root */
Node *copy_node(Node *r) {
    Node* cp = new Node();
    cp->left = r->left; cp->right = r->right;
    cp->mid = r->mid; 
    cp->cnt = r->cnt;
    cp->lc = r->lc; cp->rc = r->rc;
    return cp;
}
void build(Node *r, int left, int right) {
    r->left = left; r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->cnt = 0; 
        return;
    }
    build(r->lc = new Node(), left, r->mid);
    build(r->rc = new Node(), r->mid + 1, right);
    r->puint();
}
void update(Node *r, Node *new_r, int pos, int dif) {
    if(r->left == r->right) {
        new_r->cnt += dif;
        return;
    }
    if(pos <= r->mid) {
        new_r->lc = copy_node(r->lc);
        update(r->lc, new_r->lc, pos, dif);
    }
    else {
        new_r->rc = copy_node(r->rc);
        update(r->rc, new_r->rc, pos, dif);
    }
    new_r->puint();
}

int qry_kth(Node *L, Node *R, int k) {
    if(L->left == L->right)
	return L->left;

    int lc_cnt = R->lc->cnt - L->lc->cnt;
    if(k <= lc_cnt)
	return qry_kth(L->lc, R->lc, k);
    else
	return qry_kth(L->rc, R->rc, k - lc_cnt);
}

vector<int> id_to_cor;
map<int, int> compress;
void Coordinate_Compression() {
    for(auto &x : compress) {
        x.second = id_to_cor.size();
        id_to_cor.push_back(x.first);
    }
}


int main() {
    int n, m, k;
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 1; i <= n; i++) {
	scanf("%d", &arr[i]);
	compress[arr[i]];
    }
    
    Coordinate_Compression();

    for(int i = 1; i <= n; i++) {
	arr[i] = compress[arr[i]];
    }

    build(root_node[0] = new Node(), 0, id_to_cor.size() - 1);
    for(int i = 1; i <= n; i++) {
	root_node[i] = copy_node(root_node[i - 1]);
	update(root_node[i], root_node[i], arr[i], 1);
    }

    for(int i = 1; i <= n - m + 1; i++) {
	// (i ~ i + m - 1)
	int ans_id = qry_kth(root_node[i - 1], root_node[i + m - 1], k);
	printf("%d ", id_to_cor[ans_id]);
    }
}
