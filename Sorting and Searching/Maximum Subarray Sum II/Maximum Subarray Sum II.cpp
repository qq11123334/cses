#include <bits/stdc++.h>
#define ll long long int
using namespace std;

ll arr[200005], sp[20][200005];
ll pre[200005];
int n;
void build_sparse_table() {
    for(int i = 0;i <= n;i++) {
        sp[0][i] = pre[i];
    }

    for(int i = 1;(1 << i) <= n;i++) {
        for(int j = 0;j + (1 << i) <= n + 1;j++) {
            sp[i][j] = min(sp[i - 1][j], sp[i - 1][j + (1 << (i - 1))]);
        }
    }
}
int int_log2(ll x) {
    int cnt = 0;
    while(x > 1) {
        x /= 2;
        cnt++;
    }
    return cnt;
}
ll query(int left, int right) {
    int k = int_log2(right - left);
    return min(sp[k][left], sp[k][right - (1 << k) + 1]);
}
int main()
{
    // freopen("input.txt", "r", stdin);
    int a, b;
    scanf("%d%d%d", &n, &a, &b);
    for(int i = 1;i <= n;i++)
        scanf("%lld", &arr[i]);

    for(int i = 1;i <= n;i++)
        pre[i] = pre[i - 1] + arr[i];

    build_sparse_table();
    
    ll mx = -0x3f3f3f3f3f3f3f3f;

    for(int i = a;i <= n;i++) {
        // end with arr[i]
        mx = max(pre[i] - query(max(0, i - b), i - a), mx);
    }
    printf("%lld\n", mx);
    return 0;
}