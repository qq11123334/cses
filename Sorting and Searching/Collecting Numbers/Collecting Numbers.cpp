#include <bits/stdc++.h>
using namespace std;
int pos[200005];
int main()
{
    int n;
    scanf("%d", &n);

    for(int i = 1;i <= n;i++)
    {
        int num;
        scanf("%d", &num);
        pos[num] = i;
    }

    int ans = 0;

    for(int i = 2;i <= n;i++)
    {
        if(pos[i - 1] > pos[i])
            ans++;
    }
    printf("%d\n", ans + 1);
    return 0;
}