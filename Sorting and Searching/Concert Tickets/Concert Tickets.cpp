#include <bits/stdc++.h>
using namespace std;
multiset <int,greater<int> > ticket;
bool sold[200005];
int main()
{
    ticket.clear();
    int n,m;
    scanf("%d%d",&n,&m);
    for (int i = 0;i < n;i++)
    {
        int pri;
        scanf("%d",&pri);
        ticket.insert(pri);
    }
    while(m--)
    {
        int cus;
        scanf("%d",&cus);
        multiset<int,greater<int> >::iterator it;
        it = ticket.lower_bound(cus);
        if(it == ticket.end()) printf("-1\n");
        else
        {
            printf("%d\n",*it);
            ticket.erase(it);
        }
        
    }
    return 0;
}