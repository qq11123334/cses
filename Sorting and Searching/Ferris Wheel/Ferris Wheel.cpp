#include <bits/stdc++.h>
using namespace std;
int weight[200005];
int main()
{
    int n,x;
    scanf("%d%d",&n,&x);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&weight[i]);
    }
    sort(weight,weight+n);
    int left = 0,right = n-1;
    int cnt = 0;
    while(left < right)
    {
        if(weight[left] + weight[right] <= x)
        {
            cnt++;
            left++;
            right--;
        }
        else
        {
            right--;
        }
    }
    printf("%d",n - cnt);
    return 0;
}