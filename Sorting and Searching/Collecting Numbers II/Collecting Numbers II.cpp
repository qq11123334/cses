#include <bits/stdc++.h>
using namespace std;
int pos[200005];
int arr[200005];
int n, m;
bool in_range(int x)
{
    return (2 <= x && x <= n);
}
void print(int *a)
{
    for(int i = 1;i <= n;i++)
        printf("%d ", a[i]);
    printf("\n");
}
int main()
{
    scanf("%d%d", &n, &m);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &arr[i]);
        pos[arr[i]] = i;
    }

    int cur_ans = 1;
    for(int i = 2;i <= n;i++)
    {
        if(pos[i - 1] > pos[i])
            cur_ans++;
    }
    while(m--)
    {
        int pos_a, pos_b;
        scanf("%d%d", &pos_a, &pos_b);

        int val_a = arr[pos_a];
        int val_b = arr[pos_b];
        if(val_a > val_b)
        {
            swap(pos_a, pos_b);
            swap(val_a, val_b);
        }

        int left_a = val_a;
        int right_a = val_a + 1;
        int left_b = val_b;
        int right_b = val_b + 1;
        
        if(val_b - val_a == 0)
        {
            left_a = 0x3f3f3f3f;
            left_b = 0x3f3f3f3f;
        }
        else if(val_b - val_a == 1)
        {
            right_a = val_a;
        }

        int original = 0;
        for(int i = left_a;i <= right_a;i++)
        {
            if(!in_range(i))
                continue;
            if(pos[i - 1] > pos[i])
                original++;
        }
        for(int i = left_b;i <= right_b;i++)
        {
            if(!in_range(i))
                continue;
            if(pos[i - 1] > pos[i])
                original++;
        }

        // print(pos);
        // print(arr);
        swap(pos[val_a], pos[val_b]);
        swap(arr[pos_a], arr[pos_b]);
        // print(pos);
        // print(arr);
        int after = 0;
        for(int i = left_a;i <= right_a;i++)
        {
            if(!in_range(i))
                continue;
            if(pos[i - 1] > pos[i])
                after++;
        }
        for(int i = left_b;i <= right_b;i++)
        {
            if(!in_range(i))
                continue;
            if(pos[i - 1] > pos[i])
                after++;
        }

        cur_ans = cur_ans + after - original;
        // printf("bound=%d %d %d %d\n", left_a, right_a, left_b, right_b);
        // printf("res=%d %d\n", after, original);
        printf("%d\n", cur_ans);
    }
    return 0;
}