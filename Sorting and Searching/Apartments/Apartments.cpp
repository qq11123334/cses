#include <bits/stdc++.h>
#define MAX_N 200000
using namespace std;
int des[MAX_N + 5];
int apt[MAX_N + 5];
int main()
{
    int n, m, dif;
    scanf("%d%d%d", &n, &m, &dif);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &des[i]);
    }

    for (int i = 0; i < m; i++)
    {
        scanf("%d", &apt[i]);
    }
    sort(des, des + n);
    sort(apt, apt + m);
    int idx_a = 0,idx_d = 0,cnt = 0;
    while (idx_a < m && idx_d < n)
    {
        if(abs(apt[idx_a] - des[idx_d]) <= dif)
        {
            cnt++;
            idx_a++;
            idx_d++;
        }
        else
        {
            if(apt[idx_a] > des[idx_d])
            {
                idx_d++;
            }
            else
            {
                idx_a++;
            }
        }
    }
    printf("%d",cnt);
    return 0;
}