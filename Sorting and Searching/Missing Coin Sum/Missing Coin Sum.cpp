#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll arr[200005];
int main()
{
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++)
        scanf("%lld", &arr[i]);

    sort(arr, arr + n);
    // ... 17
    ll cur_sum = 0;
    for(int i = 0;i < n;i++)
    {
        if(cur_sum + 1 < arr[i])
        {
            printf("%lld\n", cur_sum + 1);
            return 0;
        }
        cur_sum += arr[i];
    }
    printf("%lld\n", cur_sum + 1);
    return 0;
}