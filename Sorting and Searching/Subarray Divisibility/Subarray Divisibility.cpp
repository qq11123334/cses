#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int pre_mod_p[200005];
ll cnt_mod_p[200005];
int main()
{
    int n;
    scanf("%d", &n);
    int p = n;
    ll ans = 0;
    cnt_mod_p[0] = 1;
    for(int i = 1;i <= n;i++)
    {
        int num;
        scanf("%d", &num);
        if(num < 0) num = num % p + p;
        pre_mod_p[i] = (pre_mod_p[i - 1] + num) % p;
        ans += cnt_mod_p[pre_mod_p[i]]++;
    }

    printf("%lld\n", ans);
    return 0;
}