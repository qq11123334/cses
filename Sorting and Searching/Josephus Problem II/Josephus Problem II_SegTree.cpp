#include <bits/stdc++.h>
using namespace std;
struct Node {
    int left, right;
    int mid;
    int remain;
    Node *lc, *rc;
}node[400500], *last_node = node, *root_node;
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right) {
        root->remain = 1;
        return;
    }

    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);

    root->remain = root->lc->remain + root->rc->remain;
}
int qry_remain(Node *root, int left, int right) {
    if(root->left == left && root->right == right) {
        return root->remain;
    }
    if(right <= root->mid) {
        return qry_remain(root->lc, left, right);
    } else if(left >= root->mid + 1) {
        return qry_remain(root->rc, left, right);
    } else {
        return qry_remain(root->lc, left, root->mid) + qry_remain(root->rc, root->mid + 1, right);
    }
}
int qry_kth(Node *root, int kth) {
    if(root->left == root->right) {
        return root->left;
    }
    if(root->lc->remain >= kth) {
        return qry_kth(root->lc, kth);
    } else {
        return qry_kth(root->rc, kth - root->lc->remain);
    }
}
void upd(Node *root, int pos) {
    root->remain--;
    if(root->left == root->right) {
        return;
    }
    if(pos <= root->mid) {
        upd(root->lc, pos);
    } else {
        upd(root->rc, pos);
    }
}
int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    k++;
    build(root_node = last_node++, 1, n);

    int cur_alive = 0;
    int cur_alive_cnt = n;
    for(int i = 0;i < n;i++)
    {
        int suffix_alive = cur_alive == 0 ? n : qry_remain(root_node, cur_alive, n);
        int prefix_alive = cur_alive == 0 ? 0 : qry_remain(root_node, 1, cur_alive);
        // printf("%d %d", suffix_alive, prefix_alive);
        int res = k % cur_alive_cnt;
        if(res == 0) res += cur_alive_cnt;
        if(suffix_alive >= res) {
            cur_alive = qry_kth(root_node, prefix_alive + res);
        } else {
            cur_alive = qry_kth(root_node, res - suffix_alive);
        }
        // printf("%d ", k % cur_alive_cnt);
        printf("%d ", cur_alive);
        upd(root_node, cur_alive);
        cur_alive_cnt--;
    }
    return 0;
}
// 7 5
// 1 2 3 4 5 6 7
// X X     X X X