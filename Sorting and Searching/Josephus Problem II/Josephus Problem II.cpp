#include <bits/stdc++.h>
using namespace std;
int next_alive[200005];
int last_alive[200005];
int main()
{
    int n;
    int times;
    scanf("%d%d", &n, &times);
    for(int i = 0;i < n;i++)
    {
        next_alive[i] = (i + 1);
        last_alive[i + 1] = i;
    }
    next_alive[n] = 1;
    last_alive[1] = n;

    int cur_alive = 0;
    int cur_alive_cnt = n;
    for(int i = 0;i < n;i++)
    {
        cur_alive = next_alive[cur_alive];
        for(int j = 0;j < times % cur_alive_cnt;j++)
            cur_alive = next_alive[cur_alive];
        
        printf("%d ", cur_alive);
        cur_alive_cnt--;
        int last = last_alive[cur_alive];
        int next = next_alive[cur_alive];

        last_alive[next] = last;
        next_alive[last] = next;
    }
    return 0;
}