#include <bits/stdc++.h>
#define INF (0x3f3f3f3f3f3f3f3f)
#define X real()
#define Y imag()
#define ll long long int
#define P complex<ll>
using namespace std;
const int N = 1005;
ll cross(P a, P b) {
	return (conj(a) * b).Y;
}
struct Point {
	ll x, y;
	Point(ll _x, ll _y) : x(_x), y(_y) {} 
	Point() {}
};
#define LEFT 1
#define RIGHT 2
#define ON_LINE 3
int PointTest(Point a, Point b, Point p) {
	ll res = cross({b.x - a.x, b.y - a.y}, {p.x - a.x, p.y - a.y});
	if(res > 0) return LEFT;
	if(res < 0) return RIGHT;
	if(res == 0) return ON_LINE;
}
bool Intersect(Point a, Point b, Point p) {
	bool res1 = (a.x <= p.x && p.x <= b.x) || (b.x <= p.x && p.x <= a.x);
	bool res2 = (a.y <= p.y && p.y <= b.y) || (b.y <= p.y && p.y <= a.y);
	return res1 && res2;
}
bool isSegIntersect(Point a, Point b, Point p, Point q) {
	int Tp = PointTest(a, b, p);
	int Tq = PointTest(a, b, q);
	int Ta = PointTest(p, q, a);
	int Tb = PointTest(p, q, b);
	bool Ip = Intersect(a, b, p);
	bool Iq = Intersect(a, b, q);
	bool Ia = Intersect(p, q, a);
	bool Ib = Intersect(p, q, b);
	if(Tp == ON_LINE && Tq == ON_LINE && (Ip || Iq)) return true;
	if(Ta == ON_LINE && Tb == ON_LINE && (Ia || Ib)) return true;
	if(Tp == ON_LINE && Tq == ON_LINE) return false;
	if(Tp == ON_LINE && Ip) return true;
	if(Tq == ON_LINE && Iq) return true;
	if(Ta == ON_LINE && Ia) return true;
	if(Tb == ON_LINE && Ib) return true;
	if(Tp == ON_LINE || Tp == ON_LINE || Ta == ON_LINE || Tb == ON_LINE) return false;
	return (Tp != Tq) && (Ta != Tb);
}
Point Polygon[N];
int main() {
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 0;i < n;i++) {
		scanf("%lld %lld", &Polygon[i].x, &Polygon[i].y);
	}
	ll mx_x = (ll)1e9 + 1;
	while(m--) {
		Point a;
		scanf("%lld%lld", &a.x, &a.y);
		ll dif_x = (mx_x - a.x);
		ll dif_y = dif_x + 1;
		Point b;
		b.x = a.x + dif_x;
		b.y = a.y + dif_y;
		int cnt = 0;
		bool onLine = 0;
		for(int k = 0;k < n;k++) {
			int i = k;
			int j = (i + 1) % n;
			Point p = Polygon[i];
			Point q = Polygon[j];
			cnt += isSegIntersect(a, b, p, q);
			onLine |= (PointTest(p, q, a) == ON_LINE && Intersect(p, q, a));
		}
		if(onLine) printf("BOUNDARY\n");
		else if(cnt % 2 == 1) printf("INSIDE\n");
		else if(cnt % 2 == 0) printf("OUTSIDE\n");
	}
	

	return 0;
}