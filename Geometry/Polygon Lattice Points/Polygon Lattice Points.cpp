#include <bits/stdc++.h>
#define ll long long int
#define X real()
#define Y imag()
#define P complex<ll>
using namespace std;
struct Point {
	ll x, y;
	Point() {}
	Point(ll _x, ll _y) : x(_x), y(_y) {}
};
ll cross(P a, P b) {
	return (conj(a) * b).Y;
}
ll Poly_Area(vector<Point> &point) {
	ll ans = 0, n = point.size();
	for(int i = 0;i < n;i++) {
		int j = (i + 1) % n;
		ans += cross({point[i].x, point[i].y}, {point[j].x, point[j].y});
	}
	return ans > 0 ? ans : -ans;
}
ll gcd(ll a, ll b) {
	if(b == 0) return a;
	else return gcd(b, a % b);
}
int main() {
	vector<Point> point;
	int n;
	scanf("%d", &n);
	for(int i = 0;i < n;i++) {
		ll x, y;
		scanf("%lld%lld", &x, &y);
		point.push_back(Point(x, y));
	}

	ll area = Poly_Area(point);
	ll onLine = 0;
	for(int i = 0;i < n;i++) {
		int j = (i + 1) % n;
		onLine += gcd(abs(point[i].x - point[j].x), abs(point[i].y - point[j].y));
	}

	ll inSide = (area - onLine + 2) / 2;
	printf("%lld %lld\n", inSide, onLine);
	return 0;
}