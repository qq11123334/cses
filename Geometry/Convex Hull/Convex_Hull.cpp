#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long int
using namespace std;
typedef complex<ll> P;
struct Point {
    ll x, y;
    int i;
};
ll cross(P a, P b) { // two vector
    return (conj(a) * b).Y;
}
ll cross(Point a, Point b, Point c) { // the cross can use Funciton Overloading
    return cross({b.x - a.x, b.y - a.y}, {c.x - a.x, c.y - a.y});
}
vector<Point> Convex_Hull(vector<Point> &point) {
    sort(point.begin(), point.end(), [](Point a, Point b) {
        if(a.x != b.x) return a.x < b.x; 
        return a.y < b.y;
    });
    int n = (int)point.size();
    vector <Point> upper, lower;
    for(int i = 0;i < n;i++) {
        int sz;
        while((sz = upper.size()) > 1 && cross(upper[sz - 2], point[i], upper[sz - 1]) < 0)
            upper.pop_back();
        upper.push_back(point[i]);
    }

    for(int i = n - 1;i >= 0;i--) {
        int sz;
        while((sz = lower.size()) > 1 && cross(lower[sz - 2], point[i], lower[sz - 1]) < 0)
            lower.pop_back();
        lower.push_back(point[i]);
    }
    // the convex hull order : upper[0 ~ upper.size() - 2] + lower[0 ~ lower.size() - 2]; (the upper[0] is equal with lower.back)
    vector<Point> Convex;
    for(int i = 0;i < (int)upper.size() - 1;i++) {
        Convex.push_back(upper[i]);
    }
    for(int i = 0;i < (int)lower.size() - 1;i++) {
        Convex.push_back(lower[i]);
    }
    return Convex;
}
int main() {
    int n;
    vector<Point> point;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        ll x, y;
        scanf("%lld%lld", &x, &y);
        point.push_back({x, y, i + 1}); // (x, y) and number
    }

    auto Convex = Convex_Hull(point);
    printf("%d\n", (int)Convex.size());
    for(Point P : Convex) {
        printf("%lld %lld\n", P.x, P.y);
    }
}