#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long int
#define P complex<ll>
using namespace std;
P p1, p2;
int main()
{
    int t;
    scanf("%d", &t);
    while(t--)
    {
        ll x1, y1, x2, y2, x3, y3;
        scanf("%lld %lld %lld %lld %lld %lld", &x1, &y1, &x2, &y2, &x3, &y3);
        p1 = {x2 - x1, y2 - y1};
        p2 = {x3 - x1, y3 - y1};
        ll res = (conj(p1) * p2).Y;
        if(res == 0)
            printf("TOUCH\n");
        else if(res > 0)
            printf("LEFT\n");
        else
            printf("RIGHT\n");
    }
    return 0;
}