#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long
#define P complex<ll>
using namespace std;
struct Point {
    ll x, y;
    Point(ll _x, ll _y) : x(_x), y(_y) {}
};
ll cross(P a, P b) {
    return (conj(a) * b).Y;
}
#define LEFT 1
#define RIGHT 2
#define ON_LINE 3
inline int PointTest(Point a, Point b, Point p) {
    ll res = cross({b.x - a.x, b.y - a.y}, {p.x - a.x, p.y - a.y});
    if(res > 0) return LEFT;
    if(res < 0) return RIGHT;
    if(res == 0) return ON_LINE;
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    while(t--) {
        ll x1, x2, x3, y1, y2, y3;
        cin >> x1 >> y1;
        cin >> x2 >> y2;
        cin >> x3 >> y3;
        int res = PointTest(Point(x1, y1), Point(x2, y2), Point(x3, y3));
        if(res == LEFT) {
            cout << "LEFT" << endl;
        } else if(res == RIGHT) {
            cout << "RIGHT" << endl;
        } else if(res == ON_LINE) {
            cout << "TOUCH" << endl;
        }
    }
    return 0;
}#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long
#define P complex<ll>
using namespace std;
struct Point {
    ll x, y;
    Point(ll _x, ll _y) : x(_x), y(_y) {}
};
ll cross(P a, P b) {
    return (conj(a) * b).Y;
}
#define LEFT 1
#define RIGHT 2
#define ON_LINE 3
inline int PointTest(Point a, Point b, Point p) {
    ll res = cross({b.x - a.x, b.y - a.y}, {p.x - a.x, p.y - a.y});
    if(res > 0) return LEFT;
    if(res < 0) return RIGHT;
    if(res == 0) return ON_LINE;
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    while(t--) {
        ll x1, x2, x3, y1, y2, y3;
        cin >> x1 >> y1;
        cin >> x2 >> y2;
        cin >> x3 >> y3;
        int res = PointTest(Point(x1, y1), Point(x2, y2), Point(x3, y3));
        if(res == LEFT) {
            cout << "LEFT" << endl;
        } else if(res == RIGHT) {
            cout << "RIGHT" << endl;
        } else if(res == ON_LINE) {
            cout << "TOUCH" << endl;
        }
    }
    return 0;
}