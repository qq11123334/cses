#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long
#define P complex<ll>
using namespace std;
struct Point {
    ll x, y;
    Point(ll _x, ll _y) : x(_x), y(_y) {}
};
ll cross(P a, P b) {
    return (conj(a) * b).Y;
}
#define LEFT 1
#define RIGHT 2
#define ON_LINE 3
int PointTest(Point a, Point b, Point p) {
    ll res = cross({b.x - a.x, b.y - a.y}, {p.x - a.x, p.y - a.y});
    if(res > 0) return LEFT;
    if(res < 0) return RIGHT;
    if(res == 0) return ON_LINE;
}
bool Intersect(Point a, Point b, Point p) {
    bool res1 = (a.x <= p.x && p.x <= b.x) || (b.x <= p.x && p.x <= a.x);
    bool res2 = (a.y <= p.y && p.y <= b.y) || (b.y <= p.y && p.y <= a.y);
    return res1 && res2;
}
bool IsSegIntersect(Point a, Point b, Point p, Point q) {
    /* case1 */
    int Tp = PointTest(a, b, p);
    int Tq = PointTest(a, b, q);
    int Ta = PointTest(p, q, a);
    int Tb = PointTest(p, q, b);
    bool Ip = Intersect(a, b, p);
    bool Iq = Intersect(a, b, q);
    bool Ia = Intersect(p, q, a);
    bool Ib = Intersect(p, q, b);
    /* case1 */
    if(Tp == ON_LINE && Tq == ON_LINE && (Ip || Iq)) return true;
    if(Ta == ON_LINE && Tb == ON_LINE && (Ia || Ib)) return true;
    if(Tp == ON_LINE && Tq == ON_LINE) return false;
    /* case2 */
    if(Tp == ON_LINE && Ip) return true;
    if(Tq == ON_LINE && Iq) return true;
    if(Ta == ON_LINE && Ia) return true;
    if(Tb == ON_LINE && Ib) return true;
    if(Tp == ON_LINE || Tq == ON_LINE) return false;
    if(Ta == ON_LINE || Tb == ON_LINE) return false;
    /* case3 */
    return (Tp != Tq) && (Ta != Tb);
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    while(t--) {
        ll x1, x2, x3, x4, y1, y2, y3, y4;
        cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4;
        if(IsSegIntersect(Point(x1, y1), Point(x2, y2), Point(x3, y3), Point(x4, y4))) {
            printf("YES\n");
        } else {
            printf("NO\n");
        }

    }
    return 0;
}