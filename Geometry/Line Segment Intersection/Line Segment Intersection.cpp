#include <bits/stdc++.h>
#define X real()
#define Y imag()
#define ll long long
#define P complex<ll>
using namespace std;
P p1, p2, p3;
ll cross(P a, P b) {
    return (conj(a) * b).Y;
}
bool same_sign(ll a, ll b) {
    if(a >= 0 && b >= 0) return 1;
    if(a <= 0 && b <= 0) return 1;
    else return 0;
}
bool intersection(ll a_left, ll a_right, ll b_left, ll b_right) {
    if(a_left > a_right) swap(a_left, a_right);
    if(b_left > b_right) swap(b_left, b_right);
    if(a_left > b_left) {
        swap(a_left, b_left);
        swap(a_right, b_right);
    }
    return a_right >= b_left;
}
bool IsSegIntersect(ll x1, ll x2, ll x3, ll x4, ll y1, ll y2, ll y3, ll y4) {
    p1 = {x3 - x1, y3 - y1}; p2 = {x3 - x4, y3 - y4}; 
    p3 = {x3 - x2, y3 - y2};
    bool res1 = same_sign(cross(p1, p2), cross(p2, p3));
    p1 = {x4 - x1, y4 - y1}; p2 = {x4 - x3, y4 - y3};
    p3 = {x4 - x2, y4 - y2};
    bool res2 = same_sign(cross(p1, p2), cross(p2, p3));
    p1 = {x1 - x3, y1 - y3}; p2 = {x1 - x2, y1 - y2};
    p3 = {x1 - x4, y1 - y4};
    bool res3 = same_sign(cross(p1, p2), cross(p2, p3));
    p1 = {x2 - x3, y2 - y3}; p2 = {x2 - x1, y2 - y1};
    p3 = {x2 - x4, y2 - y4};
    bool res4 = same_sign(cross(p1, p2), cross(p2, p3));
    bool res5 = intersection(x1, x2, x3, x4), res6 = intersection(y1, y2, y3, y4);
    if(res1 && res2 && res3 && res4 && res5 && res6) return true;
    else return false;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--)
    {
        ll x1, x2, x3, x4, y1, y2, y3, y4;
        scanf("%lld %lld", &x1, &y1);
        scanf("%lld %lld", &x2, &y2);
        scanf("%lld %lld", &x3, &y3);
        scanf("%lld %lld", &x4, &y4);
        printf(IsSegIntersect(x1, x2, x3, x4, y1, y2, y3, y4) ? "YES\n" : "NO\n");
    }
    return 0;
}