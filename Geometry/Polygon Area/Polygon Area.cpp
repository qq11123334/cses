#include <bits/stdc++.h>
#define X first
#define Y second
#define ll long long int
using namespace std;
typedef complex<ll> P;
ll cross(P a, P b) {
    return (conj(a) * b).imag();
}
ll Polygon_Area(vector<pair<ll, ll>> &point) {
    ll ans = 0; int n = (int)point.size();
    for(int i = 0;i < n;i++) {
        P p1 = {point[i].X, point[i].Y};
        P p2 = {point[(i + 1) % n].X, point[(i + 1) % n].Y};
        ans += cross(p1, p2);
    }
    return ans > 0 ? ans : -ans;
}
int main() {
    int n;
    scanf("%d", &n);
    vector<pair<ll, ll>> point;
    for(int i = 0;i < n;i++) {
        ll x, y; scanf("%lld%lld", &x, &y);
        point.push_back(make_pair(x, y));
    }
    printf("%lld\n", Polygon_Area(point));
    return 0;
}