#include <bits/stdc++.h>
#define ll long long int
#define MAX_Log_N 20
using namespace std;
ll sp[20][200005], arr[200005];
ll int_log2(ll x) {
    ll cnt = 0;
    while(x > 1) {
        x /= 2;
        cnt++;
    }
    return cnt++;
}
int n;
void build_sparse_table() {
    for(int i = 0;i < n;i++) {
        sp[0][i] = arr[i];
    }

    for(int i = 1;(1 << i) <= n;i++) {
        for(int j = 0;j + (1 << i) <= n;j++) {
            sp[i][j] = min(sp[i - 1][j], sp[i - 1][j + (1 << (i - 1))]);
        }
    }
}
ll query(int left, int right) {
    int k = int_log2(right - left + 1);
    return min(sp[k][left], sp[k][right - (1 << k) + 1]);
}
int main() {
    // freopen("input.txt", "r", stdin);
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &arr[i]);
    }

    build_sparse_table();

    while(q--) {
        int left, right;
        scanf("%d%d", &left, &right);
        left--, right--;
        printf("%lld\n", query(left, right));
    }
    return 0;
}
