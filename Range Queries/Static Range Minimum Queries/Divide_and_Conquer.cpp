#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int arr[N];
int range_min(int L, int R) {
    if(L == R) return arr[L];
    int M = (L + R) / 2;
    return min(range_min(L, M), range_min(M + 1, R));
}
int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &arr[i]);
    }

    while(q--) {
        int L, R;
        scanf("%d%d", &L, &R);
        printf("%d\n", range_min(L, R));
    }
    return 0;
}
