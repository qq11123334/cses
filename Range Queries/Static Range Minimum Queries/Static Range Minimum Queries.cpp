#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll arr[25][200005];
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int n,q;
    cin >> n >> q;
    for(int i = 0;i < n;i++)
    {
        cin >> arr[0][i];
    }
    ll ub = log2(n);
    for(ll i = 1;i <= ub;i++)
    {
        for(ll j = 0;j + (1 << (i - 1)) < n;j++)
        {
            arr[i][j] = min(arr[i - 1][j],arr[i - 1][j + (1 << (i - 1))]);
        }
    }
    while(q--)
    {
        ll a,b;
        cin >> a >> b;
        ll len = b - a + 1;
        ll idx = log2(len);
        ll overlap = len - (1 << idx);
        cout << min(arr[idx][a - 1],arr[idx][a - 1 + overlap]) << endl;
    }
    return 0;
}