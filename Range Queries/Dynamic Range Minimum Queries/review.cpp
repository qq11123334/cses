#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int arr[200005];
struct Node
{
    int left, right, mid;
    int val;
    Node *lc, *rc;
}node[400005], *root_node, *last_node = node;
void build(Node *root, int left, int right)
{
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right) 
    {
        root->val = arr[left];
        return;
    }
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
    root->val = min(root->lc->val, root->rc->val);
}
void upd(Node *root, int pos, int val)
{
    if(root->left == root->right)
    {
        arr[pos] = val;
        root->val = val;
        return;
    }

    if(pos <= root->mid) upd(root->lc, pos, val);
    else upd(root->rc, pos, val);

    root->val = min(root->lc->val, root->rc->val);
}
int qry(Node *root, int left, int right)
{
    int ans = INF;
    if(root->left == left && root->right == right) ans = root->val;
    else if(left > root->mid) ans = qry(root->rc, left, right);
    else if(right <= root->mid) ans = qry(root->lc, left, right);
    else ans = min(qry(root->lc, left, root->mid), qry(root->rc, root->mid + 1, right));
    return ans;
}
int main()
{
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &arr[i]);
    }

    build(root_node = last_node++, 1, n);

    while(q--)
    {
        int cmd, a, b;
        scanf("%d%d%d", &cmd, &a, &b);
        if(cmd == 1) upd(root_node, a, b);
        else printf("%d\n", qry(root_node, a, b));
    }
    return 0;
}