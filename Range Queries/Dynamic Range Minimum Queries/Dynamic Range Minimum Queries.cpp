#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
const int N = 200005;
int arr[N];
struct Node {
    int right,left,mid;
    int val;
    Node *lc, *rc;
}node[2 * N], *node_root, *last_nodes = node;
void build(Node *root,int left,int right);
int qry(Node *root,int left,int right);
void upd(Node *root,int pos,int x);
int main() {
    int n,q;
    scanf("%d%d",&n,&q);
    for(int i = 1;i <= n;i++) {
        scanf("%d",&arr[i]);
    }
    build(node_root = last_nodes++, 1, n);
    while(q--) {
        int tmp,a,b;
        scanf("%d%d%d",&tmp,&a,&b);
        if(tmp == 1) upd(node_root,a,b);
        if(tmp == 2) printf("%d\n",qry(node_root,a,b));
    }
    return 0;
}
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right)/2;
    if(left == right) {
        root->val = arr[left];
        return;
    }
    build(root->lc = last_nodes++, left, root->mid);
    build(root->rc = last_nodes++,root->mid + 1, right);
    root->val = min(root->rc->val,root->lc->val);
}
int qry(Node *root,int left,int right) {
    //printf("%d %d\n",left,right);
    if(root->left == left && root->right == right) {
        return root->val;
    }
    //if(left == right) return arr[left];
    if(left > root->mid) return qry(root->rc, left, right);
    else if(right <= root-> mid) return qry(root->lc, left, right);
    else return min(qry(root->lc, left, root->mid), qry(root->rc, root->mid + 1, right));
}
void upd(Node *r, int pos, int val) {
  if(r->left == r->right) {
    /* 邊界條件 這個node掌管的範圍長度為1 */      
    arr[pos] = val;
    r->val = val;
    return;
  }

  int mid = (r->left + r->right) / 2;
  /* 判斷 pos 位在我的左子節點或是右子節點 */
  if(pos <= mid) { /* 若位在左子節點 重新計算左子節點(左區間)的最小值 */
    upd(r->lc, pos, val);
  } else { /* 若位在右子節點 重新計算右子節點(右區間)的最小值*/
    upd(r->rc, pos, val);
  }
    
  /* 重新計算完子節點後，重新計算本身的最小值 */
  r->val = min(r->lc->val, r->rc->val);
}