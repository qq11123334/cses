#include <bits/stdc++.h>
#define ll long long
using namespace std;
struct Node {
    int left, right;
    int mid;
    ll to_left, to_right; // the min's index
    Node *lc, *rc;
}node[400500], *last_node = node, *root_node;
ll arr[200005];
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right) {
        root->to_left = left;
        root->to_right = right;
        return;
    }
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);

    {
        // caculate the to_right
        int left_lc = root->lc->to_right;
        int right_lc = root->rc->to_right;
        if(arr[left_lc] + (root->right - left_lc) <= arr[right_lc] + (root->right - right_lc)) {
            root->to_right = left_lc;
        } else {
            root->to_right = right_lc;
        }
    }
    {
        // caculate the to_left
        int left_lc = root->lc->to_left;
        int right_lc = root->rc->to_left;
        if(arr[left_lc] + (left_lc - root->left) <= arr[right_lc] + (right_lc - root->left)) {
            root->to_left = left_lc;
        } else {
            root->to_left = right_lc;
        }
    }
}
void upd(Node *root, ll pos, ll val) {

    if(root->left == root->right) {
        arr[pos] = val;
        return;
    }
    if(pos <= root->mid) {
        upd(root->lc, pos, val);
    } else {
        upd(root->rc, pos, val);
    }

    {
        // caculate the to_right
        int left_lc = root->lc->to_right;
        int right_lc = root->rc->to_right;
        if(arr[left_lc] + (root->right - left_lc) <= arr[right_lc] + (root->right - right_lc)) {
            root->to_right = left_lc;
        } else {
            root->to_right = right_lc;
        }
    }
    {
        // caculate the to_left
        int left_lc = root->lc->to_left;
        int right_lc = root->rc->to_left;
        if(arr[left_lc] + (left_lc - root->left) <= arr[right_lc] + (right_lc - root->left)) {
            root->to_left = left_lc;
        } else {
            root->to_left = right_lc;
        }
    }
}
ll qry(Node *root, ll pos) {
    if(root->left == root->right) {
        return arr[root->to_left] + abs(root->to_left - pos);
    }

    if(pos <= root->mid) {
        int right_min_pos = root->rc->to_left;
        return min(arr[right_min_pos] + abs(pos - right_min_pos), qry(root->lc, pos));
    } else {
        int left_min_pos = root->lc->to_right;
        return min(arr[left_min_pos] + abs(pos - left_min_pos), qry(root->rc, pos));
    }
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
        scanf("%lld", &arr[i]);

    build(root_node = last_node++, 1, n);
    // printf("build finish\n");
    while(q--) {
        int query;
        scanf("%d", &query);
        if(query == 1) {
            ll k, x;
            scanf("%lld%lld", &k, &x);
            upd(root_node, k, x);
        } else {
            ll k;
            scanf("%lld\n", &k);
            printf("%lld\n", qry(root_node, k));
        }
    }
    return 0;
}