#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int nxt[200005];
int arr[200005];
map<int, int> pos;
int n;
struct Node
{
    int l, r;
    int m;
    vector <int> mer;
    Node *lc, *rc;
}node[400005], *last_node = node, *root_node;
void vector_merge(vector <int> &a, vector<int> &b, vector<int> &c)
{
    int sb = (int)b.size(), sc = (int)c.size();
    int ptr_b = 0, ptr_c = 0;
    while(ptr_b < sb || ptr_c < sc)
    {
        if(ptr_b == sb) a.push_back(c[ptr_c++]);
        else if(ptr_c == sc) a.push_back(b[ptr_b++]);
        else
        {
            if(b[ptr_b] > c[ptr_c]) a.push_back(c[ptr_c++]);
            else a.push_back(b[ptr_b++]);
        }
    }
}
void build(Node *root, int left, int right)
{
    root->l = left;
    root->r = right;
    root->m = (left + right) / 2;
    
    if(left == right)
    {
        root->mer.push_back(nxt[left]);
        return;
    }
 
    build(root->lc = last_node++, left, root->m);
    build(root->rc = last_node++, root->m + 1, right);
 
    vector_merge(root->mer, root->lc->mer, root->rc->mer);
}
int query(Node *root, int x, int left, int right)
{
    if(root->l == left && root->r == right)
    {
        int pos = upper_bound(root->mer.begin(), root->mer.end(), x) - root->mer.begin();
        int res = (int)root->mer.size() - pos;
        return res;
    }
 
    if(left > root->m) return query(root->rc, x, left, right);
    else if(right <= root->m) query(root->lc, x, left, right);
    else return query(root->lc, x, left, root->m) + query(root->rc, x, root->m + 1, right); 
}
int main()
{
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &arr[i]);
    }
 
    for(int i = n;i >= 1;i--)
    {
        if(pos[arr[i]] == 0) nxt[i] = INF;
        else nxt[i] = pos[arr[i]];
 
        pos[arr[i]] = i;
    }
 
    build(root_node = last_node++, 1, n);
 
    // for(int i = 0;i < (int)root_node->mer.size();i++)
    // {
    //     printf("%d ", root_node->mer[i]);
    // }
    
    while(q--)
    {
        int left, right;
        scanf("%d%d", &left, &right);
        printf("%d\n", query(root_node, right, left, right));
    }
    return 0;
}
