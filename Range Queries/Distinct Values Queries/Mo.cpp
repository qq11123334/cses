#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int arr[N], ans[N], cnt[N];
map<int, int> reindex;
int block = 500;
struct Range {
    int left, right;
    int indx;
    bool operator < (Range a) const {
        if((left / block) != (a.left / block)) return (left / block) < (a.left / block);
        else return right < a.right;
    } 
}query[N];
void print(map<int, int> m) {
    printf("m = \n");
    for(auto P : m) {
        printf("%d %d\n", P.first, P.second);
    } 
    printf("\n");
}
int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &arr[i]);
        if(!reindex[arr[i]]) reindex[arr[i]] = i;
        arr[i] = reindex[arr[i]];
    }

    for(int i = 0;i < q;i++) {
        scanf("%d%d", &query[i].left, &query[i].right);
        query[i].indx = i;
    }

    sort(query, query + q);

    int cur_left = 0, cur_right = 0;
    int cur_cnt = 0;
    for(int i = 0;i < q;i++) {
        int left = query[i].left, right = query[i].right;
        while(cur_left < left) {
            cur_left++;
            if(cnt[arr[cur_left - 1]]-- == 1) cur_cnt--;
        }
        while(cur_right < right) {
            cur_right++;
            if(cnt[arr[cur_right]]++ == 0) cur_cnt++;
        }
        while(cur_left > left) {
            cur_left--;
            if(cnt[arr[cur_left]]++ == 0) cur_cnt++;            
        }
        while(cur_right > right) {
            cur_right--;
            if(cnt[arr[cur_right + 1]]-- == 1) cur_cnt--;
        }
        // printf("left right = %d %d\n", left, right);
        // print(cnt);
        ans[query[i].indx] = cur_cnt;
    }

    for(int i = 0;i < q;i++) {
        printf("%d\n", ans[i]);
    }
    return 0;
}