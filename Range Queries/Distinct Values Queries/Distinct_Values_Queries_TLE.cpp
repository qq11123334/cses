#include <bits/stdc++.h>
using namespace std;
const int INF = 0x3f3f3f3f;
const int N = 200005;
int nxt[N];
int arr[N];
map<int, int> pos;
int n;
struct Node {
    int l, r;
    int m;
    vector <int> mer;
    Node *lc, *rc;
    void pull() {
        mer.clear();
        for(auto x : lc->mer) {
            mer.push_back(x);
        }
        for(auto x : rc->mer) {
            mer.push_back(x);
        }
        sort(mer.begin(), mer.end());
    }
};
void build(Node *root, int left, int right) {
    root->l = left;
    root->r = right;
    root->m = (left + right) / 2;
    
    if(left == right) {
        root->mer.push_back(nxt[left]);
        return;
    }
 
    build(root->lc = new Node(), left, root->m);
    build(root->rc = new Node(), root->m + 1, right);
 
    root->pull();
}
int query(Node *root, int x, int left, int right) {
    if(root->l == left && root->r == right) {
        int ub_pos = upper_bound(root->mer.begin(), root->mer.end(), x) - root->mer.begin();
        int res = (int)root->mer.size() - ub_pos;
        return res;
    }
 
    if(left > root->m) return query(root->rc, x, left, right);
    else if(right <= root->m) query(root->lc, x, left, right);
    else return query(root->lc, x, left, root->m) + query(root->rc, x, root->m + 1, right); 
}
int main() {
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &arr[i]);
    }
 
    for(int i = n;i >= 1;i--) {
        if(pos[arr[i]] == 0) nxt[i] = INF;
        else nxt[i] = pos[arr[i]];
 
        pos[arr[i]] = i;
    }
    
    Node *root_node;
    build(root_node = new Node(), 1, n);
 
    // for(int i = 0;i < (int)root_node->mer.size();i++)
    // {
    //     printf("%d ", root_node->mer[i]);
    // }
    
    while(q--) {
        int left, right;
        scanf("%d%d", &left, &right);
        printf("%d\n", query(root_node, right, left, right));
    }
    return 0;
}
