#include <bits/stdc++.h>
#define ll long long int
#define ADD 1
#define SET 2
#define PRINT 3
using namespace std;
struct Node {
    int left, right, mid;
    ll val, lazy_val;
    int lazy_type;
    Node *lc, *rc;
    void pull() {
        val = lc->val + rc->val;
    }
}node[500500], *last_node = node, *root_node;
ll arr[200005];
void push(Node *r);
void handle_push_conflict(Node *r, int lazy_type, ll lazy_val) {
    int left = r->left, right = r->right;
    if(lazy_type == SET) {
        r->lazy_type = SET;
        r->lazy_val = lazy_val;
        r->val = (lazy_val) * (right - left + 1);
    } else if(lazy_type == ADD) {
        if(r->lazy_type == SET) push(r);
        r->lazy_type = ADD;
        r->lazy_val += lazy_val;
        r->val += (lazy_val) * (right - left + 1);
    }
}
void push(Node *r) {
    if(!r->lazy_type) return;
    if(r->left != r->right) {
        handle_push_conflict(r->lc, r->lazy_type, r->lazy_val);
        handle_push_conflict(r->rc, r->lazy_type, r->lazy_val);
    }
    r->lazy_type = 0;
    r->lazy_val = 0;
}
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    r->lazy_val = 0, r->lazy_type = 0;
    if(left == right) {
        r->val = arr[left];
        return;
    }
    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
void upd(Node *r, int ql, int qr, int type, ll val) {
    if(ql > r->right || qr < r->left) return;
    if(ql <= r->left && r->right <= qr) {
        handle_push_conflict(r, type, val);
        return;
    }
    push(r);
    upd(r->lc, ql, qr, type, val);
    upd(r->rc, ql, qr, type, val);
    r->pull(); // important
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return 0;
    if(ql <= r->left && r->right <= qr) {
        return r->val;
    }
    push(r);
    return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n, q;
    scanf("%d%d", &n, &q);

    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }
    build(root_node = last_node++, 1, n);

    while(q--) {
        int query; scanf("%d", &query);
        if(query == ADD) { // Increase each value in range [a,b] by x 
            int left, right; ll val; 
            scanf("%d%d%lld", &left, &right, &val);
            upd(root_node, left, right, ADD, val);
        } else if(query == SET) { // Set each value in range [a,b] to x.
            int left, right; ll val; 
            scanf("%d%d%lld", &left, &right, &val);
            upd(root_node, left, right, SET, val);
        } else if(query == PRINT) { // Calculate the sum of [a,b]
            int left, right; scanf("%d%d", &left, &right);
            printf("%lld\n", qry(root_node, left, right));
        }
    }
    return 0;
}