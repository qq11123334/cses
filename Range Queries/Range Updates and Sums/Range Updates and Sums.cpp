#include <bits/stdc++.h>
using namespace std;
#define ll long long int
struct Node
{
    int l, r;
    int m;
    int lazy;
    ll lazy_val;
    ll sum;
    Node *lc, *rc;
}node[400005], *last_node = node, *root_node;
ll arr[200005];
void build(Node *root, int left, int right)
{
    root->l = left;
    root->r = right;
    root->m = (left + right) / 2;
    root->lazy = 0;
    root->lazy_val = 0;

    if(left == right) 
    {
        root->sum = arr[left];
        return;
    }

    build(root->lc = last_node++, left, root->m);
    build(root->rc = last_node++, root->m + 1, right);
    
    root->sum = root->lc->sum + root->rc->sum;
}

void mark(Node *root, int status, ll val);
void refresh_lazy(Node *root);
void upd(Node *root, int left, int right, int status, ll val);
ll qry(Node *root, int left, int right);

int main()
{
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%lld", &arr[i]);
    }

    build(root_node = last_node++, 1, n);

    while(q--)
    {
        int query;
        scanf("%d", &query);

        if(query == 1)
        {
            int a, b;
            ll x;
            scanf("%d%d%lld", &a, &b, &x);
            upd(root_node, a, b, 1, x);
        }
        else if(query == 2)
        {
            int a, b;
            ll x;
            scanf("%d%d%lld", &a, &b, &x);
            upd(root_node, a, b, 2, x);            
        }
        else if(query == 3)
        {
            int a, b;
            scanf("%d%d", &a, &b);
            printf("%lld\n", qry(root_node, a, b));
        }
    }

    return 0;
}
void refresh_lazy(Node *root)
{
    if(root->lazy == 0) return;
    if(root->lazy == 1)
    {
        root->sum = root->sum + (root->r - root->l + 1) * root->lazy_val;
    }
    else if(root->lazy == 2)
    {
        root->sum = (root->r - root->l + 1) * root->lazy_val;
    }
    
    int status = root->lazy;
    ll val = root->lazy_val;
    
    root->lazy = 0;
    root->lazy_val = 0;

    if(root->l == root->r) return;

    mark(root->lc, status, val);
    mark(root->rc, status, val); 
}
void mark(Node *root, int status, ll val)
{
    if(root->lazy == 1 && status == 1)
    {
        root->lazy_val += val;
        return;
    }
    if(status == 1) refresh_lazy(root);
    root->lazy = status;
    root->lazy_val = val;
}
void upd(Node *root, int left, int right, int status, ll val)
{

    if(root->l == left && root->r == right)
    {
        mark(root, status, val);
        refresh_lazy(root);
        return;
    }

    refresh_lazy(root);

    if(right <= root->m) upd(root->lc, left, right, status, val);
    else if(left > root->m) upd(root->rc, left, right, status, val);
    else
    {
        upd(root->lc, left, root->m, status, val);
        upd(root->rc, root->m + 1, right, status, val);
    }

    if(root->rc->lazy) refresh_lazy(root->rc);
    if(root->lc->lazy) refresh_lazy(root->lc);
    root->sum = root->lc->sum + root->rc->sum;
}
ll qry(Node *root, int left, int right)
{
    refresh_lazy(root);
    // printf("%d %d %lld\n", root->l, root->r, root->sum);
    if(root->l == left && root->r == right) return root->sum;

    if(right <= root->m) return qry(root->lc, left, right);
    else if(left > root->m) return qry(root->rc, left, right);
    else return qry(root->lc, left, root->m) + qry(root->rc, root->m + 1, right);
}