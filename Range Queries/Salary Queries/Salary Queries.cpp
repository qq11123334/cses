#include <bits/stdc++.h>
#define pii pair<int,int>
#define tp tuple<int,int,int>
using namespace std;
map<int,int> val_to_idx;
vector<int> idx_to_val;
vector<int> salary;
vector<tp> query;
int bit[1000005];
void reindex()
{
    idx_to_val.push_back(0);
    for(auto i : val_to_idx)
    {
        val_to_idx[i.first] = idx_to_val.size();
        idx_to_val.push_back(i.first);
    }
}
void upd(int x, int dif)
{
    int k = x;
    while(k < (int)idx_to_val.size())
    {
        bit[k] += dif;
        k += k & -k;
    }
}
int sum(int n)
{
    int k = n;
    int ans = 0;
    while(k > 0)
    {
        ans += bit[k];
        k -= k & -k;
    } 
    return ans;
}
void print()
{
    for(int i = 0;i < (int)idx_to_val.size();i++)
    {
        printf("%d ", idx_to_val[i]);
    }
    printf("\n");
}
int main()
{
    int n, q;
    scanf("%d%d", &n, &q);

    salary.push_back(0);
    for(int i = 0;i < n;i++)
    {
        int num;
        scanf("%d", &num);
        salary.push_back(num);
        val_to_idx[num];    
    }

    for(int i = 0;i < q;i++)
    {
        char cmd;
        int a, b;
        scanf(" %c %d %d", &cmd, &a, &b);
        if(cmd == '!')
        {
            val_to_idx[b];
            query.push_back(make_tuple(1, a, b));
        }
        else
        {
            query.push_back(make_tuple(2, a, b));
        }
    }
    
    reindex();

    for(int i = 1;i <= n;i++)
    {
        upd(val_to_idx[salary[i]], 1);
    }
    for(int i = 0;i < q;i++)
    {
        int cmd, a, b;
        tie(cmd, a, b) = query[i];
        if(cmd == 1)
        {
            upd(val_to_idx[salary[a]], -1);
            upd(val_to_idx[b], 1);
            salary[a] = b;
        }
        else
        {
            auto it_a = lower_bound(idx_to_val.begin(), idx_to_val.end(), a);
            auto it_b = upper_bound(idx_to_val.begin(), idx_to_val.end(), b);
            it_a--;
            it_b--;
            printf("%d\n", sum(val_to_idx[*it_b]) - sum(val_to_idx[*it_a]));
        }
    }
    return 0;
}