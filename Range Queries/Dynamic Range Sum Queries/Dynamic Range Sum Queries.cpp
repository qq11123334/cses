#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
int n; ll arr[N], pre[N], BIT[N];
void upd(ll x,ll dif) { // update the value at position x by difference dif
    for(int i = x;i <= n;i += (i & -i)) {
        BIT[i] += dif;
    }
}
ll sum(ll x) { // return the sum of values in range [1, x]
    ll ans = 0;
    for(int i = x;i > 0;i -= (i & -i)) {
        ans = ans + BIT[i];
    }
    return ans;
}
void build() {
    for(int i = 1;i <= n;i++) { // build, index must start at 1
        int k = i & -i;
        BIT[i] = pre[i] - pre[i - k];
        // pre = prefix sum, index start at 1
    }
}
int main() {
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
        pre[i] = pre[i - 1] + arr[i];
    }
    build();
    while(q--) {
        int query; scanf("%d", &query);
        if(query == 1) {
            int k; ll u; scanf("%d%lld", &k, &u);
            upd(k, u - arr[k]);
            arr[k] = u;
        } else {
            int a, b; scanf("%d%d", &a, &b);
            printf("%lld\n", sum(b) - sum(a - 1));
        }
    }
}