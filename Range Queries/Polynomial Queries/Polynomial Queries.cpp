#include <bits/stdc++.h>
#define ll long long int
#define N (200005)
using namespace std;
ll cal_d(ll first, ll end, int len) {
    return (end - first) / (len - 1);
}
ll length(int left, int right) {
    return (right - left) + 1;
}
struct Node {
    int left, right, mid;
    ll first, end, sum; // first and end are lazy mark
    Node *lc, *rc;
    int len() {
        return (right - left + 1);
    }
    void pull() {
        sum = lc->sum + rc->sum;
    }
}node[2 * N], *last_node = node, *root_node;
ll arr[N];
void build(Node *r, int left, int right) {
    r->left = left; r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->sum = arr[left];
        r->first = 0, r->end = 0;
        return;
    }

    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
void push(Node *r) {
    if(!r->first && !r->end) return;
    if(r->left != r->right) {
        ll d = cal_d(r->first, r->end, r->len());
        ll lc_end = r->first + d * (r->lc->len() - 1);
        ll rc_first = lc_end + d;
        r->lc->sum += ((r->lc->len()) * (r->first + lc_end)) / 2;
        r->rc->sum += ((r->rc->len()) * (rc_first + r->end)) / 2;
        r->lc->first += r->first; r->lc->end += lc_end;
        r->rc->first += rc_first; r->rc->end += r->end;
    }
    r->first = 0; r->end = 0;
}
void upd(Node *r, int left, int right, ll first, ll end) {
    if(r->left == left && r->right == right) {
        r->first += first; r->end += end;
        r->sum += (r->len() * (first + end)) / 2;
        return;
    }
    r->sum += (right - left + 1) * (first + end) / 2;
    push(r);
    if(right <= r->mid) upd(r->lc, left, right, first, end);
    else if(left > r->mid) upd(r->rc, left, right, first, end);
    else {
        ll len = length(left, right);
        ll d = cal_d(first, end, len);
        ll lc_end = first + d * (length(left, r->mid) - 1);
        ll rc_first = lc_end + d;
        upd(r->lc, left, r->mid, first, lc_end);
        upd(r->rc, r->mid + 1, right, rc_first, end);
        r->pull();
    }
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return 0;
    if(ql <= r->left && r->right <= qr) return r->sum;
    push(r);
    return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    build(root_node = last_node++, 1, n);

    while(q--) {
        int query, a, b; 
        scanf("%d%d%d", &query, &a, &b);
        if(query == 1) {
            upd(root_node, a, b, 1, (b - a + 1));
        } else {
            printf("%lld\n", qry(root_node, a, b));
        }
    }

    // for(int i = 1;i <= n;i++) printf("%lld ", qry(root_node, i, i));
    return 0;
}