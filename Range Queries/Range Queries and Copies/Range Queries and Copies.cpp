#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
ll arr[N];
struct Node {
    int left, right, mid;
    ll sum;
    Node *lc, *rc;
    Node() {left = right = mid = sum = 0, lc = rc = NULL;}
    void pull() {
        sum = lc->sum + rc->sum;
    }
}*root_node[N]; // Store the history root
/* Use new_root = copy_node(old_root)
 * and Update new_root */
Node *copy_node(Node *r) {
    Node* cp = new Node();
    cp->left = r->left; cp->right = r->right;
    cp->mid = r->mid; 
    cp->sum = r->sum;
    cp->lc = r->lc; cp->rc = r->rc;
    return cp;
}
void build(Node *r, int left, int right) {
    r->left = left; r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->sum = arr[left]; 
        return;
    }
    build(r->lc = new Node(), left, r->mid);
    build(r->rc = new Node(), r->mid + 1, right);
    r->pull();
}
void upd(Node *r, Node *new_r, int pos, ll val) {
    if(r->left == r->right) {
        new_r->sum = val;
        return;
    }
    if(pos <= r->mid) {
        new_r->lc = copy_node(r->lc);
        upd(r->lc, new_r->lc, pos, val);
    }
    else {
        new_r->rc = copy_node(r->rc);
        upd(r->rc, new_r->rc, pos, val);
    }
    new_r->pull();
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return 0;
    if(ql <= r->left && r->right <= qr) return r->sum;
    return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}
int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    int root_idx = 1;
    build(root_node[root_idx++] = new Node(), 1, n);

    while(q--) {
        int query; scanf("%d", &query);
        if(query == 1) {
            int k, a; ll x;
            scanf("%d%d%lld", &k, &a, &x);
            upd(root_node[k], root_node[k], a, x);
        } else if(query == 2) {
            int k, a, b;
            scanf("%d%d%d", &k, &a, &b);
            printf("%lld\n", qry(root_node[k], a, b));
        } else if(query == 3) {
            int k;
            scanf("%d", &k);
            root_node[root_idx++] = copy_node(root_node[k]);
        }
    }
    return 0;
}