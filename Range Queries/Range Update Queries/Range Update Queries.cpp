#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll arr[200005];
struct Node
{
    int left,right,mid;
    ll val;
    Node *lc,*rc;
}node[400005], *node_root, *last_node = node;
void build(Node *root, int left, int right);
void upd(Node *root,int left, int right, int dif);
ll qry(Node *root, int x);
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int n,q;
    cin >> n >> q;
    build(node_root = last_node++, 1, n);
    for(int i = 1;i <= n;i++)
    {
        cin >> arr[i];
    }
    while(q--)
    {
        int tmp;
        cin >> tmp;
        if(tmp == 1)
        {
            int a,b,u;
            cin >> a >> b >> u;
            upd(node_root,a,b,u);
        }
        else
        {
            int k;
            cin >> k;
            cout << qry(node_root, k) << endl;
        }
    }
    return 0;
}
void build(Node *root,int left,int right)
{
    root->left = left;
    root->right = right;
    int mid = (left + right)/2;
    root->mid = mid;
    root->val = 0;
    if(left == right) return;
    build(root->lc = last_node++, left, mid);
    build(root->rc = last_node++, mid + 1, right);
} 
ll qry(Node *root, int x)
{
    if(root->left == root->right) return (root->val + arr[x]);
    if(x <= root->mid) return (root->val + qry(root->lc,x));
    else return (root->val + qry(root->rc,x));
}
void upd(Node *root, int left, int right, int dif)
{
    if(root->left == left && root->right == right)
    {
        root->val += dif;
        return;
    }
    if(right <= root->mid) upd(root->lc, left, right, dif);
    else if(left > root->mid) upd(root->rc, left, right, dif);
    else
    {
        upd(root->lc, left, root->mid, dif);
        upd(root->rc, root->mid + 1, right, dif);
    }
}