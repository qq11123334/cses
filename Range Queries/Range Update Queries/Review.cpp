#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
ll arr[N];
struct Node {
	int left, right;
	ll offest;
	Node *lc, *rc;
};

void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) {
		r->offest = 0;
		return;
	}

	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
}

void update(Node *r, int ql, int qr, ll dif) {
	if(ql > r->right || qr < r->left) return;
	if(ql <= r->left && r->right <= qr) {
		r->offest += dif;
		return;
	}
	update(r->lc, ql, qr, dif);
	update(r->rc, ql, qr, dif);
}

ll qry(Node *r, int pos) {
	if(r->left == r->right) return r->offest + arr[pos];
	int mid = (r->left + r->right) / 2;
	if(pos <= mid) return r->offest + qry(r->lc, pos);
	else return r->offest + qry(r->rc, pos);
}

int main() {
	cin.tie(0);
	ios_base::sync_with_stdio(false);

	int n, q;
	cin >> n >> q;
	for(int i = 1; i <= n; i++) {
		cin >> arr[i];
	}

	Node *root;
	build(root = new Node(), 1, n);

	while(q--) {
		int type;
		cin >> type;
		if(type == 1) {
			int a, b;
			ll u;
			cin >> a >> b >> u;
			update(root, a, b, u);
		} else if(type == 2) {
			int k;
			cin >> k;
			cout << qry(root, k) << "\n";
		}
	}
}
