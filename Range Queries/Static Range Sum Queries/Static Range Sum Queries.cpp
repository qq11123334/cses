#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll sum[200005];
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int n,q;
    cin >> n >> q;
    ll cur_sum = 0;
    for(int i = 1;i <= n;i++)
    {
        ll num;
        cin >> num;
        cur_sum = cur_sum + num;
        sum[i] = cur_sum;
    }
    while(q--)
    {
        int a,b;
        cin >> a >> b;
        cout << sum[b] - sum[a - 1] << endl;
    }
    return 0;
}