#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 200005;
struct Node {
	int left, right;
	ll sum, lazy;
	Node *lc, *rc;
	Node() {
		sum = 0;
		lazy = 0;
	}
	void pull() {
		sum = lc->sum + rc->sum;
	}
}*root;
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) return;
	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
}
void push(Node *r) {
	if(r->left != r->right && r->lazy) {
		int lc_len = (r->lc->right - r->lc->left + 1);
		int rc_len = (r->rc->right - r->rc->left + 1);
		r->lc->sum = (lc_len * r->lazy);
		r->rc->sum = (rc_len * r->lazy);
		r->lc->lazy = r->lazy;
		r->rc->lazy = r->lazy;
	}
	r->lazy = 0;
}
ll qry(Node *r, int qL, int qR) {
	if(qL > r->right || qR < r->left) return 0;
	if(qL <= r->left && r->right <= qR) return r->sum;
	push(r);
	return qry(r->lc, qL, qR) + qry(r->rc, qL, qR);
}
void upd(Node *r, int qL, int qR, ll val) {
	if(qL > r->right || qR < r->left) return ;
	if(qL <= r->left && r->right <= qR) {
		int len = (r->right - r->left + 1);
		r->sum = (len * val);
		r->lazy = val;
		return;
	}
	push(r);
	upd(r->lc, qL, qR, val);
	upd(r->rc, qL, qR, val);
	r->pull();
}
using Query = pair<int, int>; /* (right, index) */
vector<Query> query[N];
ll ans[N], arr[N], pre[N];
int main() {
	int n, q;
	scanf("%d%d", &n, &q);
	build(root = new Node(), 1, n);
	for(int i = 1; i <= n; i++) {
		scanf("%lld", &arr[i]);	
		pre[i] = pre[i - 1] + arr[i];
	}
	for(int i = 1; i <= q; i++) {
		int left, right;
		scanf("%d%d", &left, &right);
		query[left].push_back(Query(right, i));
	}
	stack<pair<ll, ll>> s; /* (val, right) */
	for(int i = n; i >= 1; i--) {
		int right = i;
		while(!s.empty() && s.top().first < arr[i]) {
			right = s.top().second;
			s.pop();
		}
		s.push(make_pair(arr[i], right));
		upd(root, i, right, arr[i]);
		for(auto Q : query[i]) {
			ans[Q.second] = qry(root, i, Q.first) - (pre[Q.first] - pre[i - 1]);
		}
	}

	for(int i = 1; i <= q; i++) {
		printf("%lld\n", ans[i]);
	}

	return 0;
}