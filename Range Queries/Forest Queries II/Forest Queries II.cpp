#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 1005;
ll grid[N][N];
ll BIT[N][N];
int n;
ll sum(int x, int y) {
    ll res = 0;
    for(int i = x;i > 0;i -= (i & -i)) {
        for(int j = y;j > 0;j -= (j & -j)) {
            res += BIT[i][j];
        }
    }
    return res;
}
void upd(int x, int y, ll dif) {
    for(int i = x;i <= n;i += (i & -i)) {
        for(int j = y;j <= n;j += (j & -j)) {
            BIT[i][j] += dif;
        }
    }
}
void build() {
    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            upd(i, j, grid[i][j]);
        }
    }
}
int main() {
    // freopen("input.txt", "r", stdin);
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            char c; scanf(" %c", &c);
            if(c == '*') {
                grid[i][j] = 1;
            }
        }
    }

    build();

    while(q--) {
        int query; scanf("%d", &query);
        if(query == 1) {
            int x, y; scanf("%d%d", &x, &y);
            if(grid[x][y] == 1) {
                grid[x][y] = 0;
                upd(x, y, -1);
            } else if(grid[x][y] == 0) {
                grid[x][y] = 1;
                upd(x, y, 1);
            }
        } else if(query == 2) {
            int x1, x2, y1, y2;
            scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
            printf("%lld\n", sum(x2, y2) - sum(x2, y1 - 1) - sum(x1 - 1, y2) + sum(x1 - 1, y1 - 1));
        }
    }
    return 0;
}