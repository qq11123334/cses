#include <bits/stdc++.h>
using namespace std;
int arr[200005];
struct Node
{
    int left, right, mid;
    int val;
    Node *lc, *rc;
}node[400005], *node_root, *last_node = node;
void build(Node *root, int left, int right);
int qry(Node *root, int x);
void upd(Node *root, int x, int dif);
int main()
{
    int n, q;
    scanf("%d%d",&n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &arr[i]);
    }
    build(node_root = last_node++, 1, n);
    while(q--)
    {
        int num;
        scanf("%d",&num);
        if(num > node_root->val) printf("0 ");
        else
        {
            int pos = qry(node_root, num);
            printf("%d ",pos);
            upd(node_root, pos, -num);
        }
    }
    return 0;
}
void build(Node *root, int left, int right)
{
    root->left = left;
    root->right = right;
    int mid = (left + right)/2;
    root->mid = mid;
    if(left == right)
    {
        root->val = arr[left];
        return;
    }
    build(root->lc = last_node++, left, mid);
    build(root->rc = last_node++, mid + 1, right);
    root->val = max(root->lc->val, root->rc->val);
}
int qry(Node *root, int x)
{
    if(root->left == root->right) return root->left;
    if(root->lc->val >= x) return qry(root->lc, x);
    else return qry(root->rc, x);
}
void upd(Node *root, int x, int dif)
{
    if(root->left == root->right)
    {
        root->val += dif;
        return;
    }
    if(x <= root->mid) upd(root->lc, x, dif);
    else upd(root->rc, x, dif);
    root->val = max(root->lc->val, root->rc->val);
}
