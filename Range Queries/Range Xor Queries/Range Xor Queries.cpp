#include <bits/stdc++.h>
using namespace std;
int pre[200005];
int main()
{
    cin.tie(0);
    ios_base::sync_with_stdio(0);
    int n,q;
    cin >> n >> q;
    for(int i = 1;i <= n;i++)
    {
        int num;
        cin >> num;
        pre[i] = pre[i - 1] ^ num;
    }
    while(q--)
    {
        int a,b;
        cin >> a >> b;
        cout << (pre[b] ^ pre[a - 1]) << endl;
    }
    return 0;
}