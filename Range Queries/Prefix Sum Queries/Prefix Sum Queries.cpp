#include <bits/stdc++.h>
#define ll long long int
#define pre_mx first
#define sum second
#define pii pair<ll, ll>
using namespace std;
ll arr[200005];
struct Node
{
    int left, right;
    int mid;
    ll pre_mx, sum;
    Node *lc, *rc;
}node[500005], *last_node = node, *root_node = node;
void build(Node *root, int left, int right)
{
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;

    if(left == right)
    {
        root->pre_mx = arr[left];
        root->sum = arr[left];
        return;
    }

    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);

    root->sum = root->lc->sum + root->rc->sum;
    root->pre_mx = max(root->lc->pre_mx, root->lc->sum + root->rc->pre_mx);
}
void upd(Node *root, ll val, ll pos)
{
    if(root->left == root->right)
    {
        arr[pos] = val;
        root->pre_mx = val;
        root->sum = val;
        return;
    }

    if(pos <= root->mid)
        upd(root->lc, val, pos);
    else
        upd(root->rc, val, pos);

    root->sum = root->lc->sum + root->rc->sum;
    root->pre_mx = max(root->lc->pre_mx, root->lc->sum + root->rc->pre_mx);
}
pii qry(Node *root, int left, int right)
{
    if(root->left == left && root->right == right)
        return make_pair(root->pre_mx, root->sum);

    if(right <= root->mid)
        return qry(root->lc, left, right);
    else if(left > root->mid)
        return qry(root->rc, left, right);
    else
    {
        pii left_qry, right_qry;
        left_qry = qry(root->lc, left, root->mid);
        right_qry = qry(root->rc, root->mid + 1, right);
        
        pii res;
        res.pre_mx = max(left_qry.pre_mx, left_qry.sum + right_qry.pre_mx);
        res.sum = left_qry.sum + right_qry.sum;
        return res;
    }
}
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 1;i <= n;i++)
        scanf("%lld", &arr[i]);

    build(root_node, 1, n);
    
    for(int i = 0;i < m;i++)
    {
        int query;
        scanf("%d", &query);
        if(query == 1)
        {
            ll k, u;
            scanf("%lld %lld", &k, &u); 
            upd(root_node, u, k);
        }
        else
        {
            int a, b;
            scanf("%d %d", &a, &b);
            printf("%lld\n", max(0LL, qry(root_node, a, b).pre_mx));
        }
    }
    return 0;
}