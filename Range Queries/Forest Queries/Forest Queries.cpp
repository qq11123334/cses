#include <bits/stdc++.h>
using namespace std;
int prefix[1005][1005];
int main()
{
    int n, q;
    scanf("%d%d",&n,&q);
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            char c;
            scanf(" %c",&c);
            prefix[i][j] = prefix[i - 1][j] + prefix[i][j - 1] - prefix[i - 1][j - 1] + (c == '*');
        }
    }
    while(q--)
    {
        int x1,x2,y1,y2;
        scanf("%d%d%d%d",&y1, &x1, &y2, &x2);
        if(x1 > x2) swap(x1, x2);
        if(y1 > y2) swap(y1, y2);
        printf("%d\n", prefix[y2][x2] - prefix[y1 - 1][x2] - prefix[y2][x1 - 1] + prefix[y1 - 1][x1 - 1]);
    }
    return 0;
}