#include <bits/stdc++.h>
using namespace std;
int arr[200005];
struct Node
{
    int left, right;
    int mid;
    int val;
    Node *lc, *rc;
}node[400005], *node_root, *last_node = node;
void build(Node *root, int left, int right)
{
    root->left = left;
    root->right = right;
    root->mid = (left + right)/2;
    root->val = 0;
    if(left == right) return;
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
}
void upd(Node *root, int left, int right)
{
    if(root->left == left && root->right == right)
    {
        root->val -= 1;
        return;
    }
    if(root->mid >= right) upd(root->lc, left, right);
    if(root->mid < left) upd(root->rc, left, right);
    else
    {
        upd(root->lc, left, root->mid);
        upd(root->rc, root->mid + 1, right);
    }
}
int query(Node *root, int x)
{
    if(root->left == root->right) return root->val;
    int ans = root->val;
    if(x <= root->mid) ans += query(root->lc, x);
    else ans +=query(root->rc, x);
    return ans;
}
int main()
{
    int n;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &arr[i]);
    }

    build(node_root = last_node++, 1, n);
    for(int i = 0;i < n;i++)
    {
        int q;
        scanf("%d", &q);
        int l = 0,r = n;
        while(l < r - 1)
        {
            int m = (l + r)/2;
            if(m + query(node_root, m) >= q) r = m;
            else l = m;
        }
        //printf("%d %d\n", r, query(node_root, r));
        printf("%d ", arr[r]);
        upd(node_root, r, n);
    }
    return 0;
}