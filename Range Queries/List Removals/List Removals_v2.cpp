#include <bits/stdc++.h>
using namespace std;
struct Node {
    int left, right;
    int mid;
    int remain;
    Node *lc, *rc;
}node[400500], *last_node = node, *root_node;
int arr[200005];
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right) {
        root->remain = 1;
        return;
    }
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);

    root->remain = root->lc->remain + root->rc->remain;
}
void upd(Node *root, int kth) {
    root->remain--;
    if(root->left == root->right) return;
    if(root->lc->remain >= kth) upd(root->lc, kth);
    else upd(root->rc, kth - root->lc->remain);
}
int qry(Node *root, int kth) {
    if(root->left == root->right) return arr[root->left];
    if(root->lc->remain >= kth) return qry(root->lc, kth);
    else return qry(root->rc, kth - root->lc->remain);
}
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++)
        scanf("%d", &arr[i]);
    
    build(root_node = last_node++, 1, n);
    for(int i = 0;i < n;i++) {
        int k;
        scanf("%d", &k);
        printf("%d ", qry(root_node, k));
        upd(root_node, k);
    }
    return 0;
}