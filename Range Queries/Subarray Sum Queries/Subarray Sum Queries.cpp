#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200500;
ll arr[N];
struct Node {
    ll left, right, mid;
    ll pre, suf;
    ll sum, mx;
    Node *lc, *rc; 
    void pull() {
        sum = lc->sum + rc->sum;
        pre = max(lc->pre, lc->sum + rc->pre);
        suf = max(rc->suf, rc->sum + lc->suf);
        mx = max({lc->suf + rc->pre, lc->mx, rc->mx});
    }
}node[2 * N], *last_node = node, *root_node;
void build(Node *root, ll l, ll r) {
    root->left = l;
    root->right = r;
    root->mid = (l + r) / 2;
    if(l == r) {
        root->pre = arr[l];
        root->suf = arr[l];
        root->mx = arr[l];
        root->sum = arr[l];
        return;
    }
    build(root->lc = last_node++, l, root->mid);
    build(root->rc = last_node++, root->mid + 1, r);
    root->pull();
}
void upd(Node *root, ll i, ll v) {
    if(root->left == root->right) {
        arr[i] = v;
        root->pre = v;
        root->suf = v;
        root->mx = v;
        root->sum = v;
        return;
    }
    if(i <= root->mid) upd(root->lc, i, v);
    else upd(root->rc, i, v);
    root->pull();
}
int main() {
    ll n, m;
    scanf("%lld%lld", &n, &m);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &arr[i]);
    }
 
    build(root_node = last_node++, 0, n - 1);
 
    while(m--) {
        ll i, v;
        scanf("%lld %lld", &i, &v);
        upd(root_node, i - 1, v);
        printf("%lld\n", max(0LL, root_node->mx));
    }
 
    return 0;
}