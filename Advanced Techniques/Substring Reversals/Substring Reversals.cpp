#include <bits/stdc++.h>
using namespace std;
struct Treap {
    int pri, size, swap_tag;
    char val;
    Treap *lc, *rc;
    Treap(char c) {
        pri = rand();
        size = 1;
        swap_tag = 0;
        val = c;
        lc = rc = NULL;
    }
};
int size(Treap *node) {
    if(node == NULL) return 0;
    else return node->size;
}
void push(Treap *node) {
    assert(node != NULL);
    if(node != NULL) {
        if(node->swap_tag) {
            swap(node->lc, node->rc);
            if(node->lc != NULL) node->lc->swap_tag ^= 1;
            if(node->rc != NULL) node->rc->swap_tag ^= 1;
        }
        node->swap_tag = 0;
    }
}
void split(Treap *node, Treap *&left, Treap *&right, int k) {
    if(node == NULL) {
        left = right = NULL;
    } else {
        push(node);
        if(size(node->lc) < k) {
            split(node->rc, node->rc, right, k - size(node->lc) - 1);
            left = node;
        } else {
            split(node->lc, left, node->lc, k);
            right = node;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
    }
}
void merge(Treap *&node, Treap *left, Treap *right) {
    if(left == NULL) node = right;
    else if(right == NULL) node = left;
    else {
        if(left->pri < right->pri) {
            push(left);
            merge(left->rc, left->rc, right);
            node = left;
        } else {
            push(right);
            merge(right->lc, left, right->lc);
            node = right;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
    }
}
void DFS_print(Treap *node) {
    if(node != NULL) {
        if(node->swap_tag) push(node);
        DFS_print(node->lc);
        printf("%c", node->val);
        DFS_print(node->rc);
    }
}
int main() {
    srand(time(0) * clock());
    int n, q;
    scanf("%d%d", &n, &q);
    
    Treap *root = NULL;
    for(int i = 0;i < n;i++) {
        char c;
        scanf(" %c", &c);
        merge(root, root, new Treap(c));
    }
    
    // printf("Origin = ");
    // DFS_print(root);
    // printf("\n");
 
    while(q--) {
        int a, b;
        scanf("%d%d", &a, &b);
        Treap *left, *mid, *right;
        split(root, left, right, b);
        split(left, left, mid, a - 1);
        mid->swap_tag ^= 1;
 
        merge(root, left, mid);
        merge(root, root, right);
    }
    
    DFS_print(root);
    printf("\n");
    return 0;
}