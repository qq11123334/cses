#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
typedef pair<int, int> Edge;
int dfn[N], low[N];
bool cut[N];
vector<int> adj[N];
vector<Edge> bridge;
void DFS(int x, int par = -1) {
    static int ord = 1;
    dfn[x] = low[x] = ord++;
    int child_cnt = 0;
    for(auto v : adj[x]) {
        if(!dfn[v]) {
            child_cnt++;
            DFS(v, x);
            low[x] = min(low[v], low[x]);
            if(dfn[x] <= low[v]) {
                cut[x] = true;
            }
            if(dfn[x] < low[v]) {
                bridge.push_back(Edge(x, v));
            }
        } else if(par != v) {
            low[x] = min(dfn[v], low[x]);
        }
    }
    if(dfn[x] == 1 && child_cnt == 1) {
        cut[x] = false;
    }
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++) {
        if(!dfn[i]) DFS(i);
    }

    if(1) {
        int sz = 0;
        for(int i = 1; i <= n; i++) {
            if(cut[i]) sz++;
        }
        printf("%d\n", sz);
        for(int i = 1; i <= n; i++) {
            if(cut[i]) printf("%d ", i);
        }
    }

    if(0) {
        printf("%d\n", (int)bridge.size());
        for(auto e : bridge) {
            printf("%d %d\n", e.first, e.second);
        }
    }
    return 0;
}