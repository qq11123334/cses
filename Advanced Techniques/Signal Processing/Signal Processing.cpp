#include <bits/stdc++.h>
using namespace std;
typedef complex<double> cd;
const double PI = acos(-1);
vector<cd> FFT(vector<cd> &F, bool inv) {
    int n = (int)F.size();
    vector<cd> res(n);
    for(int k = 0;k < n;k++) {
        int RevBit = 0;
        for(int i = 1;i < n;i <<= 1) {
            RevBit <<= 1;
            if(k & i) RevBit++;
        }
        res[RevBit] = F[k];
    }
 
    for(int Pow = 2;Pow <= n;Pow *= 2) {
        double theta = (inv ? -1 : 1) * 2.0 * PI / Pow;
        cd omega(cos(theta), sin(theta));
        for(int rnd = 0;rnd < n; rnd += Pow) {
            cd cur = 1;
            for(int i = 0;i < Pow / 2;i++) {
                cd a = res[rnd + i];
                cd b = cur * res[rnd + i + Pow / 2];
                res[rnd + i] = a + b;
                res[rnd + i + Pow / 2] = a - b;
                cur *= omega;
            }
        }
    }
 
    if(inv) {
        for(int i = 0;i < n;i++) res[i] /= n;
    }
    return res;
}
vector<cd> Poly_multi(vector<cd> A, vector<cd> B) {
    int k = (1 << ((int)log2((int)A.size() + (int)B.size()) + 1));
    A.resize(k);
    B.resize(k);
 
    A = FFT(A, 0);
    B = FFT(B, 0);
    
    vector<cd> C(k);
    for(int i = 0;i < k;i++) C[i] = A[i] * B[i];
 
    C = FFT(C, 1);
    return C;
}
vector<cd> A, B, C;
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < n;i++) {
        double ai;
        scanf("%lf", &ai);
        A.push_back(ai);
    }	
    for(int i = 0;i < m;i++) {
        double bi;
        scanf("%lf", &bi);
        B.push_back(bi);
    }

    reverse(B.begin(), B.end());

    C = Poly_multi(A, B);

    for(int i = 0;i < n + m -1;i++) {
        printf("%d ", (int)round(C[i].real()));
    }
    return 0;
}