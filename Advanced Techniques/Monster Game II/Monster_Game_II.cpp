#include <bits/stdc++.h>
#define ll long long int
#define EPS (1e-6)
using namespace std;
const int N = 200005;
const double INF = (1e18);
// double equality
bool de(double a, double b) {
	return fabs(a - b) < EPS;
}
// f(x) = s * x + c
bool compare_by_range = false;
struct Line {
	double s, c;
	double left_boundry;

	double get_val(double x) const {
		return s * x + c;
	}
	
	Line(double _s, double _c) : s(_s), c(_c) {}
	Line(set<Line>::iterator it) : s(it->s), c(it->c) {}
	Line(double _left_boundry) : left_boundry(_left_boundry) {}

	bool operator < (const Line &L) const {
		if(compare_by_range)
			return left_boundry < L.left_boundry;
		if(de(s, L.s)) return c > L.c;
		else return s > L.s;
	}

	// const is important
	double intersect(const Line &L) const {
		assert(!de(L.s, s));
		// x = (c2 - c1) / (m1 - m2)
		// y = (m2c1 - m1c2) / (m2 - m1)
		return (L.c - c) / (s - L.s);
	}
};
set<Line> line_set;
bool is_never_min(set<Line>::iterator it) {
	if(it == prev(line_set.end())) return false;
	if(de(it->s, next(it)->s)) return true;
	if(it == line_set.begin()) return false;
	return (it->intersect(*prev(it)) >= next(it)->intersect(*prev(it)));
}
void set_boundry(set<Line>::iterator it) {
	Line new_line = Line(it);
	if(it == line_set.begin()) new_line.left_boundry = -INF;
	else new_line.left_boundry = it->intersect(*prev(it));
	line_set.erase(it);
	line_set.insert(new_line);
}
void insert_line(Line L) {	
	auto [it, insert] = line_set.insert(L);
	if(!insert) return;
	while(it != line_set.begin() && is_never_min(prev(it))) {
		line_set.erase(prev(it));
	}
	if(is_never_min(it)) {
		auto nxt = next(it);
		line_set.erase(it);
		set_boundry(nxt);
		return;
	};
	set_boundry(it);
	while(next(it) != line_set.end()) {
		set_boundry(next(it));
		if(!is_never_min(next(it))) break;
		line_set.erase(next(it));
	}
}
double qry_min(double x) {
	compare_by_range = true;
	auto it = line_set.upper_bound(Line(x + EPS));
	it--;
	compare_by_range = false;
	return it->get_val(x);
}
double dp[N];
ll S[N], F[N];
int main() {
	int n;
	ll x;
	scanf("%d%lld", &n, &x);
	for(int i = 0;i < n;i++) {
		scanf("%lld", &S[i]);
	}
	for(int i = 0;i < n;i++) {
		scanf("%lld", &F[i]);
	}

	insert_line(Line(x, 0));
	for(int i = 0; i < n; i++) {
		dp[i] = qry_min(S[i]);
		insert_line(Line(F[i], dp[i]));
	}
	printf("%lld\n", (ll)(dp[n - 1] + EPS));
}
