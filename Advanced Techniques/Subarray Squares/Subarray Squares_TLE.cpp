#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 3005;
ll dp[N][N];
ll arr[N];
ll _cost[N][N], pre[N];
int n;
void build_cost() {
    for(int i = 1;i <= n;i++) {
        pre[i] = pre[i - 1] + arr[i];
    }
    for(int l = 1;l <= n;l++) {
        for(int r = l;r <= n;r++) {
            ll sum = pre[r] - pre[l - 1];
            _cost[l][r] = sum * sum;
        }
    }
    
}
ll cost(int l, int r) {
    return _cost[l][r];
}
ll solve(int pre, int k) {
    // printf("%d %d\n", pre, k);
    assert(pre >= k);
    if(k == 1) return cost(1, pre);
    if(dp[pre][k]) return dp[pre][k]; 
    ll res = (ll)8e18;
    for(int i = k;i <= pre;i++) {
        res = min(solve(i - 1, k - 1) + cost(i, pre), res);
    }
    return dp[pre][k] = res;
}
int main() {
    int k;
    scanf("%d%d", &n, &k);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    build_cost();

    printf("%lld\n", solve(n, k));
    
    return 0;
}