#include <bits/stdc++.h>
#define ll long long int
#define INF ((ll)8e18)
using namespace std;
const int N = 3005;
ll pre[N], cost[N][N], arr[N];
ll dp[N][N];
int n, k;
void build_cost() {
    for(int i = 1;i <= n;i++) {
        pre[i] = pre[i - 1] + arr[i];
    }
    for(int l = 1;l <= n;l++) {
        for(int r = l;r <= n;r++) {
            ll sum = pre[r] - pre[l - 1];
            cost[l][r] = sum * sum;
        }
    }
}
void compute(int j, int left, int right, int left_bound, int right_bound) {
    if(left > right) return;
    int mid = (left + right) / 2;
    pair<ll, int> best; // val, pos
    best = make_pair(INF, -1);
    for(int i = max(j, left_bound);i <= right_bound;i++) {
        best = min(best, make_pair(dp[i - 1][j - 1] + cost[i][mid], i));
    }
    dp[mid][j] = best.first;
    compute(j, left, mid - 1, left_bound, best.second);
    compute(j, mid + 1, right, best.second, right_bound);
}
int main() {
    scanf("%d%d", &n, &k);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    build_cost();

    for(int i = 1;i <= n;i++) {
        dp[i][1] = cost[1][i];
    }
    for(int j = 2;j <= k;j++) {
        compute(j, 1, n, 1, n);
    }

    printf("%lld\n", dp[n][k]);
    return 0;
}