#include <bits/stdc++.h>
using ll = long long;
using namespace std;
const ll INF = ((ll)8e18);
const int N = 3005;
ll pre[N], cost[N][N], arr[N];
ll dp[N][N];
int n, k;
ll range_sum(int L, int R) {
	return pre[R] - pre[L - 1];
}
void build_cost() {
	for(int i = 1;i <= n;i++) {
		pre[i] = pre[i - 1] + arr[i];
	}
	for(int l = 1;l <= n;l++) {
		for(int r = l + 2;r <= n;r++) {
			int m = (l + r + 1) / 2;
			cost[l][r] = cost[l][r - 1] + range_sum(m, r - 1);
		}
	}
}
void compute(int j, int left, int right, int left_bound, int right_bound) {
	if(left > right) return;
	int mid = (left + right) / 2;
	pair<ll, int> best; // val, pos
	best = make_pair(INF, -1);
	for(int i = max(j - 1, left_bound);i <= right_bound;i++) {
		best = min(best, make_pair(dp[i][j - 1] + cost[i][mid], i));
	}
	dp[mid][j] = best.first;
	compute(j, left, mid - 1, left_bound, best.second);
	compute(j, mid + 1, right, best.second, right_bound);
}
int main() {
	scanf("%d%d", &n, &k);
	for(int i = 1;i <= n;i++) {
		scanf("%lld", &arr[i]);
	}

	build_cost();

	for(int i = 1;i <= n;i++) {
		// cal dp[i][1]
		for(int j = 1;j <= i;j++) {
			dp[i][1] += abs(i - j) * arr[j];
		}
	}
	for(int j = 2;j <= k;j++) {
		compute(j, 1, n, 1, n);
	}

	ll ans = INF;
	for(int i = k;i <= n;i++) {
		ll res = 0;
		for(int j = i;j <= n;j++) {
			res += abs(i - j) * arr[j];
		}
		ans = min(ans, dp[i][k] + res);
	}
	printf("%lld\n", ans);
	return 0;
}