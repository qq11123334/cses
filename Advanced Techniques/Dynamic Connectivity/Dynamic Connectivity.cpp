#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int p[N], sz[N];
int n, m, k;
void build_dsu() {
	for(int i = 0; i <= n; i++) {
		p[i] = i;
		sz[i] = 1;
	}
}
int find(int x) {
	if(x == p[x]) return x;
	else return find(p[x]);
}
bool unite(int a, int b, vector<pair<int, int>> &update) {
	a = find(a), b = find(b);
	if(a == b) return false;
	if(sz[a] > sz[b]) swap(a, b);
	// update and record
	update.push_back(make_pair(a, p[a]));
	sz[b] += sz[a]; 
	p[a] = b;
	return true;
}
struct Edge {
	int a, b;
	int start, end;
	Edge() {}
	Edge(int _a, int _b, int _start) {
		a = _a;
		b = _b;
		start = _start;
		end = k + 1;
	}
};
vector<Edge> edge;
map<pair<int, int>, int> indx;
void add_start(int a, int b, int start) {
	if(a > b) swap(a, b);
	indx[make_pair(a, b)] = (int)edge.size();
	edge.push_back(Edge(a, b, start));	
}
void add_end(int a, int b, int end) {
	if(a > b) swap(a, b);
	int e = indx[make_pair(a, b)];
	edge[e].end = end;
}
bool inside(int L1, int R1, int L2, int R2) {
	// [L1, R1] cover [L2, R2]
	return (L1 <= L2) && (R2 <= R1);
}
bool intersect(int L1, int R1, int L2, int R2) {
	if(L1 > L2) {
		swap(L1, L2);
		swap(R1, R2);
	}
	return (R1 > L2);
}
map<pair<int, int>, vector<Edge>> edge_subset;
void add_range(int L, int R, const Edge &e) {
	if(inside(e.start, e.end, L, R)) {
		edge_subset[make_pair(L, R)].push_back(e);
		return;
	}

	if(!intersect(L, R, e.start, e.end)) return;

	int M = (L + R) / 2;
	add_range(L, M, e);
	add_range(M, R, e);
}
void build_range() {
	for(auto e : edge) {
		add_range(0, k + 1, e);
	}
}
int ans[N];
void solve(int L, int R, int cur_ans) {
	vector<pair<int, int>> update;
	int M = (L + R) / 2;

	for(auto &e : edge_subset[make_pair(L, R)]) {
		if(inside(e.start, e.end, L, R)) {
			if(unite(e.a, e.b, update)) {
				cur_ans--;
			}
		}
	}
	if(L == R - 1) {
		ans[L] = cur_ans;
	} else {
		solve(L, M, cur_ans);
		solve(M, R, cur_ans);
	}

	for(int i = (int)update.size() - 1; i >= 0; i--) {
		auto P = update[i];
		p[P.first] = P.second;
	}
}
int main() {
	scanf("%d%d%d", &n, &m, &k);
	build_dsu();

	for(int i = 0; i < m; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		add_start(a, b, 0);
	}

	for(int i = 1; i <= k; i++) {
		int type, a, b;
		scanf("%d%d%d", &type, &a, &b);
		if(type == 1) {
			// create
			add_start(a, b, i);
		} else {
			// delete
			add_end(a, b, i);
		}
	}

	build_range();
	solve(0, k + 1, n);

	for(int i = 0 ;i <= k; i++) {
		printf("%d ", ans[i]);
	}
	return 0;
}
