#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005, Q = 200005, M = 200005;
pair<int, int> query[Q], edge[M], range[Q];
vector<int> timeline[N];
int p[N], sz[N];
int n;
void init() {
    for(int i = 0;i <= n;i++) {
        sz[i] = 1;
        p[i] = i;
    }
}
int find(int x) {
    if(p[x] == x) return x;
    else return p[x] = find(p[x]);
}
void unite(int a, int b) {
    a = find(a), b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    sz[b] += sz[a];
    p[a] = b;
}


int main() {
    int m, q;
    scanf("%d%d%d", &n, &m, &q);
    for(int i = 1;i <= m;i++) {
        scanf("%d%d", &edge[i].first, &edge[i].second);
    }

    for(int i = 1;i <= q;i++) {
        scanf("%d%d", &query[i].first, &query[i].second);
        range[i] = make_pair(-1, m + 1);
        timeline[(-1 + (m + 1)) / 2].push_back(i);
    }

    int rnd = log2(m) + 10;
    for(int _ = 0;_ < rnd;_++) {
        init();
        for(int i = 0;i <= m;i++) {
            unite(edge[i].first, edge[i].second);
            while(!timeline[i].empty()) {
                int Q = timeline[i].back(); 
                timeline[i].pop_back();
                bool isConnect = (find(query[Q].first) == find(query[Q].second));
                if(isConnect) {
                    range[Q] = make_pair(range[Q].first, i);
                } else {
                    range[Q] = make_pair(i, range[Q].second);
                }
                if(range[Q].first != range[Q].second - 1) {
                    int mid = (range[Q].first + range[Q].second) / 2;
                    timeline[mid].push_back(Q);
                }
            }
        }
    }

    for(int i = 1;i <= q;i++) {
        assert(range[i].first == range[i].second - 1);
        if(range[i].second != m + 1) {
            printf("%d\n", range[i].second);
        } else {
            printf("-1\n");
        }
    }
    return 0;
}