#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
const int N = 505;
struct Edge {
    int to, cap, cost, rev;
    bool isReal;
    Edge() {}
    Edge(int _to, int _cap, int _cost, int _rev, bool _isReal) : to(_to), cap(_cap), cost(_cost), rev(_rev), isReal(_isReal) {}
};
vector<Edge> adj[N];
void add_edge(int a, int b, int cap, int cost) {
    adj[a].push_back(Edge(b, cap, cost, (int)adj[b].size(), 1));
    adj[b].push_back(Edge(a, 0, -cost, (int)adj[a].size() - 1, 0));
}
int dis[N], par[N], par_id[N], in_que[N];
int k;
pair<int, int> MCMF(int s, int t) {
    int flow = 0, cost = 0;
    while(true) {
        memset(dis, 0x3f, sizeof(dis));
        memset(in_que, 0, sizeof(in_que));
        dis[s] = 0;
        queue<int> que;
        que.push(s);
        while(!que.empty()) {
            int x = que.front();
            que.pop(); in_que[x] = 0;
            for(int i = 0;i < (int)adj[x].size();i++) {
                Edge &e = adj[x][i];
                if(e.cap > 0 && dis[e.to] > dis[x] + e.cost) {
                    dis[e.to] = dis[x] + e.cost;
                    par[e.to] = x;
                    par_id[e.to] = i;
                    if(!in_que[e.to]) {
                        que.push(e.to);
                        in_que[e.to] = 1;
                    }
                }
            }
        }

        if(dis[t] >= INF) break;

        int mi_flow = INF;
        for(int i = t;i != s;i = par[i]) {
            mi_flow = min(mi_flow, adj[par[i]][par_id[i]].cap);
        }

        flow += mi_flow, cost += mi_flow * dis[t];
        for(int i = t;i != s;i = par[i]) {
            Edge &e = adj[par[i]][par_id[i]];
            e.cap -= mi_flow;
            adj[e.to][e.rev].cap += mi_flow;
        }

        if(flow == k) break;
    }
    return make_pair(flow, cost);
}
vector<int> ans;
int s, t;
bool DFS_print(int x) {
    ans.push_back(x);
    if(x == t) return 1;
    for(Edge &e : adj[x]) {
        if(e.cap == 0 && e.isReal) {
            e.cap++;
            if(DFS_print(e.to)) return true;
        }
    }
    return false;
}
int main() {
    int n, m;
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        add_edge(a, b, 1, 1);
    }

    s = 1, t = n;
    auto P = MCMF(s, t);
    if(P.first != k) {
        printf("-1\n");
    } else {
        printf("%d\n", P.second);
        for(int i = 0;i < P.first;i++) {
            ans.clear();
            DFS_print(s);
            printf("%d\n", (int)ans.size());
            for(auto v : ans) {
                printf("%d ", v);
            }
            printf("\n");
        }
    }
    return 0;
}