#include <bits/stdc++.h>
#define ll long long int
#define INF ((ll)8e18)
using namespace std;
const int N = 5005;
ll dp[N][N];
int pos[N][N];
ll arr[N], pre[N];
ll cost(int left, int right) {
    return pre[right] - pre[left - 1];
}
int n;
void build_cost() {
    for(int i = 1;i <= n;i++) {
        pre[i] = pre[i - 1] + arr[i];
    }
}
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    build_cost();
    
    for(int i = 1;i <= n;i++) {
        pos[i][i] = i;
        dp[i][i] = 0;
    }

    for(int l = 2;l <= n;l++) {
        for(int i = 1;i + l - 1 <= n;i++) {
            // compute dp[i][i + l - 1]
            int left = i, right = i + l - 1;
            pair<ll, int> best;
            best = make_pair(INF, -1);
            for(int p = pos[left][right - 1];p <= pos[left + 1][right];p++) {
                best = min(best, make_pair(dp[left][p] + dp[p + 1][right] + cost(left, right), p));
            }
            pos[left][right] = best.second;
            dp[left][right] = best.first;
        }
    }

    printf("%lld\n", dp[1][n]);
    return 0;
}