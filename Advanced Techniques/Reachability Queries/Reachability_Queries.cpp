#include <bits/stdc++.h>
using namespace std;
const int N = 50005;
vector<int> adj[N], RevAdj[N], ord, SCC_adj[N];
int SCC_id[N];
bitset<N> reachable[N], SCC_vis, vis;
int n, m, q;
void RevDFS(int x) {
	vis[x] = true;
	for(auto v : RevAdj[x]) {
		if(!vis[v]) {
			RevDFS(v);
		}
	}
	ord.push_back(x);
}
void DFS(int x, int id) {
	SCC_id[x] = id;
	for(auto v : adj[x]) {
		if(!SCC_id[v]) {
			DFS(v, id);
		}
	}
}
int scc_id;
void Kosaraju() {
	for(int i = 1; i <= n; i++) {
		if(!vis[i]) RevDFS(i);
	}

	for(int i = (int)ord.size() - 1; i >= 0; i--) {
		int x = ord[i];
		if(!SCC_id[x]) {
			DFS(x, ++scc_id);
		}
	}
}
void SCC_DFS(int x) {
	SCC_vis[x] = true;
	reachable[x][x] = true;
	for(auto v : SCC_adj[x]) {
		if(!SCC_vis[v]) {
			SCC_DFS(v);
		}
		reachable[x] |= reachable[v];
	}
}
void add_edge(int a, int b) {
	adj[a].push_back(b);
	RevAdj[b].push_back(a);
}
int main() {
    scanf("%d%d%d", &n, &m, &q);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
		add_edge(a, b);
	}

	Kosaraju();

	for(int i = 1; i <= n; i++) {
		for(auto v : adj[i]) {
			SCC_adj[SCC_id[i]].push_back(SCC_id[v]);
		}
	}

	for(int i = 1; i <= scc_id; i++) {
		if(!SCC_vis[i]) {
			SCC_DFS(i);
		}
	}

	while(q--) {
		int v, u;
		scanf("%d%d", &v, &u);
		printf(reachable[SCC_id[v]][SCC_id[u]] ? "YES\n" : "NO\n");
	}
}
