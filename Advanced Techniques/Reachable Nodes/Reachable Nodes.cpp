// #pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
// #pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
const int N = 50005;
bitset<N> reachable[N];
bool vis[N];
vector<int> adj[N];
void DFS(int x) {
    reachable[x][x] = 1;
    vis[x] = 1;
    for(auto v : adj[x]) {
        if(!vis[v])
            DFS(v);
        reachable[x] |= reachable[v];
    }
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
    }
    
    for(int i = 1;i <= n;i++) {
        if(!vis[i]) {
            DFS(i);
        }
        printf("%d ", (int)reachable[i].count());
    }
}
