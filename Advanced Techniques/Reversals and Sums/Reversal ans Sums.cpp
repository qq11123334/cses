#include <bits/stdc++.h>
#define ll long long int
using namespace std;
struct Treap {
    int size, pri, swap_tag;
    ll val, sum;
    Treap *lc, *rc;
    Treap(ll num) {
        size = 1;
        pri = rand();
        swap_tag = 0;
        val = num;
        sum = num;
        lc = rc = NULL;
    }
};
int size(Treap *node) {
    if(node == NULL) return 0;
    else return node->size;
}
ll sum(Treap *node) {
    if(node == NULL) return 0;
    else return node->sum;
}
void push(Treap *node) {
    assert(node != NULL);
    if(node != NULL) {
        if(node->swap_tag) {
            if(node->lc != NULL) node->lc->swap_tag ^= 1;
            if(node->rc != NULL) node->rc->swap_tag ^= 1;
            swap(node->lc, node->rc);
        }
        node->swap_tag = 0;
    }
}
void split(Treap *node, Treap *&left, Treap *&right, int k) {
    if(node == NULL) left = right = NULL;
    else {
        push(node);
        if(size(node->lc) < k) {
            split(node->rc, node->rc, right, k - size(node->lc) - 1);
            left = node;
        } else {
            split(node->lc, left, node->lc, k);
            right = node;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
        node->sum = sum(node->lc) + sum(node->rc) + node->val;
    }
}
void merge(Treap *&node, Treap *left, Treap *right) {
    if(left == NULL) node = right;
    else if(right == NULL) node = left;
    else {
        if(left->pri < right->pri) {
            push(left);
            merge(left->rc, left->rc, right);
            node = left;
        } else {
            push(right);
            merge(right->lc, left, right->lc);
            node = right;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
        node->sum = sum(node->lc) + sum(node->rc) + node->val;
    }
}
int main() {
    srand(time(0) * clock());
    int n, q;
    scanf("%d%d", &n, &q);
    Treap *root = NULL;
    for(int i = 0;i < n;i++) {
        ll num;
        scanf("%lld", &num);
        merge(root, root, new Treap(num));
    }
 
    while(q--) {
        int query, a, b;
        scanf("%d%d%d", &query, &a, &b);
        Treap *left, *mid, *right;
        if(query == 1) {
            split(root, left, right, b);
            split(left, left, mid, a - 1);
            
            mid->swap_tag ^= 1;
 
            merge(root, left, mid);
            merge(root, root, right);
        } else {    
            split(root, left, right, b);
            split(left, left, mid, a - 1);
            
            printf("%lld\n", mid->sum); 
            
            merge(root, left, mid);
            merge(root, root, right);
        }
    }
    return 0;
}