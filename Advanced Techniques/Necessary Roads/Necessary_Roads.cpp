#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
vector<int> adj[N];
vector<pair<int, int>> bridge;
int dfn[N], low[N];
void DFS(int x, int par = -1) {
    int static ord = 1;
    dfn[x] = low[x] = ord++;
    for(auto v : adj[x]) {
        if(!dfn[v]) {
            DFS(v, x);
            low[x] = min(low[x], low[v]);
            if(low[v] > dfn[x]) {
                bridge.push_back({x, v});
            }
        } else if(par != v) {
            low[x] = min(low[x], dfn[v]);
        }
    }
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++) {
        if(!dfn[i]) DFS(i);
    }

    printf("%d\n", (int)bridge.size());
    for(auto e : bridge) {
        printf("%d %d\n", e.first, e.second);
    }
    return 0;
}