#include <bits/stdc++.h>
#define ll long long int
using namespace std;
typedef complex<double> cd;
const double PI = acos(-1);
vector<cd> FFT(vector<cd> &F, bool inv) {
    int n = (int)F.size();
    vector<cd> res(n);
    for(int k = 0;k < n;k++) {
        int RevBit = 0;
        for(int i = 1;i < n;i <<= 1) {
            RevBit <<= 1;
            if(i & k) RevBit++;
        }
        res[RevBit] = F[k];
    }
    for(int Pow = 2;Pow <= n;Pow *= 2) {
        double theta = (inv ? -1 : 1) * 2.0 * PI / Pow;
        cd omega(cos(theta), sin(theta));
        for(int rnd = 0;rnd < n;rnd += Pow) {
            cd cur = 1;
            for(int i = 0;i < Pow / 2;i++) {
                cd a = res[rnd + i];
                cd b = res[rnd + Pow / 2 + i] * cur;
                res[rnd + i] = a + b;
                res[rnd + Pow / 2 + i] = a - b;
                cur *= omega;
            }
        }
    }
    if(inv) {
        for(int i = 0;i < n;i++) res[i] /= n;
    }
    return res;
}
vector<cd> Poly_multi(vector<cd> A, vector<cd> B) {
    int k = (1 << ((int)log2(A.size() + B.size()) + 1));
    A.resize(k);
    B.resize(k);

    A = FFT(A, 0);
    B = FFT(B, 0);

    vector<cd> C(k);
    for(int i = 0;i < k;i++) C[i] = A[i] * B[i];

    C = FFT(C, 1);
    return C;
}
int main() {
    // freopen("input.txt", "r", stdin);
    cin.tie(0), ios_base::sync_with_stdio(false);
    string s; cin >> s;
    int n = (int)s.size();
    vector<cd> A(n), B(n);
    for(int i = 0;i < n;i++) {
        A[i] = ((cd)(s[i] - '0'));
        B[i] = ((cd)(s[n - i - 1] - '0'));
    }

    vector<cd> C = Poly_multi(A, B);

    for(int i = n;i < 2 * n - 1;i++) {
        cout << (ll)round(C[i].real()) << " ";
    }
    return 0;
}