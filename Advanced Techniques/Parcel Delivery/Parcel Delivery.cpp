#include <bits/stdc++.h>
#define INF (0x3f3f3f3f)
#define ll long long int
using namespace std;
const int N = 505;
struct Edge {
    int to;
    ll cap, cost;
    int rev;
    Edge() {}
    Edge(int _to, ll _cap, ll _cost, int _rev) : to(_to), cap(_cap), cost(_cost), rev(_rev) {}
};
vector<Edge> adj[N];
void add_edge(int a, int b, ll cap, ll cost) {
    adj[a].push_back(Edge(b, cap, cost, (int)adj[b].size()));
    adj[b].push_back(Edge(a, 0, -cost, (int)adj[a].size() - 1));
}
ll k;
ll dis[N];
int par[N], par_id[N];
bool in_que[N];
pair<ll, ll> MCMF(int s, int t) {
    ll flow = 0, cost = 0;
    while(true) {
        memset(dis, INF, sizeof(dis));
        memset(in_que, 0, sizeof(in_que));
        dis[s] = 0;
        queue<int> que;
        que.push(s);
        while(!que.empty()) {
            int x = que.front(); 
            in_que[x] = 0; que.pop();
            for(int i = 0;i < (int)adj[x].size();i++) {
                Edge &e = adj[x][i];
                if(e.cap > 0 && dis[e.to] > dis[x] + e.cost) {
                    dis[e.to] = dis[x] + e.cost;
                    par[e.to] = x;
                    par_id[e.to] = i;
                    if(!in_que[e.to]) {
                        in_que[e.to] = 1;
                        que.push(e.to);
                    }
                }
            }
        }

        if(dis[t] >= INF) break;
        ll mi_flow = INF;
        for(int i = t;i != s;i = par[i]) {
            mi_flow = min(mi_flow, adj[par[i]][par_id[i]].cap);
        }

        if(flow + mi_flow > k) {
            mi_flow = k - flow;
        }

        flow += mi_flow, cost += mi_flow * dis[t];
        for(int i = t;i != s;i = par[i]) {
            Edge &e = adj[par[i]][par_id[i]];
            e.cap -= mi_flow;
            adj[e.to][e.rev].cap += mi_flow;
        }
        if(flow == k) break;
    }
    return make_pair(flow, cost);
}
int main() {
    int n, m;
    scanf("%d%d%lld", &n, &m, &k);
    for(int i = 0;i < m;i++) {
        int a, b;
        ll cap, cost;
        scanf("%d%d%lld%lld", &a, &b, &cap, &cost);
        add_edge(a, b, cap, cost);
    }

    pair<ll, ll> ans = MCMF(1, n);
    if(ans.first == k) printf("%lld\n", ans.second);
    else printf("-1\n");
}