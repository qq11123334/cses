#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
#define ll long long int
using namespace std;
bitset<3005> grid[3005];
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        for(int j = 0;j < n;j++) {
            char d;
            scanf(" %c", &d);
            grid[i][j] = d == '0' ? 0 : 1;
        }
    }

    ll ans = 0;
    for(int i = 0;i < n;i++) {
        for(int j = i + 1;j < n;j++) {
            ll itsec = (grid[i] & grid[j]).count();
            ans += (itsec * (itsec - 1)) / 2;
        }
    }
    printf("%lld\n", ans);
    return 0;
}