#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
bitset<30> b[20005];
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    int n, k;
    cin >> n >> k;
    for(int i = 0;i < n;i++) {
        string s; 
        cin >> s;
        for(int j = 0;j < k;j++)
            b[i][j] = s[j] == '1' ? 1 : 0;
    }

    int mi = 30;
    for(int i = 0;i < n;i++) {
        for(int j = i + 1;j < n;j++) {
            bitset<30> res;
            res = b[i] ^ b[j];
            mi = min((int)res.count(), mi);
        }
    }
    cout << mi << endl;
    return 0;
}