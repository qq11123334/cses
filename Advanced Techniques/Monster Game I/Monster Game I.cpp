#include <bits/stdc++.h>
#define ll long long int
#define x first
#define y second
#define EPS (1e-6)
using namespace std;
typedef pair<double, double> Point;
const int N = 200005;
struct Line {
    double slope, c;
    double get_val(double x) {
        return slope * x + c;
    }
    Line(double _slope, double _c) : slope(_slope), c(_c) {}
};
vector<Line> Line_vec;
double dp[N];
ll  S[N], F[N];
Point intersect(Line a, Line b) {
    /* y = m1x + c1
     * y = m2x + c2
     * 
     * 0 = (m1 - m2)x + (c1 - c2)
     * x = (c2 - c1) / (m1 - m2)
     * 
     * m2y = m1m2x + m2c1
     * m1y = m1m2x + m1c2
     * (m2 - m1)y = (m2c1 - m1c2)
     * y = (m2c1 - m1c2) / (m2 - m1)
     * */
    // printf("%f %f\n", a.slope, b.slope);
    assert(a.slope != b.slope);
    double x = (b.c - a.c) / (a.slope - b.slope);
    double y = (b.slope * a.c - a.slope * b.c) / (b.slope - a.slope);
    return make_pair(x, y);
}
void Insert_Line(Line L) {
    while(Line_vec.size() >= 2LL) {
        int sz = (int)Line_vec.size();
        if(fabs(Line_vec[sz - 1].slope - L.slope) < EPS) {
            if(Line_vec[sz - 1].c > L.c) {
                Line_vec.pop_back();
                continue;   
            } else {
                break;
            }
        }
        Point p1 = intersect(Line_vec[sz - 1], L);
        Point p2 = intersect(Line_vec[sz - 2], Line_vec[sz - 1]);
        if((p1.x < p2.x || (fabs(p1.x - p2.x) < EPS && p1.y < p2.y))) {
            Line_vec.pop_back();
            continue;
        }
        break;
    }
    if(fabs(Line_vec.back().slope - L.slope) > EPS) Line_vec.push_back(L);
}
int main() {
    int n;
    ll x;
    scanf("%d%lld", &n, &x);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &S[i]);
    }
    for(int i = 0;i < n;i++) {
        scanf("%lld", &F[i]);
    }
    Line_vec.push_back(Line(x, 0));
    for(int i = 0;i < n;i++) {
        int l = -1, r = (int)Line_vec.size() - 1;
        while(l < r - 1) {
            int m = (l + r) / 2;
            double cur_x = intersect(Line_vec[m], Line_vec[m + 1]).x;
            if(cur_x < S[i]) {
                l = m;
            } else {
                r = m;
            }
        }
        dp[i] = Line_vec[r].get_val(S[i]);
        Insert_Line(Line(F[i], dp[i]));
    }

    printf("%lld\n", (ll)round(dp[n - 1]));
}
/*
2000
3000
3000
4000
4800
*/