#include <bits/stdc++.h>
#define ll long long int
using namespace std;
typedef complex<double> cd;
const double PI = acos(-1);
vector<cd> FFT(vector<cd> &F, bool inv) {
    int n = (int)F.size();
    vector<cd> res(n);
    for(int k = 0;k < n;k++) {
        int RevBit = 0;
        for(int i = 1;i < n;i <<= 1) {
            RevBit <<= 1;
            if(k & i) RevBit++;
        }
        res[RevBit] = F[k];
    }

    for(int Pow = 2;Pow <= n;Pow *= 2) {
        double theta = (inv ? -1 : 1) * 2.0 * PI / Pow;
        cd omega(cos(theta), sin(theta));
        for(int rnd = 0;rnd < n; rnd += Pow) {
            cd cur = 1;
            for(int i = 0;i < Pow / 2;i++) {
                cd a = res[rnd + i];
                cd b = cur * res[rnd + i + Pow / 2];
                res[rnd + i] = a + b;
                res[rnd + i + Pow / 2] = a - b;
                cur *= omega;
            }
        }
    }

    if(inv) {
        for(int i = 0;i < n;i++) res[i] /= n;
    }
    return res;
}
vector<cd> Poly_multi(vector<cd> A, vector<cd> B) {
    int k = (1 << ((int)log2((int)A.size() + (int)B.size()) + 1));
    A.resize(k);
    B.resize(k);

    A = FFT(A, 0);
    B = FFT(B, 0);
    
    vector<cd> C(k);
    for(int i = 0;i < k;i++) C[i] = A[i] * B[i];

    C = FFT(C, 1);
    return C;
}
int arr_A[200005];
int arr_B[200005];
int main() {
    // freopen("input.txt", "r", stdin);
    int k, n, m;
    scanf("%d%d%d", &k, &n, &m);
    int tmp_k = k;
    k++;

    vector<cd> A(k), B(k);

    for(int i = 0;i < n;i++) {
        int apple;
        scanf("%d", &apple);
        arr_A[apple]++;
    }
    for(int i = 0;i < m;i++) {
        int banana;
        scanf("%d", &banana);
        arr_B[banana]++;
    }

    for(int i = 0;i < k;i++) {
        A[i] = arr_A[i];
        B[i] = arr_B[i];
    }

    vector<cd> C = Poly_multi(A, B);

    for(int i = 2;i <= 2 * tmp_k;i++) {
        printf("%lld ", (ll)round(C[i].real()));
    }
    printf("\n");
    return 0;
}