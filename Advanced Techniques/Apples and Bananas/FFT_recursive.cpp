#include <bits/stdc++.h>
using namespace std;
typedef complex<double> cd;
const double PI = acos(-1);
vector<cd> FFT(vector<cd> &F, bool inv) {
    int n = F.size();
    if(n == 1) return F;
    vector<cd> rec[2];
    for(int i = 0;i < n;i++) {
        rec[i & 1].push_back(F[i]);
    }
    
    rec[0] = FFT(rec[0], inv);
    rec[1] = FFT(rec[1], inv);
    double theta = (inv ? -1 : 1) * 2 * PI / n;
    cd cur = 1, omega(cos(theta), sin(theta));
    vector<cd> ans(n);
    for(int i = 0;i < n / 2;i++) {
        ans[i] = rec[0][i] + cur * rec[1][i];
        ans[i + n / 2] = rec[0][i] - cur * rec[1][i];
        cur *= omega;
    }
    if(inv) for(int i = 0;i < n;i++) ans[i] /= 2;
    return ans;
}
int arr_A[200005];
int arr_B[200005];
int main() {
    // freopen("input.txt", "r", stdin);
    int k, n, m;
    scanf("%d%d%d", &k, &n, &m);
    int tmp_k = k;
    k++;

    vector<cd> A(k); 
    vector<cd> B(k);

    for(int i = 0;i < n;i++) {
        int apple;
        scanf("%d", &apple);
        arr_A[apple]++;
    }
    for(int i = 0;i < m;i++) {
        int banana;
        scanf("%d", &banana);
        arr_B[banana]++;
    } 

    for(int i = 0;i < k;i++) {
        A[i] = arr_A[i];
        B[i] = arr_B[i];
    }
    k = 1 << (__lg((int)A.size() + (int)B.size()) + 1);
    A.resize(k);
    B.resize(k);
    A = FFT(A, 0);
    B = FFT(B, 0);

    vector<cd> C(k);
    for(int i = 0;i < k;i++) {
        C[i] = A[i] * B[i];
    }

    C = FFT(C, 1);

    for(int i = 2;i <= 2 * tmp_k;i++) {
        printf("%d ", (int)(C[i].real() + 1e-6));
    }
    return 0;
}