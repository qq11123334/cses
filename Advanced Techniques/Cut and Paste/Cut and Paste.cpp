#include <bits/stdc++.h>
using namespace std;
struct Node {
    int pri, size;
    char val;
    Node *lc, *rc;
    Node(char c) {
        pri = rand();
        size = 1;
        val = c;
        lc = rc = NULL;
    }
};
int size(Node *Treap) {
    if(Treap == NULL) return 0;
    else return Treap->size;
}
void split(Node *Treap, Node *&left, Node *&right, int k) {
    if(Treap == NULL) left = right = NULL;
    else {
        if(size(Treap->lc) < k) {
            split(Treap->rc, Treap->rc, right, k - size(Treap->lc) - 1);
            left = Treap;
        } else {
            split(Treap->lc, left, Treap->lc, k);
            right = Treap;
        }
        Treap->size = size(Treap->lc) + size(Treap->rc) + 1;
    }
}
void merge(Node *&Treap, Node *left, Node *right) {
    if(left == NULL) Treap = right;
    else if(right == NULL) Treap = left;
    else {
        if(left->pri < right->pri) {
            merge(left->rc, left->rc, right);
            Treap = left;
        } else {
            merge(right->lc, left, right->lc);
            Treap = right;
        }
        Treap->size = size(Treap->lc) + size(Treap->rc) + 1;
    }
}
void DFS_print(Node *Treap) {
    if(Treap == NULL) return;
    DFS_print(Treap->lc);
    printf("%c", Treap->val);
    DFS_print(Treap->rc);
}
void print_Treap(Node *Treap) {
    DFS_print(Treap);
    printf("\n");
}
int main() {
    srand(time(0) * clock());
    Node *Treap = NULL;
    int n, q;
    scanf("%d%d", &n, &q);
    
    for(int i = 0;i < n;i++) {
        char c; scanf(" %c", &c);
        merge(Treap, Treap, new Node(c));
    }
    
    while(q--) {
        Node *left, *right, *mid;
        int a, b; scanf("%d%d", &a, &b);
        split(Treap, mid, right, b);
        split(mid, left, mid, a - 1);
        merge(Treap, left, right);
        merge(Treap, Treap, mid);
    }
    print_Treap(Treap);
    
    return 0;
}