#include <bits/stdc++.h>
using namespace std;
struct Player {
    int deg;
    int id;
    bool operator < (const Player A) const {
        return deg < A.deg;
    }
};
void unable() {
    printf("IMPOSSIBLE\n");
    exit(0);
}
priority_queue<Player> pq;
vector<Player> tmp;
vector<pair<int, int>> game;
int main() {
    int n; scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        Player player;
        scanf("%d", &player.deg);
        player.id = i + 1;
        pq.push(player);
    }

    while(!pq.empty()) {
        Player top = pq.top(); pq.pop();
        while(top.deg && !pq.empty()) {
            Player oppe = pq.top(); pq.pop();
            game.push_back(make_pair(top.id, oppe.id));
            oppe.deg--;
            top.deg--;
            tmp.push_back(oppe);
        }

        for(auto player : tmp) {
            pq.push(player);
        }
        tmp.clear();

        if(top.deg) {
            unable();
        }
    }

    printf("%d\n", (int)game.size());
    for(auto P : game) {
        printf("%d %d\n", P.first, P.second);
    }
    return 0;
}
/*
10
9 7 6 5 5 4 4 4 3 1
0 6 5 4 4 3 3 3 2 0
*/