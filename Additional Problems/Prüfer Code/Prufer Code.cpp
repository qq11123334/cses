#include <bits/stdc++.h>
using namespace std;
int cnt[200005];
int pru_seq[200005];
set<int> leaf;
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        leaf.insert(i);
    }
    for(int i = 0;i < n - 2;i++) {
        scanf("%d", &pru_seq[i]);
        leaf.erase(pru_seq[i]);
        cnt[pru_seq[i]]++;
    }

    for(int i = 0;i < n - 2;i++) {
        auto mi = leaf.begin();
        printf("%d %d\n", pru_seq[i], *mi);
        leaf.erase(mi);
        cnt[pru_seq[i]]--;
        if(cnt[pru_seq[i]] == 0) {
            leaf.insert(pru_seq[i]);
        }
    }

    printf("%d %d\n", *leaf.begin(), *next(leaf.begin()));
    return 0;
}