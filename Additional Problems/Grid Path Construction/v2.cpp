#include <bits/stdc++.h>

using namespace std;
using Pos = pair<int, int>;
#define x first
#define y second

pair<bool, string> check_solve(int, int, Pos, Pos);
bool is_valid(int n, int m, Pos p) {
    return (1 <= p.x && p.x <= n) && (1 <= p.y && p.y <= m);
}

void path(string dir, Pos s, vector<Pos> &res) {
    res.clear();
    res.push_back(s);
    for(auto c : dir) {
        if(c == 'U') s.x--;
        else if(c == 'D') s.x++;
        else if(c == 'L') s.y--;
        else if(c == 'R') s.y++;
        res.push_back(s);
    }
}
map<pair<pair<int, int>, pair<Pos, Pos>>, string> mp;
const int UP = 2500;

string store(int n, int m, Pos s, Pos t, string ans) {
    if(n * m <= UP) mp[{{n, m}, {s, t}}] = ans;
    return ans;
}

string read(int n, int m, Pos s, Pos t) {
    if(n * m > UP) return "";
    return mp[{{n, m}, {s, t}}];
}

bool same_color(Pos s,  Pos t) {
    return ((s.x + s.y + t.x + t.y) & 1) == 0;
}

int check(int n, int m, Pos s, Pos t) {
    if(n > m) {
        swap(n, m);
        swap(s.x, s.y);
        swap(t.x, t.y);
    }

    if(s.y > t.y) {
         swap(s, t);
    }
    
    if(n * m > 1 && s == t) return 0;
    if(((n & 1) && (m & 1)) && (!same_color(s, t) || (((s.x + s.y) & 1) == 1))) return 0;
    if((!(n & 1) || !(m & 1)) && (same_color(s, t))) return 0;
    if(n == 1 && (s.y != 1 || t.y != m)) return 0;
    if(n == 2) {
        if(s.x != t.x && t.y - s.y == 1) return 0;
        if(s.y == t.y && (s.y != 1 && s.y != m)) return 0;
    }
    if(n == 3 && (m & 1) == 0 && !same_color(s, {1, 1}) && (s.y < t.y - 1 || (s.x == 2 && s.y < t.y)))
        return 0;
    return 1;
}
string solve(int n, int m, Pos s, Pos t) {
    if(!check(n, m, s, t)) return "";
    if(n == 1 && m == 1) return "";
    if(n * m <= UP && read(n, m, s, t).size()) {
        return read(n, m, s, t);
    }
    
    for(int i = min(s.x, t.x); i < max(s.x, t.x); i++) {
        for(int j = 1; j <= m; j++) {
            string res, res1, res2;
            if(s.x < t.x) {
                if(!check(i, m, s, {i, j}) || !check(n - i, m, {1, j}, {t.x - i, t.y})) continue;
                res1 = solve(i, m, s, {i, j});
                res2 = solve(n - i, m, {1, j}, {t.x - i, t.y});
            }
            if(s.x > t.x) {
                if(!check(n - i, m, {s.x - i, s.y}, {1, j}) || !check(i, m, {i, j}, t)) continue;
                res1 = solve(n - i, m, {s.x - i, s.y}, {1, j});
                res2 = solve(i, m, {i, j}, t); 
            }
            if(s.x < t.x) res = res1 + "D" + res2;
            if(s.x > t.x) res = res1 + "U" + res2;
            return store(n, m, s, t, res);
        }
    }
    for(int i = 1; i <= n; i++) {
        for(int j = min(s.y, t.y); j < max(s.y, t.y); j++) {
            string res, res1, res2;
            if(s.y < t.y) {
                if(!check(n, j, s, {i, j}) || !check(n, m - j, {i, 1}, {t.x, t.y - j})) continue;
                res1 = solve(n, j, s, {i, j});
                res2 = solve(n, m - j, {i, 1}, {t.x, t.y - j});
            }
            if(s.y > t.y) {
                if(!check(n, m - j, {s.x, s.y - j}, {i, 1}) || !check(n, j, {i, j}, t)) continue;
                res1 = solve(n, m - j, {s.x, s.y - j}, {i, 1});
                res2 = solve(n, j, {i, j}, t);
            }

            if(s.y < t.y) res = res1 + "R" + res2;
            if(s.y > t.y) res = res1 + "L" + res2;
            return store(n, m, s, t, res);
        }
    }

    vector<Pos> tmp_path;
    for(int i = 1; i < min(s.x, t.x); i++) {
        auto ret = check_solve(n - i, m, {s.x - i, s.y}, {t.x - i, t.y});
        if(!ret.first) continue;
        string res, tmp = ret.second;
        path(tmp, {s.x - i, s.y}, tmp_path);
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            if(tmp_path[k - 1].x == 1 && tmp_path[k].x == 1) {
                auto ret1 = check_solve(i, m, {i, tmp_path[k - 1].y}, {i, tmp_path[k].y});
                if(!ret1.first) continue;
                res = tmp.substr(0, k - 1) + "U" + ret1.second + "D" + tmp.substr(k);
                return store(n, m, s, t, res);
            }
        }
    }
    
    for(int i = max(s.x, t.x); i < n; i++) {
        auto ret = check_solve(i, m, s, t);
        if(!ret.first) continue;
        string tmp = ret.second, res;
        path(tmp, s, tmp_path);
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            if(tmp_path[k - 1].x == i && tmp_path[k].x == i) {
                auto ret1 = check_solve(n - i, m, {1, tmp_path[k - 1].y}, {1, tmp_path[k].y});
                if(!ret1.first) continue;
                res = tmp.substr(0, k - 1) + "D" + ret1.second + "U" + tmp.substr(k);
                return store(n, m, s, t, res);
            }
        }
    }

    for(int j = 1; j < min(s.y, t.y); j++) {
        auto ret = check_solve(n, m - j, {s.x, s.y - j}, {t.x, t.y - j});
        if(!ret.first) continue;
        string tmp = ret.second, res;
        path(tmp, {s.x, s.y - j}, tmp_path);

        for(int k = 1; k < (int)tmp_path.size(); k++) {
            if(tmp_path[k - 1].y == 1 && tmp_path[k].y == 1) {
                cerr << n << " " << m - j << " " << s.x << " " << s.y - j << " " << tmp << endl;;
                cerr << tmp_path[k - 1].x << endl;
                auto ret1 = check_solve(n, j, {tmp_path[k - 1].x, j}, {tmp_path[k].x, j});
                if(!ret1.first) continue;
                res = tmp.substr(0, k - 1) + "L" + ret1.second + "R" + tmp.substr(k);
                return store(n, m, s, t, res);
            }
        }
    }
    
    for(int j = max(s.y, t.y); j < m; j++) {
        auto ret = check_solve(n, j, s, t);
        if(!ret.first) continue;
        string tmp = ret.second, res;
        path(tmp, s, tmp_path);
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            if(tmp_path[k - 1].y == j && tmp_path[k].y == j) {
                auto ret1 = check_solve(n, m - j, {tmp_path[k - 1].x, 1}, {tmp_path[k].x, 1});
                if(!ret1.first) continue;
                res = tmp.substr(0, k - 1) + "R" + ret1.second + "L" + tmp.substr(k);
                return store(n, m, s, t, res);
            }
        }
    }

    assert(false);
}

pair<bool, string> check_solve(int n, int m, Pos s, Pos t) {
    bool res = check(n, m, s, t);
    if(!res) return {false, ""};
    return {true, solve(n, m, s, t)};
}

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n, m, x1, y1, x2, y2;
        cin >> n >> m >> x1 >> y1 >> x2 >> y2;
        auto ret = check_solve(n, m, {x1, y1}, {x2, y2});
        if(ret.first) cout << "YES\n" << ret.second << "\n";
        else cout << "NO\n";
    }
}
