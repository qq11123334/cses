#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")

#include <bits/stdc++.h>

using namespace std;
using Pos = pair<int, int>;
#define x first
#define y second

inline bool is_valid(const int n, const int m, const Pos p) {
    return (1 <= p.x && p.x <= n) && (1 <= p.y && p.y <= m);
}

void path(string &dir, Pos s, vector<Pos> &res) {
    res.clear();
    res.push_back(s);
    for(auto c : dir) {
        if(c == 'U') s.x--;
        else if(c == 'D') s.x++;
        else if(c == 'L') s.y--;
        else if(c == 'R') s.y++;
        res.push_back(s);
    }
}
map<pair<pair<int, int>, pair<Pos, Pos>>, string> mp;
const int UP = 500;

inline void store(int &n, int &m, Pos &s, Pos &t, string &ans) {
    if(n * m <= UP) mp[{{n, m}, {s, t}}] = ans;
}

inline string read(int &n, int &m, Pos &s, Pos &t) {
    if(n * m > UP) return "";
    return mp[{{n, m}, {s, t}}];
}

bool same_color(const Pos s, const Pos t) {
    return ((s.x + s.y + t.x + t.y) & 1) == 0;
}

int check(int n, int m, Pos s, Pos t) {
    if(n > m) {
        swap(n, m);
        swap(s.x, s.y);
        swap(t.x, t.y);
    }

    if(s.y > t.y) {
         swap(s, t);
    }
    
    if(n * m > 1 && s == t) return 0;
    if(((n & 1) && (m & 1)) && (!same_color(s, t) || (((s.x + s.y) & 1) == 1))) return 0;
    if((!(n & 1) || !(m & 1)) && (same_color(s, t))) return 0;

    if(n == 1) {
        if(s.y != 1 && s.y != m) return 0;
        if(t.y != 1 && t.y != m) return 0;
        return 1;
    }
    if(n == 2) {
        if(s.x != t.x && abs(s.y - t.y) == 1) return 0;
        if(s.y == t.y && (s.y != 1 && s.y != m)) return 0;
        return 1;
    }
    if(n == 3) {
        if((m & 1) == 0 && !same_color(s, {1, 1}) && (s.y < t.y - 1 || (s.x == 2 && s.y < t.y)))
            return 0;
    }
    return 1;
}
string solve(int n, int m, Pos s, Pos t) {
    if(!check(n, m, s, t)) return "-";

    if(n == 1 && m == 1) return "";
    if(n * m <= UP && read(n, m, s, t).size()) {
        return read(n, m, s, t);
    }

    for(int i = min(s.x, t.x); i < max(s.x, t.x); i++) {
        for(int j = 1; j <= m; j++) {
            string res, res1, res2;

            if(s.x < t.x) {
                if(!check(n - i, m, {1, j}, {t.x - i, t.y})) continue;
                res1 = solve(i, m, s, {i, j});
                if(res1[0] == '-') continue;
                res2 = solve(n - i, m, {1, j}, {t.x - i, t.y});
                if(res2[0] == '-') continue;
            }
            if(s.x > t.x) {
                if(!check(i, m, {i, j}, t)) continue;
                res1 = solve(n - i, m, {s.x - i, s.y}, {1, j});
                if(res1[0] == '-') continue;
                res2 = solve(i, m, {i, j}, t); 
                if(res2[0] == '-') continue;
            }

            if(s.x < t.x) res = res1 + "D" + res2;
            if(s.x > t.x) res = res1 + "U" + res2;

            if((int)res.length() == n * m - 1) {
                store(n, m, s, t, res);
                return res;
            }
        }
    }
    for(int i = 1; i <= n; i++) {
        for(int j = min(s.y, t.y); j < max(s.y, t.y); j++) {
            string res, res1, res2;
            if(s.y < t.y) {
                if(!check(n, m - j, {i, 1}, {t.x, t.y - j})) continue;
                res1 = solve(n, j, s, {i, j});
                if(res1[0] == '-') continue;
                res2 = solve(n, m - j, {i, 1}, {t.x, t.y - j});
                if(res2[0] == '-') continue;
            }
            if(s.y > t.y) {
                if(!check(n, j, {i, j}, t)) continue;
                res1 = solve(n, m - j, {s.x, s.y - j}, {i, 1});
                if(res1[0] == '-') continue;
                res2 = solve(n, j, {i, j}, t);
                if(res2[0] == '-') continue;
            }

            if(s.y < t.y) res = res1 + "R" + res2;
            if(s.y > t.y) res = res1 + "L" + res2;
            if((int)res.size() == n * m - 1) {
                store(n, m, s, t, res);
                return res;
            }
        }
    }

    vector<Pos> tmp_path;
    for(int i = 1; i < min(s.x, t.x); i++) {
        string tmp = solve(n - i, m, {s.x - i, s.y}, {t.x - i, t.y});
        if(tmp[0] == '-') continue;
        string res;
        path(tmp, {s.x - i, s.y}, tmp_path);
        bool flag = false;
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            bool flag2 = false;
            if(!flag && tmp_path[k - 1].x == 1 && tmp_path[k].x == 1) {
                string mid = solve(i, m, {i, tmp_path[k - 1].y}, {i, tmp_path[k].y});
                if(mid[0] == '-') continue;
                if((int)mid.size() + (int)tmp.size() == (n * m - 2)) {
                    res += "U" + mid + "D";
                    flag = true;
                    flag2 = true;
                }
            }

            if(!flag2) res += tmp[k - 1];
        }

        if(flag) {
            store(n, m, s, t, res);
            return res;
        }
    }
    
    for(int i = max(s.x, t.x); i < n; i++) {
        string tmp = solve(i, m, s, t);
        if(tmp[0] == '-') continue;
        string res;
        path(tmp, s, tmp_path);

        bool flag = false;
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            bool flag2 = false;
            if(!flag && tmp_path[k - 1].x == i && tmp_path[k].x == i) {
                string mid = solve(n - i, m, {1, tmp_path[k - 1].y}, {1, tmp_path[k].y});
                if(mid[0] == '-') continue;
                if((int)mid.size() + (int)tmp.size() == (n * m - 2)) {
                    res += "D" + mid + "U";
                    flag = true;
                    flag2 = true;
                }
            }

            if(!flag2) res += tmp[k - 1];
        }

        if(flag) {
            store(n, m, s, t, res);
            return res;
        }
    }

    for(int j = 1; j < min(s.y, t.y); j++) {
        string tmp = solve(n, m - j, {s.x, s.y - j}, {t.x, t.y - j});
        if(tmp[0] == '-') continue;
        string res;
        path(tmp, {s.x, s.y - j}, tmp_path);

        bool flag = false;
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            bool flag2 = false;
            if(!flag && tmp_path[k - 1].y == 1 && tmp_path[k].y == 1) {
                string mid = solve(n, j, {tmp_path[k - 1].x, j}, {tmp_path[k].x, j});
                if(mid[0] == '-') continue;
                if((int)mid.size() + (int)tmp.size() == (n * m - 2)) {
                    res += "L" + mid + "R";
                    flag = true;
                    flag2 = true;
                }
            }

            if(!flag2) res += tmp[k - 1];
        }

        if(flag) {
            store(n, m, s, t, res);
            return res;
        }
    }
    
    for(int j = max(s.y, t.y); j < m; j++) {
        string tmp = solve(n, j, s, t);
        if(tmp[0] == '-') continue;
        string res;
        path(tmp, s, tmp_path);
        
        bool flag = false;
        for(int k = 1; k < (int)tmp_path.size(); k++) {
            bool flag2 = false;
            if(!flag && tmp_path[k - 1].y == j && tmp_path[k].y == j) {
                string mid = solve(n, m - j, {tmp_path[k - 1].x, 1}, {tmp_path[k].x, 1});
                if(mid[0] == '-') continue;
                if((int)mid.size() + (int)tmp.size() == (n * m - 2)) {
                    res += "R" + mid + "L";
                    flag = true;
                    flag2 = true;
                }
            }
            
            if(!flag2) res += tmp[k - 1];
        }

        if(flag) {
            store(n, m, s, t, res);
            return res;
        }
    }
    string res = "-"; 
    store(n, m, s, t, res);
    assert(false);
    return res;
}

int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;
    while(t--) {
        int n, m, x1, y1, x2, y2;
        cin >> n >> m >> x1 >> y1 >> x2 >> y2;
        string ans = solve(n, m, {x1, y1}, {x2, y2});
        if((int)ans.size() == n * m - 1) cout << "YES\n" << ans << "\n";
        else cout << "NO\n";
    }
}
