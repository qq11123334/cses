#include <bits/stdc++.h>
#define ll unsigned long long int
using namespace std;
vector<ll> itov(ll x) {
    vector<ll> res;
    while(x) {
        res.push_back(x % 10);
        x /= 10;
    }
    reverse(res.begin(), res.end());
    return res;
}
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x;
        y >>= 1;
        x = x * x;
    }
    return res;
}
ll need_one(ll x) {
    vector<ll> v = itov(x);
    int n = (int)v.size();
    ll pre_v[105], suf_v[105];
    memset(pre_v, 0, sizeof(pre_v));
    memset(suf_v, 0, sizeof(suf_v));
    pre_v[0] = v[0];
    pre_v[n] = 0;
    for(int i = 1;i < n;i++) {
        pre_v[i] = 10 * pre_v[i - 1] + v[i];
    }

    suf_v[n] = 0;
    for(int i = n - 1;i >= 0;i--) {
        suf_v[i] = suf_v[i + 1] + fp(10, n - i - 1) * v[i];
    }

    ll cur_pow10 = 1;
    ll ans = 0;
    for(int i = n - 1;i >= 0;i--) {
        if(i) {
            if(v[i] > 1) {
                ans += ((pre_v[i - 1] + 1) * cur_pow10);
            } else if(v[i] == 1) {
                ans += ((pre_v[i - 1]) * cur_pow10);
                ans += (suf_v[i + 1] + 1);
            } else if(v[i] == 0) {
                ans += ((pre_v[i - 1]) * cur_pow10);
            }
        } else {
            if(v[i] == 1)
                ans += (suf_v[i + 1] + 1);
            else {
                ans += cur_pow10;
            }
        }
        cur_pow10 *= 10;
    }
    return ans;
}
int main() {
    ll n; cin >> n;
    ll l = 1, r = 1e18;
    while(l < r - 1) {
        ll m = (r + l) / 2;
        if(need_one(m) > n) r = m;
        else l = m;
    }
    cout << l << endl;
    return 0;
}