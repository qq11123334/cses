#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
const int N = 105;
struct Edge {
    int to, cap, cost, rev;
    bool isReal;
    Edge() {}
    Edge(int _to, int _cap, int _cost, int _rev, bool _isReal) : to(_to), cap(_cap), cost(_cost), rev(_rev), isReal(_isReal) {}
};
vector<Edge> adj[N];
int dis[N], par[N], par_id[N];
bool in_que[N];
pair<int, int> MCMF(int s, int t) {
    int flow = 0, cost = 0;
    while(true) {
        for(int i = 0;i < N;i++) {
            dis[i] = -INF;
        }
        queue<int> que;
        que.push(s);
        dis[s] = 0;
        while(!que.empty()) {
            int x = que.front();
            que.pop(); in_que[x] = 0;
            for(int i = 0;i < (int)adj[x].size();i++) {
                Edge &e = adj[x][i];
                if(e.cap > 0 && dis[e.to] < dis[x] + e.cost) {
                    dis[e.to] = dis[x] + e.cost;
                    par[e.to] = x;
                    par_id[e.to] = i;
                    if(!in_que[e.to]) {
                        in_que[e.to] = 1;
                        que.push(e.to);
                    }
                }
            }
        }

        if(dis[t] == -INF) break;

        int mi_flow = INF;
        for(int i = t;i != s;i = par[i]) {
            mi_flow = min(mi_flow, adj[par[i]][par_id[i]].cap);
        }

        flow += mi_flow, cost += mi_flow * dis[t];

        for(int i = t;i != s;i = par[i]) {
            Edge &e = adj[par[i]][par_id[i]];
            e.cap -= mi_flow;
            adj[e.to][e.rev].cap += mi_flow;
        }
    }
    return make_pair(flow, cost);
}
void add_edge(int a, int b, int cap, int cost) {
    adj[a].push_back(Edge(b, cap, cost, (int)adj[b].size(), 1));
    adj[b].push_back(Edge(a, 0, -cost, (int)adj[a].size() - 1, 0));
}
int row[N], col[N];
#define ROW 1
#define COL 2
int n;
int number(int num, int type) {
    if(type == ROW) return num;
    if(type == COL) return num + n;
}
int main() {
    scanf("%d", &n);
    int row_sum = 0, col_sum = 0;
    for(int i = 1;i <= n;i++) {
        scanf("%d", &row[i]);
        row_sum += row[i];
    }

    for(int i = 1;i <= n;i++) {
        scanf("%d", &col[i]);
        col_sum += col[i];
    }

    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            int cost; scanf("%d", &cost);
            add_edge(number(i, ROW), number(j, COL), 1, cost);
        }
    }

    int s = 0, t = 2 * n + 1;
    for(int i = 1;i <= n;i++) {
        add_edge(s, number(i, ROW), row[i], 0);
    }

    for(int i = 1;i <= n;i++) {
        add_edge(number(i, COL), t, col[i], 0);
    }

    auto P = MCMF(s, t);
    if(P.first != row_sum || P.first != col_sum) {
        printf("-1\n");
        exit(0);
    }

    
    printf("%d\n", P.second);
    char ans[N][N];
    for(int i = 1;i <= n;i++) {
        for(Edge &e : adj[number(i, ROW)]) {
            if(e.isReal) {
                int _row = i;
                int _col = e.to - n;
                if(e.cap == 0) {
                    ans[_row][_col] = 'X';
                } else {
                    ans[_row][_col] = '.';
                }
            }
        }
    }

    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            printf("%c", ans[i][j]);
        }
        printf("\n");
    }
}