#include <bits/stdc++.h>
using ll = long long;
using namespace std;
const ll MOD = (ll)1e18 + 7;
ll sum, cnt[26];
void mod(ll &x) {
	x = ((x % MOD) + MOD) % MOD;
}
int main() {
	string s;
	cin >> s;
	sum = 1;
	for(auto c : s) {
		ll plus = (sum - cnt[c - 'a']);
		mod(plus);
		cnt[c - 'a'] += plus;
		sum += plus;
		mod(cnt[c - 'a']);
		mod(sum);
	}
	ll ans = 0;
	for(int i = 0; i < 26; i++) {
		ans += cnt[i];
		mod(ans);
	}
	cout << ans << "\n";
}
