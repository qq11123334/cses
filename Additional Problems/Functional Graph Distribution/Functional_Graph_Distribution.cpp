#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 5005;
int n;
ll S[N][N], C[N][N], n_power[N];
const ll MOD = (ll)1e9 + 7;
ll fp(ll x, ll y) {
	ll res = 1;
	while(y) {
		if(y & 1) res = res * x % MOD;
		y >>= 1;
		x = x * x % MOD;
	}
	return res;
}
void build_Combination() {
	for(int i = 1; i < N; i++) {
		C[i][0] = 1, C[i][i] = 1;
		// C[i][1 ~ N - 1]
		for(int j = 1; j < i; j++) {
			C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % MOD;
		}
	}
}
void build_stirling() {
	S[1][1] = 1;
	for(int i = 2; i < N; i++) {
		for(int j = 1; j <= i; j++) {
			S[i][j] = (S[i - 1][j - 1] + S[i - 1][j] * (i - 1)) % MOD;
		}
	}
}
void build_power() {
	n_power[0] = 1;
	for(int i = 1; i < N; i++) {
		n_power[i] = n_power[i - 1] * n % MOD;
	}
}
ll T(int n, int k) {
	ll res = 0;
	ll inv_n = fp(n, MOD - 2);
	for(int j = k; j <= n; j++) {
		res += ((C[n][j] * j % MOD * inv_n % MOD) * n_power[n - j] % MOD * S[j][k]);
		res %= MOD;
	}
	return (res % MOD + MOD) % MOD;
}
int main() {
	cin >> n;
	build_power();
	build_stirling();
	build_Combination();
	for(int k = 1; k <= n; k++) {
		cout << T(n, k) << "\n";
	}
	return 0;
}
