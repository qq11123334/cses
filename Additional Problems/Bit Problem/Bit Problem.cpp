#include <bits/stdc++.h>
using namespace std;
const int N = (1 << 20) + 5, K = 20;
int arr[N];
int dp[N][3];
int rev(int x) {
	return x ^ ((1 << K) - 1);
}
int main() {
	int n;
	scanf("%d", &n);
	for(int i = 0; i < n; i++) {
		scanf("%d", &arr[i]);
	}

	for(int i = 0; i < n; i++) {
		dp[arr[i]][0]++;
		dp[rev(arr[i])][1]++;
		dp[arr[i]][2]++;
	}

	for(int k = 0;k < K; k++) {
		for(int s = 0; s < (1 << K); s++) {
			if(s & (1 << k)) {
				dp[s][0] += dp[s ^ (1 << k)][0];
				dp[s][1] += dp[s ^ (1 << k)][1];
				dp[s][2] += dp[s ^ (1 << k)][2];
			}
		}
	}

	for(int i = 0; i < n; i++) {
		printf("%d %d %d\n", dp[arr[i]][0], dp[rev(arr[i])][1], n - dp[rev(arr[i])][2]);
	}
	return 0;
}