#include <bits/stdc++.h>
using ll = long long int;
using namespace std;
const int N = 200005;
struct Event {
    ll x;
    ll y1, y2;
    string type;
    Event(ll _x, ll _y1, ll _y2, string _type): x(_x), y1(_y1), y2(_y2), type(_type) {} 
    bool operator < (const Event a) {
        return x < a.x;
    }
};
map<ll, ll> compress;
vector<ll> id_to_cor;
vector<Event> SweepLine;
struct Node {
    int left, right;
    ll uni, num;
    Node *lc, *rc;
    void pull() {
        if(num) uni = id_to_cor[right] - id_to_cor[left];
        else if(left != right - 1) uni = lc->uni + rc->uni;
        else uni = 0;
    }
}*root_node;
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->uni = 0;
    if(left == right - 1) return;
    int mid = (left + right) / 2;
    build(r->lc = new Node(), left, mid);
    build(r->rc = new Node(), mid, right);
}
ll qry_uni(Node *r) { return r->uni; }
void upd(Node *r, int left, int right, ll dif) {
    if(left >= r->right || right <= r->left) return;
    if(left <= r->left && r->right <= right) {
        r->num += dif;
        r->pull();
        return;
    }
    upd(r->lc, left, right, dif);
    upd(r->rc, left, right, dif);
    r->pull();
}
struct Rectangle {
    ll x1, x2;
    ll y1, y2;
};
void Coordinate_Compression() {
    for(auto &x : compress) {
        x.second = id_to_cor.size();
        id_to_cor.push_back(x.first);
    }
}
ll Area_of_Union(vector<Rectangle> v) {
    for(auto Rec : v) {
        ll x1 = Rec.x1, x2 = Rec.x2; 
        ll y1 = Rec.y1, y2 = Rec.y2;
        compress[y1], compress[y2];
        SweepLine.push_back(Event(x1, y1, y2, "Add"));
        SweepLine.push_back(Event(x2, y1, y2, "Del"));
    }
    
    Coordinate_Compression();
    build(root_node = new Node(), 0, (int)compress.size());
    sort(SweepLine.begin(), SweepLine.end());
    
    ll ans = 0, last_cor = SweepLine[0].x;
    for(Event i : SweepLine) {
        ans += qry_uni(root_node) * (i.x - last_cor);
        if(i.type == "Add") {
            upd(root_node, compress[i.y1], compress[i.y2], 1);
        } else if(i.type == "Del") {
            upd(root_node, compress[i.y1], compress[i.y2], -1);
        }
        last_cor = i.x;
    }
    return ans;
}
int main() {
    int n; 
    scanf("%d", &n);
    vector<Rectangle> v;
    for(int i = 0;i < n;i++) {
        ll x1, x2;
        ll y1, y2;
        scanf("%lld %lld %lld %lld", &x1, &y1, &x2, &y2);
        v.push_back({x1, x2, y1, y2});
    }
    printf("%lld\n", Area_of_Union(v));
    return 0;
}