#include <bits/stdc++.h>
using namespace std;
const int W = 26, N = 1000005;;
int cnt[W];
char get_min() {
	for(int i = 0; i < W; i++) {
		if(cnt[i]) return (i + 'A');
	}
	return 0;
}
char get_secmin() {
	bool first = 0;
	for(int i = 0; i < W; i++) {
		if(first && cnt[i]) return (i + 'A');
		else if(cnt[i]) first = 1;
	}
	return 0;
}
int mx_cnt() {
	int mx = 0;
	for(int i = 0; i < W; i++) {
		mx = max(cnt[i], mx);
	}
	return mx;
}
int main() {
	cin.tie(0);
	ios_base::sync_with_stdio(false);
	string s;
	cin >> s;
	int n = (int)s.size();
	for(int i = 0; i < n; i++) {
		cnt[s[i] - 'A']++;
	}

	string ans;
	int remain = n;
	char last = 0;
	for(int i = 0; mx_cnt() * 2 <= remain; i++) {
		char mi = get_min();
		char secmi = get_secmin();
		if(last == mi) {
			ans += secmi;
			last = secmi;
			cnt[secmi - 'A']--;
			remain--;
			continue;
		}
		ans += mi;
		cnt[mi - 'A']--;
		remain--;
		if(mx_cnt() * 2 > remain) break;
		ans += secmi;
		last = secmi;
		cnt[secmi - 'A']--;
		remain--;
	}
	// cout << ans << endl;
	if(remain) {
		int mx_indx = 0;
		for(int i = 0; i < W; i++) {
			if(cnt[mx_indx] < cnt[i]) mx_indx = i;
		}
		
		for(int i = 0; remain > 1; i++) {
			ans += (mx_indx + 'A');
			cnt[mx_indx]--;
			remain--;
			if(!remain) break;
			if(get_min() - 'A' == mx_indx) {
				if(get_secmin() == 0) break;
				ans += get_secmin();
				cnt[get_secmin() - 'A']--;
				remain--;
			} else {
				ans += get_min();
				cnt[get_min() - 'A']--;
				remain--;
			} 
		}
		ans += (mx_indx + 'A');
		remain--;
	}
	
	bool isable = 1;
	for(int i = 0; i < (int)ans.size() - 1; i++) {
		if(ans[i] == ans[i + 1]) isable = 0;
	}
	if(isable && remain == 0) cout << ans << "\n";
	else cout << "-1\n";
	return 0;
}