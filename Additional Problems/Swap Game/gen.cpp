#include <bits/stdc++.h>
#define L 0
#define R 1
#define U 2
#define D 3
using namespace std;
typedef vector<vector<int>> Grid;
Grid vv;
void print(Grid &a) {
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
}
bool is_valid(int x, int y) {
    return x >= 0 && y >= 0 && x < 3 && y < 3;
}
int dir_x[4] = {0, 0, -1, 1};
int dir_y[4] = {-1, 1, 0, 0};
void op(int x, int y, int opea) {
    int nxt_x = x + dir_x[opea];
    int nxt_y = y + dir_y[opea];
    if(is_valid(nxt_x, nxt_y) && is_valid(x, y)) {
        swap(vv[x][y], vv[nxt_x][nxt_y]);
    }
}
default_random_engine gen(time(NULL));
uniform_int_distribution<int> unif(0, 3);
int main() {
    freopen("input.txt", "w", stdout);
    for(int i = 0;i < 3;i++) {
        vector<int> v;
        vv.push_back(v);
        for(int j = 0;j < 3;j++) {
            vv[i].push_back(i * 3 + j + 1);
        }
    }

    int times = 20;
    while(times--) {
        for(int i = 0;i < 3;i++) {
            for(int j = 0;j < 3;j++) {
                op(i, j, unif(gen));
            }
        }
    }
    print(vv);
    return 0;
}