#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
typedef vector<vector<int>> Grid;
map<int, int> vis1;
map<int, int> vis2;
queue<pair<pair<int, Grid>, int>> q;
int dir_x[2] = {1, 0};
int dir_y[2] = {0, 1};
bool is_valid(int x, int y) {
    return x >= 0 && y >= 0 && x < 3 && y < 3;
}
int number(Grid &vv) {
    int num = 0;
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            num += vv[i][j];
            num *= 10;
        }
    }
    return num;
}
void init(Grid &vv) {
    for(int i = 0;i < 3;i++) {
        vector<int> v;
        vv.push_back(v);
        for(int j = 0;j < 3;j++) {
            vv[i].push_back(0);
        }
    }
}
void print(Grid a) {
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
int main() {
    freopen("input.txt", "r", stdin);
    Grid grid;
    init(grid);
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            scanf("%d", &grid[i][j]);
        }
    }
    Grid finl;
    init(finl);
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            finl[i][j] = i * 3 + j + 1;
        }
    }
    vis1[number(grid)] = 1;
    vis2[number(finl)] = 1;
    q.push({{1, grid}, 1});
    q.push({{1, finl}, 2});
    while(!q.empty()) {
        int type = q.front().second;
        int dis = q.front().first.first;
        Grid now = q.front().first.second;
        // print(now);
        q.pop();
        if(type == 1 && vis2[number(now)]) {
            printf("%d\n", dis + vis2[number(now)] - 2);
            return 0;
        }
        if(type == 2 && vis1[number(now)]) {
            printf("%d\n", dis + vis1[number(now)] - 2);
            return 0;
        }
        for(int k = 0;k < 2;k++) {
            for(int i = 0;i < 3;i++) {
                for(int j = 0;j < 3;j++) {
                    int x = i, y = j;
                    int adj_x = x + dir_x[k], adj_y = y + dir_y[k];
                    if(!is_valid(adj_x, adj_y)) continue;
                    swap(now[x][y], now[adj_x][adj_y]);
                    int now_number = number(now);
                    if(type == 1) {
                        if(!vis1[now_number]) {
                            q.push({{dis + 1, now}, 1});
                            vis1[now_number] = dis + 1; 
                        }
                    } else {
                        if(!vis2[now_number]) {
                            q.push({{dis + 1, now}, 2});
                            vis2[now_number] = dis + 1; 
                        }
                    }
                    swap(now[x][y], now[adj_x][adj_y]);
                }
            }
        }
    }
    return 0;
}