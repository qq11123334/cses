#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
typedef vector<vector<int>> Grid;
map<int, bool> vis;
queue<pair<int, int>> q;
int dir_x[2] = {1, 0};
int dir_y[2] = {0, 1};
bool is_valid(int x, int y) {
    return x >= 0 && y >= 0 && x < 3 && y < 3;
}
int number(Grid &vv) {
    int num = 0;
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            num *= 10;
            num += vv[i][j];
        }
    }
    return num;
}
void init(Grid &vv) {
    for(int i = 0;i < 3;i++) {
        vector<int> v;
        vv.push_back(v);
        for(int j = 0;j < 3;j++) {
            vv[i].push_back(0);
        }
    }
}
Grid to_Grid(int num) {
    Grid res; init(res);
    for(int i = 2;i >= 0;i--) {
        for(int j = 2;j >= 0;j--) {
            res[i][j] = num % 10;
            num /= 10;
        }
    }
    return res;
}
void print_Grid(Grid a) {
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
void print_Grid(int x) {
    print_Grid(to_Grid(x));
}
int main() {
    // freopen("input.txt", "r", stdin);
    Grid grid;
    init(grid);
    for(int i = 0;i < 3;i++) {
        for(int j = 0;j < 3;j++) {
            scanf("%d", &grid[i][j]);
        }
    }
    int finl = 123456789;
    vis[number(grid)] = 1;
    q.push({0, number(grid)});
    while(!q.empty()) {
        int dis = q.front().first;
        int now = q.front().second;
        // print_Grid(now);
        q.pop();
        if(now == finl) {
            printf("%d\n", dis);
            return 0;
        }
        Grid now_grid = to_Grid(now);
        for(int k = 0;k < 2;k++) {
            for(int i = 0;i < 3;i++) {
                for(int j = 0;j < 3;j++) {
                    int x = i, y = j;
                    int adj_x = x + dir_x[k], adj_y = y + dir_y[k];
                    if(!is_valid(adj_x, adj_y)) continue;
                    swap(now_grid[x][y], now_grid[adj_x][adj_y]);
                    int now_number = number(now_grid);
                    if(!vis[now_number]) {
                        q.push({dis + 1, now_number});
                        vis[now_number] = 1; 
                    }
                    swap(now_grid[x][y], now_grid[adj_x][adj_y]);
                }
            }
        }
    }
    return 0;
}