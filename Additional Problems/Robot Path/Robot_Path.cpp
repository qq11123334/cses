#include <bits/stdc++.h>
#define DEBUG 0
using namespace std;
using ll = long long int;
using Pos = pair<ll, ll>;
const int N = 100005;
const ll INF = 1e18;
ll dis(Pos a, Pos b) {
	return abs(a.first - b.first) + abs(a.second - b.second);
}
struct Line {
	int x1, x2;
	int y1, y2;
}line[N];
struct Event {
	int ts;
	ll x;
	ll y1, y2;
	ll y;
	string type; // "qry", "add", "del"
	Event() {}
	Event(int _ts, ll _x, ll _y, string _type) : ts(_ts), x(_x), y(_y), type(_type) {}
	Event(int _ts, ll _x, ll _y1, ll _y2, string _type) : ts(_ts), x(_x), y1(_y1), y2(_y2), type(_type) {}
	bool operator < (const Event &e) {
		if(x != e.x) return x < e.x;
		else {
			if(e.type == "qry") {
				if(type == "add") return true;
				if(type == "del") return false;
			}
			if(type == "qry") {
				if(e.type == "add") return false;
				if(e.type == "del") return true;
			}
			if(type == "add" && e.type == "del") return true;
			if(type == "del" && e.type == "add") return false;
			assert(type == e.type);
			return ts < e.ts;
		}
		/*
		if(abs(ts - e.ts) <= 1) {
			if(e.type == "qry") {
				if(type == "add") return false;
				if(type == "del") return true;
			}
			if(type == "qry") {
				if(e.type == "add") return true;
				if(e.type == "del") return false;
			}
			if(type == "add" && e.type == "del") return true;
			if(type == "del" && e.type == "add") return false;
		} else {
			if(e.type == "qry") {
				if(type == "add") return true;
				if(type == "del") return false;
			}
			if(type == "qry") {
				if(e.type == "add") return false;
				if(e.type == "del") return true;
			}
			if(type == "add" && e.type == "del") return true;
			if(type == "del" && e.type == "add") return false;
		}
		return true;
		*/
	}
};
vector<int> id_to_cor;
map<ll, ll> compress;
void Coordinate_Compression() {
    for(auto &x : compress) {
        x.second = id_to_cor.size();
        id_to_cor.push_back(x.first);
    }
}
struct Node {
	int left, right;
	multiset<ll> ts_set;
	Node *lc, *rc;
};
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) return;
	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
}
pair<ll, ll> qry(Node *r, int ql, int qr, ll qts) { // {ts, step}
	if(ql > r->right || qr < r->left) return make_pair(INF, INF);
	if(ql <= r->left && r->right <= qr) {
		if(r->ts_set.empty()) return make_pair(INF, INF);
		else {
			pair<ll, ll> earlyest(INF, INF);
			for(auto L : r->ts_set) {
				if(abs(L - qts) <= 1) continue;
				assert(L == 1 || line[L].y1 == line[L].y2);
				assert(line[qts].x1 == line[qts].x2);
				assert(L != qts);
				if(L > qts) {
					Pos pos1(line[L].x1, line[L].y1);
					Pos pos2(line[qts].x1, line[L].y1);
					earlyest = min(earlyest, make_pair(L, dis(pos1, pos2)));
				} else if(L < qts){
					Pos pos1(line[qts].x1, line[qts].y1);
					Pos pos2(line[qts].x1, line[L].y1);
					earlyest = min(earlyest, make_pair(qts, dis(pos1, pos2)));
				}
			}
			return earlyest;
		}
	}
	return min(qry(r->lc, ql, qr, qts), qry(r->rc, ql, qr, qts));
}
void upd(Node *r, int pos, int ts, int dif) {
	if(dif == 1) r->ts_set.insert(ts);
	if(dif == -1) r->ts_set.erase(r->ts_set.find(ts));
	if(r->left == r->right) return;
	int mid = (r->left + r->right) / 2;
	if(pos <= mid) upd(r->lc, pos, ts, dif);
	else upd(r->rc, pos, ts, dif);
}
ll pre[N];
bool oppsite(char op, char last_op) {
	if(op == 'U' && last_op == 'D') return true;
	if(op == 'D' && last_op == 'U') return true;
	if(op == 'R' && last_op == 'L') return true;
	if(op == 'L' && last_op == 'R') return true;
	return false;
}
bool isver(char op) {
	return op == 'U' || op == 'D';
}
bool ishor(char op) {
	return op == 'R' || op == 'L';
}
int main() {
	int n;
	scanf("%d", &n);
	vector<Event> event;
	ll x = 0, y = 0;

	char last_op = 'A';
	
	ll mi_ans = INF;
	for(int i = 1; i <= n; i++) {
		char op;
		ll dist;
		scanf(" %c %lld", &op, &dist);
		pre[i] = pre[i - 1] + dist;
		ll aft_x = x, aft_y = y;
		if(op == 'U') {
			aft_y += dist;
		} else if(op == 'D') {
			aft_y -= dist;
		} else if(op == 'R') {
			aft_x += dist;
		} else if(op == 'L') {
			aft_x -= dist;
		}
	
		if(oppsite(op, last_op)) mi_ans = min(mi_ans, pre[i - 1]);
		
		if(isver(op)) {
			ll y1 = y, y2 = aft_y;
			if(y1 > y2) swap(y1, y2);
			Event e(i, x, y1, y2, "qry");
			event.push_back(e);
		} else if(ishor(op)) {
			ll x1 = x, x2 = aft_x;
			if(x1 > x2) swap(x1, x2);
			Event e1(i, x1, y, "add");
			Event e2(i, x2, y, "del");
			event.push_back(e1);
			event.push_back(e2);
		}
	
		if(i != 1 && isver(op) && (x == 0)) {
			if(y * aft_y <= 0) mi_ans = min(mi_ans, abs(y) + pre[i - 1]);
		}
		if(i != 1 && ishor(op) && (y == 0)) {
			if(x * aft_x <= 0) mi_ans = min(mi_ans, abs(x) + pre[i - 1]);
		}

		compress[y], compress[aft_y];
		line[i].x1 = x, line[i].y1 = y;
		line[i].x2 = aft_x, line[i].y2 = aft_y;
		x = aft_x;
		y = aft_y;
		last_op = op;
	}
	sort(event.begin(), event.end());
	Coordinate_Compression();
	Node *root = new Node();
	build(root, 0, (int)id_to_cor.size());	
	
	pair<ll, ll> min_ts(INF, INF);
	for(auto e : event) {
		if(DEBUG) {
			printf("ts = %d\n", e.ts);
			fflush(stdout);
		}
		if(e.type == "qry") {
			if(DEBUG) {
				printf("qry(%lld %lld)\n", e.y1, e.y2);
				fflush(stdout);
			}
			e.y1 = compress[e.y1];
			e.y2 = compress[e.y2];
			pair<ll, ll> res = qry(root, e.y1, e.y2, e.ts);
			min_ts = min(min_ts, res);
		} else if(e.type == "add") {
			if(DEBUG) {
				printf("add %lld\n", e.y);
				fflush(stdout);
			}
			e.y = compress[e.y];
			upd(root, e.y, e.ts, 1);
		} else if(e.type == "del") {
			if(DEBUG) {
				printf("del %lld\n", e.y);
				fflush(stdout);
			}
			e.y = compress[e.y];
			upd(root, e.y, e.ts, -1);
		}
		if(DEBUG) printf("%lld %lld\n", min_ts.first, min_ts.second);
		fflush(stdout);
	}

	// printf("%lld %lld\n", min_ts.first, min_ts.second);
	if(min_ts.first != INF) printf("%lld\n", min(mi_ans, pre[min_ts.first - 1] + min_ts.second));
	else printf("%lld\n", min(mi_ans, pre[n]));	
	return 0;
}
