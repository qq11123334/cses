#pragma GCC optimize("Ofast", "no-stack-protector", "no-math-errno", "unroll-loops")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,sse4.2,popcnt,abm,mmx,avx")
#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
const int INF = 0x3f3f3f3f;
vector<int> adj[N];
vector<int> dep[N];
int depth[N];
int n, d;
void DFS_level(int x, int level) {
	depth[x] = level;
	for(auto &v : adj[x]) {
		if(!depth[v]) {
			DFS_level(v, level + 1);
		}
	}
}
int dis[N];
queue<int> q;
inline void BFS(int s) {
	dis[s] = 0;
	q.push(s);
	while(ptr < top) {
		int x = q.front();
		q.pop();
		if(dis[x] == d - 1) break;
		for(auto &v : adj[x]) {
			if(dis[v] > dis[x] + 1) {
				dis[v] = dis[x] + 1;
				q.push(v);
			}
		}
	}
}
int main() {
	scanf("%d%d", &n, &d);
	for(int i = 1; i < n; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
		adj[b].push_back(a);
	}
	
	DFS_level(1, 1);

	for(int i = 1; i <= n; i++) {
		dep[depth[i]].push_back(i);
	}

	memset(dis, 0x3f, sizeof(dis));
	
	int ans = 0;
	for(int i = n; i >= 0; i--) {
		for(auto &x : dep[i]) {
			if(dis[x] == INF) {
				ans++;
				BFS(x);
			}
		}
	}
	
	printf("%d\n", ans);
	for(int i = 1; i <= n; i++) {
		if(dis[i] == 0) {
			printf("%d ", i);
		}
	}
	printf("\n");
}
