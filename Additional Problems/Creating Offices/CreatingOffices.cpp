#include<cstdio>
#include<iostream>
#include<vector>
#include<algorithm>
 
using namespace std;
const int INF = 0x3f3f3f3f;
int N,D;
vector<vector<int> > T;
vector<int> opt, optdist, sacrdist;
 
void DFS(int pos, int par) {
	// Base case: pos is a leaf
	if(T[pos].size() == 1 && par != -1) {
		opt[pos] = 1;
		optdist[pos] = 0;
		sacrdist[pos] = 1e9;
		return;
	}
 
	int v = T[pos][0];
	bool is_par = (v == par);
	if(is_par) v = T[pos][1];
 
	DFS(v, pos);
	if(optdist[v] + 1 >= D) {
		opt[pos] = opt[v] + 1;
		optdist[pos] = 0;
		sacrdist[pos] = optdist[v] + 1;
	} else {
		opt[pos] = opt[v];
		optdist[pos] = optdist[v] + 1;
		sacrdist[pos] = sacrdist[v] + 1;
	}
	
	for(int i = 1 + is_par; i < (int)T[pos].size(); ++i) {
		int v = T[pos][i];
		if(v == par) continue; 
		DFS(v, pos);
		
		// distance between closest points and 
		// distance to root
		// in the 4 possible solution combinations. 
		int doo = optdist[pos] + optdist[v] + 1;
		int mdoo = min(optdist[pos], optdist[v] + 1);
		
		int dos = optdist[pos] + sacrdist[v] + 1;
		int mdos = min(optdist[pos], sacrdist[v] + 1);

		int dso = sacrdist[pos] + optdist[v] + 1;
		int mdso = min(sacrdist[pos], optdist[v] + 1);

		int dss = sacrdist[pos] + sacrdist[v] + 1;
		int mdss = min(sacrdist[pos], sacrdist[v] + 1);
		
		if(doo >= D) {
			opt[pos] += opt[v];
			optdist[pos] = mdoo;
			sacrdist[pos] = max(mdos, mdso);
		} else if (dos >= D || dso >= D) {
			opt[pos] += opt[v] - 1;
			if(dos >= D && dso >= D) optdist[pos] = max(mdos, mdso);
			else if(dos >= D) optdist[pos] = mdos;
			else if(dso >= D) optdist[pos] = mdso;
			sacrdist[pos] = mdss;
		} else {
			cout << "error!" << endl;
			exit(1);
		}
	}
}
 
int main() {
	scanf("%d%d", &N, &D);
	if(N == 1) {
		printf("1\n");
		exit(0);
	}
	T = vector<vector<int> > (N + 1);
	opt = vector<int> (N + 1, 0);
	optdist = vector<int> (N + 1, 0);
	sacrdist = vector<int> (N + 1, 0);
	for(int i = 1; i < N; ++i) {
		int a, b;
		scanf("%d%d", &a, &b);
		T[a].push_back(b);
		T[b].push_back(a);
	}
	DFS(1, -1);
	printf("%d\n", opt[1]);

	// for(int i = 1; i <= N; i++) {
	// 	printf("opt[%d] = %d optdist[%d] = %d sarcdist[%d] = %d\n", i, opt[i], i, optdist[i], i, sacrdist[i]);
	// }
	return 0;
}
