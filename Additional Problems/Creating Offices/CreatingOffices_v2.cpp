#include <bits/stdc++.h>
using namespace std;
const int N = 200005, INF = (int)1e9;
vector<int> adj[N];
int par[N], opt[N], optdist[N], secdist[N];
int status[N];
int n, d;
void DFS(int x, int p = -1) {
	par[x] = p;
	int child_sz = (int)adj[x].size() - (p != -1);
	if(child_sz == 0) {
		opt[x] = 1;
		optdist[x] = 0;
		secdist[x] = INF;
		return;
	}

	if(adj[x][0] == par[x]) swap(adj[x][0], adj[x][1]);

	int v = adj[x][0];
	DFS(v, x);
	if(optdist[v] + 1 >= d) {
		opt[x] = opt[v] + 1;
		optdist[x] = 0;
		secdist[x] = optdist[v] + 1;
	} else {
		opt[x] = opt[v];
		optdist[x] = optdist[v] + 1;
		secdist[x] = secdist[v] + 1;
	}
	
	for(int i = 1; i < (int)adj[x].size(); i++) {
		int v = adj[x][i];
		if(v == par[x]) continue;
		DFS(v, x);

		vector<int> dis = {optdist[x], secdist[x], optdist[v] + 1, secdist[v] + 1};
		sort(dis.begin(), dis.end());
		
		int doo = optdist[x] + optdist[v] + 1;
		int mdoo = min(optdist[x], optdist[v] + 1);
		
		int dos = optdist[x] + secdist[v] + 1;
		int mdos = min(optdist[x], secdist[v] + 1);

		int dso = secdist[x] + optdist[v] + 1;
		int mdso = min(secdist[x], optdist[v] + 1);

		int dss = secdist[x] + secdist[v] + 1;
		int mdss = min(secdist[x], secdist[v] + 1);
		
		if(optdist[x] + optdist[v] + 1 >= d) {
			opt[x] += opt[v];
			optdist[x] = dis[0];
			secdist[x] = dis[1];
		} else {
			opt[x] += (opt[v] - 1);
			if(dos >= d && dso >= d) optdist[x] = max(mdos, mdso);
			else if(dos >= d) optdist[x] = mdos;
			else if(dso >= d) optdist[x] = mdso;
			secdist[x] = mdss;
		}
	}
}
int main() {
	scanf("%d%d", &n, &d);
	for(int i = 0; i < (n - 1); i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
		adj[b].push_back(a);
	}

	DFS(1);
	printf("%d\n", opt[1]);
	printf("\n");
}
