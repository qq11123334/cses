#include <bits/stdc++.h>
using namespace std;
const int INF = 0x3f3f3f3f, N = 200005;
int n, d;
vector<int> adj[N];
int dep[N], par[N];
int tree_arr[N], din[N], dout[N];
bool paint[N];

struct Node {
	int left, right;
	int mi;
	Node *lc, *rc;
	void pull() {
		mi = min(lc->mi, rc->mi);
	}
}*root;
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) {
		r->mi = INF;
		return;
	}

	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
	r->pull();
}
int qry(Node *r, int pos) {
	if(r->left == r->right) {
		return r->mi;
	}
	int mid = (r->left + r->right) / 2;
	if(pos <= mid) return min(r->mi, qry(r->lc, pos));
	else return min(r->mi, qry(r->rc, pos));
}
void upd(Node *r, int ql, int qr, int val) {
	if(ql > r->right || qr < r->left) return;
	if(ql <= r->left && r->right <= qr) {
		r->mi = min(r->mi, val);
		return;
	}
	upd(r->lc, ql, qr, val);
	upd(r->rc, ql, qr, val);
}
void DFS(int x, int level) {
	static int ts = 1;
	din[x] = ts;
	tree_arr[ts++] = x;
	dep[x] = level;
	for(auto v : adj[x]) {
		if(par[x] != v) {
			par[v] = x;
			DFS(v, level + 1);
		}
	}
	dout[x] = ts;
}
int main() {
	scanf("%d%d", &n, &d);
	for(int i = 0; i < n - 1; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
		adj[b].push_back(a);
	}
	
	par[1] = -1;
	DFS(1, 1);
	build(root = new Node(), 1, n);

	vector<pair<int, int>> sort_by_dep;
	for(int i = 1; i <= n; i++) {
		sort_by_dep.push_back({dep[i], i});
	}

	sort(sort_by_dep.rbegin(), sort_by_dep.rend());
	
	int ans = 0;
	for(auto P : sort_by_dep) {
		int x = P.second;
		if(dep[x] + qry(root, din[x]) < d) continue;
		paint[x] = true;
		ans++;
		for(int i = 0, v = x; i <= d && v != -1; i++, v = par[v]) {
			upd(root, din[v], dout[v] - 1, dep[x] - 2 * dep[v]);
		}
	}
	
	printf("%d\n", ans);
	for(int i = 1; i <= n; i++) {
		if(paint[i]) {
			printf("%d ", i);
		}
	}
	printf("\n");
	return 0;
}
