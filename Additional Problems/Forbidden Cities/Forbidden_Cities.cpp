#include <bits/stdc++.h>
using namespace std;
const int N = 100005, M = 200005;
const int MAX_log = 23;
typedef pair<int, int> Edge; // neighbor, number
int dfn[N], low[N], BCC_e[M], BCC_v[N];
vector<Edge> adj[N];
stack<int> sta;
int BCC_id;
bool isCut[N], pushed[M];
void DFS(int x, int par = -1) {
	static int ord = 1;
	dfn[x] = low[x] = ord++;
	int child_cnt = 0;
	for(auto [v, e] : adj[x]) {
		if(v == par) continue;
		if(!pushed[e]) {
			sta.push(e);
			pushed[e] = true;
		}
		if(!dfn[v]) {
			child_cnt++;
			DFS(v, x);
			low[x] = min(low[v], low[x]);
			if(dfn[x] <= low[v]) {
				isCut[x] = true;
				BCC_id++;
				while(1) {
					int edge = sta.top();
					sta.pop();
					BCC_e[edge] = BCC_id;
					if(edge == e) break;
				}
			}
		} else if(par != v) {
			low[x] = min(dfn[v], low[x]);
		}
	}
	if(dfn[x] == 1 && child_cnt <= 1) {
		isCut[x] = false;
	}
}

int n, m, q;

vector<int> adj_BCC[2 * N];
vector<pair<int, int>> edge;
int par[2 * N][MAX_log], dep[2 * N];
bool vis[2 * N];
void DFS_BCC(int x, int level = 1) {
	dep[x] = level;
	vis[x] = true;
	for(auto v : adj_BCC[x]) {
		if(!vis[v]) {
			par[v][0] = x;
			DFS_BCC(v, level + 1);
		}
	}
}
void build() {
    for (int i = 1; i < MAX_log; i++) {
        for (int j = 1; j <= BCC_id; j++) {
			if(j <= n && !isCut[j]) continue;
            par[j][i] = par[par[j][i - 1]][i - 1];
        }
    }
}
int LCA(int a, int b) {
    if (dep[a] > dep[b]) swap(a, b);
    int dif = dep[b] - dep[a];
    for (int i = 0; i < MAX_log; i++) {
        if (dif & (1 << i)) {
            b = par[b][i];
        }
    }
    if (a == b) return a;
    for (int i = MAX_log - 1; i >= 0; i--) {// must be form MAX_log ~ 0
        if (par[a][i] != par[b][i]) {
            a = par[a][i];
            b = par[b][i];
        }
    }
    return par[a][0];
}
int dis(int a, int b) {
    return (dep[a] + dep[b] - 2 * dep[LCA(a,b)]);
}

int main() {
	scanf("%d%d%d", &n, &m, &q);
	BCC_id = n;

	edge.resize(m);
	for(int i = 0; i < m; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(Edge(b, i));
		adj[b].push_back(Edge(a, i));
		edge[i] = {a, b};
	}

	DFS(1);
	for(int i = 0; i < m; i++) {
		auto [a, b] = edge[i];
		for(auto v : {a, b}) {
			if(isCut[v]) {
				adj_BCC[BCC_e[i]].push_back(v);
				adj_BCC[v].push_back(BCC_e[i]);
				BCC_v[v] = v;
			} else {
				BCC_v[v] = BCC_e[i];
			}
		}
	}
	
	DFS_BCC(n + 1);
	build();

	while(q--) {
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		if(a == c || b == c) printf("NO\n");
		else if(!isCut[c]) printf("YES\n");
		else {
			a = BCC_v[a];
			b = BCC_v[b];
			if(dis(a, b) == dis(a, c) + dis(c, b)) {
				printf("NO\n");
			} else {
				printf("YES\n");
			}
		}
	}
	return 0;
}
