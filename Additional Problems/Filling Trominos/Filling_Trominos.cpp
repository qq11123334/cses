#include <bits/stdc++.h>
using namespace std;
const int N = 100, C = 8;
bool dp[N + 50][N + 50];
pair<int, int> par[N + 50][N + 50];
int n, m;
int grid[N][N];
bool valid(int x, int y) {
	return (x >= 0) && (x < n) && (y >= 0) && (y < m);
}
int dir_x[] = {1, -1, 0, 0};
int dir_y[] = {0, 0, 1, -1};
void set_L(vector<pair<int, int>> L) {
	bool used[C];
	memset(used, false, sizeof(used));
	for(auto [x, y] : L) {
		assert(valid(x, y));
		grid[x][y] = -1;
	}
	for(auto [x, y] : L) {
		for(int i = 0; i < 4; i++) {
			int adj_x = x + dir_x[i];
			int adj_y = y + dir_y[i];
			if(valid(adj_x, adj_y)) {
				int color = grid[adj_x][adj_y];
				if(color != -1) {
					used[color] = true;
				}
			}
		}
	}

	int color = -1;
	for(int i = 0; i < C; i++) {
		if(!used[i]) {
			color = i;
			break;
		}
	}
	
	assert(color != -1);
	for(auto [x, y] : L) {
		grid[x][y] = color;
	}
}
void Fill_L(int x1, int y1, int x2, int y2) {
	int len_x = (x2 - x1 + 1);
	int len_y = (y2 - y1 + 1);
	if(len_x == 2 && len_y == 3) {
		set_L({{x1, y1}, {x1 + 1, y1}, {x1, y1 + 1}});
		set_L({{x2, y2}, {x2 - 1, y2}, {x2, y2 - 1}});
		return;
	} 
	if(len_x == 3 && len_y == 2) {
		set_L({{x1, y1}, {x1 + 1, y1}, {x1, y1 + 1}});
		set_L({{x2, y2}, {x2 - 1, y2}, {x2, y2 - 1}});
		return;
	} 
	if(len_x == 5 && len_y == 9) {
		string s[] = 
		{"BBXXQWWPP",
		 "BCXOQQWPY",
		 "CCVOOGGYY",
		 "SVVDEGTTR",
		 "SSDDEETRR"};
		for(int c = 0; c < 26; c++) {
			vector<pair<int, int>> L;
			for(int i = 0; i < 5; i++) {
				for(int j = 0; j < 9; j++) {
					if(s[i][j] == c + 'A') {
						L.emplace_back(x1 + i, y1 + j);
					}
				}
			}
			if(L.size()) set_L(L);
		}
		return;
	} 
	if(len_x == 9 && len_y == 5) {
		string s[] = 
		{"BBXXQWWPP",
		 "BCXOQQWPY",
		 "CCVOOGGYY",
		 "SVVDEGTTR",
		 "SSDDEETRR"};
		for(int c = 0; c < 26; c++) {
			vector<pair<int, int>> L;
			for(int i = 0; i < 5; i++) {
				for(int j = 0; j < 9; j++) {
					if(s[i][j] == c + 'A') {
						L.emplace_back(x1 + j, y1 + i);
					}
				}
			}
			if(L.size()) set_L(L);
		}
		return;
	}

	auto [par_x, par_y] = par[len_x][len_y];
	if(par_x == len_x) {
		Fill_L(x1, y1, x2, y1 + par_y - 1);
		Fill_L(x1, par_y, x2, y2);
	} else if(par_y == len_y) {
		Fill_L(x1, y1, x1 + par_x - 1, y2);
		Fill_L(par_x, y1, x2, y2);
	}
}
void build() {
	dp[2][3] = true;
	dp[3][2] = true;
	dp[5][9] = true;
	dp[9][5] = true;
	for(int i = 1; i <= N; i++) {
		for(int j = 1; j <= N; j++) {
			if(dp[i][j] == false) continue;
            if(i % 2 == 0) {
				dp[i][j + 3] = true;
				par[i][j + 3] = {i, j};
			}
			if(i % 3 == 0) {
				dp[i][j + 2] = true;
				par[i][j + 2] = {i, j};
			}
            if(i % 5 == 0) {
                dp[i][j + 9] = true;
                par[i][j + 9] = {i, j};
            }
            if(i % 9 == 0) {
                dp[i][j + 5] = true;
                par[i][j + 5] = {i, j};
            }
			if(j % 2 == 0) {
				dp[i + 3][j] = true;
				par[i + 3][j] = {i, j};
			}
			if(j % 3 == 0) {
				dp[i + 2][j] = true;
				par[i + 2][j] = {i, j};
			}
            if(j % 5 == 0) {
                dp[i + 9][j] = true;
                par[i + 9][j] = {i, j};
            }
            if(j % 9 == 0) {
                dp[i + 5][j] = true;
                par[i + 5][j] = {i, j};
            }
		}
	}
}
void init() {
	memset(grid, -1, sizeof(grid));
}
int main() {
	cin.tie(0);
	ios_base::sync_with_stdio(false);
	build();
/*
{
    int i, j;
    while(cin >> i >> j) {
		cerr << i << " " << j << " " << par[i][j].first << " " << par[i][j].second << endl;
    }
}*/
	int t;
	cin >> t;
	while(t--) {
		cin >> n >> m;
		if(dp[n][m]) {
			init();
			cout << "YES\n";
			Fill_L(0, 0, n - 1, m - 1);
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < m; j++) {
					cout << (char)('A' + grid[i][j]);
				}
				cout << "\n";
			}
		}
		else cout << "NO\n";
	}
}
