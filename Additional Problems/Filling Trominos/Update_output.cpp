#include <bits/stdc++.h>
using namespace std;
int main() {
	ifstream original_output;
	ofstream new_output;
	char filename[] = "test_output.txt";
	char new_filename[] = "output2.txt";
	original_output.open(filename);
	new_output.open(new_filename);
	string s;
	while(original_output >> s) {
		if(s == "YES" || s == "NO") {
			new_output << s << endl;
		}
	}
	return 0;
}
