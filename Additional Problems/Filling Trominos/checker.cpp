#include <bits/stdc++.h>
using namespace std;
int main() {
	ifstream output1;
	ifstream output2;
	ifstream input;
	output1.open("output1.txt");
	output2.open("output2.txt");
	input.open("input.txt");
	string s1, s2;
	int t;
	input >> t;
	while((output1 >> s1) && (output2 >> s2)) {
		int n, m;
		input >> n >> m;
		if(s1 != s2) {
			cout << n << " " << m << endl;
		}
	}
	return 0;
}
