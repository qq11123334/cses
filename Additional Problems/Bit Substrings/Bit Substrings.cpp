#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
using cd = complex<double>;
const double PI = acos(-1);
vector<cd> FFT(vector<cd> &F, bool inv) {
	int n = (int)F.size();
	vector<cd> res(n);
	for(int k = 0; k < n; k++) {
		int RevBit = 0;
		for(int i = 1; i < n; i <<= 1) {
			RevBit <<= 1;
			if(k & i) RevBit++;
		}
		res[RevBit] = F[k];
	}

	for(int Pow = 2; Pow <= n; Pow *= 2) {
		double theta = (inv ? -1 : 1) * 2.0 * PI / Pow;
		cd omega(cos(theta), sin(theta));
		for(int rnd = 0; rnd < n; rnd += Pow) {
			cd cur = 1;
			for(int i = 0; i < Pow / 2; i++) {
				cd a = res[rnd + i];
				cd b = cur * res[rnd + i + Pow / 2];
				res[rnd + i] = a + b;
				res[rnd + i + Pow / 2] = a - b;
				cur *= omega;
			}
		}
	}
	if(inv) {
		for(int i = 0; i < n; i++) res[i] /= n;
	}
	return res;
}
vector<cd> Poly_multi(vector<cd> A, vector<cd> B) {
	int k = (1 << ((int)log2((int)A.size() + (int)B.size()) + 1));
	A.resize(k);
	B.resize(k);
	A = FFT(A, 0);
	B = FFT(B, 0);
	vector<cd> C(k);
	for(int i = 0; i < k; i++) C[i] = A[i] * B[i];
	return FFT(C, 1);
}
const int N = 200005;
ll pre[N], ans[N];
int main() {
	string s;
	cin >> s;
	int n = (int)s.size();
	vector<cd> cnt(n + 1, 0);
	vector<cd> reverse_cnt(n + 1, 0);
	ll cnt_0 = 0;
	ll contin_0 = 0;
	for(int i = 1; i <= n; i++) {
		pre[i] = pre[i - 1] + (s[i - 1] == '1');
		cnt[pre[i]] += 1.0;
		reverse_cnt[n - pre[i - 1]] += 1.0;
		if(s[i - 1] == '0') {
			contin_0++;
			cnt_0 += contin_0;
		} else {
			contin_0 = 0;
		}
	}

	auto res = Poly_multi(cnt, reverse_cnt);

	for(int i = 0; i <= n; i++) {
		if(i == 0) cout << cnt_0 << " ";
		else cout << (ll)round(res[i + n].real()) << " ";
	}
	return 0;
}