#include <bits/stdc++.h>
using namespace std;
const int N = 1000005, W = 26;
int l_shift[N], cnt[W], hash_indx;
vector<int> indx[W];
int ptr[W];
string bwt, s;
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    
    cin >> bwt;
    int n = (int)bwt.size();
    for(int i = 0; i < (int)n; i++) {
        if(bwt[i] == '#') {
            hash_indx = i;   
        } else {
            int c = bwt[i] - 'a';
            cnt[c]++;
            indx[c].push_back(i);
        }
    }

    s += '#';
    for(int i = 0; i < W; i++) {
        while(cnt[i]--) s += (i + 'a');
    }

    for(int i = 0; i < n; i++) {
        if(s[i] == '#') {
            l_shift[i] = hash_indx;
        } else {
            int c = s[i] - 'a';
            l_shift[i] = indx[c][ptr[c]++];
        }
    }

    hash_indx = l_shift[hash_indx];
    for(int i = 0; i < n - 1; i++, hash_indx = l_shift[hash_indx]) {
        cout << bwt[hash_indx];
    }
    return 0;
}