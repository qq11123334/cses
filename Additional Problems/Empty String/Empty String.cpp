#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 505;
const ll MOD = (ll)1e9 + 7;
ll dp[N][N];
ll fact[N], ifact[N];
ll fp(ll x, ll y) {
	ll res = 1;
	while(y) {
		if(y & 1) res = res * x % MOD;
		y >>= 1;
		x = x * x % MOD;
	}
	return res;
}
void build_fact() {
	fact[0] = 1;
	ifact[0] = fp(fact[0], MOD - 2);
	for(int i = 1; i < N; i++) {
		fact[i] = fact[i - 1] * i % MOD;
		ifact[i] = fp(fact[i], MOD - 2);
	}
}
ll C(ll a, ll b) {
	assert(a >= b);
	return fact[a] * ifact[b] % MOD * ifact[a - b] % MOD;
}
int main() {
	build_fact();
	string s;
	cin >> s;
	int n = (int)s.size();
	for(int i = 0; i < n - 1;i++) {
		if(s[i] == s[i + 1]) {
			dp[i][i + 1] = 1;
		}
	}

	for(int l = 4; l <= n; l += 2) {
		for(int i = 0; i + l - 1 < n; i++) {
			// calculate dp[i][i + l - 1]
			for(int j = i + 1; j <= i + l - 1; j++) {
				int left = i, right = (i + l - 1), mid = j;
				if(s[left] == s[mid]) {
					ll left_len = ((mid - 1) - (left + 1) + 1);
					ll right_len = ((right) - (mid + 1) + 1);
					// printf("len = %lld %lld pos = %d %d %d\n", left_len, right_len, left, mid, right);
					assert(!(left_len == 0 && right_len == 0));
					if(left_len == 0) {
						// printf("val = %lld %lld\n", dp[mid + 1][right], C((left_len + right_len + 2) / 2, 0));
						dp[left][right] += dp[mid + 1][right] * C((right_len + 2) / 2, 1);
					}
					else if(right_len == 0) {
						dp[left][right] += dp[left + 1][mid - 1] * C((left_len + 2) / 2, 0);
					}
					else {
						dp[left][right] += dp[left + 1][mid - 1] * dp[mid + 1][right] % MOD * C((left_len + right_len + 2) / 2, right_len / 2);
					}

					dp[left][right] %= MOD;
				}
			}
			// printf("dp[%d][%d] = %lld\n", i, i + l - 1, dp[i][i + l - 1]);
		}
	}
	
	printf("%lld\n", dp[0][n - 1]);
	return 0;
}