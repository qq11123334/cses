#include <bits/stdc++.h>
#define X first
#define Y second
using namespace std;
using Grid = pair<int, int>;
using ll = long long;
const int M = 1005, N = 2000005;
const ll MOD = (ll)1e9 + 7;
ll dp[M];
ll fact[N], ifact[N];
ll fp(ll x, ll y) {
	ll res = 1;
	while(y) {
		if(y & 1) res = res * x % MOD;
		y >>= 1;
		x = x * x % MOD;
	}
	return res;
}
ll find_rev(ll x) {
	return fp(x, MOD - 2);
}
void build_fact() {
	for(int i = 0; i < N; i++) {
		if(i == 0) fact[i] = 1;
		else fact[i] = fact[i - 1] * i % MOD;
		ifact[i] = find_rev(fact[i]);
	}
}
Grid trap[M];
bool reachable(Grid a, Grid b) {
	// a -> b
	return (a.X <= b.X) && (a.Y <= b.Y);
}
ll C(ll a, ll b) {
	return fact[a] * ifact[b] % MOD * ifact[a - b] % MOD;
}
ll path_num(Grid a, Grid b) {
	if(!reachable(a, b)) return 0;
	int x_dif = (b.X - a.X);
	int y_dif = (b.Y - a.Y);
	// printf("(%d, %d) -> (%d, %d) res = %lld\n", a.X, a.Y, b.X, b.Y, C(x_dif + y_dif, x_dif));
	return C(x_dif + y_dif, x_dif);
}
int main() {
	build_fact();
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 0; i < m; i++) {
		scanf("%d %d", &trap[i].X, &trap[i].Y);
	}

	sort(trap, trap + m);

	for(int i = 0; i < m; i++) {
		dp[i] = path_num(Grid(1, 1), trap[i]);
		for(int j = 0; j < i; j++) {
			dp[i] -= dp[j] * path_num(trap[j], trap[i]);
			dp[i] = (dp[i] % MOD + MOD) % MOD;
		}
	}
	ll ans = path_num(Grid(1, 1), Grid(n, n));
	for(int i = 0; i < m; i++) {
		ans -= dp[i] * path_num(trap[i], Grid(n, n));
		ans = (ans % MOD + MOD) % MOD;
	}
	printf("%lld\n", ans);
	return 0;
}