#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
void v_print(vector<ll> &v) {
	for(auto x : v) {
		printf("%lld ", x);
	}
	printf("\n");
}
void v_mer(vector<ll> &a, vector<ll> &b, vector<ll> &c) {
	c.clear();
	int idx_a = 0, idx_b = 0;
	int n = (int)a.size(), m = (int)b.size();
	while(idx_a < n || idx_b < m) {
		if(idx_a == n) c.push_back(b[idx_b++]);
		else if(idx_b == m) c.push_back(a[idx_a++]);
		else if(a[idx_a] < b[idx_b]) c.push_back(a[idx_a++]);
		else c.push_back(b[idx_b++]);
	}
}
struct Node {
	int left, right;
	vector<ll> v, pre;
	ll sum, abs_sum;
	Node *lc, *rc;
	void pull() {
		v_mer(lc->v, rc->v, v);
		sum = lc->sum + rc->sum;
		abs_sum = lc->abs_sum + rc->abs_sum;
		int n = (int)v.size();
		pre.resize(n + 1, 0);
		for(int i = 1; i <= n; i++) {
			pre[i] = pre[i - 1] + v[i - 1];
		}
	}
}*root;
ll dif[2 * N], pre_dif[2 * N];
int n;
void build(Node *r, int L , int R) {
	r->left = L, r->right = R;
	if(L == R) {
		ll val = pre_dif[L];
		r->v.push_back(val);
		r->pre.resize(2, 0);
		r->pre[1] = val;
		r->sum = val;
		r->abs_sum = abs(val);
		return;
	}

	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
	r->pull();
}
ll qry_sum(Node *r, int ql, int qr) {
	if(ql > r->right || qr < r->left) return 0;
	if(ql <= r->left && r->right <= qr) return r->sum;
	return qry_sum(r->lc, ql, qr) + qry_sum(r->rc, ql, qr);
}
ll qry(Node *r, int ql, int qr) {
	if(ql > r->right || qr < r->left) return 0;
	if(ql <= r->left && r->right <= qr) {
		int vn = (int)r->v.size();
		ll pre_sum = pre_dif[ql - 1];
		if(pre_sum == 0) return r->abs_sum;
		vector<ll>::iterator lb, ub;
		if(pre_sum > 0) {
			lb = upper_bound(r->v.begin(), r->v.end(), 0);
			ub = upper_bound(r->v.begin(), r->v.end(), pre_sum);
			ub--;
		} else {
			lb = upper_bound(r->v.begin(), r->v.end(), pre_sum);
			ub = upper_bound(r->v.begin(), r->v.end(), 0);
			ub--;
		}
		int lb_idx = lb - r->v.begin();
		int ub_idx = ub - r->v.begin();

		int sz1 = lb_idx, sz2 = ub_idx - lb_idx + 1, sz3 = vn - ub_idx - 1;
		/*
		for(int i = 0; i < vn; i++) {
			if(lb_idx == i) printf("|");
			printf("%lld ", r->v[i]);
			if(ub_idx == i) printf("|");
		}
		printf("\n");
		*/
		// 1 ~ (lb_idx), lb_idx + 1 ~ ub_idx + 1, ub_idx + 2 ~ vn
		ll sum1 = abs(r->pre[lb_idx] - sz1 * pre_sum);
		ll sum2 = abs(sz2 * pre_sum - (r->pre[ub_idx + 1] - r->pre[lb_idx]));
		ll sum3 = abs((r->pre[vn] - r->pre[ub_idx + 1]) - sz3 * pre_sum);
		ll res = sum1 + sum2 + sum3;
		//v_print(r->v);
		//v_print(r->pre);
		//printf("pre_sum = %lld\n", pre_sum);
		//printf("sum1 = %lld sum2 = %lld sum3 = %lld\n", sum1, sum2, sum3);
		//printf("[%d %d] res = %lld\n", r->left, r->right, res);
		return res;
	}
	return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}
int A[N], B[N];
int main() {
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%d", &A[i]);
	}

	for(int i = 1; i <= n; i++) {
		scanf("%d", &B[i]);
	}

	for(int i = 1; i <= n; i++) {
		dif[i] = A[i] - B[i];
		dif[i + n] = dif[i];
	}

	for(int i = 1; i <= 2 * n; i++) {
		pre_dif[i] = pre_dif[i - 1] + dif[i];
	}

	build(root = new Node(), 1, 2 * n);
	ll ans = 4e18;
	for(int i = 1; i <= n; i++) {
		// printf("ql = %d qr = %d res = %lld\n", i, i + n - 1, qry(root, i, i + n - 1));
		ans = min(ans, qry(root, i, i + n - 1));
	}

	printf("%lld\n", ans);
	return 0;
}
