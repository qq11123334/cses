#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
int n;
ll a[N], b[N];
ll dif[2 * N], pre[2 * N];
int main() {
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%lld", &a[i]);
	}
	for(int i = 1; i <= n; i++) {
		scanf("%lld", &b[i]);
	}
 
	for(int i = 1; i <= n; i++) {
		dif[i] = a[i] - b[i];
		dif[i + n] = dif[i];
		printf("%lld ", dif[i]);
	}
	printf("\n");
	ll ans = (ll)4e18;
	for(int i = 1; i <= n; i++) {
		ll res = 0, cur_sum = 0;;
		for(int j = i; j <= i + n; j++) {
			res += abs(cur_sum);
			if(res >= ans) break;
			cur_sum += dif[j];
		}
		ans = min(ans, res);
	}
	
	printf("%lld\n", ans);
	return 0;
}
