#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
ll grid[2][N];
ll currentSum[2];
int main() {
	int n;
	scanf("%d", &n);
    
	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < n; j++) {
			scanf("%lld", &grid[i][j]);
			grid[i][j]--;
		}
	}
	
	ll ans = 0;
	for(int i = 0; i < n; i++) {
		currentSum[0] += grid[0][i];
		currentSum[1] += grid[1][i];
		
		if(currentSum[0] * currentSum[1] < 0) {
			ll change = min(abs(currentSum[0]), abs(currentSum[1]));
			ans += change;
			if(currentSum[0] < 0) currentSum[0] += change;
			else currentSum[0] -= change;
			
			if(currentSum[1] < 0) currentSum[1] += change;
			else currentSum[1] -= change;
		}
		ans += (abs(currentSum[0]) + abs(currentSum[1]));
	}
	
	printf("%lld\n", ans);
	return 0;
}
