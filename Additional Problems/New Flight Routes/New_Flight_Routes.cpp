#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> Edge;
const int N = 100005;
int dfn[N], low[N];
int SCC_id[N], SCC_cnt;
bool inS[N];
vector<int> adj[N], s, SCC_vertex[N];
vector<Edge> ans;
void DFS(int x) {
	static int ord = 1;
	dfn[x] = low[x] = ord++;
	inS[x] = 1;
	s.push_back(x);
	for(auto v : adj[x]) {
		if(!dfn[v]) {
			DFS(v);
			low[x] = min(low[x], low[v]);
		} else if(inS[v]) {
			low[x] = min(low[x], dfn[v]);
		}
	}
	if(low[x] == dfn[x]) {
		SCC_cnt++;
		while(s.back() != x) {
			SCC_id[s.back()] = SCC_cnt;
			SCC_vertex[SCC_cnt].push_back(s.back());
			inS[s.back()] = 0;
			s.pop_back();
		}
		SCC_vertex[SCC_cnt].push_back(x);
		SCC_id[x] = SCC_cnt;
		inS[x] = 0;
		s.pop_back();
	}
}
int n, m;
void Tarjan_SCC() {
	SCC_cnt = 0;
	for(int i = 1;i <= n;i++) {
		if(!dfn[i]) {
			DFS(i);
		}
	}
}
bool vis_SCC[N], isRoot[N], isLeaf[N];
vector<int> root_s, leaf_s, toRoot;

int stov(int SCC_id) {
	return SCC_vertex[SCC_id][0];
}
int p[N], sz[N];
void init() {
	for(int i = 0; i < N; i++) {
		p[i] = i;
		sz[i] = 1;
	}
}
int find(int x) {
	if(p[x] == x) return x;
	return p[x] = find(p[x]);
}
void unite(int a, int b) {
	a = find(a), b = find(b);
	if(a == b) return;
	if(sz[a] > sz[b]) swap(a, b);
	p[a] = b;
	sz[b] += sz[a];
}
void add_edge(int a, int b) {
	unite(a, b);
	ans.push_back(Edge(stov(a), stov(b)));
}
int root;
bool DFS_SCC(int s) {
	if(vis_SCC[s] && root == s) return true;
	if(vis_SCC[s]) return false;
	vis_SCC[s] = true;
	bool res = false;
	for(auto x : SCC_vertex[s]) {
		for(auto v : adj[x]) {
			int nxt_s = SCC_id[v];
			res |= DFS_SCC(nxt_s);
		}
	}
	if(isLeaf[s]) {
		bool find_nxt = false;
		while(!root_s.empty()) {
			int nxt_s = root_s.back();
			root_s.pop_back();
			if(DFS_SCC(nxt_s)) {
				add_edge(s, nxt_s);
				return true;
			} else {
				toRoot.push_back(nxt_s);
			}
		} 
		if(!find_nxt) {
			leaf_s.push_back(s);
		}
		return false;
	}
	return res;
}
int main() {
	init();
	scanf("%d%d", &n, &m);
	for(int i = 0;i < m;i++) {
	int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
	}
    
	Tarjan_SCC();

	if(SCC_cnt == 1) {
		printf("0\n");
		exit(0);
	}

	fill(isRoot, isRoot + N, true);
	fill(isLeaf, isLeaf + N, true);
	for(int i = 1; i <= SCC_cnt; i++) {
		for(auto x : SCC_vertex[i]) {
			assert(SCC_id[x] == i);
			for(auto v : adj[x]) {
				if(SCC_id[v] == i) continue;
				unite(SCC_id[v], SCC_id[x]);
				isRoot[SCC_id[v]] = false;
				isLeaf[SCC_id[x]] = false;
			}
		}
	}

	for(int i = 1; i <= SCC_cnt; i++) {
		if(isRoot[i]) {
			root_s.push_back(i);
		}
	}

	root = root_s[0];
	DFS_SCC(root);
	
	while(!toRoot.empty() && !leaf_s.empty()) {
		add_edge(leaf_s.back(), toRoot.back());
		toRoot.pop_back();
		leaf_s.pop_back();
	}
	
	for(auto s : toRoot) {
		if(root != s) add_edge(root, s);
	}
	
	for(auto s : leaf_s) {
		if(root != s) add_edge(s, root);
	}

	printf("%d\n", (int)ans.size());
	for(Edge &e : ans) {
		printf("%d %d\n", e.first, e.second);
	}
	return 0;
}
