#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 1005;
ll toLeft[N][N];
char grid[N][N];
ll res[N][N];
int n, m;
void solve(int j) {
	stack<pair<ll, ll>> sta;
	for(int i = 1; i <= n; i++) {
		while(!sta.empty() && toLeft[i][j] <= sta.top().first) {
			sta.pop();
		}
		ll width = (sta.empty() ? (i) : abs(i - sta.top().second));
		res[i][j] += toLeft[i][j] * width;
		sta.push(make_pair(toLeft[i][j], i));
	}

	while(!sta.empty()) sta.pop();

	for(int i = n; i >= 1; i--) {
		while(!sta.empty() && toLeft[i][j] <= sta.top().first) {
			sta.pop();
		}
		ll width = (sta.empty() ? (n - i + 1) : abs(i - sta.top().second));
		res[i][j] += toLeft[i][j] * width;
		sta.push(make_pair(toLeft[i][j], i));
	}

	for(int i = 1; i <= n; i++) {
		res[i][j] -= toLeft[i][j];
	}
}
int main() {
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; i++) {
		for(int j = 1; j <= m; j++) {
			scanf(" %c", &grid[i][j]);
		}
	}

	for(int i = 1; i <= n; i++) {
		for(int j = 1; j <= m; j++) {
			if(grid[i][j] == '.') {
				toLeft[i][j] = toLeft[i][j - 1] + 1;
			} else {
				toLeft[i][j] = 0;
			}
		}
	}

	for(int i = 1; i <= m; i++) {
		solve(i);
	}
	ll ans = 0;
	for(int i = 1; i <= n; i++) {
		for(int j = 1; j <= m; j++) {
			ans = max(ans, res[i][j]);
			// printf("%lld ", res[i][j]);
		}
		// printf("\n");
	}
	printf("%lld\n", ans);
	return 0;
}