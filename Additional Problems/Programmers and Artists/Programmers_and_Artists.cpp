#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
struct Person {
    ll pro, art;
}person[N];
bool operator < (const Person a, const Person b) {
    return (a.pro - a.art) > (b.pro - b.art);
}

ll prog_sum[N], arti_sum[N];
priority_queue<ll, vector<ll>, greater<ll>> prog, arti;

int main() {
    int a, b, n;
    scanf("%d%d%d", &a, &b, &n);
    for(int i = 0; i < n; i++) {
        scanf("%lld%lld", &person[i].pro, &person[i].art);
    }

    sort(person, person + n);
    
    for(int i = 0; i < a; i++) {
        prog_sum[i] = prog_sum[i - 1];
        prog_sum[i] += person[i].pro;
        prog.push(person[i].pro);
    }

    for(int i = n - 1; i > n - 1 - b; i--) {
        arti_sum[i] = arti_sum[i + 1];
        arti_sum[i] += person[i].art;
        arti.push(person[i].art);
    }

    for(int i = a; i < n; i++) {
        prog_sum[i] = prog_sum[i - 1];
        prog.push(person[i].pro);
        prog_sum[i] += person[i].pro;
        prog_sum[i] -= prog.top();
        prog.pop();
    }

    for(int i = n - b - 1; i >= 0; i--) {
        arti_sum[i] = arti_sum[i + 1];
        arti.push(person[i].art);
        arti_sum[i] += person[i].art;
        arti_sum[i] -= arti.top();
        arti.pop();
    }

    ll ans = 0;
    for(int i = a - 1; i < n - b; i++) {
        ans = max(ans, prog_sum[i] + arti_sum[i + 1]);
    }
    printf("%lld\n", ans);
    return 0;
}
