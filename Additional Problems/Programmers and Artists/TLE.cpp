#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll INF = 1e18;
const int N = 200050;
struct Edge {
    int to; 
    ll cap, cost;
    int rev;
    Edge() {}
    Edge(int _to, ll _cap, ll _cost, int _rev) : to(_to), cap(_cap), cost(_cost), rev(_rev) {}
};
vector<Edge> adj[N];
ll dis[N];
int par[N], par_id[N];
bool in_que[N];
pair<ll, ll> MCMF(int s, int t) {
    ll flow = 0, cost = 0;
    while(true) {
        memset(dis, 0x3f, sizeof(dis));
        memset(in_que, 0, sizeof(in_que));
        queue<int> que;
        que.push(s); dis[s] = 0;
        while(!que.empty()) { // SPFA
            int x = que.front();
            que.pop(); in_que[x] = 0;
            for(int i = 0;i < (int)adj[x].size();i++) {
                Edge &e = adj[x][i];
                if(e.cap > 0 && dis[e.to] > dis[x] + e.cost) {
                    dis[e.to] = dis[x] + e.cost;
                    par[e.to] = x;
                    par_id[e.to] = i;
                    if(!in_que[e.to]) {
                        in_que[e.to] = 1;
                        que.push(e.to);
                    }
                }
            }
        }

        if(dis[t] >= INF) break;

        ll mi_flow = INF;
        for(int i = t;i != s;i = par[i]) {
            mi_flow = min(mi_flow, adj[par[i]][par_id[i]].cap);
        }

        flow += mi_flow, cost += mi_flow * dis[t];
        for(int i = t;i != s;i = par[i]) {
            Edge &e = adj[par[i]][par_id[i]];
            e.cap -= mi_flow;
            adj[e.to][e.rev].cap += mi_flow;
        }
    }
    return make_pair(flow, cost);
}
void add_edge(int a, int b, ll cap, ll cost) {
    adj[a].push_back(Edge(b, cap, cost, (int)adj[b].size()));
    adj[b].push_back(Edge(a, 0, -cost, (int)adj[a].size() - 1));
}

ll prog[N], arti[N];
int main() {
	int a, b, n;
	scanf("%d%d%d", &a, &b, &n);

	int s = n, PROG = n + 1, ARTI = n + 2, t = n + 3;
	for(int i = 0; i < n; i++) {
		scanf("%lld%lld", &prog[i], &arti[i]);
	}

	add_edge(s, PROG, a, 0);
	add_edge(s, ARTI, b, 0);

	for(int i = 0; i < n; i++) {
		add_edge(PROG, i, 1, -prog[i]);
		add_edge(ARTI, i, 1, -arti[i]);
		add_edge(i, t, 1, 0);
	}

	printf("%lld\n", -MCMF(s, t).second);

}
