#include <bits/stdc++.h>
#define ll long long int
#ifdef _WIN32
#define lld "%I64d"
#else
#define lld "%lld"
#endif
#define empty 0
using namespace std;
struct Node
{
    ll left, right, mid;
    ll val;
    Node *lc, *rc;
}node[1000005], *node_root, *last_node = node;
struct point
{
    ll x;
    ll y;
    int status;
};
bool cmp(point a, point b)
{
    if(a.x == b.x && a.status == b.status) return a.y < b.y;
    if(a.x == b.x) return a.status < b.status;
    return a.x < b.x;
}
void build(Node *root, ll left, ll right);
void upd(Node *root, ll pos, ll add);
ll qry(Node *root, ll left, ll right);
vector <point> scan; 
map<ll,ll> val_to_idx;
vector<ll> idx_to_val;
void add(ll x)
{
    val_to_idx[x] = 1;
}
void reindex()
{
    idx_to_val.push_back(empty);
    for(auto i : val_to_idx)
    {
        val_to_idx[i.first] = idx_to_val.size();
        idx_to_val.push_back(i.first);
    }
}
int main()
{
    val_to_idx.clear();
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n;i++)
    {
        ll x1, x2, y1, y2;
        scanf(lld lld lld lld,&x1,&y1,&x2,&y2);
        add(x1), add(x2), add(y1), add(y2);
        if(x1 == x2)
        {
            scan.push_back({x1, y1, 2});
            scan.push_back({x2, y2, 2});
        }
        else if(y1 == y2)
        {
            scan.push_back({x1, y1, 1});
            scan.push_back({x2, y2, 3});
        }           
    }
    reindex();
    build(node_root = last_node++, 1, idx_to_val.size());
    sort(scan.begin(), scan.end(), cmp);
    
    ll ans = 0;
    for(int i = 0;i < (int)scan.size();i++)
    {
        if(scan[i].status == 1) upd(node_root, val_to_idx[scan[i].y], 1);
        else if(scan[i].status == 3) upd(node_root, val_to_idx[scan[i].y], -1);
        else
        {
            point a = scan[i];
            point b = scan[++i];
            ans = ans + qry(node_root, val_to_idx[a.y], val_to_idx[b.y]);
        }
    }
    printf(lld,ans);
    printf("\n");
    return 0;
}

void build(Node *root, ll left, ll right)
{
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    root->val = 0;
    if(root->left == root->right) return;
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
}
void upd(Node *root, ll pos, ll add)
{
    root->val = root->val + add;
    if(root->left == root->right) return;
    if(pos <= root->mid) upd(root->lc, pos, add);
    else upd(root->rc, pos, add);
}
ll qry(Node *root, ll left, ll right)
{
    if(left == root->left && right == root->right) return root->val;
    if(left > root->mid) return qry(root->rc, left, right);
    if(right <= root->mid) return qry(root->lc, left, right);
    return qry(root->lc, left, root->mid) + qry(root->rc, root->mid + 1, right);
}