#include <bits/stdc++.h>
using namespace std;
int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        int n, k;
        scanf("%d%d", &n, &k);
        int block = (n / k + (n % k != 0));
        if(block > k) printf("IMPOSSIBLE\n");
        else {
            for(int i = block - 1;i >= 0;i--) {
                for(int j = 1;j <= k;j++) {
                    if(j + (i * k) <= n) printf("%d ", j + (i * k));
                }
            }
            printf("\n");
        }
    }
    return 0;
}