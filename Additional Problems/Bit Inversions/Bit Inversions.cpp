#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
struct Node {
    int left, right, mid;
    int pre1, suf1, mx1;
    int pre0, suf0, mx0;
    bool all_one;
    bool all_zero;
    Node *lc, *rc;
    void pull() {
        all_one = lc->all_one && rc->all_one;
        
        if(lc->all_one) pre1 = lc->pre1 + rc->pre1;
        else pre1 = lc->pre1;

        if(rc->all_one) suf1 = lc->suf1 + rc->suf1;
        else suf1 = rc->suf1;

        mx1 = max({lc->mx1, rc->mx1, lc->suf1 + rc->pre1});        
        
        all_zero = lc->all_zero && rc->all_zero;
        
        if(lc->all_zero) pre0 = lc->pre0 + rc->pre0;
        else pre0 = lc->pre0;

        if(rc->all_zero) suf0 = lc->suf0 + rc->suf0;
        else suf0 = rc->suf0;

        mx0 = max({lc->mx0, rc->mx0, lc->suf0 + rc->pre0});
    }
}node[2 * N], *last_node = node, *root_node;
string s;
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        if(s[left] == '1') {
            r->all_one = true;
            r->mx1 = r->pre1 = r->suf1 = 1;
            r->all_zero = false;
            r->mx0 = r->pre0 = r->suf0 = 0;
        } else {
            r->all_one = false;
            r->mx1 = r->pre1 = r->suf1 = 0;
            r->all_zero = true;
            r->mx0 = r->pre0 = r->suf0 = 1;
        }
        return;
    }

    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
void upd(Node *r, int pos) {
    if(r->left == r->right) {
        if(s[pos] == '1') {
            s[pos] = '0';
            r->all_one = false;
            r->mx1 = r->pre1 = r->suf1 = 0;
            r->all_zero = true;
            r->mx0 = r->pre0 = r->suf0 = 1;
        } else {
            s[pos] = '1';
            r->all_one = true;
            r->mx1 = r->pre1 = r->suf1 = 1;
            r->all_zero = false;
            r->mx0 = r->pre0 = r->suf0 = 0;
        }
        return;
    }
    if(r->mid >= pos) upd(r->lc, pos);
    else upd(r->rc, pos);
    r->pull();
}
int qry(Node *r) {
    return max(r->mx1, r->mx0);
}
int main() {
    cin >> s;
    int n = (int)s.size();
    build(root_node = last_node++, 0, n - 1);
    int m;
    cin >> m;
    while(m--) {
        int x;
        cin >> x;
        x--;
        upd(root_node, x);
        cout << qry(root_node) << " ";
    }
    return 0;
}