#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
vector<int> adj[N];
vector<int> leaf;
vector<pair<int, int>> ans;
void DFS(int x, int p) {
    int child_cnt = 0;
    for(auto v : adj[x]) {
        if(v != p) {
            DFS(v, x);
            child_cnt++;
        }
    }
    if(child_cnt == 0 || (p == -1 && child_cnt == 1)) {
        leaf.push_back(x);
    }
}
int main() {
    int n; scanf("%d", &n);
    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1, -1);

    int sz = (int)leaf.size();
    for(int i = 0;i < ((int)leaf.size() + 1) / 2;i++) {
        ans.emplace_back(leaf[i], leaf[sz / 2 + i]);
    }

    printf("%d\n", (int)ans.size());
    for(auto [a, b] : ans) {
        printf("%d %d\n", a, b);
    }
    return 0;
}