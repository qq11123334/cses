#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int MOD = (ll)1e9 + 7;
const int N = 1005;
ll dp[N][N][2];
// dp[i][j][k] = the number of permutation which has
// i numbers
// j pairs are adjacent and whose difference is 1
// k = 0, (i) and (i - 1) are not adjacent
// k = 1, (i) and (i - 1) are adjacent
int n;
int main() {
    cin >> n;
    dp[1][0][0] = 1;
    for(int i = 2;i <= n;i++) {
        for(int j = 0;j <= i - 1;j++) {
            // calculate dp[i][j][0]
            dp[i][j][0] += (j + 1) * dp[i - 1][j + 1][0];   // split a pair from dp[i - 1][*][0]
            dp[i][j][0] += (i - j - 2) * dp[i - 1][j][0];   // no split a pair from dp[i - 1][*][0]
            dp[i][j][0] += (j) * dp[i - 1][j + 1][1];       // split a pair from dp[i - 1][*][1]
            dp[i][j][0] += (i - j - 1) * dp[i - 1][j][1];   // no split a pair from dp[i - 1][*][1]

            // calculate dp[i][j][1]
            dp[i][j][1] += dp[i - 1][j][1];                 // split a pair and next to (i - 1) from dp[i - 1][*][1] 
            if(j) dp[i][j][1] += dp[i - 1][j - 1][1];       // no split a pair and next to (i - 1) from dp[i - 1][*][1]
            if(j) dp[i][j][1] += 2 * dp[i - 1][j - 1][0];   // next to (i - 1) from dp[i - 1][*][0] 

            dp[i][j][0] %= MOD;
            dp[i][j][1] %= MOD;
        }
    }
    cout << dp[n][0][0];
    return 0;
}