#include <bits/stdc++.h>
#define ll long long int
using namespace std;
vector<int> Per;
int main() {
    int n;
    scanf("%d", &n);
    Per.clear();
    for(int i = 1;i <= n;i++) {
        Per.push_back(i);
    }

    ll ans = 0;
    do {
        bool isValid = 1;
        for(int i = 0;i < n - 1;i++) {
            if(abs(Per[i] - Per[i + 1]) == 1) {
                isValid = 0;
            }
        }
        ans += isValid;
    } while(next_permutation(Per.begin(), Per.end()));

    printf("%lld\n", ans);
    return 0;
}

/*

*/