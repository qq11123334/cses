#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int arr[N], pos[N], n;
vector<pair<int, int>> ans[2];
bool vis[N];
void my_swap(int a, int b) {
	int pos_a = pos[a], pos_b = pos[b];
	arr[pos_a] = b;
	arr[pos_b] = a;
	pos[a] = pos_b;
	pos[b] = pos_a;
}
void DFS(int cur) {
	if(arr[cur] != cur && arr[cur] != pos[cur]) {
		int next = pos[cur];
		ans[0].emplace_back(pos[cur], pos[arr[arr[cur]]]);
		my_swap(cur, arr[arr[cur]]);
		DFS(next);
	}
}
void print() {
	for(int i = 1; i <= n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int main() {
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%d", &arr[i]);
		pos[arr[i]] = i;
	}
	
	for(int i = 1; i <= n; i++) {
		DFS(i);
	}

	for(int i = 1; i <= n; i++) {
		if(arr[i] != i) {
			ans[1].emplace_back(i, arr[i]);
			my_swap(i, arr[i]);
		}
	}

	int rnd = (ans[0].size() != 0LL) + (ans[1].size() != 0LL);
	printf("%d\n", rnd);
	for(int i = 0; i < 2; i++) {
		if(ans[i].size() == 0LL) continue;
		printf("%d\n", (int)ans[i].size());
		for(auto P : ans[i]) {
			printf("%d %d\n", P.first, P.second);
		}
	}
	return 0;
}
