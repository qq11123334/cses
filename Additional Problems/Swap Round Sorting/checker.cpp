#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int arr[N], n, t;
void print_arr() {
	for(int i = 1; i <= n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int main() {
	ifstream input, output;
	input.open("input.txt");
	output.open("output.txt");
	input >> n;
	for(int i = 1; i <= n; i++) {
		input >> arr[i];
	}

	output >> t;

	bool same_pos = false;
	int pos = -1;
	for(int i = 1; i <= t; i++) {
		int pair;
		output >> pair;
		set<int> s;
		for(int j = 1; j <= pair; j++) {
			int a, b;
			output >> a >> b;
			same_pos |= s.count(a);
			same_pos |= s.count(b);
			if(s.count(a)) pos = a;
			if(s.count(b)) pos = b;
			s.insert(a);
			s.insert(b);
			swap(arr[a], arr[b]);
		}
		if(n <= 100) {
			printf("after %d-th round: \n", i);
			print_arr();
		}
	}

	bool sorted = true;
	for(int i = 1;i <= n; i++) {
		if(arr[i] != i) {
			sorted = false;
		}
	}

	if(!sorted) printf("didn't sort successful\n");
	if(same_pos) {
		printf("same pos at same round\n");
		printf("%d\n", pos);
	}
	
	return 0;
}
