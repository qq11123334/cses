#include <bits/stdc++.h>
using namespace std;

bool fail = false;
string ans = string(100, '0');
string gen(int one, int zero) {
    string s;
    while(one || zero) {
        if(s.size() > ans.size() || one < 0 || zero < 0) {
            fail = true;
            return "";
        }
        if(one > zero) {
            s += "1";
            one -= (zero + 1);
        } else {
            s += "0";
            zero -= (one + 1);
        }
    }

    reverse(s.begin(), s.end());
    return s;
}
int main() {
    int n;
    cin >> n;
    for(int i = 0; i <= n; i++) {
        fail = false;
        string s = gen(i, n - i);
        if(fail) continue;
        if(ans.size() > s.size()) {
            ans = s;
        }
    }

    cout << ans << "\n";
    return 0;
}
