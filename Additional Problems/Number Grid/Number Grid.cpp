#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll upper_log2(ll x) {
    bool plus = 0;
    ll cnt = 0;
    while(x > 1) {
        if(x % 2) plus = 1;
        x /= 2;
        cnt++;
    }
    return cnt + plus;
}
ll solve(ll x, ll y) {
    // cout << x << " " << y << endl;
    if(x == 1 && y == 1) return 0;
    ll upper_pow2 = (1 << upper_log2(max(x, y)));
    if(upper_pow2 / 2 < x && upper_pow2 / 2 >= y) return solve(x - upper_pow2 / 2, y) + upper_pow2 / 2;
    if(upper_pow2 / 2 < x && upper_pow2 / 2 < y) return solve(x - upper_pow2 / 2, y - upper_pow2 / 2);
    if(upper_pow2 / 2 >= x && upper_pow2 / 2 < y) return solve(x, y - upper_pow2 / 2) + upper_pow2 / 2;
}
int main() {
    ll x, y; 
    cin >> x >> y;
    cout << solve(x, y) << endl;
    return 0;
}