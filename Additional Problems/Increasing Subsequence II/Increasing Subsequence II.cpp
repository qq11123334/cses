#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
ll arr[N], dp[N];
int n;
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
    }

    ll ans = 0;
    for(int i = 1;i <= n;i++) {
        dp[i] = 1;
        for(int j = 1;j < i;j++) {
            if(arr[j] < arr[i]) {
                dp[i] += dp[j];
            }
        }
        ans += dp[i];
    }

    printf("%lld\n", ans);
    return 0;
}