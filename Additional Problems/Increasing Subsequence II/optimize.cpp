#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
using namespace std;
const int N = 200005;
ll arr[N], dp[N], BIT[N];
int n;
void upd(int pos, ll val) {
    for(int i = pos;i <= n;i += (i & -i)) {
        BIT[i] += val;
        BIT[i] %= MOD;
    }
}
ll qry(int pos) {
    ll res = 0;
    for(int i = pos;i >= 1;i -= (i & -i)) {
        res += BIT[i];
        res %= MOD;
    }
    return res;
}
map<ll, ll> compress;
vector<ll> id_to_cor;
void reindex() {
    for(auto &x : compress) {
        x.second = id_to_cor.size();
        id_to_cor.push_back(x.first);
    }
}
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &arr[i]);
        compress[arr[i]];
    }

    reindex();

    ll ans = 0;
    for(int i = 1;i <= n;i++) {
        dp[i] = 1 + qry(compress[arr[i]]);
        upd(compress[arr[i]] + 1, dp[i]);
        ans += dp[i];
        ans %= MOD;
    }

    printf("%lld\n", ans);
    return 0;
}