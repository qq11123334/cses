#include <bits/stdc++.h>
#define ROW 1
#define COL 2
using namespace std;
const int N = 205;
int match[N];
bool used[N];
vector<int> adj[N];
vector<int> adj_new[N];
bool DFS(int x) {
    for(auto u : adj[x]) {
        if(used[u]) continue;
        used[u] = 1;
        int next = match[u];
        if(next == -1 || DFS(next)) {
            match[u] = x;
            return 1;
        }
    }
    return 0;
}
bool vis[N];
int n;
int number(int x, int type) {
    if(type == ROW) return x;
    if(type == COL) return x + n;
}
void DFS_new(int x) {
    vis[x] = 1;
    for(auto v : adj_new[x]) {
        if(!vis[v]) DFS_new(v);
    }
}
int Bipartite_Match() {
    memset(match, -1, sizeof(match));
    int match_number = 0;
    for(int i = 1;i <= n;i++) {
        memset(used, 0, sizeof(used));
        match_number += DFS(i);
    }
    return match_number;
}
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            char c; scanf(" %c", &c);
            if(c == 'o') {
                adj[i].push_back(j);
            }
        }
    }
    printf("%d\n", Bipartite_Match());

    for(int i = 1;i <= n;i++) {
        for(auto u : adj[i]) {
            if(match[u] != i) {
                adj_new[i].push_back(number(u, COL));
            } else {
                adj_new[number(u, COL)].push_back(i);
            }
        }
    }

    memset(used, 0, sizeof(used));

    for(int i = 1;i <= n;i++) {
        if(match[i] != -1) used[match[i]] = 1;
    }

    for(int i = 1;i <= n;i++) {
        if(!used[i]) DFS_new(i);
    }

    for(int i = 1;i <= n;i++) {
        if(!vis[i]) printf("1 %d\n", i);
        if(vis[number(i, COL)]) printf("2 %d\n", i);
    }
    return 0;
}