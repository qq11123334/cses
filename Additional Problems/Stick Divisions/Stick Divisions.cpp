#include <bits/stdc++.h>
#define ll long long int
using namespace std;
priority_queue<ll, vector<ll>, greater<ll>> pq;
int main() {
    ll x, n;
    scanf("%lld %lld", &x, &n);
    for(int i = 0;i < n;i++) {
        ll stick;
        scanf("%lld", &stick);
        pq.push(stick);
    }

    ll ans = 0;
    while(pq.size() > 1) {
        ll st1 = pq.top(); pq.pop();
        ll st2 = pq.top(); pq.pop();
        ans += (st1 + st2);
        pq.push(st1 + st2);
    }
    printf("%lld\n", ans);
    return 0;
}