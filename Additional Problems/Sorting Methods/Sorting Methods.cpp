#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
int arr[N], BIT[N], arr2[N], pos2[N];
int n, mx_pos; 
void upd(int x, int dif) {
    for(int i = x;i <= n;i += (i & -i)) {
        BIT[i] += dif;
    }
}
int sum(int x) {
    int res = 0;
    for(int i = x;i >= 1;i -= (i & -i)) {
        res += BIT[i];
    }
    return res;
}
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &arr[i]);
        arr2[i] = arr[i];
        pos2[arr2[i]] = i;
    }

    ll ans1 = 0, ans2 = 0, ans3 = 0, ans4 = 0;

    for(int i = 1;i <= n;i++) {
        ans1 += (arr[i] - i);
        ans1 += (sum(n) - sum(arr[i]));
        upd(arr[i], 1);
    }

    for(int i = 1;i <= n;i++) {
        if(arr2[i] != i) {
            swap(arr2[i], arr2[pos2[i]]);
            pos2[arr2[pos2[i]]] = pos2[i];
            pos2[i] = i;
            ans2++;
        }
    }

    vector<int> LIS;
    for(int i = 1;i <= n;i++) {
        auto it = lower_bound(LIS.begin(), LIS.end(), arr[i]);
        if(it == LIS.end()) {
            LIS.push_back(arr[i]);
        } else {
            *it = arr[i];
        }
    }
    ans3 = n - (int)LIS.size();

    int tar_val = n;
    int cnt_dec = 0;
    for(int i = n;i > 0;i--) {
        if(arr[i] == tar_val) {
            cnt_dec++;
            tar_val--;
        }
    }
    ans4 = n - cnt_dec;

    printf("%lld %lld %lld %lld\n", ans1, ans2, ans3, ans4);
    return 0;
}