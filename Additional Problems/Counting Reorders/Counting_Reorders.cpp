#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll MOD = 1e9 + 7;
const int N = 10005, W = 26;
ll fact[N], ifact[N];
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD;
    }
    return res;
}
ll inv(ll x) {
    return fp(x, MOD - 2);
}
void build_fact() {
    fact[0] = 1;
    ifact[0] = 1;
    for(int i = 1; i < N; i++) {
        fact[i] = fact[i - 1] * i % MOD;
        ifact[i] = inv(fact[i]);
    }
}

ll C(ll a, ll b) {
    return fact[a] * ifact[b] % MOD * ifact[a - b] % MOD;
}

ll H(ll a, ll b) {
    return C(a + b - 1, b);
}

ll cnt[W];
ll dp[W + 1][N];
int main() {
    build_fact();

    string s;
    cin >> s;

    int n = (int)s.size();
    for(auto c : s) {
        cnt[c - 'a' + 1]++;
    }

    dp[0][0] = 1;
    int sum = 0;
    for(int i = 1; i <= 26; i++) {
        if(cnt[i]) {
            for(int j = 0; j <= sum; j++) {
                for(int k = 1; k <= cnt[i]; k++) {
                    dp[i][j + k] += dp[i - 1][j] * H(j + 1, k) % MOD * H(k, cnt[i] - k) % MOD;
                    dp[i][j + k] %= MOD;
                }
            }
        } else {
            for(int j = 0; j <= n; j++) {
                dp[i][j] = dp[i - 1][j];
            }
        }
        sum += cnt[i];
    }

    ll ans = 0;

    for(int j = 1; j <= n; j++) {
        ans += dp[26][j] * fp(-1 + MOD, (n - j));
        ans %= MOD;
    }

    cout << ans << "\n";
}
