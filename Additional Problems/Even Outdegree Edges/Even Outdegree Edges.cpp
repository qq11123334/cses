#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> Edge;
const int N = 100005;
vector<int> adj[N];
vector<Edge> ans;
int dep[N];
bool vis[N];
bool DFS_isEven(int x, int par, int level) {
    int out_degree = 0;
    dep[x] = level;
    for(auto v : adj[x]) {
        if(par == v) continue;
        if(!dep[v]) {
            if(DFS_isEven(v, x, level + 1)) {
                out_degree++;
                ans.push_back(Edge(x, v));
            } else {
                ans.push_back(Edge(v, x));
            }
        } else if(dep[x] > dep[v]) {
            ans.push_back(Edge(x, v));
            out_degree++;
        }
    }
    return out_degree % 2 == 0;
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    bool is_able = 1;
    for(int i = 1;i <= n;i++) {
        if(!vis[i]) {
            is_able &= DFS_isEven(i, -1, 1);
        }
    }

    if(is_able) {
        for(Edge e : ans) {
            printf("%d %d\n", e.first, e.second);
        }
    } else {
        printf("IMPOSSIBLE\n");
    }
    return 0;
}