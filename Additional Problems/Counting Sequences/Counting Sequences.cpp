#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
using namespace std;
const int N = 1000005;
ll fact[N], ifact[N];
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y % 2) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD;
    }
    return res;
}
ll find_modRev(ll x) {
    return fp(x, MOD - 2);
}
void build_fact(int n) {
    fact[0] = 1;
    for(int i = 1;i <= n;i++) {
        fact[i] = fact[i - 1] * i % MOD;
    }
    for(int i = 0;i <= n;i++) {
        ifact[i] = find_modRev(fact[i]);
    }
}
ll C(ll a, ll b) {
    return (fact[a] * ifact[b] % MOD * ifact[a - b]) % MOD;
}
int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    build_fact(n);
    ll ans = 0;
    for(int i = 0;i < k;i++) {
        int inv;
        if(i % 2 == 0) inv = 1;
        else inv = -1;
        ans += (inv * (C(k, i) * fp(k - i, n))) % MOD;
        ans %= MOD;
    }
    printf("%lld\n", (ans + MOD) % MOD);
    return 0;
}