#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
vector<int> adj[N];
bool vis[N];
void DFS(int x, int &sz) {
    vis[x] = 1; sz++;
    for(auto v : adj[x]) {
        if(!vis[v]) DFS(v, sz);
    }
}
map<int, int> SZ;
vector<int> item;
bitset<N> is_able;
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++) {
        int sz = 0;
        if(!vis[i]) {
            DFS(i, sz);
            SZ[sz]++;
        }
    }

    for(auto &P : SZ) {
        for(int k = 1;k <= P.second;k *= 2) {
            item.push_back(k * P.first);
            P.second -= k;
        }
        if(P.second) {
            item.push_back(P.second * P.first);
        }
    }

    is_able[0] = 1;
    for(auto it : item) {
        is_able |= is_able << it;
    }

    for(int i = 1;i <= n;i++) {
        printf("%d", (int)is_able[i]);
    }
    return 0;
}