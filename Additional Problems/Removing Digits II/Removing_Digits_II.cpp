#include <bits/stdc++.h>
using namespace std;
using ll = long long int;

ll dp[20][10][10];
int state[20][10][10];
// (XXX...)(999...)?
// i = how many 9's
// j = max(X,X,X, ...)
// k = ?

void build_dp() {
    // i = 0

    dp[0][0][0] = 0;
    state[0][0][0] = 0;
    for(int k = 1; k < 10; k++) {
	dp[0][0][k] = 1;
	state[0][0][k] = 0;
    }

    for(int j = 1; j < 10; j++) {
	for(int k = 0; k < 10; k++) {
	    if(j > k) {
		dp[0][j][k] = 1;
		state[0][j][k] = k - j + 10;
	    } else {
		dp[0][j][k] = 2;
		state[0][j][k] = 10 - j;
	    }
	}
    }

    for(int i = 1; i < 20; i++) {
	for(int j = 0; j < 10; j++) {
	    for(int k = 0; k < 10; k++) {
		int digit = k;
		for(int l = 9; l >= 0; l--) {
		    dp[i][j][k] += dp[i - 1][max(j, l)][digit];
		    digit = state[i - 1][max(j, l)][digit];
		}
		state[i][j][k] = digit;
	    }
	}
    }
}

int number[20];
int main() {
    string s;
    cin >> s;
    
    build_dp();

    int n = (int)s.size();
    if(n == 1) {
	printf("1\n");
	exit(0);
    }

    for(int i = 0; i < n; i++) {
	number[i] = (s[i] - '0');
    }
    ll ans = 0;
    int digit = number[n - 1];
    for(int d = n - 2; d >= 0; d--) {
	if(number[d] == 9) continue;
	int pre_max = 0;
	for(int i = 0; i < d; i++) {
	    pre_max = max(pre_max, number[i]);
	}
	
	for(int j = number[d]; j >= 0; j--) {
	    int i = (n - d - 2);
	    ans += dp[i][max(pre_max, j)][digit];
	    digit = state[i][max(pre_max, j)][digit];
	}
	
	number[d] = 9;
	for(int i = d - 1; i >= 0; i--) {
	    if(number[i]) {
		number[i]--;
		break;
	    } else {
		number[i] = 9;
	    }
	}
    }

    cout << ans << endl;
}
