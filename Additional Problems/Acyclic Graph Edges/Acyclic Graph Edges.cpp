#include <bits/stdc++.h>
using namespace std;
struct Edge {
    int a, b;
    Edge(){}
    Edge(int _a, int _b): a(_a), b(_b){}
};
vector<Edge> edge;
vector<int> adj[100005];
int depth[100005];
bool vis[100005];
void DFS(int x, int par) {
    vis[x] = 1;
    depth[x] = ~par ? depth[par] + 1 : 0;
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS(v, x);
        }
    }
} 
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
        edge.push_back(Edge(a, b));
    }

    for(int i = 1;i <= n;i++) {
        if(!vis[i]) {
            DFS(i, -1);
        }
    }

    for(auto e : edge) {
        if(depth[e.a] >= depth[e.b]) {
            printf("%d %d\n", e.a, e.b);
        } else {
            printf("%d %d\n", e.b, e.a);
        }
    }
    return 0;
}