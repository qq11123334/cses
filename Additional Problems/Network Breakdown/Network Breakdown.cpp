#include <bits/stdc++.h>
#define pii pair<int, int>
using namespace std;
const int N = 100005;
int p[N], sz[N];
void init() {
    for(int i = 0;i < N;i++) {
        p[i] = i, sz[i] = 1;
    }
}
int find(int x) {
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
bool unite(int x, int y) {
    x = find(x);
    y = find(y);
    if(x == y) return 0;
    if(sz[x] > sz[y]) swap(x, y);
    p[x] = p[y];
    sz[x] += sz[y];
    return 1;
}
map<pii, int> edge_list;
vector<pii> query_list;
int main() {
    init();
    int n, m, k;
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0;i < m;i++) {
        pii edge;
        scanf("%d%d", &edge.first, &edge.second);
        if(edge.first > edge.second) swap(edge.first, edge.second);
        edge_list[edge]++;
    }
    for(int i = 0;i < k;i++) {
        pii edge;
        scanf("%d%d", &edge.first, &edge.second);
        if(edge.first > edge.second) swap(edge.first, edge.second);
        query_list.push_back(edge);
        edge_list[edge]--;
    }
    reverse(query_list.begin(), query_list.end());

    int sz = n;
    for(auto P : edge_list) {
        pii edge = P.first;
        if(P.second) sz -= unite(edge.first, edge.second);
    }

    vector<int> ans;
    for(auto edge : query_list) {
        ans.push_back(sz);
        sz -= unite(edge.first, edge.second);
    }

    reverse(ans.begin(), ans.end());

    for(auto i : ans) {
        printf("%d ", i);
    }
    return 0;
}