#include <bits/stdc++.h>
using namespace std;
const int N = 200005, K = 18, M = 1000005;
const int INF = 1e9;
int n, q;
struct Event {
	int start, end;
}event[N];
bool cmp(Event a, Event b) {
	return a.end < b.end;
}
int nxt[N][K], start_event[M];
int main() {
	scanf("%d%d", &n, &q);
	for(int i = 0; i < n; i++) {
		scanf("%d %d", &event[i].start, &event[i].end);
	}

	event[n].start = INF;
	event[n].end = INF;
	
	sort(event, event + n, cmp);

	memset(start_event, -1, sizeof(start_event));

	for(int i = 0; i < n; i++) {
		int start = event[i].start, end = event[i].end;
		if(start_event[start] == -1 || event[start_event[start]].end > end) {
			start_event[start] = i;
		}
	}

	for(int i = M - 2; i >= 1; i--) {
		int pre_event = start_event[i + 1];
		int cur_event = start_event[i];
		if(cur_event == -1 || (pre_event != -1 && event[cur_event].end > event[pre_event].end)) {
			start_event[i] = pre_event;
		}
	}
	
	// two pointer
	int right = 0;
	for(int left = 0; left < n; left++) {
		while(event[left].end > event[right].start) {
			right++;
		}
		nxt[left][0] = right;
	}

	nxt[n][0] = n;

	for(int k = 1; k < K; k++) {
		for(int i = 0; i <= n; i++) {
			nxt[i][k] = nxt[nxt[i][k - 1]][k - 1];
		}
	}

	while(q--) {
		int s, e;
		scanf("%d%d", &s, &e);

		if(start_event[s] == -1) {
			printf("0\n");
			continue;
		}

		int ans = 0;
		int cur_event = start_event[s];
		if(event[cur_event].end > e) {
			printf("0\n");
			continue;
		}
		for(int k = K - 1; k >= 0; k--) {
			if(event[nxt[cur_event][k]].end <= e) {
				cur_event = nxt[cur_event][k];
				ans += (1 << k);
			}
		}
		printf("%d\n", ans + 1);
	}
	return 0;
}
