#include <bits/stdc++.h>
#define ll long long int
#define pii pair<ll, ll>
using namespace std;
struct Node {
    int left, right;
    int mid;
    ll mi, pos;
    Node *lc, *rc;
    void pull() {
        if(lc->mi < rc->mi) {
            mi = lc->mi;
            pos = lc->pos;
        } else {
            mi = rc->mi;
            pos = rc->pos;
        }
    }
}node[400500], *last_node = node, *root_node;
ll arr[200005];
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->mi = arr[left];
        r->pos = left;
        return;
    }

    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);

    r->pull();
}
pii qry(Node *r, int left, int right) {
    if(r->left == left && r->right == right) {
        return make_pair(r->mi, r->pos);
    }

    if(right <= r->mid) {
        return qry(r->lc, left, right);
    } else if(left > r->mid) {
        return qry(r->rc, left, right);
    } else {
        pii L = qry(r->lc, left, r->mid);
        pii R = qry(r->rc, r->mid + 1, right);
        return min(L, R);
    }
}
ll solve(ll left, ll right) {
    if(left > right) return 0;
    pii P = qry(root_node, left, right);
    return max({(right - left + 1) * P.first, solve(left, P.second - 1), solve(P.second + 1, right)});
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &arr[i]);
    }

    build(root_node = last_node++, 1, n);

    printf("%lld\n", solve(1, n));
    return 0;
}