#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
vector<ll> area(N, 0);
stack<int> stak;
ll height[N];
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &height[i]);
    }

    for(int i = 0;i < n;i++) {
        while(!stak.empty() && height[stak.top()] >= height[i]) {
            stak.pop();
        }
        ll width = (stak.empty() ? (i + 1) : (i - stak.top()));
        area[i] += width * height[i];
        // printf("width[%d] = %lld height[%d] = %lld\n", i, width, i, height[i]);
        stak.push(i);
    }

    while(!stak.empty()) stak.pop();

    for(int i = n - 1;i >= 0;i--) {
        while(!stak.empty() && height[stak.top()] >= height[i]) {
            stak.pop();
        }
        ll width = (stak.empty() ? (n - i) : (stak.top() - i));
        // printf("width[%d] = %lld height[%d] = %lld\n", i, width, i, height[i]);
        area[i] += width * height[i];
        stak.push(i);
    }

    for(int i = 0;i < n;i++) {
        area[i] -= height[i];
        // printf("area[%d] = %lld\n", i, area[i]);
    }

    printf("%lld\n", *max_element(area.begin(), area.end()));
    return 0;
}