#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
#define pll pair<ll, ll>
using namespace std;
ll arr[200005];
struct Node {
    int left, right, mid;
    ll mi;
    int pos;
    Node *lc, *rc;
    void pull() {
        if(lc->mi < rc->mi) pos = lc->pos;
        else pos = rc->pos;
        mi = min(lc->mi, rc->mi);
    }
}node[500500], *last_node = node, *root_node;
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->mi = arr[left];
        r->pos = left;
        return;
    }
    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
pll qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return make_pair(INF, -1);
    if(ql <= r->left && r->right <= qr) return make_pair(r->mi, r->pos);
    return min(qry(r->lc, ql, qr), qry(r->rc, ql, qr));
}
ll solve(int left, int right) {
    if(left > right) return 0;
    pll P = qry(root_node, left, right);
    return max({P.first * (right - left + 1), solve(left, P.second - 1), solve(P.second + 1, right)});
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n;
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &arr[i]);
    }

    build(root_node = last_node++, 0, n - 1);
    printf("%lld\n", solve(0, n - 1));
    return 0;
}