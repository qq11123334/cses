#include <bits/stdc++.h>
#define ll long long int
const int N = 1000005;
using namespace std;
ll ans[N];
int main() {
    ll n, k;
    cin >> n >> k;
    
    int last_num = 1;
    for(int i = n - 1;i >= 0;i--) {
        if(k < i) break;
        k -= i;
        ans[i] = last_num++;
    }
    if(!ans[k]) ans[k] = last_num++;
    for(int i = 0;i < n;i++) {
        if(!ans[i]) ans[i] = last_num++;
    }

    for(int i = 0;i < n;i++) {
        printf("%lld ", ans[i]);
    }

    return 0;
}