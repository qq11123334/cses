#include <bits/stdc++.h>
using namespace std;
struct Edge {
    int a, b;
    Edge(){}
    Edge(int _a, int _b): a(_a), b(_b){}
};
const int N = 100005;
vector<Edge> edge;
vector<int> adj[N];
int depth[N];
bool vis[N];
void DFS(int x, int par) {
    vis[x] = 1;
    depth[x] = ~par ? depth[par] + 1 : 0;
    for(auto v : adj[x]) {
        if(!vis[v]) {
            edge.push_back(Edge(x, v));
            DFS(v, x);
        } else if(v != par) {
            if(depth[x] >= depth[v]) edge.push_back(Edge(x, v));
        }
    }
}
int n, m;
vector<int> adj_dir[N], RevAdj[N], ord;
int SCC_id[N];
bool vis_SCC[N];
void add_edge(int a, int b) {
    adj_dir[a].push_back(b);
    RevAdj[b].push_back(a);
}
void DFS_SCC(int x, int SCC) {
    SCC_id[x] = SCC;
    for(auto v : adj_dir[x]) {
        if(!SCC_id[v]) DFS_SCC(v, SCC);
    }
}
void RevDFS(int x) {
    vis_SCC[x] = 1;
    for(auto v : RevAdj[x]) {
        if(!vis_SCC[v]) RevDFS(v);
    }
    ord.push_back(x);
}
void Kosaraju() {
    for(int i = 1;i <= n;i++) {
        if(!vis_SCC[i]) RevDFS(i);
    }
    int SCC = 1;
    for(int i = (int)ord.size() - 1;i >= 0;i--) {
        int x = ord[i];
        if(!SCC_id[x]) DFS_SCC(x, SCC++);
    }
}
int main() {
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++) {
        if(!vis[i]) {
            DFS(i, -1);
        }
    }

    for(auto e : edge) {
        add_edge(e.a, e.b);
    }

    Kosaraju();

    bool is_SCC = 1;
    for(int i = 2;i <= n;i++) {
        if(SCC_id[1] != SCC_id[i])
            is_SCC = 0;
    }

    if(is_SCC) {
        for(auto e : edge) {
            printf("%d %d\n", e.a, e.b);
        }
    }
    else {
        printf("IMPOSSIBLE\n");
    }
    return 0;
}