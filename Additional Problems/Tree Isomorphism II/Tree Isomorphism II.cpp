#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 100500;
struct Node {
    int deg;
    ll deg_sum;
    vector<int> adj_deg;
    vector<int> adj;
    vector<ll> adj_deg_sum;
    bool operator < (const Node a) const {
        if(deg != a.deg) return deg < a.deg;
        else if(adj_deg != a.adj_deg) return adj_deg < a.adj_deg;
        else return adj_deg_sum < a.adj_deg_sum;
    }
    bool operator == (const Node a) const {
        return (deg == a.deg && adj_deg == a.adj_deg && adj_deg_sum == a.adj_deg_sum);
    }
    bool operator != (const Node a) const {
        return !(*this == a);
    }
}node1[N], node2[N];
void init(int n) {
    for(int i = 0;i <= n;i++) {
        node1[i].deg = 0;
        node1[i].deg_sum = 0;
        node1[i].adj_deg.clear();
        node1[i].adj.clear();
        node1[i].adj_deg_sum.clear();
        node2[i].deg = 0;
        node2[i].deg_sum = 0;
        node2[i].adj_deg.clear();
        node2[i].adj.clear();
        node2[i].adj_deg_sum.clear();
    }
}
void solve() {
    int n; cin >> n;
    init(n);
    for(int i = 0;i < n - 1;i++) {
        int a, b; cin >> a >> b;
        node1[a].deg++;
        node1[b].deg++;
        node1[a].adj.push_back(b);
        node1[b].adj.push_back(a);
    }
    for(int i = 0;i < n - 1;i++) {
        int a, b; cin >> a >> b;
        node2[a].deg++;
        node2[b].deg++;
        node2[a].adj.push_back(b);
        node2[b].adj.push_back(a);
    }

    for(int i = 1;i <= n;i++) {
        for(auto v : node1[i].adj) {
            node1[i].adj_deg.push_back(node1[v].deg);
            node1[i].deg_sum += node1[v].deg;
        }
        sort(node1[i].adj_deg.begin(), node1[i].adj_deg.end());
        for(auto v : node2[i].adj) {
            node2[i].adj_deg.push_back(node2[v].deg);
            node2[i].deg_sum += node2[v].deg;
        }
        sort(node2[i].adj_deg.begin(), node2[i].adj_deg.end());
    }    
    
    for(int i = 1;i <= n;i++) {
        for(auto v : node1[i].adj) {
            node1[i].adj_deg_sum.push_back(node1[v].deg_sum);
        }
        sort(node1[i].adj_deg_sum.begin(), node1[i].adj_deg_sum.end());
        for(auto v : node2[i].adj) {
            node2[i].adj_deg_sum.push_back(node2[v].deg_sum);
        }
        sort(node2[i].adj_deg_sum.begin(), node2[i].adj_deg_sum.end());
    }

    sort(node1 + 1, node1 + 1 + n);
    sort(node2 + 1, node2 + 1 + n);
    bool is_Iso = 1;
    for(int i = 1;i <= n;i++) {
        if(node1[i] != node2[i]) is_Iso = 0;
    }
    cout << (is_Iso ? "YES\n" : "NO\n");
}
int main() {
    ios_base::sync_with_stdio(0), cin.tie(0);
    vector<int> a, b;
    int T; cin >> T;
    while(T--) {
        solve();
    }
}