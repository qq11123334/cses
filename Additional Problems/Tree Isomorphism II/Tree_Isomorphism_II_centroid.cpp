#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
vector<int> adj[2][N];
int n;

int max_sz[N], sz[N];
void DFS_sz(int i, int x, int p = -1) {
    sz[x] = 1;
    for(auto v : adj[i][x]) {
        if(v == p) continue;
        DFS_sz(i, v, x);
        sz[x] += sz[v];
        max_sz[x] = max(max_sz[x], sz[v]);
    }
    max_sz[x] = max(max_sz[x], n - sz[x]);
}

vector<int> find_centroid(int i) {
    for(int j = 1; j <= n; j++) {
        max_sz[j] = 0;
    }


    DFS_sz(i, 1);
    vector<int> centroids;
    for(int j = 1; j <= n; j++) {
        if(max_sz[j] <= n / 2) {
            centroids.push_back(j);
        }
    }
    return centroids;
}

map<vector<int>, int> id;
int get_id(vector<int> &v) {
    static int indx = 1;
    if(!id[v]) return id[v] = indx++;
    else return id[v];
}

int DFS(int i, int x, int p = -1) {
    vector<int> form;
    for(auto v : adj[i][x]) {
        if(v == p) continue;
        form.push_back(DFS(i, v, x));
    }
    sort(form.begin(), form.end());
    return get_id(form);
}

void init() {
    for(int i = 0; i <= n; i++) {
        adj[0][i].clear();
        adj[1][i].clear();
    }
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;
    while(t--) {
        cin >> n;
        init();
        for(int i = 0; i < 2; i++) {
            for(int j = 0; j < n - 1; j++) {
                int a, b;
                cin >> a >> b;
                adj[i][a].push_back(b);
                adj[i][b].push_back(a);
            }
        }

        vector<int> centroid[2] = {find_centroid(0), find_centroid(1)};

        set<int> forms;
        for(auto x : centroid[0]) {
            forms.insert(DFS(0, x));
        }

        bool isIsomorphism = false;
        for(auto x : centroid[1]) {
            int form = DFS(1, x);
            if(forms.find(form) != forms.end()) {
                isIsomorphism = true;
            }
        }

        if(isIsomorphism) cout << "YES\n";
        else cout << "NO\n";
    }
}
