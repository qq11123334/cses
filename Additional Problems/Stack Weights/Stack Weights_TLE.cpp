#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int cnt[N][2];
int main() {
	int n;
	scanf("%d", &n);
	for(int i = 0; i < n; i++) {
		int coin, stak;
		scanf("%d%d", &coin, &stak);
		stak--;
		cnt[coin][stak]++;
		int pre[2] = {0};
		bool gt = false, st = false;
		for(int j = n; j >= 1; j--) {
			pre[0] += cnt[j][0];
			pre[1] += cnt[j][1];
			if(pre[0] > pre[1]) gt = true;
			if(pre[0] < pre[1]) st = true;
		}

		if(gt && st) printf("?\n");
		else if(gt) printf(">\n");
		else if(st) printf("<\n");
		else assert(0);
	}
	return 0;
}