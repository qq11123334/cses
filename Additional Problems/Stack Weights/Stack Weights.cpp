#include <bits/stdc++.h>
using namespace std;
struct Node {
	int left, right;
	int mi, mx, dif;
	Node *lc, *rc;
	void pull() {
		mi = min(lc->mi + lc->dif, rc->mi + rc->dif);
		mx = max(lc->mx + lc->dif, rc->mx + rc->dif);
	}
}*root;
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	r->mi = 0, r->mx = 0, r->dif = 0;
	if(L == R) return;
	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
}
void upd(Node *r, int ql, int qr, int dif) {
	if(ql > r->right || qr < r->left) return;
	if(ql <= r->left && r->right <= qr) {
		r->dif += dif;
		return;
	}	
	upd(r->lc, ql, qr, dif);
	upd(r->rc, ql, qr, dif);
	r->pull();
}
int qry_mi() {
	return root->mi + root->dif;
}
int qry_mx() {
	return root->mx + root->dif;
}
int main() {
	int n;
	scanf("%d", &n);
	build(root = new Node(), 1, n);
	for(int i = 0; i < n; i++) {
		int coin, stak;
		scanf("%d%d", &coin, &stak);
		if(stak == 1) upd(root, 1, coin, -1);
		else if(stak == 2) upd(root, 1, coin, 1);

		int mi = qry_mi(), mx = qry_mx();
		if(mi < 0 && mx > 0) printf("?\n");
		else if(mi < 0) printf(">\n");
		else if(mx > 0) printf("<\n");
	}
}