#pragma GCC optimize("Ofast,fast-math,O3")
#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int INF = 2e9;
const int N = 200005;
const int LEVEL = 31;
struct Node {
    int left, right;
    int ori_left, ori_right;
    ll sum;
    int mi;
    Node *lc, *rc;
    void pull() {
        ori_left = lc->ori_left;
        ori_right = rc->ori_right;
        sum = lc->sum + rc->sum;
        mi = min(lc->mi, rc->mi);
    }
}*root[LEVEL], node[N * 4], *last_node = node;
vector<pair<ll, int>> coin[LEVEL];
inline void build(Node *r, int L, int R, int level) {
    r->left = L, r->right = R;
    if(L == R) {
        ll coin_val = coin[level][L].first;
        r->sum = coin_val;
        if(coin_val) r->mi = coin_val;
        else r->mi = INF;

        r->ori_left = r->ori_right = coin[level][L].second;
        return;
    }

    int M = (L + R) / 2;
    build(r->lc = last_node++, L, M, level);
    build(r->rc = last_node++, M + 1, R, level);
    r->pull();
}
inline ll qry_min(Node *r, int ql, int qr) {
    if(ql > r->ori_right || qr < r->ori_left) return INF;
    if(ql <= r->ori_left && r->ori_right <= qr) return r->mi;
    return min(qry_min(r->lc, ql, qr), qry_min(r->rc, ql, qr));
}
int pre_sum[Level][n];
inline ll qry_sum(int level, int ql, int qr) {
    return pre_sum[level][qr] - pre_sum[level][ql - 1];
}
inline int my_log2(ll x) {
    int cnt = 0;
    while(x != 1) {
        cnt++;
        x /= 2;
    }
    return cnt;
}
const int Q = 200005;
struct Query {
    int L, R;
    int indx;
};
inline bool operator < (const Query a, const Query b) {
    return a.L < b.L;
}
Query query[Q];
ll ans[Q];
int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1; i <= n; i++) {
        ll c;
        scanf("%lld", &c);
        coin[my_log2(c)].emplace_back(c, i);
    }

    for(int i = 0; i < LEVEL; i++) {
        if(coin[i].size()) 
            build(root[i] = last_node++, 0, coin[i].size() - 1, i);
    }
    
    for(int i = 0; i < q; i++) {
        scanf("%d%d", &query[i].L, &query[i].R);
        query[i].indx = i;
    }
    sort(query, query + q);
    
    for(int i = 0; i < q; i++) {
        int ql = query[i].L, qr = query[i].R;
        ll cur_ans = 0;
        for(int l = 0; l < LEVEL; l++) {
            if(coin[l].empty()) continue;
            if(qry_min(root[l], ql, qr) <= cur_ans + 1 || (1 << (l + 1)) <= cur_ans + 1) {
                cur_ans += qry_sum(root[l], ql, qr);
            } else {
                break;
            }
        }
        ans[query[i].indx] = cur_ans + 1;
    }

    for(int i = 0; i < q; i++) {
        printf("%lld\n", ans[i]);
    }
    return 0;
}
