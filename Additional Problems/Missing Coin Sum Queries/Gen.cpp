#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 200005;
ll coin[N];
int main() {
    int n, q;
    cin >> n >> q;

    cout << n << " " << n / 2 * 2 << endl;
    for(int i = 0; i < n; i++) {
        cin >> coin[i];
    }
    for(int i = 0; i < n; i++) {
        if(i) cout << " ";
        cout << coin[i];
    }

    cout << endl;
    for(int i = 1; i <= n; i++) {
        cout << 1 << " " << n << endl;
    }
}
