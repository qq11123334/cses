#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll INF = 1e18;
const int N = 200005;
const int LEVEL = 31;
vector<pair<ll, int>> coin[LEVEL];
vector<ll> sp[LEVEL][21];
int n;
int my_log2(ll x) {
    int cnt = 0;
    while(x > 1) {
        cnt++;
        x /= 2;
    }
    return cnt;
}
ll lb(int level, int key) {
    int l = -1, r = (int)coin[level].size();
    while(l < r - 1) {
        int m = (l + r) / 2;
        if(coin[level][m].second < key) l = m;
        else r = m;
    }
    return r;
}

ll ub(int level, int key) {
    int l = -1, r = (int)coin[level].size();
    while(l < r - 1) {
        int m = (l + r) / 2;
        if(coin[level][m].second <= key) l = m;
        else r = m;
    }
    return r;
}

void build_sparse_table() {
    for(int l = 0; l < LEVEL; l++) {
        sp[l][0].resize((int)coin[l].size() + 1, 0);
        for(int i = 0; i < (int)coin[l].size(); i++) {
            if(coin[l][i].first) sp[l][0][i] = coin[l][i].first;
            else sp[l][0][i] = INF;
        }
        for(int i = 1; (1 << i) <= (int)coin[l].size(); i++) {
            sp[l][i].resize((int)coin[l].size() + 1, 0);
            for(int j = 0; j + (1 << i) <= (int)coin[l].size(); j++) {
                sp[l][i][j] = min(sp[l][i - 1][j], sp[l][i - 1][j + (1 << (i - 1))]);
            }
        }
    }
}
ll qry_min(int level, int ql, int qr) {
    int left = lb(level, ql);
    int right = ub(level, qr) - 1;
    int k = my_log2(right - left + 1);
    return min(sp[level][k][left], sp[level][k][right - (1 << k) + 1]);
}

vector<ll> pre_sum[LEVEL];
void build_pre_sum() {
    for(int i = 0; i < LEVEL; i++) {
        pre_sum[i].resize((int)coin[i].size() + 1, 0);
        for(int j = 0; j < (int)coin[i].size(); j++) {
            pre_sum[i][j] = pre_sum[i][j - 1] + coin[i][j].first;
        }
    }
}
ll qry_sum(int level, int ql, int qr) {
    int left = lb(level, ql);
    int right = ub(level, qr) - 1;
    return pre_sum[level][right] - pre_sum[level][left - 1];
}
const int Q = 200005;
struct Query {
    int L, R;
    int indx;
};
bool operator < (const Query a, const Query b) {
    return a.L < b.L;
}
Query query[Q];
ll ans[Q];
int main() {
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1; i <= n; i++) {
        ll c;
        scanf("%lld", &c);
        coin[my_log2(c)].emplace_back(c, i);
    }
    
    build_pre_sum();
    build_sparse_table();
    
    for(int i = 0; i < q; i++) {
        scanf("%d%d", &query[i].L, &query[i].R);
        query[i].indx = i;
    }
    sort(query, query + q);

    for(int k = 0; k < q; k++) {
        int ql = query[k].L, qr = query[k].R;
        ll cur_ans = 0;
        for(int i = 0; i < LEVEL; i++) {
            // if(qry_min(i, ql, qr) == INF) continue;
            if(qry_min(i, ql, qr) <= cur_ans + 1 || (1 << (i + 1)) <= cur_ans + 1) {
                cur_ans += qry_sum(i, ql, qr);
            } else {
                break;
            }
        }
        ans[query[k].indx] = cur_ans + 1;
    }

    for(int i = 0; i < q; i++) {
        printf("%lld\n", ans[i]);
    }
    return 0;
}
