#include <bits/stdc++.h>
#define ll long long int
using namespace std;
typedef pair<ll, ll> Edge;
const int N = 100005;
vector<Edge> adj[N], RevAdj[N];
int n, m;
ll dis[N];
vector<int> ord;
void Dijkstra(int s, int t) {
    memset(dis, 0x3f, sizeof(dis));
    priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> pq; // {distance, index}
    dis[s] = 0;
    pq.push({0, s});
    while(!pq.empty()) {
        ll _dis = pq.top().first, x = pq.top().second;
        pq.pop();
        if(dis[x] < _dis) continue;
        ord.push_back(x);
        for(Edge &e : adj[x]) {
            int v = e.first;
            if(dis[v] > dis[x] + e.second) {
                dis[v] = dis[x] + e.second;
                pq.push({dis[v], v});
            }
        }
    }
}
double dp[N];
void add_edge(int a, int b, ll w) {
    adj[a].push_back(Edge(b, w));
    RevAdj[b].push_back(Edge(a, w));
}
int main() {
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; ll w;
        scanf("%d%d%lld", &a, &b, &w);
        add_edge(a, b, w);
    }
    int s = 1, t = n;
    Dijkstra(s, t);
    memset(dp, 0, sizeof(dp));
    dp[t] = 1.0;
    reverse(ord.begin(), ord.end());
    for(auto x : ord) {
        int cnt = 0;
        for(Edge &e : RevAdj[x]) {
            int v = e.first;
            if(dis[x] == dis[v] + e.second) {
                cnt++;
            }
        }
        for(Edge &e : RevAdj[x]) {
            int v = e.first;
            if(dis[x] == dis[v] + e.second) {
                dp[v] += (dp[x] / cnt);
            }
        }
    }
 
    vector<int> ans;
    for(int i = 1;i <= n;i++) {
        if(fabs(dp[i] - 1) <= 1e-6) {
            ans.push_back(i);
        }
    }
    printf("%d\n", (int)ans.size());
    for(auto v : ans) {
        printf("%d ", v);
    }
    return 0;
}