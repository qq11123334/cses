#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
const int L = 21;
vector<int> init_adj[N];
vector<int> adj[N], revadj[N];
int label[N], revlabel[N];
int sdom[N], idom[N];
int par[N][L], mi[N][L];
void DFS(int x, int p = 0) {
    static int ord = 1;
    label[x] = ord;
    revlabel[ord] = x;
    ord++;
    par[label[x]][0] = label[p];
    for(auto v : init_adj[x]) {
        if(!label[v]) DFS(v, x);
    }
}
int n, m;

int min_sdom(int a, int b) {
    if(sdom[a] > sdom[b]) return b;
    else return a;
}

void build_par() {
    for(int i = 1; i <= n; i++) {
	mi[i][0] = i;
	mi[i][0] = min_sdom(i, par[i][0]);
    }

    for(int i = 1; i < L; i++) {
	for(int j = 1; j <= n; j++) {
	    int mid = par[j][i - 1];
	    par[j][i] = par[mid][i - 1];
	    mi[j][i] = min_sdom(mi[j][i - 1], mi[mid][i - 1]);
	}
    }
}
int p[N], minsdom[N];

int find(int x) {
    if(p[x] == x) return -1;
    int v = find(p[x]);
    if(v == -1) return x;
    if(sdom[minsdom[p[x]]] < sdom[minsdom[x]])
	minsdom[x] = minsdom[p[x]];
    return p[x] = v;
}

int find_min_sdom(int x) {
    int v = find(p[x]);
    if(v == -1) return x;
    if(sdom[minsdom[p[x]]] < sdom[minsdom[x]])
        minsdom[x] = minsdom[p[x]];
    p[x] = v;
    return minsdom[x];
}

void unite(int a, int b) {
    p[b] = a;
}

void init() {
    for(int i = 1; i <= n; i++) {
	p[i] = i;
	minsdom[i] = i;
	sdom[i] = i;
    }
}
int main() {
    scanf("%d%d", &n, &m);
    init();
    for(int i = 0; i < m; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        init_adj[a].push_back(b);
    }
    
    for(int i = 1; i <= n; i++) {
	if(!label[i]) DFS(i);
    }
    
    for(int i = 1; i <= n; i++) {
        for(auto v : init_adj[i]) {
            adj[label[i]].push_back(label[v]);
            revadj[label[v]].push_back(label[i]);
        }
    }
    
    for(int i = n; i >= 2; i--) {
	for(auto v : revadj[i]) {
	    sdom[i] = min(sdom[i], sdom[find_min_sdom(v)]);
	}
	minsdom[i] = sdom[i];
	unite(par[i][0], i);
    }

    build_par();

    for(int i = 2; i <= n; i++) {
	int v = i, u = i;
	for(int k = L - 1; k >= 0; k--) {
	    if(par[v][k] > sdom[i]) {
		if(sdom[u] > sdom[mi[v][k]]) {
		    u = mi[v][k];
		}
		v = par[v][k];
	    }
	}
	
	if(sdom[u] == sdom[i]) {
	    idom[i] = sdom[i];
	} else {
	    idom[i] = idom[u];
	}
    }

    vector<int> ans;
    for(int v = label[n]; v != 0; v = idom[v]) {
	ans.push_back(revlabel[v]);
    }
    
    sort(ans.begin(), ans.end());
    printf("%d\n", (int)ans.size());
    for(auto x : ans) {
	printf("%d ", x);
    }
    printf("\n");
}
