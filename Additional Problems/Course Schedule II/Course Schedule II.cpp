#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
priority_queue<int> pq;
int inDegree[N];
vector<int> adj[N];
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[b].push_back(a);
        inDegree[a]++;
    }

    for(int i = 1;i <= n;i++) {
        if(inDegree[i] == 0) {
            pq.push(i);
        }
    }

    vector<int> ans;
    while(!pq.empty()) {
        int x = pq.top(); pq.pop();
        ans.push_back(x);
        for(auto v : adj[x]) {
            if(--inDegree[v] == 0) {
                pq.push(v);
            }
        }
    }

    reverse(ans.begin(), ans.end());
    for(auto v : ans) {
        printf("%d ", v);
    }
    return 0;
}