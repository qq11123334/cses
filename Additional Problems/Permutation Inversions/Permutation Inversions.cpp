#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
const ll N = 505;
using namespace std;
vector<ll> dp(N * (N - 1) / 2);
vector<ll> pre(N * (N - 1) / 2);
int main() {
    int n, k; 
    cin >> n >> k;
    dp[0] = 1;
    dp[1] = 0;
    for(int i = 2;i <= n;i++) {
        for(int j = 0;j <= k;j++) {
            pre[j] = dp[j];
            if(j) pre[j] += pre[j - 1];
            if(pre[j] >= MOD) pre[j] %= MOD;
        }
        for(int j = 0;j <= min(k, i * (i - 1) / 2);j++) {
            dp[j] = pre[j];
            int last = max(0, j - i + 1);
            if(last) dp[j] -= pre[last - 1];
            if(dp[j] < 0 || dp[j] >= MOD) dp[j] = (dp[j] % MOD + MOD) % MOD;
        }
    }
    cout << dp[k] << endl;
    return 0;
}