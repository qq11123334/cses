#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
const int N = 501;
using namespace std;
ll dp[N][N * (N - 1) / 2];
int main() {
    int n, k; 
    cin >> n >> k;
    
    dp[1][0] = 1;
    dp[1][1] = 0;
    for(int i = 2;i <= n;i++) {
        for(int j = 0;j <= min(k, i * (i - 1) / 2);j++) {
            for(int u = 0;u <= i - 1;u++) {
                dp[i][j] += dp[i - 1][j - u];
                dp[i][j] %= MOD;
            }
        }
    }
    cout << dp[n][k] << endl;
    return 0;
}