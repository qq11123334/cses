#include <bits/stdc++.h>
using ll = long long;
using namespace std;
const int N = 200005;
const ll INF = (ll)1e18;
pair<int, int> arr[N];
struct Node {
	int left, right;
	int offest;
	Node *lc, *rc;
};
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) {
		r->offest = 0;
		return ;
	}
	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
}
void upd(Node *r, int ql, int qr, int val) {
	if(ql > r->right || qr < r->left) return;
	if(ql <= r->left && r->right <= qr) {
		r->offest += val;
		return;
	}
	upd(r->lc, ql, qr, val);
	upd(r->rc, ql, qr, val);
}
int qry(Node *r, int pos) {
	if(r->left == r->right) return r->offest;

	int mid = (r->left + r->right) / 2;
	if(pos <= mid) return r->offest + qry(r->lc, pos);
	else return r->offest + qry(r->rc, pos);
}
int main() {
	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%d", &arr[i].first);
		arr[i].second = i;
	}

	sort(arr + 1, arr + 1 + n);
	
	Node *root;
	build(root = new Node(), 1, n);

	ll ans = 0;
	
	for(int i = 1; i <= n; i++) {
		int ori_pos = arr[i].second;
		int cur_pos = ori_pos + qry(root, ori_pos);
		ans += min(n - cur_pos, cur_pos - i);
		// assume move to i
		upd(root, 1, ori_pos, 1);
	}

	printf("%lld\n", ans);
	return 0;
}
