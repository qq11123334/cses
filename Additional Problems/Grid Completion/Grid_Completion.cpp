#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 505;
const ll MOD = 1e9 + 7;
char grid[N][N];
ll A[N], B[N];

ll fact[N], ifact[N];
ll fp(ll x, ll y) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % MOD;
        y >>= 1;
        x = x * x % MOD;
    }
    return res;
}
ll inv(ll x) {
    return fp(x, MOD - 2);
}
void build_fact() {
    fact[0] = 1;
    ifact[0] = 1;
    for(ll i = 1; i < N; i++) {
        fact[i] = fact[i - 1] * i % MOD;
        ifact[i] = inv(fact[i]);
    }
}

ll C(int a, int b) {
    return fact[a] * ifact[b] % MOD * ifact[a - b] % MOD;
}

int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    build_fact();
    int n;
    cin >> n;

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= n; j++) {
            cin >> grid[i][j];
        }
    }

    set<int> As, Bs;
    set<int> s;
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= n; j++) {
            if(grid[i][j] == 'A') {
                A[i] = j;
                s.insert(j);
                As.insert(j);
            }
            else if(grid[i][j] == 'B') {
                B[i] = j;
                s.insert(j);
                Bs.insert(j);
            }
        }
    }

    ll cnt0A = 0, cnt0B = 0, cnt_un = n - s.size(), cnt[3] = {0};
    for(int i = 1; i <= n; i++) {
        if(A[i] == 0)
            cnt0A++;

        if(B[i] == 0)
            cnt0B++;
        
        if(s.count(A[i])) s.erase(A[i]);
        if(s.count(B[i])) s.erase(B[i]);

        if(!A[i] && !B[i]) cnt[0]++;
        if(A[i] && !B[i] && !Bs.count(A[i])) cnt[1]++;
        if(!A[i] && B[i] && !As.count(B[i])) cnt[2]++;
    }


    ll ans = 0;
    for(ll i = 0; i <= min(cnt_un, cnt[0]); i++) {
        for(ll j = 0; j <= cnt[1]; j++) {
            for(ll k = 0; k <= cnt[2]; k++) {
                ll multer = (((i + j + k) % 2 == 0) ? 1 : (-1 + MOD));
                ans += C(cnt[0], i) * C(cnt_un, i) % MOD * C(cnt[1], j) % MOD * C(cnt[2], k) % MOD * fact[i] % MOD * fact[cnt0A - i - k] % MOD * fact[cnt0B - i - j] % MOD * multer % MOD;
                ans = (ans % MOD + MOD) % MOD;
            }
        }
    }
    cout << ans << "\n";
}
