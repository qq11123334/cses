#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 200005;
map<vector<ll>, ll> cnt_dif;
map<char, int> reindex;
int arr[N], W;
int main() {
	string s;
	cin >> s;
	int last_indx = 1;
	for(int i = 0; i < (int)s.size(); i++) {
		char c = s[i];
		if(!reindex[c]) {
			reindex[c] = last_indx++;
			W++;
		}
		arr[i] = reindex[c] - 1;
	}
	vector<ll> cnt(W, 0);
	ll ans = 0;
	cnt_dif[cnt]++;
	for(int i = 0; i < (int)s.size(); i++) {
		cnt[arr[i]]++;
		ll mi = s.size();
        for(int j = 0; j < W; j++) {
            mi = min(cnt[j], mi);
        }

        for(int j = 0; j < W; j++) {
            cnt[j] -= mi;
        }
        ans += cnt_dif[cnt];
		cnt_dif[cnt]++;
	}
	cout << ans << endl;
	return 0;
}
