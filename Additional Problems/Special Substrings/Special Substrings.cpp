#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 200005;
map<vector<int>, ll> cnt_dif;
map<char, int> reindex;
bool used[30];
ll cnt[30];
int arr[N], W;
int main() {
	string s;
	cin >> s;
	int last_indx = 1;
	for(int i = 0; i < (int)s.size(); i++) {
		char c = s[i];
		if(!reindex[c]) {
			reindex[c] = last_indx++;
			W++;
		}
		arr[i] = reindex[c];
	}
	vector<int> dif(W - 1, 0);
	ll ans = 0;
	cnt_dif[dif]++;
	for(int i = 0; i < (int)s.size(); i++) {
		cnt[arr[i]]++;
		for(int j = 1; j <= W - 1; j++) {
			dif[j - 1] = cnt[j + 1] - cnt[j];
		}
		ans += cnt_dif[dif];
		cnt_dif[dif]++;
	}
	cout << ans << endl;
	return 0;
}