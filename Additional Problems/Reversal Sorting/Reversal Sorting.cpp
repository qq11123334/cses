#include <bits/stdc++.h>
using namespace std;
const int INF = 0x3f3f3f3f, N = 200005;
struct Treap {
	int size, pri, swap_tag;
	int val;
	int mi, mi_pos;
	Treap *lc, *rc;
	Treap(int num) {
		size = 1;
		pri = rand();
		swap_tag = 0;
		mi = num;
		val = num;
		mi_pos = 1;
		lc = rc = NULL;
	}
};
int size(Treap *node) {
	if(node == NULL) return 0;
	else return node->size;
}
int mi(Treap *node) {
	if(node == NULL) return INF;
	else return node->mi;
}
void pull(Treap *node) {
	node->size = size(node->lc) + size(node->rc) + 1;
	node->mi = min({node->val, mi(node->lc), mi(node->rc)});
	if(node->mi == node->val) {
		node->mi_pos = size(node->lc) + 1;
	} else if(node->mi == mi(node->lc)) {
		if(!node->lc->swap_tag) node->mi_pos = node->lc->mi_pos;
		else node->mi_pos = size(node->lc) - node->lc->mi_pos + 1;
	} else if(node->mi == mi(node->rc)) {
		if(!node->rc->swap_tag) node->mi_pos = size(node->lc) + 1 + node->rc->mi_pos;
		else node->mi_pos = size(node->lc) + 1 + (size(node->rc) - node->rc->mi_pos + 1);
	}
}
void push(Treap *node) {
	if(node != NULL) {
		if(node->swap_tag) {
			if(node->lc != NULL) node->lc->swap_tag ^= 1;
			if(node->rc != NULL) node->rc->swap_tag ^= 1;
			swap(node->lc, node->rc);
			pull(node);
		}
		node->swap_tag = 0;
	}
}
void split(Treap *node, Treap *&left, Treap *&right, int k) {
	if(node == NULL) left = right = NULL;
	else {
		push(node);
		if(size(node->lc) < k) {
			split(node->rc, node->rc, right, k - size(node->lc) - 1);
			left = node;
		} else {
			split(node->lc, left, node->lc, k);
			right = node;
		}
		pull(node);
	}
}
void merge(Treap *&node, Treap *left, Treap *right) {
	if(left == NULL) node = right;
	else if(right == NULL) node = left;
	else {
		if(left->pri < right->pri) {
			push(left);
			merge(left->rc, left->rc, right);
			node = left;
		} else {
			push(right);
			merge(right->lc, left, right->lc);
			node = right;
		}
		pull(node);
	}
}
int arr[N];
int main() {
	srand(time(NULL) * clock());

	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%d", &arr[i]);
	}

	Treap *root = NULL;
	for(int i = 1; i <= n; i++) {
		merge(root, root, new Treap(arr[i]));
	}
	
	vector<pair<int, int>> ans;
	for(int i = 1; i <= n; i++) {
		Treap *left, *mid, *right;
		split(root, left, right, i - 1);

		if(right == NULL || right->mi_pos == 1) {
			merge(root, left, right);
		} else {
			ans.push_back(make_pair(i, i + right->mi_pos - 1));
			split(right, mid, right, right->mi_pos);
			mid->swap_tag ^= 1;
			merge(root, left, mid);
			merge(root, root, right);
		}

	}
	printf("%d\n", (int)ans.size());
	for(auto P : ans) {
		printf("%d %d\n", P.first, P.second);
	}
	return 0;
}