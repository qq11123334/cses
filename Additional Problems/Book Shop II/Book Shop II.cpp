#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 105, X = 100005;
ll weight[N], value[N], quantity[N];
ll dp[X];
struct Item {
    ll weight, value;
    Item() {}
    Item(ll _weight, ll _value) : weight(_weight), value(_value) {}
};
vector<Item> item;
int main() {
    // freopen("input.txt", "r", stdin);
    int n; ll x;
    scanf("%d%lld", &n, &x);
    for(int i = 0;i < n;i++) {
        scanf("%lld", &weight[i]);
    }
    for(int i = 0;i < n;i++) {
        scanf("%lld", &value[i]);
    }
    for(int i = 0;i < n;i++) {
        scanf("%lld", &quantity[i]);
    }

    for(int i = 0;i < n;i++) { // for each item and Quantity[i] copy
        for(int k = 1;k <= quantity[i];k <<= 1) {
            quantity[i] -= k;
            item.push_back(Item(k * weight[i], k * value[i]));
        }
        if(quantity[i] > 0) item.push_back(Item(quantity[i] * weight[i], quantity[i] * value[i]));
    }
    // and use 01-Knapsack to solve problem

    dp[0] = 0;
    for(int i = 0;i < (int)item.size();i++) {
        Item &I = item[i];
        for(int j = x;j >= I.weight;j--) {
            dp[j] = max(dp[j], dp[j - I.weight] + I.value);
        }
    }
    printf("%lld\n", dp[x]);
    return 0;
}