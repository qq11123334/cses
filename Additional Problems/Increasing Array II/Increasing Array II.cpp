#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 200005;
ll arr[N];
priority_queue<ll> pq;
int main() {
	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) {
		scanf("%lld", &arr[i]);
		// arr[i] -= i;
	}

	ll ans = 0;
	for(int i = 1; i <= n; i++) {
		pq.push(arr[i]);
		pq.push(arr[i]);
		if(pq.top() > arr[i]) {
			ans += (pq.top() - arr[i]);
		}
		pq.pop();
	}
	printf("%lld\n", ans);
	return 0;
}