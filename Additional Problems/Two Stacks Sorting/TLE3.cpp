#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int n, pos[N], arr[N], out[N];
struct Node {
    int L, R;
    int mx;
    Node *lc, *rc;
    void pull() {
        mx = max(lc->mx, rc->mx);
    }
};
void build(Node *r, int L, int R) {
    r->L = L, r->R = R;
    if(L == R) {
        r->mx = arr[L];
        return;
    }
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
    r->pull();

}
int UP = 200000;
void qry_pos(Node *r, int ql, int qr, int val, vector<int> &res) {
    if((int)res.size() > UP) return;
    if(ql > r->R || qr < r->L) return;
    if(r->mx <= val) return;
    if(r->L == r->R) return res.push_back(r->L);
    qry_pos(r->lc, ql, qr, val, res);
    qry_pos(r->rc, ql, qr, val, res);
}
int id(int x) {
    if(x > n) return x - n;
    else return x;
}
int p[2 * N], sz[2 * N], mi[2 * N];
void build() {
    for(int i = 0; i < 2 * N; i++) {
        p[i] = i;
        sz[i] = 1;
        mi[i] = i;
    }
}
int find(int x) {
    if(p[x] == x) return x;
    else return p[x] = find(p[x]);
}
void unite(int a, int b) {
    a = find(a), b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    p[a] = b;
    sz[b] += sz[a];
    if(id(mi[a]) < id(mi[b])) swap(mi[a], mi[b]);
}
int neg(int x) {
    return x + n;
}
void add_edge(int a, int b) {
    unite(a, neg(b));
    unite(neg(a), b);
}
void check(int x) {
    if(find(x) == find(neg(x))) {
        cout << "IMPOSSIBLE\n";
        exit(0);
    }
}
int main() {
    cin >> n;
    for(int i = 1; i <= n; i++) {
        cin >> arr[i];
        pos[arr[i]] = i;
    }
     
    Node *root;
    build();
    build(root = new Node(), 1, n);

    for(int i = 1; i <= n; i++) {
        out[i] = max(out[i - 1], pos[i]);
    }

    vector<int> v;
    for(int i = 1; i <= n; i++) {
        v.clear();
        qry_pos(root, pos[i], out[i], i, v);

        for(auto j : v) {
            add_edge(pos[i], j);
            check(pos[i]);
            check(j);
        }

        if(!v.empty()) UP = max(UP / 10, 300);
    }
    
    for(int i = 1; i <= n; i++) {
        if(mi[find(i)] > n) cout << "2 ";
        else cout << "1 ";
    }
    cout << "\n";
}
