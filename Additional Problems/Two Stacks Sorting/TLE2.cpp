#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int n, pos[N], arr[N], out[N];
 
vector<int> adj[N];
bool vis[N];
int color[N];
void DFS(int x, int c) {
    vis[x] = true;
    color[x] = c;
    for(auto v : adj[x]) {
        if(!vis[v]) DFS(v, !c);
        else if(color[v] == color[x]) {
            cout << "IMPOSSIBLE\n";
            exit(0);
        }
    }
}
void add_edge(int a, int b) {
    adj[a].push_back(b);
    adj[b].push_back(a);
}
struct Node {
    int L, R;
    int mx;
    Node *lc, *rc;
    void pull() {
        mx = max(lc->mx, rc->mx);
    }
};
void build(Node *r, int L, int R) {
    r->L = L, r->R = R;
    if(L == R) {
        r->mx = arr[L];
        return;
    }
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
    r->pull();
 
}
void qry_pos(Node *r, int ql, int qr, int val, vector<int> &res) {
    if(ql > r->R || qr < r->L) return;
    if(r->mx <= val) return;
    if(r->L == r->R) return res.push_back(r->L);
    qry_pos(r->lc, ql, qr, val, res);
    qry_pos(r->rc, ql, qr, val, res);
}
int main() {
    cin >> n;
    for(int i = 1; i <= n; i++) {
        cin >> arr[i];
        pos[arr[i]] = i;
    }
    
    Node *root;
    build(root = new Node(), 1, n);
 
    for(int i = 1; i <= n; i++) {
        out[i] = max(out[i - 1], pos[i]);
    }
 
    for(int i = 1; i <= n; i++) {
        vector<int> v;
        qry_pos(root, pos[i], out[i], i, v);
 
        for(auto j : v) 
            add_edge(pos[i], j);
    }
 
    for(int i = 1; i <= n; i++) {
        if(!vis[i]) DFS(i, 0);
    }
 
    for(int i = 1; i <= n; i++) {
        cout << color[i] + 1 << " ";
    }
    cout << "\n";
}
