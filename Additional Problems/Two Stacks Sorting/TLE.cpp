#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int n, pos[N], arr[N], out[N];

vector<int> adj[N];
bool vis[N];
int color[N];
void DFS(int x, int c) {
    vis[x] = true;
    color[x] = c;
    for(auto v : adj[x]) {
        if(!vis[v]) DFS(v, !c);
        else if(color[v] == color[x]) {
            cout << "IMPOSSIBLE\n";
            exit(0);
        }
    }
}
int main() {
    cin >> n;
    for(int i = 1; i <= n; i++) {
        cin >> arr[i];
        pos[arr[i]] = i;
    }

    for(int i = 1; i <= n; i++) {
        out[i] = max(out[i - 1], pos[i]);
    }

    for(int i = 1; i <= n; i++) {
        if(out[i] == pos[i]) continue;
        for(int j = i + 1; j <= n; j++) {
            if(out[i] > pos[j] && pos[i] < pos[j]) {
                adj[i].push_back(j);
                adj[j].push_back(i);
            }
        }
    }

    for(int i = 1; i <= n; i++) {
        if(!vis[i]) DFS(i, 0);
    }

    for(int i = 1; i <= n; i++) {
        cout << color[arr[i]] + 1 << " ";
    }
    cout << "\n";
}
