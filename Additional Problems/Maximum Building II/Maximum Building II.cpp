#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 1005;
ll n, m;
char grid[N][N];
ll toUpper[N][N];
ll tol[N], tor[N];
ll a[N][N], b[N][N];
void add_a(int x1, int y1, int x2, int y2, ll value) {
    if(x1 > x2 || y1 > y2) return;
    a[x1][y1] += value;
    a[x1][y2 + 1] -= value;
    a[x2 + 1][y1] -= value;
    a[x2 + 1][y2 + 1] += value;
}
void add_b(int x1, int y1, int x2, int y2, ll value) {
    if(x1 > x2 || y1 > y2) return;
    b[x1][y1] += value;
    b[x1][y2 + 1] -= value;
    b[x2 + 1][y1] -= value;
    b[x2 + 1][y2 + 1] += value;
}

ll aa[N][N], bb[N][N];
void print() {
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            aa[i][j] = a[i][j] + aa[i - 1][j] + aa[i][j - 1] - aa[i - 1][j - 1];
            bb[i][j] = b[i][j] + bb[i - 1][j] + bb[i][j - 1] - bb[i - 1][j - 1];
        }
    }
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            cerr << aa[i][j] * j + bb[i][j] << " ";
        }
        cerr << endl;
    }
    cerr << "----" << endl;
}
void solve(int i) {
    stack<pair<ll, ll>> sta;
    sta.push({0, 0});
    for(int j = 1; j <= m; j++) {
        while(!sta.empty() && toUpper[i][j] <= sta.top().first) {
            sta.pop();
        }

        tol[j] = (sta.empty() ? 0 : sta.top().second);
        sta.push({toUpper[i][j], j});
    }

    while(!sta.empty()) sta.pop();
    
    sta.push({0, m + 1});
    for(int j = m; j >= 1; j--) {
        while(!sta.empty() && toUpper[i][j] < sta.top().first) {
            sta.pop();
        }

        tor[j] =  (sta.empty() ? (m + 1) : sta.top().second);
        sta.push({toUpper[i][j], j});
    }

    for(int j = 1; j <= m; j++) {
        ll w = toUpper[i][j];
        if(w == 0) continue;

        ll l = abs(j - tol[j]), r = abs(j - tor[j]);
        if(l > r) swap(l, r);
        add_a(1, 1, w, l, 1);
        add_b(1, l + 1, w, r, l);
        add_a(1, r + 1, w, l + r - 1, -1);
        add_b(1, r + 1, w, l + r - 1, l + r);

        // cerr << "(i, j, l, r, w) " << i << " " << j << " " << tol[j] << " " << tor[j] << " " << w << endl;
        // print();
    }
}

int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);

    cin >> n >> m;
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            cin >> grid[i][j];
        }
    }

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            if(grid[i][j] == '.') toUpper[i][j] = toUpper[i - 1][j] + 1;
            else toUpper[i][j] = 0;
        }
    }

    for(int i = 1; i <= n; i++) {
        solve(i);
    }

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            a[i][j] = a[i][j] + a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1];
            b[i][j] = b[i][j] + b[i - 1][j] + b[i][j - 1] - b[i - 1][j - 1];
        }
    }

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= m; j++) {
            cout << a[i][j] * j + b[i][j] << " ";
        }
        cout << "\n";
    }
}
