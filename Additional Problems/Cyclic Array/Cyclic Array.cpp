#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 400005, L = 21;
ll arr[N];
int nxt[N][L];
bool used[N];
int main() {
	ll n, k;
	scanf("%lld %lld", &n, &k);
	for(int i = 0; i < n; i++) {
		scanf("%lld", &arr[i]);
		arr[i + n] = arr[i];
	}

	int r = -1;
	ll sum = 0;

	for(int l = 0; l < 2 * n; l++) {
		while(r < min(l + n - 1, 2 * n - 1) && sum + arr[r + 1] <= k) {
			sum += arr[r + 1];
			r++;
		}
		
		nxt[l][0] = r + 1;
		sum -= arr[l];
	}

	nxt[2 * n][0] = 2 * n;

	for(int j = 1; j < L; j++) {
		for(int i = 0; i <= 2 * n; i++) {
			nxt[i][j] = nxt[nxt[i][j - 1]][j - 1];
		}
	}

	int ans = 1e9;

	for(int i = 0; i < n; i++) {
		int l = i;
		int res = 0;
		for(int j = L - 1; j >= 0; j--) {
			if(nxt[l][j] < i + n) {
				l = nxt[l][j];
				res += (1 << j);
			}
		}
		ans = min(ans, res + 1);
	}

	printf("%d\n", ans);
	return 0;
}
