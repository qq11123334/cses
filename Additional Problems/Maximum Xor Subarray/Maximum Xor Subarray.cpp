#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const ll L = 31, N = 200005 * L;
ll trie[N][2];
ll pow_2(ll y) {
    ll res = 1, x = 2;
    while(y) {
        if(y & 1) res *= x;
        y >>= 1;
        x = x * x;
    }
    return res;
}
void Insert(ll x) {
    static int pos = 1;
    int now = 0;
    for(ll i = pow_2(L);i > 0;i /= 2) {
        bool And = (i & x);
        if(!trie[now][And]) {
            trie[now][And] = pos++;
        }
        now = trie[now][And];
    }
}
ll query(ll x) {
    ll mx = 0;
    int now = 0;
    for(ll i = pow_2(L);i > 0;i /= 2) {
        bool And = (i & x);
        if(trie[now][!And]) {
            now = trie[now][!And];
            if(!And) mx |= i;
        } else if(trie[now][And]) {
            now = trie[now][And];
            if(And) mx |= i;
        } else {
            assert(0);
        }
    }
    return mx;
}
int main() {
    int n;
    scanf("%d", &n);

    ll pre_xor = 0, mx = 0;
    Insert(pre_xor);
    for(int i = 0;i < n;i++) {
        ll num;
        scanf("%lld", &num);
        pre_xor ^= num;
        mx = max(mx, pre_xor ^ query(pre_xor));
        // printf("mx = %lld\n", pre_xor ^ query(pre_xor));
        Insert(pre_xor);
    }
    printf("%lld\n", mx);
    return 0;
}