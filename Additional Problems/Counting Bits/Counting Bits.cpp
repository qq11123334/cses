#include <bits/stdc++.h>
#define ll long long int
using namespace std;
int main() {
    ios_base::sync_with_stdio(0), cin.tie(0);
    ll n;
    cin >> n;

    ll suf_sum = 0;
    ll high_bit = 0;
    ll ans = 0;
    for(ll i = 1;i <= n;i <<= 1) {
        if(n & i) {
            ans += (suf_sum + 1);
            suf_sum += i;
            high_bit = i;
            // cout << "ans = " << ans << endl;
        }
    }

    ll pre_sum = 0;
    for(ll i = high_bit;i >= 1;i >>= 1) {
        ans += (pre_sum / 2);
        if(n & i) pre_sum += i;
        // cout << "ans = " << ans << endl;
    }

    cout << ans << endl;
    return 0;
}
/*
 000
 001
 010
 011
 100
 101
 110
 111
*/