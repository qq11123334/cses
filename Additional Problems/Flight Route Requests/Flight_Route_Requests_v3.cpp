#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int p[N], sz[N];
bool has_cycle[N];
void init() {
	for(int i = 0; i < N; i++) {
		p[i] = i;
		sz[i] = 1;
	}
}
int find(int x) {
	if(p[x] == x) return x;
	else return p[x] = find(p[x]);
}
void unite(int a, int b) {
	a = find(a), b = find(b);
	if(a == b) return;
	if(sz[a] > sz[b]) swap(a, b);
	p[a] = b;
	sz[b] += sz[a];
	has_cycle[b] |= has_cycle[a];
}
vector<int> adj[N];
int vis[N];
void DFS(int x) {
	vis[x] = 1;
	for(auto v : adj[x]) {
		if(!vis[v]) {
			DFS(v);
		}
		else if(vis[v] == 1) {
			has_cycle[x] = true;
		}
	}
	vis[x] = 2;
}
int main() {
	init();
	int n, m;
	scanf("%d%d", &n, &m);
	for(int i = 0; i < m; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		adj[a].push_back(b);
	}

	for(int i = 1; i <= n; i++) {
		if(!vis[i]) DFS(i);
	}

	for(int x = 1; x <= n; x++) {
		for(auto v : adj[x]) {
			unite(v, x);
		}
	}

	int ans = n;
	for(int i = 1; i <= n; i++) {
		if(p[i] == i && !has_cycle[i]) ans--;
	}
	printf("%d\n", ans);
	return 0;
}
