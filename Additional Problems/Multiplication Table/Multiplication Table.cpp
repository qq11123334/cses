#include <bits/stdc++.h>
#define ll long long int
using namespace std;
ll n;
ll F(ll x) {
    ll cnt = 0;
    for(ll i = 1;i <= n;i++) {
        ll start = 1;
        ll end = min(n, (x / i));
        cnt += (end - start + 1);
    }
    return cnt;
}
int main() {
    cin >> n;
    ll l = n - 1, r = n * n;
    while(l < r - 1) {
        ll m = (l + r) / 2;
        if(F(m) >= (n * n + 1) / 2) r = m;
        else l = m;
    }
    cout << r << endl;
}