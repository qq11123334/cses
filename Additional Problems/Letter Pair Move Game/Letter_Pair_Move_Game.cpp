#include <bits/stdc++.h>
using namespace std;
string s;
bool check() {
    bool B = false;
    for(auto c : s) {   
        if(c == 'A' && B) return false;
        if(c == 'B') B = true;
    }
    return true;
}
int left_B() {
    for(int i = 0; i < (int)s.size(); i++) {
        if(s[i] == 'B') return i;
    }
    return -1;
}
int left_A(int start) {
    for(int i = start; i < (int)s.size(); i++) {
        if(s[i] == 'A') return i;
    }
    return -1;
}
int space() {
    for(int i = 0; i < s.size(); i++) {
        if(s[i] == '.') return i;
    }
}
vector<string> ans;
void op(int left, int right) {
    if(check()) return;
    if(left > right) swap(left, right);
    swap(s[left], s[right]);
    swap(s[left + 1], s[right + 1]);
    ans.push_back(s);
}
void print() {
    if(check()) {
        cout << ans.size() << "\n";
        for(auto str : ans) {
            cout << str << "\n";
	    }
    } else {
	    cout << -1 << "\n";
    }
    exit(0);
}
int main() {
    int n;
    cin >> n >> s;
    if(check()) {
        printf("0\n");
	    exit(0);
    }
    if(n <= 2) {
        printf("-1\n");
        exit(0);
    }

    n *= 2;
    int sp = space();
    if(sp == n - 3) {
        op(sp - 2, sp);
        op(sp - 2, n - 2);
    } else if(sp != n - 2) {
        op(sp, n - 2);
    }
    
    if(n == 6) {
        if(s == "ABAB.." || s == "BABA..") print();
    }

    while(ans.size() < 990L && !check()) {
        int b = left_B();
        int a = left_A(b + 2);
        if(a == -1) break;
        if(a == n - 3) {
            op(b, n - 2);
            op(a, b);
            int bb = left_B();
            op(bb, n - 3);
            op(bb, n - 2);
            continue;
        }
        op(b, n - 2);
        op(a, b);
        op(a, n - 2);
    }

    if(!check()) {
        int b = left_B();
        int a = b + 1;
        int bb = a + 1;
        op(bb, n - 2);
        op(b, bb);
        op(bb + 1, b);
    }
    print();
}
