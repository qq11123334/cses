#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
using namespace std;
const int N = 1000005, C = 4;
string s;

char toC[C];
map<char, int> toN;
void init() {
    toC[0] = 'A';
    toC[1] = 'C';
    toC[2] = 'G';
    toC[3] = 'T';
    toN['A'] = 0;
    toN['C'] = 1;
    toN['G'] = 2;
    toN['T'] = 3;
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    init();
    cin >> s;

    string ans;
    bool used[4];
    memset(used, 0, sizeof(used));
    int used_cnt = 0;

    for(auto c : s) {
        if(!used[toN[c]]) {
            used[toN[c]] = 1;
            used_cnt++;
        }

        if(used_cnt == 4) {
            used_cnt = 0;
            memset(used, 0, sizeof(used));
            ans += c;
        }
    }

    for(int i = 0;i < C;i++) {
        if(!used[i]) {
            ans += toC[i];
            break;
        }
    }
    cout << ans << endl;
    return 0;
}