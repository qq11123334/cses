#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll MOD = (ll)1e9 + 7;
const int N = 105, X = 5005, T = 105;
const int offset = 10005;
ll arr[N];
ll dp[2][N][X + offset + T];
/*
dp[i][j][k] is the number of way that
first i person
j unfinished team 
the penelty is k
*/
int main() {
	int n, x;
	scanf("%d%d", &n, &x);
	for(int i = 0; i < n; i++) {
		scanf("%lld", &arr[i]);
	}

	sort(arr, arr + n);

	dp[0][0][offset] = 1;
	dp[0][1][-arr[0] + offset] = 1;
	for(int i = 1; i < n; i++) {
		for(int j = 0; j < n; j++) {
			for(int k = 0; k <= x + offset; k++) {
				int i_new = (i % 2);
				int i_old = ((i - 1) % 2);
				// calculate dp[i][j][k]
				dp[i_new][j][k] = dp[i_old][j][k]; // himself make a finished team
				if(j) dp[i_new][j][k] += dp[i_old][j - 1][k + arr[i]]; // himself make a unfinished team
				dp[i_new][j][k] += j * dp[i_old][j][k]; // join a unfinished team
				if(k - arr[i] >= 0) dp[i_new][j][k] += (j + 1) * dp[i_old][j + 1][k - arr[i]]; // join a unfinished team and finish it

				dp[i_new][j][k] %= MOD;
			}
		}
	}
	ll ans = 0;
	for(int i = offset; i <= x + offset; i++) {
		ans += dp[(n - 1) % 2][0][i];
		ans %= MOD;
	}
	printf("%lld\n", ans);

	return 0;
}