#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200005;
ll Even(ll x) {
    // x! = 2^res * ...
    ll res = 0;
    while(x) {
        res += (x / 2);
        x /= 2;
    }
    return res;
}
bool isEven_C(ll n, ll m) {
    // is C(n, m) even
    ll cnt1 = Even(n);
    ll cnt2 = Even(m) + Even(n - m);
    // cout << "n = " << n << " m = " << m << endl;
    // cout << "cnt1 = " << cnt1 << " cnt2 = " << cnt2 << endl;
    return cnt1 > cnt2;
}
ll arr[N];
int main() {
    int n;
    scanf("%d", &n);
    n--;
    for(int i = 0;i <= n;i++)
        scanf("%lld", &arr[i]);

    ll ans = 0;
    for(int i = 0;i <= n;i++) {
        if(!isEven_C(n, i)) ans ^= arr[i];
    }
    printf("%lld\n", ans);
    return 0;
}