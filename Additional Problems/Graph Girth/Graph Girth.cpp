#include <bits/stdc++.h>
using namespace std;
const int N = 2505;
vector<int> adj[N];
int level[N], par[N];
int BFS(int n) {
    // printf("\nn = %d\n", n);
    memset(level, 0, sizeof(level));
    memset(par, 0, sizeof(par));
    queue<int> q;
    level[n] = 1;
    par[n] = -1;
    q.push(n);
    int res = N;
    while(!q.empty()) {
        int x = q.front();
        q.pop();
        for(auto v : adj[x]) {
            if(v != par[x]) {
                if(level[v]) {
                    res = min(res, level[x] + level[v] - 1);
                } else {
                    level[v] = level[x] + 1;
                    par[v] = x;
                    q.push(v);
                }
            }
        }
    }
    return res;
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    int ans = N;
    for(int i = 1;i <= n;i++) {
        ans = min(BFS(i), ans);
    }

    if(ans == N) ans = -1;
    printf("%d\n", ans);
    return 0;
}