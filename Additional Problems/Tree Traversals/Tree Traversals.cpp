#include <bits/stdc++.h>
using namespace std;
int pre[100005], in[100005];
int pre_pos[100005], in_pos[100005];
int left_child[100005], right_child[100005];
int find_root(int pre_left, int pre_right, int in_left, int in_right) {
    if(pre_left > pre_right || in_left > in_right) return 0;
    int root = pre[pre_left];
    int left_sz = in_pos[root] - in_left;
    int right_sz = in_right - in_pos[root];
    int root_pos = in_pos[root];
    left_child[root] = find_root(pre_left + 1, pre_left + 1 + left_sz - 1, in_left, root_pos - 1);
    right_child[root] = find_root(pre_right - right_sz + 1, pre_right, root_pos + 1, in_right);
    return root;
}
vector<int> postorder;
void DFS_Post(int x) {
    if(left_child[x]) DFS_Post(left_child[x]);
    if(right_child[x]) DFS_Post(right_child[x]);
    postorder.push_back(x);
}
int main() {
    int n; scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &pre[i]);
        pre_pos[pre[i]] = i;
    }
    for(int i = 1;i <= n;i++) {
        scanf("%d", &in[i]);
        in_pos[in[i]] = i;
    }

    DFS_Post(find_root(1, n, 1, n));
    
    for(auto x : postorder) {
        printf("%d ", x);
    }
    return 0;
}