#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int n;
map<vector<int>, int> mp;
vector<int> adj[N];
bool vis[N];
int idx;
int DFS(int x, int par = -1) {
    vector<int> config;
    for(auto v : adj[x]) {
        if(v != par) config.push_back(DFS(v, x));
    }
    sort(config.begin(), config.end());
    if(!mp[config]) mp[config] = idx++;
    return mp[config];
}
void init() {
    for(int i = 1;i <= n;i++) {
        adj[i].clear();
    }
}
void init2() {
    mp.clear();
    idx = 1;
}
int main() {
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        init();
        init2();
        for(int i = 0;i < n - 1;i++) {
            int a, b; scanf("%d%d", &a, &b);
            adj[a].push_back(b);
            adj[b].push_back(a);
        }

        int res1 = DFS(1);
        init();
        for(int i = 0;i < n - 1;i++) {
            int a, b; scanf("%d%d", &a, &b);
            adj[a].push_back(b);
            adj[b].push_back(a);
        }

        int res2 = DFS(1);

        if(res1 == res2) printf("YES\n");
        else printf("NO\n");
    }
    return 0;
}