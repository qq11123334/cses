#include <bits/stdc++.h>
using namespace std;
const int N = 105, INF = 0x3f3f3f3f;
int row[N], col[N];
struct Edge {
    int to, cap, rev;
    Edge() {}
    Edge(int _to, int _cap, int _rev) : to(_to), cap(_cap), rev(_rev) {}
};
vector<Edge> adj[N + 5];
int level[N], iter[N];
int s, t;
void BFS() {
    memset(level, -1, sizeof(level));
    level[s] = 0;
    queue<int> q; 
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();
        for(auto e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                q.push(e.to);
                level[e.to] = level[frt] + 1;
            }
        }
    }
}
int DFS(int now, int flow) {
    if(now == t) return flow;
    for(int &i = iter[now];i < (int)adj[now].size();i++) {
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            int ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
void add_edge(int from, int to, int cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int max_flow() {
    int ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        memset(iter, 0, sizeof(iter));
        int tmp;
        while((tmp = DFS(s, INF)) > 0) {
            ret += tmp;
        }
    }
    return ret;
}
int main() {
    int n;
    int row_sum = 0, col_sum = 0;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &row[i]);
        row_sum += row[i];
    }
    for(int i = 1;i <= n;i++) {
        scanf("%d", &col[i]);
        col_sum += col[i];
    }

    s = 0, t = 2 * n + 1;
    for(int i = 1;i <= n;i++) {
        for(int j = n + 1;j <= 2 * n;j++) {
            add_edge(i, j, 1);
        }
    }

    for(int i = 1;i <= n;i++) {
        add_edge(s, i, row[i]);
    }
    for(int i = n + 1;i <= 2 * n;i++) {
        add_edge(i, t, col[i - n]);
    }

    int flow = max_flow();
    if(flow != row_sum || flow != col_sum) {
        printf("-1\n");
        exit(0);
    }
    // for(Edge &e : adj[0]) {
    //     if(e.cap > 0) {
    //         printf("-1\n");
    //         exit(0);
    //     }
    // }
    // for(int i = n + 1;i <= 2 * n;i++) {
    //     for(Edge &e : adj[i]) {
    //         if(e.to == t) {
    //             if(e.cap > 0) {
    //                 printf("-1\n");
    //                 exit(0);
    //             }
    //         }
    //     }
    // }

    char ans[N][N];

    for(int row = 1;row <= n;row++) {
        for(Edge e : adj[row]) {
            if(e.to == s) continue;
            int col = e.to - n;
            if(e.cap > 0) {
                ans[row][col] = '.';
            } else {
                ans[row][col] = 'X';
            }
        }
    }

    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            printf("%c", ans[i][j]);
        }
        printf("\n");
    }
    return 0;
}