#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 105;
int n, m;
ll a[N], b[N * N];
void solve(ll a0) {
	multiset<ll> s;
	for(int i = 0; i < m; i++) {
		s.insert(b[i]);
	}
	a[0] = a0;
	for(int i = 1; i < n; i++) {
		auto mi = s.begin();
		a[i] = *mi - a[0];
		for(int j = 0; j < i; j++) {
			auto it = s.find(a[i] + a[j]);
			if(it == s.end()) return;
			s.erase(it);
		}
	}
	for(int i = 0; i < n; i++) {
		printf("%lld ", a[i]);
	}
	printf("\n");
	exit(0);
}
int main() {
	scanf("%d", &n);
	m = (n * (n - 1)) / 2;
	for(int i = 0; i < m; i++) {
		scanf("%lld", &b[i]);
	}
	sort(b, b + m);
	for(int i = 2; i < n; i++) {
		ll sum = b[0] + b[1] + b[i];
		if(sum % 2 == 1) continue;
		ll a0 = sum / 2 - b[i];
		if(a0 <= 0) continue;
		solve(a0);
	}
}
