#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
default_random_engine gen(time(NULL) * clock());
uniform_int_distribution<int> unif(0, (int)1e9);
int main() {
	int n = 3;
	vector<int> ans;
	for(int i = 0; i < n; i++) {
		ans.push_back(unif(gen) % 10 + 1);
	}

	vector<int> sum;
	for(int i = 0; i < n; i++) {
		for(int j = (i + 1); j < n; j++) {
			sum.push_back(ans[i] + ans[j]);
		}
	}

	sort(sum.begin(), sum.end());
	sort(ans.begin(), ans.end());

	for(auto x : ans) {
		cout << x << " ";
	}
	cout << endl;

	for(auto x : sum) {
		cout << x << " ";
	}
	cout << endl;
}
