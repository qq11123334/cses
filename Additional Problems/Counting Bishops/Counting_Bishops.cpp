#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const int N = 505;
const ll MOD = (ll)1e9 + 7;
ll dp[N][N * 2];
int main() {
	int n, k;
	scanf("%d%d", &n, &k);
	if(n == 1 && k == 1) {
		printf("1\n");
		exit(0);
	} else if(n == 2 && k == 1) {
		printf("4\n");
		exit(0);
	} else if(n == 2 && k == 2) {
		printf("4\n");
		exit(0);
	} else if(k >= 2 * n - 1) {
		printf("0\n");
		exit(0);
	} 

	dp[0][0] = 1;
	dp[0][1] = 2;
	dp[1][0] = 1;
	dp[1][1] = 4;
	dp[1][2] = 2;

	for(int i = 2; i < n - 1; i++) {
		for(int j = 0; j <= 2 * (i / 2 + 1); j++) {
			ll put[3] = {0};
			put[0] = dp[i - 2][j];
			if(j - 1 >= 0) put[1] = dp[i - 2][j - 1] * 2 * ((i + 1) - (j - 1));
			if(j - 2 >= 0) put[2] = dp[i - 2][j - 2] * ((i + 1) - (j - 2)) * ((i + 1) - (j - 2) - 1);
			dp[i][j] = (put[0] + put[1] + put[2]) % MOD;
		}
	}

	ll ans = 0;

	// the index of mid diagonal = n - 1

	// mid diagonal no bishop
	for(int i = 0; i <= k; i++) {
		ans += dp[n - 2][i] * dp[n - 3][k - i] % MOD;
		ans %= MOD;
	}

	// mid diagonal has one bishop
	for(int i = 0; i <= k - 1; i++) {
		ll cnt = dp[n - 3][i] * dp[n - 2][k - i - 1] % MOD * (n - i);
		ans += cnt;
		ans %= MOD;
	}
	printf("%lld\n", ans);
	return 0;
}
