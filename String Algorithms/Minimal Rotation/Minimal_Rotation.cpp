#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
#define B (137LL)
using namespace std;
const int N = 2000005;
ll H[N], Pow_B[N];
void Rolling_Hash(string &s) {
    int n = (int)s.size();
    Pow_B[0] = 1;
    for(int i = 1;i <= n;i++) {
        H[i] = (H[i - 1] * B + s[i - 1]) % MOD;
        Pow_B[i] = Pow_B[i - 1] * B % MOD;
    }
}
ll get_hash(int left, int right) {
    return ((H[right + 1] - H[left] * Pow_B[right - left + 1]) % MOD + MOD) % MOD;
}
string s;
int my_strcmp(int L1, int R1, int L2, int R2) {
    assert(R1 - L1 == R2 - L2);
    int len = (R1 - L1 + 1);
    int l = 0, r = (len + 1);
    while(l < r - 1) {
        int m = (l + r) / 2;
        bool res = (get_hash(L1, L1 + m - 1) == get_hash(L2, L2 + m - 1));
		if(res) l = m;
        else r = m;
    }
    if(r == len + 1) return 0;
    else if(s[L1 + r - 1] > s[L2 + r - 1]) return 1;
    else if(s[L1 + r - 1] < s[L2 + r - 1]) return -1;
	exit(1);
}
int main() {
    cin.tie(0);
    ios_base::sync_with_stdio(false);
    cin >> s;
    int n = (int)s.size();
    s += s;
    Rolling_Hash(s);

    int cur_mi = 0;
    for(int i = 1;i < n;i++) {
        if(my_strcmp(cur_mi, cur_mi + n - 1, i, i + n - 1) > 0) {
            cur_mi = i;
        }
    }
    cout << s.substr(cur_mi, n) << "\n";
    return 0;
}
