#include <bits/stdc++.h>
using namespace std;
const int N = 200005, W = 26;
int state[N][W], link[N], length[N];
int dp[N];
int last_state = 0, new_state = 1;
int clone(int x) {
	int z = new_state++;
	for(int i = 0; i < W; i++) {
		state[z][i] = state[x][i];
	}
	link[z] = link[x];
	return z;
}
void SA(string &s) {
	int n = (int)s.size();
	link[0] = -1;
	for(int i = 0; i < n; i++) {
		int c = (s[i] - 'a');
		int x = last_state;
		int y = new_state++;
		last_state = y;

		length[y] = length[x] + 1;
		link[y] = 0;
		
		int s;
		for(s = x; s != -1 && state[s][c] == 0; s = link[s]) {
			state[s][c] = y;
		}

		if(s == -1) continue;

		int u = state[s][c];
		if(length[s] + 1 == length[u]) {
			link[y] = u;
			continue;
		}

		int z = clone(u);
		length[z] = length[s] + 1;
		
		for(int j = s; j != -1 && state[j][c] == u; j = link[j]) {
			state[j][c] = z;
		}
		link[u] = link[y] = z;
	}

	for(int i = last_state; i != -1; i = link[i]) {
		dp[i] = 1;
	}
}
bool vis[N];
void DFS(int x) {
	vis[x] = 1;
	for(int i = 0; i < W; i++) {
		int v = state[x][i];
		if(v) {
			if(!vis[v]) DFS(v);
			dp[x] += dp[v];
		}
	}
}
int qry(string &t) {
	int now = 0;
	for(auto c : t) {
		if(state[now][c - 'a']) now = state[now][c - 'a'];
		else return 0;
	}
	return dp[now];
}
int main() {
	cin.tie(0);
	ios_base::sync_with_stdio(false);
	string s;
	int k;
	cin >> s >> k;
	SA(s);
	DFS(0);
	while(k--) {
		string t;
		cin >> t;
		cout << qry(t) << "\n";
	}
}
