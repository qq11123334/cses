#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int cnt[N], pos[N];
vector<pair<pair<int, int>, int>> a_new(N);
void radix_sort(vector<pair<pair<int, int>, int>> &a) {
    int n = (int)a.size();
    {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.second]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.second;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    } {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.first]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.first;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    }
    
}
vector<int> p(N), c(N);
void SA(string &s) {
    s += '$';
    int n = (int)s.size();
    {
        vector<pair<int, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {s[i], i};
        sort(a.begin(), a.end());

        for(int i = 0;i < n;i++) p[i] = a[i].second;
        
        c[p[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
    }
    for(int k = 0;(1 << k) <= n;k++) {
        vector<pair<pair<int, int>, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {{c[i], c[(i + (1 << k)) % n]}, i};
        radix_sort(a);
        
        for(int i = 0;i < n;i++) p[i] = a[i].second;
        c[p[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
    }
}
int main() {
    cin.tie(0), ios_base::sync_with_stdio(false);
    string ss;
    cin >> ss;
    SA(ss);
    int n = (int)ss.size();
    // for(int i = 0;i < n;i++) {
    //     cout << ss.substr(p[i], n - p[i]) << endl;
    // }
    int q;
    cin >> q;
    while(q--) {
        string s;
        cin >> s;
        int l = -1, r = n;
        while(l < r - 1) {
            int m = (l + r) / 2;
            bool isLess = 0;
            for(int i = 0;i < (int)s.size();i++) {
                if(s[i] < ss[i + p[m]]) {
                    isLess = 0;
                    break;
                } else if(s[i] > ss[i + p[m]]) {
                    isLess = 1;
                    break;
                }
            }
            if(isLess) l = m;
            else r = m;
        }

        int LowerBound = r;


        l = -1, r = n;
        while(l < r - 1) {
            int m = (l + r) / 2;
            bool isBig = 0;
            for(int i = 0;i < (int)s.size();i++) {
                if(s[i] < ss[i + p[m]]) {
                    isBig = 1;
                    break;;
                } else if(s[i] > ss[i + p[m]]) {
                    isBig = 0;
                    break;
                }
            }

            if(isBig) r = m;
            else l = m;
        }

        int UpperBound = r;

        // cout << "UB LB = " << UpperBound << " " << LowerBound << endl; 
        cout << UpperBound - LowerBound << endl;
    }
    return 0;
}