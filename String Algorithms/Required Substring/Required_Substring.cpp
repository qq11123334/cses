#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll MOD = (ll)1e9 + 7;
const int N = 1005, M = 105, W = 26;
ll dp[N][M][2];
int F[N], nextState[N][W];
void Failure(string &pat) {
	int n = (int)pat.size(), j = 0;
	F[0] = 0;
	for(int i = 1; i < n; i++) {
		while(j != 0 && pat[i] != pat[j])
			j = F[j - 1];
		if(pat[i] == pat[j])
			j++;
		F[i] = j;
	}
	for(int i = 0; i <= n; i++) {
		for(int k = 0; k < W; k++) {
			if(i < n && pat[i] == k + 'A') {
				nextState[i][k] = i + 1;
			} else {
				if(i) nextState[i][k] = nextState[F[i - 1]][k];
			}
		}
	}
}
int main() {
	int n;
	string s;
	cin >> n >> s;
	int m = (int)s.size();
	Failure(s);
	dp[0][0][0] = 1;
	for(int i = 0; i < n; i++) {
		for(int j = 0; j <= m; j++) {
			for(int k = 0; k < W; k++) {
				int nextS = nextState[j][k];
				bool b = (nextS == m);
				dp[i + 1][nextS][b] += dp[i][j][0];
				dp[i + 1][nextS][b] %= MOD;
				dp[i + 1][nextS][1] += dp[i][j][1];
				dp[i + 1][nextS][1] %= MOD;
			}
		}
	}
	ll ans = 0;
	for(int i = 0; i <= m; i++) {
		ans += dp[n][i][1];
		ans %= MOD;
	}
	printf("%lld\n", ans);
}
