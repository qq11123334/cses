#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int N = 200005, W = 26;
int state[N][W], link[N], length[N];
ll dp1[N], dp2[N];
int last_state = 0, new_state = 1;
int clone(int x) {
	int z = new_state++;
	for(int i = 0; i < W; i++) {
		state[z][i] = state[x][i];
	}
	link[z] = link[x];
	return z;
}
void SA(string &s) {
	int n = (int)s.size();
	link[0] = -1;
	for(int i = 0; i < n; i++) {
		int c = (s[i] - 'a');
		int x = last_state;
		int y = new_state++;
		last_state = y;

		length[y] = length[x] + 1;
		link[y] = 0;
		
		int s;
		for(s = x; s != -1 && state[s][c] == 0; s = link[s]) {
			state[s][c] = y;
		}

		if(s == -1) continue;

		int u = state[s][c];
		if(length[s] + 1 == length[u]) {
			link[y] = u;
			continue;
		}

		int z = clone(u);
		length[z] = length[s] + 1;
		
		for(int j = s; j != -1 && state[j][c] == u; j = link[j]) {
			state[j][c] = z;
		}
		link[u] = link[y] = z;
	}

	for(int i = last_state; i != -1; i = link[i]) {
		dp1[i] = 1;
	}
}
bool vis[N];
void DFS(int x, bool flag) {
	vis[x] = 1;
	if(flag) dp2[x] = dp1[x];
	for(int i = 0; i < W; i++) {
		int v = state[x][i];
		if(v) {
			if(!vis[v]) DFS(v, flag);
			if(flag) dp2[x] += dp2[v];
			else dp1[x] += dp1[v];
		}
	}
}
int main() {
	string s;
	ll k;
	cin >> s >> k;

	SA(s);
	DFS(0, 0);
	memset(vis, 0, sizeof(vis));
	DFS(0, 1);

	string ans;
	int now = 0;
	while(k > 0) {
		ll cur_sum = 0;
		for(int i = 0; i < W; i++) {
			int v = state[now][i];
			if(v) {
				if(cur_sum + dp2[v] >= k) {
					ans += (i + 'a');
					k -= cur_sum;
					k -= dp1[v];
					now = v;
					break;
				} else {
					cur_sum += dp2[v];
				}
			}
		}
	}
	cout << ans << "\n";
}
