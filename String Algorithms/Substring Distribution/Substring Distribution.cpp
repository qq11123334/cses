#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int cnt[N], pos[N];
vector<pair<pair<int, int>, int>> a_new(N);
void radix_sort(vector<pair<pair<int, int>, int>> &a) {
    int n = (int)a.size();
    {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.second]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.second;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    } {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.first]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.first;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    }
}
int sa[N], c[N];
void SA(string s) {
    s += "$";
    int n = (int)s.size();
    {
        vector<pair<char, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {s[i], i};
        sort(a.begin(), a.end());

        for(int i = 0;i < n;i++) sa[i] = a[i].second;

        c[sa[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[sa[i]] = c[sa[i - 1]];
            else c[sa[i]] = c[sa[i - 1]] + 1;
        }
    }
    for(int k = 0;(1 << k) <= n;k++) {
        vector<pair<pair<int, int>, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {{c[i], c[(i + (1 << k)) % n]}, i}; // % n
        radix_sort(a);

        for(int i = 0;i < n;i++) sa[i] = a[i].second;
        c[sa[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[sa[i]] = c[sa[i - 1]];
            else c[sa[i]] = c[sa[i - 1]] + 1;
        }
    }
    // sa is the Suffix String Array
    // s.substr(sa[i], n - sa[i])
}

int rk[N], lcp[N];
void LCP(string s) {
    s += '$';
    int n = (int)s.size();
    int pre = 0;
    for(int i = 0;i < n;i++) {
        rk[sa[i]] = i;
    }

    for(int i = 0;i < n;i++) {
        if(rk[i] == 0) lcp[0] = 0;
        else {
            int j = sa[rk[i] - 1];
            if(pre) pre--;
            while(i + pre < n && j + pre < n && s[i + pre] == s[j + pre]) pre++;
            lcp[rk[i]] = pre;
        }
    }
}

int ans[N], suf[N];
int main() {
    string s;
    cin >> s;
    int n = (int)s.size();
    SA(s);
    LCP(s);
    for(int i = 1;i <= n;i++) {
        ans[i] = n - i + 1;
    }

    for(int i = 1;i <= n;i++) {
        suf[lcp[i]]++;
    }

    for(int i = n - 1;i >= 1;i--) {
        suf[i] += suf[i + 1];
    }
    for(int i = 1;i <= n;i++) {
        printf("%d ", ans[i] - suf[i]);
    }
    return 0;
}