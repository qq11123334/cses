#include <bits/stdc++.h>
using ll = long long;
using namespace std;
const ll B = 137, MOD = (1004535809LL);
const int N = 200005;
string s;
int n, q;
ll powB[N];
void build_powB() {
	powB[0] = 1;
	for(int i = 1; i < N; i++) {
		powB[i] = powB[i - 1] * B % MOD;
	}
}
struct Node {
	int left, right;
	ll hash_val;
	ll hash_val_rev;
	Node *lc, *rc;
	void pull() {
		hash_val = (lc->hash_val + rc->hash_val * (powB[rc->left - lc->left])) % MOD;
		hash_val_rev = (rc->hash_val_rev + lc->hash_val_rev * (powB[rc->right - lc->right])) % MOD;
	}
};
void build(Node *r, int L, int R) {
	r->left = L, r->right = R;
	if(L == R) {
		r->hash_val = s[L];
		r->hash_val_rev = s[L];
		return;
	}
	int M = (L + R) / 2;
	build(r->lc = new Node(), L, M);
	build(r->rc = new Node(), M + 1, R);
	r->pull();
}
ll qry(Node *r, int ql, int qr) {
	if(ql > r->right || qr < r->left) return 0;
	if(ql <= r->left && r->right <= qr) return (r->hash_val * powB[r->left - ql]) % MOD;
	return (qry(r->lc, ql, qr) + qry(r->rc, ql, qr)) % MOD;
}
ll qry_rev(Node *r, int ql, int qr) {
	if(ql > r->right || qr < r->left) return 0;
	if(ql <= r->left && r->right <= qr) return (r->hash_val_rev * powB[qr - r->right]) % MOD;
	return (qry_rev(r->lc, ql, qr) + qry_rev(r->rc, ql, qr)) % MOD;
}
void upd(Node *r, int pos, int val) {
	if(r->left == r->right) {
		r->hash_val = val;
		r->hash_val_rev = val;
		s[pos] = (char)val;
		return;
	}

	int mid = (r->left + r->right) / 2;
	if(pos <= mid) upd(r->lc, pos, val);
	else upd(r->rc, pos, val);
	r->pull();
}
int main() {
	cin.tie(0);
	ios_base::sync_with_stdio(false);
	
	build_powB();
	cin >> n >> q;
	cin >> s;
	Node *root;
	build(root = new Node(), 0, n - 1);
	while(q--) {
		int query; cin >> query;
		if(query == 1) {
			int pos;
			char c;
			cin >> pos >> c;
			pos--;
			upd(root, pos, c);
		} else {
			int L, R;
			cin >> L >> R;
			L--, R--;
			int hash = qry(root, L, R);
			int hash_rev = qry_rev(root, L, R);
			if(hash == hash_rev) cout << "YES\n";
			else cout << "NO\n";
		}
	}
	return 0;
}