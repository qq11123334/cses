#include <bits/stdc++.h>
#define N ((int)1e6 + 5)
using namespace std;
int Z[N];
void Z_algorithm(string s) {
    int n = (int)s.size();
    int x = -1, y = -1;
    Z[0] = n;
    for(int i = 1;i < n;i++) {
        if(y < i) {
            x = y = i;
            while(y < n && s[y - x] == s[y])
                y++;
            Z[i] = y - x;
            y--;
        } else if(Z[i - x] + i < y) {
            Z[i] = Z[i - x];
        } else {
            x = i;
            while(y < n && s[y - x] == s[y])
                y++;
            Z[i] = y - x;
            y--;
        }
    }
}
int main() {
    string s; cin >> s;
    Z_algorithm(s);
    int n = (int)s.size();
    for(int len = 1;len <= n;len++) {
        bool is_able = 1;
        for(int i = 0;i < n;i += len) {
            if(Z[i] < len && Z[i] + i < n) {
                is_able = 0;
            }
        }
        if(is_able) printf("%d ", len);
    }
    printf("\n");
    return 0;
}