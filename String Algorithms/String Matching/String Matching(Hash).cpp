#include <bits/stdc++.h>
#define ll long long int
#define MOD ((ll)1e9 + 7)
#define B (137LL)
using namespace std;
const int N = 1000005;
ll H[N], Pow_B[N];
void Rolling_Hash(string &s) {
    int n = (int)s.size();
    Pow_B[0] = 1;
    for(int i = 1;i <= n;i++) {
        H[i] = (H[i - 1] * B + s[i - 1]) % MOD;
        Pow_B[i] = Pow_B[i - 1] * B % MOD;
    }
}
ll get_hash(int left, int right) {
    return (((H[right + 1] - H[left] * Pow_B[right - left + 1] % MOD) + MOD) % MOD);
}
ll all_hash(string &s) {
    ll res = 0;
    for(auto c : s) {
        res = (res * B + c) % MOD;
    }
    return res;
}
int main() {
    string a, b;
    cin >> a >> b;
    int n = (int)a.size();
    Rolling_Hash(a);
    ll b_hash = all_hash(b);
    int b_len = (int)b.size();
    int cnt = 0;
    for(int i = 0;i < n;i++) {
        if(i - b_len + 1 < 0) continue;
        if(get_hash(i - b_len + 1, i) == b_hash) {
            cnt++;
        }
    }
    cout << cnt << endl;
    return 0;
}