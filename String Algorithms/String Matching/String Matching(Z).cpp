#include <bits/stdc++.h>
using namespace std;
const int N = 2000005;
string pat, text, fit;
int Z[N];
void Z_algorithm(string &s) {
    int n = (int)s.size();
    int x = -1, y = -1;
    Z[0] = 0;
    for(int i = 1;i < n;i++) {
        if(y < i) {
            x = y = i;
            while(s[y - x] == s[y] && y < n)
                y++;
            Z[i] = y - x;
            y--; 
        } else if(i + Z[i - x] < y) {
            Z[i] = Z[i - x];
        } else {
            x = i;
            while(s[y - x] == s[y] && y < n)
                y++;
            Z[i] = y - x;
            y--;
        }
    }
}
int main() {
    cin >> text >> pat;
    fit = pat + "$" + text;
    Z_algorithm(fit);
    
    int ans = 0;
    for(int i = 0;i < (int)fit.size();i++) {
        if(Z[i] == (int)pat.size()) {
            ans++;
        }
    }
    printf("%d\n", ans);
    return 0;
}