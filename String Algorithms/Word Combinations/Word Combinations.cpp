#include <bits/stdc++.h>
#define ll long long int
#define MOD_A ((ll)1e9 + 7)
#define MOD ((ll)1004535809)
#define B (31LL)
using namespace std;
const int N = 5005;
ll H[N];
ll fac[N];
void Rolling_Hash(string &s) {
    int n = (int)s.size();
    fac[0] = 1;
    for(int i = 1;i <= n;i++) {
        H[i] = ((s[i - 1]) + H[i - 1] * B) % MOD;
        fac[i] = (fac[i - 1] * B) % MOD;
    }
}
ll get_hash(int left, int right) {
    int len = (right - left + 1);
    ll res = (H[right + 1] - H[left] * fac[len] % MOD);
    return (res % MOD + MOD) % MOD;
}
ll all_hash(string &s) {
    ll res = 0;
    for(auto c : s) {
        res = (res * B + c) % MOD;
    }
    return res;
}
unordered_set<int> str_len;
unordered_set<ll> str_hash;
int main() {
    ios_base::sync_with_stdio(0), cin.tie(0);
    string s; cin >> s;
    int n = (int)s.size();
    Rolling_Hash(s);
    int k; cin >> k;
    while(k--) {
        string sk; cin >> sk;
        str_len.insert((int)sk.size());
        str_hash.insert(all_hash(sk));
    }

    ll dp[N];
    memset(dp, 0, sizeof(dp));
    dp[0] = 1;
    for(int i = 0;i < n;i++) {
        // cout << i << " " << dp[i] << endl;
        for(auto len : str_len) {
            if(i - len + 1 >= 0) {
                ll hash_value = get_hash(i - len + 1, i);
                if(str_hash.count(hash_value)) {
                    dp[i + 1] += dp[(i + 1) - len];
                    dp[i + 1] %= MOD_A;
                }
            }
        }
    }
    cout << dp[n];
    return 0;
}