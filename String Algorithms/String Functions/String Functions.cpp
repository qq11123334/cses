#include <bits/stdc++.h>
using namespace std;
const int N = 1000005;
int Z[N];
void Z_algorithm(string &s) {
    int n = (int)s.size();
    int x = -1, y = -1;
    Z[0] = 0;
    for(int i = 1;i < n;i++) {
        if(y < i) {
            x = y = i;
            while(y < n && s[y - x] == s[y])
                y++;
            Z[i] = y - x;
            y--;
        } else if(i + Z[i - x] < y) {
            Z[i] = Z[i - x];
        } else {
            x = i;
            while(y < n && s[y - x] == s[y])
                y++;
            Z[i] = y - x;
            y--;
        }
    }
}
int F[N];
void Failure(string &pat) {
    int n = (int)pat.size();
    int j = 0;
    F[0] = 0;
    for(int i = 1;i < n;i++) {
        while(j != 0 && pat[i] != pat[j]) 
            j = F[j - 1];
        if(pat[i] == pat[j])
            j++;
        F[i] = j;
    }
}
int main() {
    cin.tie(0), ios_base::sync_with_stdio(false);
    string s;
    cin >> s;
    Z_algorithm(s);
    Failure(s);
    int n = (int)s.size();
    for(int i = 0;i < n;i++) {
        cout << Z[i] << " ";
    }
    cout << "\n";
    for(int i = 0;i < n;i++) {
        cout << F[i] << " ";
    }
    return 0;
}