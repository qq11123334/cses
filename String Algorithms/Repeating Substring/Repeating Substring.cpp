#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int cnt[N], pos[N];
vector<pair<pair<int, int>, int>> a_new(N);
void radix_sort(vector<pair<pair<int, int>, int>> &a) {
    int n = (int)a.size();
    {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.second]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.second;
            a_new[pos[i]] = x;
            pos[i]++;
        } 
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    } {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.first]++;
        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        for(auto x : a) {
            int i = x.first.first;
            a_new[pos[i]] = x;
            pos[i]++;
        } 
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    }
}
vector<int> p(N), c(N);
void SA(string s) {
    s += '$';
    int n = (int)s.size();
    {
        vector<pair<char, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {s[i], i};
        sort(a.begin(), a.end());
        for(int i = 0;i < n;i++) p[i] = a[i].second;

        c[p[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
    } {
        for(int k = 0;(1 << k) <= n;k++) {
            vector<pair<pair<int, int>, int>> a(n);
            for(int i = 0;i < n;i++) a[i] = {{c[i], c[(i + (1 << k)) % n]}, i};
            radix_sort(a);
            for(int i = 0;i < n;i++) p[i] = a[i].second;
            
            c[p[0]] = 0;
            for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
        }
    }
}
int lcp[N], rk[N];
void LCP(string s) {
    s += '$';
    int n = (int)s.size();
    int pre = 0;
    for(int i = 0;i < n;i++) {
        rk[p[i]] = i;
    }

    for(int i = 0;i < n;i++) {
        if(rk[i] == 0) {
            lcp[0] = 0;
        } else {
            int j = p[rk[i] - 1];
            if(pre) pre--;
            while(j + pre < n && i + pre < n && s[j + pre] == s[i + pre]) pre++;
            lcp[rk[i]] = pre;
        }
    }
}
int main() {
    string s;
    cin >> s;
    SA(s);
    LCP(s);
    int n = (int)s.size() + 1;
    int mx_pos = 0;
    for(int i = 1;i < n;i++) {
        if(lcp[i] > lcp[mx_pos]) {
            mx_pos = i;
        }
    }
    if(lcp[mx_pos]) cout << s.substr(p[mx_pos], lcp[mx_pos]) << "\n";
    else cout << "-1\n";
    return 0;
}