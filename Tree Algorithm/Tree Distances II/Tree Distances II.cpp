#include <bits/stdc++.h>
#define ll long long int
#ifdef _WIN32
#define lld "%I64d"
#else
#define lld "%lld"
#endif
using namespace std;
vector <ll> adj[200005];
ll cnt[200005];
ll subtree[200005];
ll sum[200005];
int n;
void toleaf(int x, int par)
{
    subtree[x] = 1;
    for(auto i : adj[x])
    {
        if(i != par) 
        {
            toleaf(i, x);
            subtree[x] = subtree[x] + subtree[i];
        }
    }
}
ll cnt_toleaf(int x, int par)
{
    for(auto i : adj[x])
    {
        if(i != par) cnt[x] = cnt[x] + cnt_toleaf(i, x) + subtree[i];
    }
    return cnt[x];
}
void dis_sum(int x, int par)
{
    if(par != 0) sum[x] = sum[par] - subtree[x] + (n - subtree[x]);
    else sum[x] = cnt[x];
    for(auto i : adj[x]) if(i != par) dis_sum(i, x);

}
int main()
{
    scanf("%d",&n);
    for(int i = 0;i < n - 1;i++)
    {
        int a, b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    toleaf(1,0);
    cnt_toleaf(1,0);
    dis_sum(1,0);

    for(int i = 1;i <= n;i++)
    {
        printf(lld " ",sum[i]);
    }
    return 0;
}