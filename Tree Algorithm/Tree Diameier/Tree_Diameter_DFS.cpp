#include <bits/stdc++.h>
using ll = long long int;
using namespace std;
const int N = 200005;
vector<int> adj[N];
ll dep[N];
void DFS(int x, ll dis, ll &mx, ll p) {
    dep[x] = dis;
    if(dep[mx] <= dep[x]) {
        mx = x;
    }
    for(auto v : adj[x]) {
        if(v == p) continue;
        DFS(v, dis + 1, mx, x);
    }
}
ll Tree_Diameter() {
    ll c = 1;
    DFS(1, 0, c, -1);
    DFS(c, 0, c, -1);
    return dep[c];
}
int main() {
    int n; scanf("%d", &n);
    for(int i = 0;i < n - 1;i++) {
        int a, b; scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    printf("%lld\n", Tree_Diameter());
} 