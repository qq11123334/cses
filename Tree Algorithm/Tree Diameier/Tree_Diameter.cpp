#include <bits/stdc++.h>
using namespace std;
vector<int> adj[200005];
int dp[200005];
int par[200005];
bool vis[200005];
int toleaf(int x)
{
    if(dp[x]) return dp[x];
    vis[x] = 1;
    int ans = 0;
    for(auto i : adj[x])
    {
        if(!vis[i]) 
        {
            par[i] = x;
            ans = max(ans, toleaf(i) + 1);
        }
    }
    return dp[x] = ans;
}
int maxlength(int x)
{
    int mx1 = 0, mx2 = 0;
    for(int i : adj[x])
    {
        if(par[x] == i) continue;
        int tmp = toleaf(i) + 1;
        if(mx1 < tmp) swap(mx1, tmp);
        if(mx2 < tmp) swap(mx2, tmp);
    }
    //printf("%d %d %d\n",x , mx1, mx2);
    return mx1 + mx2;
}
int main()
{
    int n;
    scanf("%d",&n);
    for(int i = 1;i <= n - 1;i++)
    {
        int a, b;
        scanf("%d%d",&a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    toleaf(1);
    int mx = 0;
    for(int i = 1;i <= n;i++)
    {
        mx = max(mx, maxlength(i));
    }
    printf("%d",mx);
    return 0;
}