#include <bits/stdc++.h>
#define MAX_log 31
using namespace std;
vector <int> adj[200005];
int par[200005][32];
int dep[200005];
int n, q;
void DFS(int level, int x, int u)
{
    dep[x] = level;
    for(auto i : adj[x])
    {
        if(i != u) 
        {
            DFS(level + 1, i, x);
        }
    }
}
int LCA(int a, int b)
{
    if(dep[a] > dep[b]) swap(a, b);
    int dif = dep[b] - dep[a];
    for(int i = 0;i < MAX_log;i++)
    {
        if(dif & (1 << i))
        {
            b = par[b][i];
        }
    }

    if(a == b) return a;

    for(int i = MAX_log - 1;i >= 0;i--)
    {
        if(par[a][i] != par[b][i])
        {
            a = par[a][i];
            b = par[b][i];
        }
    }
    return par[a][0];
}
void build()
{
    for(int i = 1;i < MAX_log;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            par[j][i] = par[par[j][i - 1]][i - 1];
        }
    }
}
int main()
{
    scanf("%d%d", &n, &q);
    for(int i = 2;i <= n;i++)
    {
        int ancestor;
        scanf("%d", &ancestor);
        adj[ancestor].push_back(i);
        par[i][0] = ancestor;
    }
    DFS(0, 1, 0);
    build();
    while(q--)
    {
        int a, b;
        scanf("%d %d", &a, &b);
        printf("%d\n", LCA(a, b));
    }
    return 0;
}