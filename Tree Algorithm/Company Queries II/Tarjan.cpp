#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
typedef pair<int, int> Query;
int p[N], ans[N];
vector<int> adj[N];
vector<Query> query[N];
bool mark[N];
int find(int x) {
	if(p[x] == x) return x;
	else return p[x] = find(p[x]);
}
void DFS(int x, int par) {
	mark[x] = true;
	for(auto [v, q] : query[x]) {
		if(!mark[v]) continue;
		ans[q] = find(v);
	}
	for(auto v : adj[x]) {
		if(v == par) continue;
		DFS(v, x);
	}
	p[x] = par;
}
void init() {
	for(int i = 0; i < N; i++) {
		p[i] = i;
	}
}
int main() {
	init();
	int n, q;
	scanf("%d%d", &n, &q);
	for(int i = 2; i <= n; i++) {
		int par;
		scanf("%d", &par);
		adj[par].push_back(i);
	}

	for(int i = 0; i < q; i++) {
		int a, b;
		scanf("%d%d", &a, &b);
		query[a].push_back({b, i});
		query[b].push_back({a, i});
	}

	DFS(1, 0);
	for(int i = 0; i < q; i++) {
		printf("%d\n", ans[i]);
	}
	return 0;
}
