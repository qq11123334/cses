#include <bits/stdc++.h>
#define ll long long int
using namespace std;
struct Node {
    int left, right, mid;
    ll add;
    Node *lc, *rc;
}node[500500], *last_node = node, *root_node;
void build(Node *root, int left, int right) {
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right) {
        root->add = 0;
        return;
    }

    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
}
void upd(Node *root, int left, int right, ll dif) {
    if(root->left == left && root->right == right) {
        root->add += dif;
        return;
    }

    if(right <= root->mid) {
        upd(root->lc, left, right, dif);
    } else if(left > root->mid) {
        upd(root->rc, left, right, dif);
    } else {
        upd(root->lc, left, root->mid, dif);
        upd(root->rc, root->mid + 1, right, dif);
    }
}
ll qry(Node *root, int pos) {
    if(root->left == root->right) {
        return root->add;
    }
    if(pos <= root->mid) {
        return root->add + qry(root->lc, pos);
    } else {
        return root->add + qry(root->rc, pos);
    }
}
bitset<200005> vis;
vector<int> adj[200005];
ll sz[200005];
vector<int> tree_arr;
int node_to_index[200005];
ll path_sum[200005]; // sum of root to i = path_sum[i] 
ll node_val[200005];
void DFS(int x, ll dis) {
    vis[x] = 1;
    sz[x] = 1;
    node_to_index[x] = (int)tree_arr.size(); 
    tree_arr.push_back(x);
    path_sum[x] = dis + node_val[x];
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS(v, path_sum[x]);
            sz[x] += sz[v];
        }
    }
    // printf("%d %lld\n", x, sz[x]);
}
int main() {
    // freopen("input.txt", "r", stdin);
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &node_val[i]);
    }
    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    tree_arr.push_back(0);
    vis.reset();
    DFS(1, 0);
    build(root_node = last_node++, 1, n);

    while(q--) {
        int query;
        scanf("%d", &query);
        if(query == 1) {
            int node;
            ll val;
            scanf("%d%lld", &node, &val);
            ll dif = val - node_val[node];
            upd(root_node, node_to_index[node], node_to_index[node] + sz[node] - 1, dif);
            node_val[node] = val;
        } else {
            int x;
            scanf("%d", &x);
            printf("%lld\n", path_sum[x] + qry(root_node, node_to_index[x]));
        }
    }
    return 0;
}