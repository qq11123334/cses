#include <bits/stdc++.h>
using namespace std;
vector <int> adj[200005];
int cnt[200005];
void DFS(int x)
{
    cnt[x] = 1;
    for(int i = 0;i < (int)adj[x].size();i++)
    {
        DFS(adj[x][i]);
        cnt[x] += cnt[adj[x][i]];
    }
}
int main()
{
    int n;
    scanf("%d",&n);
    for(int i = 2;i <= n;i++)
    {
        int p;
        scanf("%d",&p);
        adj[p].push_back(i);
    }
    DFS(1);
    for(int i = 1;i <= n;i++)
    {
        printf("%d ",cnt[i] - 1);
    }
    return 0;
}