#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200002;
int sz[N], max_sz[N];
bool vis[N];
int cnt[N];
vector<int> adj[N], node;
vector<pair<int, int>> dis_fa;
ll ans;
int k;
void DFS_dis(int x, int p, int sub_fa, int level) {
    if(level > k) return;
    dis_fa.emplace_back(level, sub_fa);
    for(auto v : adj[x]) {
        if(v != p && !vis[v]) DFS_dis(v, x, sub_fa, level + 1);
    }
}
void DFS_SZ(int x) {
    sz[x] = 1, max_sz[x] = 0;
    vis[x] = 1;
    node.push_back(x);
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS_SZ(v);
            sz[x] += sz[v];
            max_sz[x] = max(max_sz[x], sz[v]);
        }
    }
}
void init() {
    node.clear();
    dis_fa.clear();
}
void DFS(int x) {
    init();
    DFS_SZ(x);
    int n = (int)node.size(), centroid = -1;
    for(auto v : node) {
        vis[v] = 0;
        cnt[v] = 0;
        int max_comp = max(max_sz[v], n - sz[v]);
        if(max_comp * 2 <= n) {
            centroid = v;
        }
    }
    vis[centroid] = 1;

    dis_fa.emplace_back(0, centroid);
    for(auto v : adj[centroid]) {
        if(!vis[v]) DFS_dis(v, centroid, v, 1);
    }

    sort(dis_fa.begin(), dis_fa.end());

    int m = (int)dis_fa.size();
    int left = m;
    int right = m - 1;
    ll tmp = ans;
    for(int i = 0;i < m;i++) {
        int len = dis_fa[i].first;
        int sub_fa = dis_fa[i].second;
        while(right != -1 && dis_fa[right].first > k - len) {
            cnt[dis_fa[right].second]--;
            right--;
        }
        while(left && dis_fa[left - 1].first >= k - len) {
            left--;
            cnt[dis_fa[left].second]++;
        }

        ans += (right - left + 1);
        ans -= (cnt[sub_fa]);
    }
    
    if(tmp == ans) return ;

    for(auto v : adj[centroid]) {
        if(!vis[v]) DFS(v);
    }
}
int main() {
    int n;
    scanf("%d%d", &n, &k);
    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1);

    printf("%lld\n", ans / 2);
    return 0;
}
