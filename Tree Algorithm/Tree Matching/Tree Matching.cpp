#include <bits/stdc++.h>
using namespace std;
vector <int> adj[200005];
bool mark[200005];
bool vis[200005];
int cnt;
bool DFS(int x, int v)
{
    vis[x] = 1;
    bool status = 0;
    for(int i : adj[x])
    {
        if(i != v)
        {
            status |= DFS(i, x);
        }
    }
    if(status) 
    {
        cnt++;
        return 0;
    }
    else return 1;
}
int main()
{
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n - 1;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    for(int i = 1;i <= n;i++)
    {
        if(!vis[i]) DFS(i, 0);
    }
    printf("%d",cnt);
    return 0;
}