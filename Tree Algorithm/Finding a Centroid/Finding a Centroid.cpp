#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
vector<int> adj[N];
bool vis[N];
int sz[N], mx_sz[N];
int n;
void DFS(int x) {
    sz[x] = 1; vis[x] = 1;
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS(v);
            sz[x] += sz[v];
            mx_sz[x] = max(mx_sz[x], sz[v]);
        }
    }
    mx_sz[x] = max(mx_sz[x], n - sz[x]);
}
int main() {
    scanf("%d", &n); 
    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d" ,&a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
 
    DFS(1);
 
    for(int i = 1;i <= n;i++) {
        if(mx_sz[i] <= n / 2) {
            printf("%d\n", i);
            return 0;
        }
    }
    return 0;
}