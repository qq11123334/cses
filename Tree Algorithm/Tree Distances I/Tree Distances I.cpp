#include <bits/stdc++.h>
using namespace std;
int max1[200005], max2[200005];
int leaf[200005];
int par[200005];
int dir[200005];
bool vis[200005];
vector <int> adj[200005];
int toleaf(int x)
{
    vis[x] = 1;
    for(int i = 0;i < (int)adj[x].size();i++)
    {
        if(!vis[adj[x][i]]) 
        {
            par[adj[x][i]] = x;
            int res = toleaf(adj[x][i]) + 1;
            if(res > max1[x]) 
            {
                swap(res, max1[x]);
                dir[x] = adj[x][i];
            }
            if(res > max2[x]) swap(res, max2[x]);
        }
    }
    return max1[x];
}
void maxlength(int x)
{
    if(vis[x]) return;
    vis[x] = 1;
    if(par[x] != 0)
    {
        int res;
        //printf("%d %d %d\n",x, par[x],  dir[par[x]]);
        if(x != dir[par[x]]) res = max1[par[x]] + 1;
        else res = max2[par[x]] + 1;
        if(res > max1[x]) 
        {
            swap(res, max1[x]);
            dir[x] = par[x];
        }
        if(res > max2[x]) swap(res, max2[x]);
    }
    for(int i : adj[x])
    {
        maxlength(i);
    }
}
int main()
{
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n - 1;i++)
    {
        int a, b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    toleaf(1);

    memset(vis, 0 ,sizeof(vis));
    maxlength(1);
    for(int i = 1;i <= n;i++)
    {
        printf("%d ", max1[i]);
    }
    return 0;
}