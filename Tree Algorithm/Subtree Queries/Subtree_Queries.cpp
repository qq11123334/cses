#include <bits/stdc++.h>
#define ll long long int
using namespace std;
struct Node
{  
    int left, right, mid;
    ll val;
    Node *lc, *rc;
}node[400005], *node_root, *last_node = node;
void build(Node *root, int left, int right);
ll query(Node *root, int left, int right);
void upd(Node *root, int x, ll dif);
vector <int>adj[200005], trav_arr;
int subtree[200005], value[200005], tmp[200005];
int node_to_idx[200005];
void DFS(int x, int p)
{
    subtree[x] = 1;
    trav_arr.push_back(x);
    for(auto i : adj[x])
    {
        if(i != p) 
        {
            DFS(i, x);
            subtree[x] += subtree[i];
        }
    }
}
int main()
{
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &tmp[i]);
    }
    for(int i = 0;i < n - 1;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    
    trav_arr.push_back(0);
    DFS(1, 0);
    for(int i = 1;i <= n;i++)
    {
        value[i] = tmp[trav_arr[i]];
        node_to_idx[trav_arr[i]] = i;
    }

    build(node_root = last_node++, 1, n);

    while(q--)
    {
        int cmd;
        scanf("%d", &cmd);
        if(cmd == 1)
        {
            int x, val;
            scanf("%d%d", &x, &val);
            upd(node_root, node_to_idx[x], val);
        }
        else
        {
            int s;
            scanf("%d", &s);
            printf("%lld\n", query(node_root, node_to_idx[s], node_to_idx[s] + subtree[s] - 1));
        }
    }
    return 0;
}
void build(Node *root, int left, int right)
{
    root->left = left;
    root->right = right;
    root->mid = (left + right) / 2;
    if(left == right)
    {
        root->val = value[left];
        return;
    }
    build(root->lc = last_node++, left, root->mid);
    build(root->rc = last_node++, root->mid + 1, right);
    root->val = root->lc->val + root->rc->val;
}
ll query(Node *root, int left, int right)
{
    //printf("%d %d %d %d %d\n", root->lc->left, root->lc->right, root->mid, left, right);
    if(root->left == left && root->right == right) return root->val;
    else if(left > root->mid) return query(root->rc, left, right);
    else if(right <= root->mid) return query(root->lc, left, right);
    else return query(root->lc, left, root->mid) + query(root->rc, root->mid + 1, right);
}
void upd(Node *root, int x, ll val)
{
    if(root->left == root->right)
    {
        value[x] = val;
        root->val = val;
        return;
    }
    if(x <= root->mid) upd(root->lc, x, val);
    if(x > root->mid) upd(root->rc, x, val);
    root->val = root->lc->val + root->rc->val;
}