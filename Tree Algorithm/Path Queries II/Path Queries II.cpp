#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#include <bits/stdc++.h>
using namespace std;
const int N = 200500;
// to[x] = the largest child of x
// fr[x] = the lead of the chain passing through x
vector<int> adj[N];
int sz[N], par[N], to[N], fr[N], dep[N];
int tree_arr[N], val[N], dfn[N];
void DFS(int x, int p) {
    sz[x] = 1;
    par[x] = p;
    dep[x] = dep[p] + 1;
    to[x] = -1;
    for(auto v : adj[x]) {
        if(v != p) {
            DFS(v, x);
            sz[x] += sz[v];
            if(to[x] == -1 || sz[v] > sz[to[x]]) to[x] = v;
        }
    }
}
void Link(int x, int f) {
    static int ord = 1;
    dfn[x] = ord;
    tree_arr[ord++] = x;
    // and use Data Structure maintain tree_arr
    fr[x] = f;
    if(to[x] != -1) Link(to[x], f);
    for(int v : adj[x]) {
        if(v == par[x] || v == to[x]) continue;
        Link(v, v);
    }
}
vector<pair<int, int>> Query(int u, int v) {
    // return the intervals on the path from u to v
    int fu = fr[u], fv = fr[v];
    vector<pair<int, int>> res;
    while(fu != fv) {
        if(dep[fu] < dep[fv]) {
            swap(fu, fv);
            swap(u, v);
        }
        res.emplace_back(dfn[fu], dfn[u]);
        u = par[fu];
        fu = fr[u];
    }
    if(dep[u] > dep[v]) swap(u, v);
    // u is LCA
    res.emplace_back(dfn[u], dfn[v]);
    return res;
}
struct Node {
    int left, right, mid;
    int mx_val;
    Node *lc, *rc;
    void pull() {
        mx_val = max(lc->mx_val, rc->mx_val);
    }
}node[2 * N], *last_node = node, *root_node;
void build(Node *r, int left, int right) {
    r->left = left;
    r->right = right;
    r->mid = (left + right) / 2;
    if(left == right) {
        r->mx_val = val[tree_arr[left]];
        return;
    }
    build(r->lc = last_node++, left, r->mid);
    build(r->rc = last_node++, r->mid + 1, right);
    r->pull();
}
int qry(Node *r, int ql, int qr) {
    if(ql > r->right || qr < r->left) return 0;
    if(ql <= r->left && r->right <= qr) return r->mx_val;
    return max(qry(r->lc, ql, qr), qry(r->rc, ql, qr));
}
void upd(Node *r, int pos, int val_x) {
    if(r->left == r->right) {
        r->mx_val = val_x;
        return;
    }
    if(r->mid >= pos) upd(r->lc, pos, val_x);
    else upd(r->rc, pos, val_x);
    r->pull();
}
int main() {
    ios_base::sync_with_stdio(0), cin.tie(0);
    int n, q; cin >> n >> q;
    for(int i = 1;i <= n;i++) {
        cin >> val[i];
    }
    for(int i = 0;i < n - 1;i++) {
        int a, b; cin >> a >> b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1, 0);
    Link(1, 1);
    build(root_node = last_node++, 1, n);
    while(q--) {
        int query; cin >> query;
        if(query == 1) {
            int s, x; cin >> s >> x;
            upd(root_node, dfn[s], x);
        } else if(query == 2) {
            int a, b; cin >> a >> b;
            int mx = 0;
            auto QryV = Query(a, b);
            for(auto &P : QryV) {
                mx = max(mx, qry(root_node, P.first, P.second));
            }
            cout << mx << " ";
        }
    }
}