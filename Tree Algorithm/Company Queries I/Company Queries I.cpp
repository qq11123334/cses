#include <bits/stdc++.h>
using namespace std;
int par[200005][32];
int main()
{
    int n, q;
    scanf("%d%d", &n, &q);
    for(int i = 2;i <= n;i++)
    {
        int anc;
        scanf("%d", &anc);
        par[i][0] = anc;
    }
    int t = log2(n);
    for(int i = 1;i <= t;i++)
    {
        for(int j = 2;j <= n;j++)
        {
            par[j][i] = par[par[j][i - 1]][i - 1];
        }
    }
    while(q--)
    {
        int num, level;
        scanf("%d%d", &num, &level);
        while(level != 0)
        {
            int t = log2(level);
            num = par[num][t];
            level = level - (1 << t);
        }
        printf("%d\n", num == 0 ? -1 : num);
    }
    return 0;
}