#include <bits/stdc++.h>
#define mx_log 25
using namespace std;
vector <int> adj[200005];
vector <pair<int, int>> sort_by_dep;
int n;
int par[200005][32];
int cnt[200005];
int dep[200005];
int mark[200005];
void DFS(int cur, int las, int level)
{
    dep[cur] = level;
    for(auto nxt : adj[cur])
    {
        if(nxt != las)
        {
            par[nxt][0] = cur;
            DFS(nxt, cur, level + 1);
        }
    }
}
void build_anc()
{
    for(int i = 1;i < mx_log;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            par[j][i] = par[par[j][i - 1]][i - 1];
        }
    }
}
int LCA(int a, int b)
{
    if(dep[a] > dep[b]) swap(a, b);
    int dif = dep[b] - dep[a];
    for(int i = 0;i < mx_log;i++)
    {
        if(dif & (1 << i))
        {
            b = par[b][i];
        }
    }

    if(a == b) return a;

    for(int i = mx_log;i >= 0;i--)
    {
        if(par[a][i] != par[b][i])
        {
            a = par[a][i];
            b = par[b][i];
        }
    }

    return par[a][0];
}
int main()
{
    int m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < n - 1;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1, 0, 0);
    build_anc();

    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        int lca = LCA(a, b);
        
        // 重點
        mark[par[lca][0]]--;
        mark[lca]--;
        //
        
        mark[a]++;
        mark[b]++;

        // printf("%d %d %d\n", a, b, par[lca][0]);
    }

    for(int i = 1;i <= n;i++)
    {
        sort_by_dep.push_back(make_pair(-dep[i], i));
    }

    sort(sort_by_dep.begin(), sort_by_dep.end());

    for(int i = 0;i < n;i++)
    {
        int cur = sort_by_dep[i].second;
        cnt[cur] += mark[cur];
        cnt[par[cur][0]] += cnt[cur];
    }

    // printf("??");

    for(int i = 1;i <= n;i++)
    {
        printf("%d ", cnt[i]);
    }
    return 0;
}