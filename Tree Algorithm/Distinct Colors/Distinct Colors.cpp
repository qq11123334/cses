#include <bits/stdc++.h>
using namespace std;
int ans[200005];
int color[200005];
map<int, int> subtree_color[200005];
vector<int> adj[200005];
void DFS(int x, int par) {
    subtree_color[x][color[x]]++;
    for(auto v : adj[x]) {
        if(v != par) {
            DFS(v, x);
            if(subtree_color[v].size() > subtree_color[x].size())
                swap(subtree_color[v], subtree_color[x]);
            for(auto p : subtree_color[v]) {
                subtree_color[x][p.first] += p.second;
            }
        }
    }

    ans[x] = (int)subtree_color[x].size();
}
int main() {
    int n;
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &color[i]);
    }

    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1, 0);
    
    for(int i = 1;i <= n;i++) {
        printf("%d ", ans[i]);
    }
    return 0;
}