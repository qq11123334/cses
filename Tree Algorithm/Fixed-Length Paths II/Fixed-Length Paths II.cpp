#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 200001;
vector<int> adj[N], node;
vector<pair<int, int>> dis_fa;
bool vis[N];
int sz[N], max_sz[N], cnt[N], k1, k2;
void DFS_dis(int x, int p, int sub_fa, int level) {
    dis_fa.emplace_back(level, sub_fa);
    for(auto v : adj[x]) {
        if(v != p && !vis[v]) DFS_dis(v, x, sub_fa, level + 1);
    }
}
void DFS_SZ(int x) {
    sz[x] = 1;
    vis[x] = 1;
    max_sz[x] = 0;
    node.push_back(x);
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS_SZ(v);
            sz[x] += sz[v];
            max_sz[x] = max(max_sz[x], sz[v]);
        }
    }
}
void init() {
    dis_fa.clear(); node.clear();
}
ll ans;
void DFS(int x) {
    init(); DFS_SZ(x);
    int n = (int)node.size(), centroid = -1;
    for(auto v : node) {
        vis[v] = 0; cnt[v] = 0;
        int mx_comp = max(n - sz[v], max_sz[v]);
        if(mx_comp * 2 <= n + 1) centroid = v; 
    }
    vis[centroid] = 1;
    dis_fa.emplace_back(0, centroid);
    for(auto v : adj[centroid]) {
        if(!vis[v]) {
            DFS_dis(v, centroid, v, 1);
        }
    }
    sort(dis_fa.begin(), dis_fa.end());
    int m = (int)dis_fa.size();
    int left = m, right = m - 1;
    
    ll tmp = ans;
    // use Two Pointer
    for(int i = 0;i < m;i++) {
        int len = dis_fa[i].first;
        int sub_fa = dis_fa[i].second;
        while(right != -1 && dis_fa[right].first > k2 - len) {
            cnt[dis_fa[right].second]--;
            right--;
        }
        while(left && dis_fa[left - 1].first >= k1 - len) {
            left--;
            cnt[dis_fa[left].second]++;
        }
        ans += (right - left + 1);
        ans -= (cnt[sub_fa]); // in Same Subtree
    }

    if(ans == tmp) return;

    for(auto v : adj[centroid]) {
        if(!vis[v]) DFS(v);
    }
}
int main() {
    int n;
    scanf("%d%d%d", &n, &k1, &k2);
    for(int i = 0;i < n - 1;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    
    dis_fa.resize(n);
    node.resize(n);
    DFS(1);

    printf("%lld\n", ans / 2);
    return 0;
}
