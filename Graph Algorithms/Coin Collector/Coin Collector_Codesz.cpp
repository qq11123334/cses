#include <bits/stdc++.h>
#define ll long long int
#define N 100005
using namespace std;
vector<ll> adj[N], Readj[N], scc[N], ord;
set<ll> Sadj[N];
bool vis[N];
ll Sid[N], n, m, id, v[N], dp[N];
void RD(int x) {
    vis[x] = 1;
    for(ll v : Readj[x]) {
        if(!vis[v]) RD(v);
    }
    ord.push_back(x);
}
void DFS(int x, int id) {
    Sid[x] = id;
    scc[id].push_back(x);
    for(ll v : adj[x]) {
        if(!Sid[v]) DFS(v, id);
    }
}
void Ko() {
    for(ll i = 1;i <= n;i++) {
        if(!vis[i]) RD(i);
    }
    id = 0;
    for(ll i = n - 1;i >= 0;i--) {
        ll x = ord[i];
        if(!Sid[x]) DFS(x, ++id);
    }
}
int main() {
    cin >> n >> m;
    for(ll i = 1;i <= n;i++) {
        cin >> v[i];
    }
    for(ll i = 0;i < m;i++) {
        ll a, b;
        cin >> a >> b;
        adj[a].push_back(b);
        Readj[b].push_back(a);
    }

    Ko();

    for(ll i = 1;i <= id;i++) {
        for(ll x : scc[i]) {
            dp[i] += v[x];
            for(ll v : adj[x]) {
                if(Sid[x] != Sid[v]) Sadj[Sid[x]].insert(Sid[v]);
            }
        } 
    }

    ll ans = 0;
    for(ll i = 1;i <= id;i++) {
        ll mx = 0;
        for(ll v : Sadj[i]) {
            mx = max(mx, dp[v]);
        }
        dp[i] += mx;
        ans = max(dp[i], ans);
    }
    cout << ans;
}