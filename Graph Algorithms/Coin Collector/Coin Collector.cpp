#include <bits/stdc++.h>
#define ll long long int
using namespace std;
vector<int> adj[100005], Rev_adj[100005];
vector<int> scc[100005], ord;
set<int> scc_adj[100005];
bool vis[100005];
int scc_id[100005];
int n, m;
void RevDFS(int x) {
    vis[x] = 1;
    for(auto v : Rev_adj[x]) {
        if(!vis[v]) RevDFS(v);
    }
    ord.push_back(x);
}
void DFS(int x, int id) {
    scc_id[x] = id;
    scc[id].push_back(x);
    for(auto v : adj[x]) {
        if(!scc_id[v]) DFS(v, id);
    }
}
int id;
void Kosaraju() {
    for(int i = 1;i <= n;i++) {
        if(!vis[i]) RevDFS(i);
    }
    id = 0;
    for(int i = n - 1;i >= 0;i--) {
        int x = ord[i];
        if(!scc_id[x]) DFS(x, ++id);
    }
}
ll val[100005], dp[100005];
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &m);
    for(int i = 1;i <= n;i++) {
        scanf("%lld", &val[i]);
    }
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        Rev_adj[b].push_back(a);
    }
 
    Kosaraju();
 
    for(int i = 1;i <= id;i++) {
        for(auto x : scc[i]) {
            dp[i] += val[x];
            
            for(auto v : adj[x]) {
                if(scc_id[x] != scc_id[v]) scc_adj[scc_id[x]].insert(scc_id[v]);
            }
        } 
    }
 
    ll ans = 0;
    for(int i = 1;i <= id;i++) {
        ll mx = 0;
        for(auto v : scc_adj[i]) {
            mx = max(mx, dp[v]);
        }
 
        dp[i] += mx;
        ans = max(dp[i], ans);
    }
 
    printf("%lld\n", ans);
    return 0;
}