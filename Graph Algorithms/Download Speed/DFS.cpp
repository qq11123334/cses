#include <bits/stdc++.h>
#define INF 0x3f3f3f3f3f3f3f3f
#define ll long long int
using namespace std;
int n, m;
const int N = 505;
struct Edge {
    int to; ll cap; int rev;
    Edge(){}
    Edge(int _to, ll _cap, int _rev): to(_to), cap(_cap), rev(_rev) {}
};
vector<Edge> adj[N];
void add_edge(int from, int to, ll cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int iter[N];
int s, t; // the max-flow of s to v;
bool vis[N];
ll DFS(int now, ll flow) {
    if(now == t) return flow;
    vis[now] = 1;
    for(int i = 0;i < (int)adj[now].size();i++) {
        Edge &e = adj[now][i];
        if(e.cap > 0 && !vis[e.to]) {
            ll ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
ll max_flow() {
    ll ret = 0;
    ll tmp = 0;

    while((tmp = DFS(s, INF)) > 0)  {
        ret += tmp;
        memset(vis, 0, sizeof(vis));
    }
    return ret;
}
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &m);
    s = 1, t = n;
    for(int i = 0;i < m;i++) {
        int from, to;
        ll cap;
        scanf("%d%d%lld", &from, &to, &cap);
        add_edge(from, to, cap);
    }

    printf("%lld\n", max_flow());
    return 0;
}