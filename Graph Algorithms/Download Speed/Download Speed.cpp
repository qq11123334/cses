#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
#define pii pair<ll,ll>
#ifdef _WIN32
#define lld "%I64d"
#else
#define lld "%lld"
#endif
using namespace std;
vector<pii> adj[505];
ll flow[505];
int par[505];
int x, y; // x為源點, y為匯點
int n, m;
bool vis[505];
void DFS(int a)
{
    vis[a] = 1;
    for(auto i : adj[a])
    {
        if(vis[y]) return; // 如果匯點已走過 就先停止探訪
        int b = i.first;
        ll w = i.second;
        if(!vis[b] && w != 0)
        {
            par[b] = a; //記錄母節點
            flow[b] = min(w, flow[a]); //做relaxation
            DFS(b);
        } 
    }
}
int main()
{
    scanf("%d%d", &n, &m);
    x = 1, y = n;
    for(int i = 0;i < m;i++)
    {
        int a, b;
        ll w;
        scanf("%d%d" lld, &a, &b, &w);
        adj[a].push_back({b, w});
    }

    ll ans = 0;
    do
    {
        memset(flow, 0, sizeof(flow));
        memset(vis, 0, sizeof(vis));
        memset(par, 0, sizeof(par));
        flow[x] = INF;
        DFS(x);
        if(vis[y])
        {
            ans = ans + flow[y];
            ll sink = flow[y];
            int b = y;
            int a = par[y];
            while(b != x)
            {
                for(int i = 0;i < (int)adj[a].size();i++) //要找是哪條edge
                {
                    if(adj[a][i].first != b || adj[a][i].second < sink) continue; //找到流到匯點之中的路徑
                    adj[a][i].second -= sink; // 算過了就扣掉
                    adj[b].push_back({a, sink}); // Max-flow的概念重點 扣掉了要加回反向的同flow
                    break;  // Debug的重點
                }
                b = a;
                a = par[a];
            }
        }
    }
    while(vis[y]);
    printf(lld "\n", ans);
    return 0;
}