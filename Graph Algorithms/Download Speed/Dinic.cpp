#include <bits/stdc++.h>
using ll = long long int;
const int N = 505;
const ll INF = 0x3f3f3f3f3f3f3f3f;
using namespace std;
int n, m;
struct Edge {
    int to; ll cap; int rev;
    Edge(){}
    Edge(int _to, ll _cap, int _rev): to(_to), cap(_cap), rev(_rev) {}
};
vector<Edge> adj[N];
int level[N];
int iter[N];
void add_edge(int from, int to, ll cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int s, t; // the max-flow of s to v;
void BFS() {
    memset(level, -1, sizeof(level));
    level[s] = 0;
    queue<int> q; 
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();
        for(auto e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                level[e.to] = level[frt] + 1;
                q.push(e.to);
            }
        }
    }
}
ll DFS(int now, ll flow) {
    if(now == t) return flow;
    for(int &i = iter[now];i < (int)adj[now].size();i++) {// & 很重要，為了讓iter[now]改變
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            ll ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
ll max_flow() {
    ll ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        memset(iter, 0, sizeof(iter));
        ll tmp;
        while((tmp = DFS(s, INF)) > 0)  {
            ret += tmp;
        }
    }
    return ret;
}
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &m);
    s = 1, t = n;
    for(int i = 0;i < m;i++) {
        int from, to;
        ll cap;
        scanf("%d%d%lld", &from, &to, &cap);
        add_edge(from, to, cap);
    }

    printf("%lld\n", max_flow());
    return 0;
}
