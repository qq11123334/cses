#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int low[N], dfn[N], dfn_to_idx[N];
int SCC_cnt, SCC_id[N];
bool in_s[N];
vector <int> s;
vector <int> adj[N];
void DFS(int x) {
    static int ord = 1;
    dfn[x] = low[x] = ord++;
    s.push_back(x);
    in_s[x] = 1;
    for(auto v : adj[x]) {
        if(!dfn[v]) {
            DFS(v);
            low[x] = min(low[x], low[v]);
        }
        else if(in_s[v])
        {
            low[x] = min(low[x], dfn[v]);
        }
    }
    if(dfn[x] == low[x]) {
        SCC_cnt++;
        while(s.back() != x) {
            SCC_id[s.back()] = SCC_cnt;
            in_s[s.back()] = 0;
            s.pop_back();
        }
        SCC_id[x] = SCC_cnt;
        in_s[x] = 0;
        s.pop_back();
    }
}
int n, m;
void Tarjan_SCC() {
    SCC_cnt = 0;
    for(int i = 1;i <= n;i++) {
        if(!dfn[i]) DFS(i);
    }
}
int main() {
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
    }

    Tarjan_SCC();

    printf("%d\n", SCC_cnt);
    for(int i = 1;i <= n;i++) {
        printf("%d ", SCC_id[i]);
    }
    return 0;
}