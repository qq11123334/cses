#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
vector<int> adj[N], Revadj[N], ord;
bool vis[N]; int SCC_id[N];
int n, m;
void RevDFS(int x) {
    vis[x] = 1;
    for(auto v : Revadj[x]) {
        if(!vis[v]) RevDFS(v);
    }
    ord.push_back(x);
}
void DFS(int x, int id) {
    SCC_id[x] = id;
    for(auto v : adj[x]) {
        if(!SCC_id[v]) DFS(v, id);
    }
}
int scc_id;
void Kosaraju() {
    for(int i = 1;i <= n;i++) {
        if(!vis[i]) RevDFS(i);
    }

    scc_id = 0;
    for(int i = n - 1;i >= 0;i--) {
        int x = ord[i];
        if(!SCC_id[x]) DFS(x, ++scc_id);
    }
}
void add_edge(int a, int b) {
    adj[a].push_back(b);
    Revadj[b].push_back(a);
}
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        add_edge(a, b);
    }

    Kosaraju();
    
    printf("%d\n", scc_id);
    for(int i = 1;i <= n;i++) {
        printf("%d ", SCC_id[i]);
    }
    return 0;
}