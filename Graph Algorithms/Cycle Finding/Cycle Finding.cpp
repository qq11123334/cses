#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
using namespace std;
const int N = 2505;
struct Edge {
    ll from, to, w;
    Edge() {}
    Edge(ll _from, ll _to, ll _w) : from(_from), to(_to), w(_w) {}
};
vector<Edge> edge;
ll dis[N], par[N];
int n, m; 
int Bellman_Ford() {
    for(int i = 1;i <= n;i++) {
        for(Edge &e : edge) {
            if(dis[e.to] > dis[e.from] + e.w) {
                dis[e.to] = dis[e.from] + e.w;
                par[e.to] = e.from;
                if(i == n) {
                    return e.to;
                }
            }
        }
    }
    return 0;
}
int main() {
    // freopen("input.txt", "r", stdin);
    scanf("%d%d", &n, &m); 
    for(int i = 0;i < m;i++) {
        ll x, y, w; scanf("%lld%lld%lld", &x, &y, &w);
        edge.push_back(Edge(x, y, w));
    }
    int res = Bellman_Ford();

    if(res) {
        printf("YES\n");
        int a = par[res];
        int b = par[a];
        while(a != b) {
            a = par[a];
            b = par[par[b]];
        }
        a = res;
        while(a != b) {
            a = par[a];
            b = par[b];
        }
        b = par[a];
        vector<int> ans;
        ans.push_back(a);
        while(a != b) {
            ans.push_back(b);
            b = par[b];
        }
        ans.push_back(a);

        reverse(ans.begin(), ans.end());
        for(auto i : ans) {
            printf("%d ", i);
        }
    } else {
        printf("NO\n");
    }
    return 0;
}