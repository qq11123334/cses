#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int p[N], sz[N];
int find(int x) {
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
void unite(int a,int b) {
    a = find(a); b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    p[a] = p[b];
    assert(p[b] == b);
    sz[b] += sz[a];
}
void init() {
    for(int i = 1; i <= N;i++) {
        sz[i] = 1; p[i] = i;
    }
}

int main() {
    init();
    int n, m;
    cin >> n >> m;
    for(int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        unite(a, b);
    }

    vector<pair<int, int>> ans;
    for(int i = 2; i <= n; i++) {
        if(find(i) != find(1)) {
            unite(1, i);
            ans.push_back({1, i});
        }
    }

    cout << ans.size() << "\n";
    for(auto [a, b] : ans) {
        cout << a << " " << b << "\n";
    }
}
