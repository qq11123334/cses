#include <bits/stdc++.h>
using namespace std;
vector <int> adj[100005];
bool vis[100005];
vector <int> ans;
void DFS(int x)
{
    vis[x] = 1;
    for(auto i : adj[x])
    {
        if(!vis[i]) DFS(i);
    }
}
int main()
{
    memset(vis,0,sizeof(vis));
    int n,m;
    scanf("%d%d",&n,&m);

    for(int i = 0;i < m;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    DFS(1);
    for(int i = 2;i <= n;i++)
    {
        if(!vis[i])
        {
            DFS(i);
            ans.push_back(i);
        }
    }

    printf("%d\n",ans.size());

    for(int i = 0;i < (int)ans.size();i++)
    {
        printf("1 %d\n",ans[i]);
    }
    return 0;
}