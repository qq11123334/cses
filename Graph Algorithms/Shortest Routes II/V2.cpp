#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
#define INF 0x3f3f3f3f3f3f3f3f3f
#define ll long long int
using namespace std;
ll adj[505][505];
ll dis[505][505];
int main()
{
    memset(dis, 0x3f, sizeof(dis));
    int n,m;
    scanf("%d%d",&n,&m);

    for(int i = 0;i < m;i++)
    {
        ll a,b,w;
        scanf("%lld%lld%lld",&a,&b,&w);
        dis[a][b] = min(dis[a][b], w);
    }
    
    for(int i = 1;i <= n;i++)
    {
        dis[i][i] = 0;
    }
    for(int k = 1;k <= n;k++)
    {
        for(int i = 1;i <= n;i++)
        {
            for(int j = 1;j <= n;j++)
            {
                dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
            }
        }
    }


    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            printf("%lld ", dis[i][j]);
        }
        printf("\n");
    }
    // while(q--)
    // {
    //     int a,b;
    //     scanf("%d%d",&a,&b);
    //     if(dis[a][b] != INF) printf("%lld\n",dis[a][b]);
    //     else printf("-1\n");
    // }
    return 0;
}