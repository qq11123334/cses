#include <bits/stdc++.h>
#define ll long long int
const ll INF = 0x3f3f3f3f3f3f3f3f; 
using namespace std;
const int maxn = 505;
ll dis[maxn][maxn], graph[maxn][maxn];
int n, m, q;

void init() {
    for(int i = 0; i <= n; i++) {
        for(int j = 0; j <= n; j++) {
            dis[i][j] = graph[i][j];
        }
    }
    for(int i = 0; i <= n; i++) dis[i][i] = min(dis[i][i], (ll)0);
}
void Floyd_Warshell() {
    for(int k = 1; k <= n; k++) {
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
            }
        }
    }
}
void add_edge(int a, int b, ll weight) {
    graph[a][b] = min(graph[a][b], weight);
}
int main() {
    scanf("%d%d%d", &n, &m, &q);

    for(int i = 0; i <= n; i++) {
        for(int j = 0; j <= n; j++) {
            graph[i][j] = INF;
        }
    }
    // or use memset(graph, 0x3f, sizeof(graph))
    for(int i = 0; i < m; i++) {
        int a, b; ll w;
        scanf("%d%d%lld", &a, &b, &w);
        add_edge(a, b, w);
        add_edge(b, a, w);
    }
    init();
    Floyd_Warshell();
    while(q--) {
        int a, b; scanf("%d%d",&a, &b);
        if(dis[a][b] != INF) printf("%lld\n", dis[a][b]);
        else printf("-1\n");
    }
    return 0;
}
