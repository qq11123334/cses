#include <bits/stdc++.h>
#define INF (0x3f3f3f3f3f3f3f3f)
#define ll long long int
using namespace std;
ll dis[505][505];
int n, m, q;
void init() {
    memset(dis, 0x3f, sizeof(dis)); // INF can be 0x3f3f3f3f3f3f3f3f
    for(int i = 0; i < m; i++) {
        ll a,b,w; scanf("%lld%lld%lld",&a, &b, &w);
        dis[a][b] = min(dis[a][b], w);
	    dis[b][a] = min(dis[b][a], w);
    }
    for(int i = 0;i <= n;i++) dis[i][i] = 0;
}
void Floyd_Warshell() {
	int times = 3;
	while(times--) {
   		for(int i = 1; i <= n; i++) {
    	    for(int j = 1; j <= n; j++) {
    			for(int k = 1; k <= n; k++) {
    	            dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
    	        }
    	    }
    	}
	}
}
int main()
{
    scanf("%d%d%d", &n, &m, &q);
    init();
    Floyd_Warshell();
    while(q--) {
        int a, b; scanf("%d%d",&a, &b);
        if(dis[a][b] != INF) printf("%lld\n", dis[a][b]);
        else printf("-1\n");
    }
}
