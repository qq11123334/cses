#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
int vis[100005];
vector <int> adj[100005];
vector <int> ans;
bool cyclic = 0;
void DFS(int x)
{
    vis[x] = 1;
    for(auto i : adj[x])
    {
        if(vis[i] == 1) cyclic = 1;
        if(!vis[i]) DFS(i);
    }
    ans.push_back(x);
    vis[x] = 2;
}
int main()
{
    memset(vis,0,sizeof(vis));
    int n,m;
    scanf("%d%d",&n,&m);
    for(int i = 0;i < m;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
    }
    for(int i = 1;i <= n;i++)
    {
        if(!vis[i]) DFS(i);
    }

    if(cyclic) printf("IMPOSSIBLE");
    else
    {
        for(int i = ans.size() - 1;i >= 0;i--)
        {
            printf("%d ",ans[i]);
        }
    }
    
    return 0;
}