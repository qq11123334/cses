#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
const int logN = 22;
int nxt[N];
bool vis[N], on_cycle[N];
int incycle_num[N], cycle_num[N], cycle_size[N];
int cycle_dis[N], first[N];
int par[N][logN + 1];
bool same_chain(int a, int b) {
    int dif = cycle_dis[a] - cycle_dis[b];
    int res = a;
    for(int i = 0;i <= logN;i++) {
        if((1 << i) & dif) {
            res = par[res][i];
        }
    }
    return (res == b);
}
void build(int x) {
    int a, b;
    a = nxt[x];
    b = nxt[nxt[x]];
    bool is_vis = 0;
    while(a != b) {
        if(vis[a] || vis[b]) {
            is_vis = 1;
            break;
        }
        a = nxt[a];
        b = nxt[nxt[b]];
    }
    vector<int> ord;
    if(!is_vis) {
        a = x;
        vector<int> cycle;
        while(a != b) {
            ord.push_back(a);
            a = nxt[a];
            b = nxt[b];
        }
        int _first = a;
        cycle.push_back(_first);
        b = nxt[a];
        while(a != b) {
            ord.push_back(b);
            cycle.push_back(b);
            b = nxt[b];
        }

        static int idx = 0;
        idx++;
        for(int i = 0;i < (int)cycle.size();i++) {
            int v = cycle[i];
            on_cycle[v] = 1;
            cycle_num[v] = idx;
            incycle_num[v] = i + 1;
            cycle_size[v] = (int)cycle.size();
        }

        for(int i = (int)ord.size() - 1;i >= 0;i--) {
            int v = ord[i];
            if(on_cycle[v]) continue;
            first[v] = _first;
        }
    } else {
        a = x;
        while(!vis[a]) {
            ord.push_back(a);
            a = nxt[a];
        }
        int _first;
        if(on_cycle[a]) {
            _first = a;
        }
        else {
            _first = first[a];
        } 
        for(int i = (int)ord.size() - 1;i >= 0;i--) {
            int v = ord[i];
            first[v] = _first;
        }
    }

    for(int i = (int)ord.size() - 1;i >= 0;i--) {
        int v = ord[i];
        vis[v] = 1;
        if(on_cycle[v]) continue;
        if(on_cycle[nxt[v]]) cycle_dis[v] = 1;
        else cycle_dis[v] = cycle_dis[nxt[v]] + 1;
    }
}
int solve(int a, int b) {
    if(cycle_num[first[a]] != cycle_num[first[b]]) return -1;
    int _a = a;
    if(!on_cycle[a] && !on_cycle[b]) {
        if(cycle_dis[b] > cycle_dis[a] || !same_chain(a, b)) {
            return -1;
        } else {
            return cycle_dis[a] - cycle_dis[b];
        }
    } else {
        if(!on_cycle[b]) return -1;
        if(!on_cycle[a]) a = first[a];
        int ans = (incycle_num[b] - incycle_num[a] + cycle_size[a]) % cycle_size[a];
        if(!on_cycle[_a]) ans += cycle_dis[_a];
        return ans;
    }
}
int n;
void build_par() {
    for(int i = 1;i < logN;i++) {
        for(int v = 1;v <= n;v++) {
            par[v][i] = par[par[v][i - 1]][i - 1];
        }
    }
}
int main() {
    int q;
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &nxt[i]);
        par[i][0] = nxt[i];
    }

    build_par();

    for(int i = 1;i <= n;i++) {
        if(!vis[i]) build(i);
    }
    for(int i = 1;i <= n;i++) {
        if(on_cycle[i]) first[i] = i;
    }
    while(q--) {
        int a, b;
        scanf("%d%d", &a, &b);
        printf("%d\n", solve(a, b));
    }
    return 0;
}