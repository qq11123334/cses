#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
bool vis[N];
int n, nxt[N], ans[N];
bool on_cycle[N];
void print_vec(vector<int> v) {
    for(auto x : v) {
        printf("%d ", x);
    }
    printf("\n");
}
void solve(int x) {
    int a, b;
    a = nxt[x];
    b = nxt[nxt[x]];
    bool is_vis = 0;
    while(a != b) {
        if(vis[a] || vis[b]) {
            is_vis = 1;
            break;
        }
        a = nxt[a];
        b = nxt[nxt[b]];
    }

    vector<int> ord;
    if(!is_vis) {
        a = x;
        while(a != b) {
            ord.push_back(a);
            a = nxt[a];
            b = nxt[b];
        }
        int first = a;
        b = nxt[a];
        vector<int> cycle;
        cycle.push_back(first);
        while(a != b) {
            ord.push_back(b);
            cycle.push_back(b);
            b = nxt[b];
        }
        for(auto v : cycle) {
            on_cycle[v] = 1;
            ans[v] = (int)cycle.size();
        }
    } else {
        a = x;
        while(!vis[a]) {
            ord.push_back(a);
            a = nxt[a];
        }
    }
    for(int i = (int)ord.size() - 1;i >= 0;i--) {
        int v = ord[i];
        if(on_cycle[v]) continue;
        ans[v] = ans[nxt[v]] + 1; 
    }
    for(auto v : ord) {
        vis[v] = 1;
    }
}
int main() {
    scanf("%d", &n);
    for(int i = 1;i <= n;i++) {
        scanf("%d", &nxt[i]);
    }

    for(int i = 1;i <= n;i++) {
        if(!vis[i]) solve(i);
    }
    for(int i = 1;i <= n;i++) {
        printf("%d ", ans[i]);
    }
    return 0;
}