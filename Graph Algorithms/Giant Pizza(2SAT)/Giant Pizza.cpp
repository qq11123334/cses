#include <bits/stdc++.h>
using namespace std;
vector<int> adj[200005];
int n, m;
int number(int exp, char is_not)
{
    if(is_not == '-') return exp + m;
    if(is_not == '+') return exp;
    assert(0); return 0;
}
int not_exp(int x)
{
    if(x > m) return x - m;
    if(x <= m) return x + m;
    assert(0); return 0;
}
// SCC Tarjan
int low[200005], dfn[200005];
int comp[200005];
int idx, comp_cnt;
stack<int> s;
bool in_s[200005];
void DFS(int x)
{
    low[x] = dfn[x] = idx++;
    s.push(x);
    in_s[x] = 1;

    for(auto v : adj[x])
    {
        if(!dfn[v])
        {
            DFS(v);
            low[x] = min(low[v], low[x]);
        }
        else if(in_s[v]) // important
        {
            low[x] = min(dfn[v], low[x]);
        }
    }

    if(low[x] == dfn[x]) // find a SCC root
    {
        comp_cnt++;
        while(s.top() != x)
        {
            comp[s.top()] = comp_cnt;
            in_s[s.top()] = 0;
            s.pop();
        }
        comp[x] = comp_cnt;
        in_s[x] = 0;
        s.pop();
    }
}
int main()
{
    // freopen("input.txt", "r", stdin);
    scanf("%d %d", &n, &m);
    for(int i = 0;i < n;i++)
    {
        char eva1; int top1;
        scanf(" %c %d", &eva1, &top1);
        char eva2; int top2;
        scanf(" %c %d", &eva2, &top2);

        int number1 = number(top1, eva1);
        int number2 = number(top2, eva2);
        if(top1 == top2)
        {
            if(eva1 == eva2)
            {
                if(eva1 == '-')
                    adj[number1].push_back(number2);
                else
                    adj[number2].push_back(number1);
            }
        }
        else
        {
            adj[not_exp(number1)].push_back(number2);
            adj[not_exp(number2)].push_back(number1);
        }
    }

    idx = 1;
    comp_cnt = 0;
    for(int i = 1;i <= 2 * m;i++)
        if(!dfn[i]) DFS(i);
    
    char ans[200005];
    bool is_able = 1;
    for(int i = 1;i <= m;i++)
    {
        int good = number(i, '+');
        int bad = number(i, '-');
        // printf("%d %d\n", comp[good], comp[bad]);
        if(comp[good] == comp[bad])
            is_able = 0;
        if(comp[good] > comp[bad])
            ans[i] = '-';
        if(comp[good] < comp[bad])
            ans[i] = '+';
    }

    if(is_able)
    {
        for(int i = 1;i <= m;i++)
            printf("%c ", ans[i]);
    }
    else
    {
        printf("IMPOSSIBLE");
    }
    
    
    printf("\n");
    return 0;
}