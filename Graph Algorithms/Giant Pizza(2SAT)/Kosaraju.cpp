#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
int n, m;
int number(int exp, char is_good) {
    if(is_good == '+') return exp;
    if(is_good == '-') return exp + n;
}
int not_exp(int exp) {
    if(exp <= n) return exp + n;
    if(exp > n) return exp - n;
}
vector<int> adj[N], RevAdj[N], ord;
bool vis[N];
int SCC_id[N];
void RevDFS(int x) {
    vis[x] = 1;
    for(auto v : RevAdj[x]) {
        if(!vis[v]) RevDFS(v);
    }
    ord.push_back(x);
}
void DFS(int x, int id) {
    SCC_id[x] = id;
    for(auto v : adj[x]) {
        if(!SCC_id[v]) DFS(v, id);
    }
}
void Kosaraju() {
    for(int i = 1;i <= 2 * n;i++) {
        if(!vis[i]) RevDFS(i);
    }
    int comp = 1;
    for(int i = 2 * n - 1;i >= 0;i--) {
        int x = ord[i];
        if(!SCC_id[x]) DFS(x, comp++);
    }
}
void add_edge(int a, int b) {
    adj[a].push_back(b);
    RevAdj[b].push_back(a);
}
int main() {
    scanf("%d%d", &m, &n);
    for(int i = 0;i < m;i++) {
        int topping1, topping2;
        char is_good1, is_good2;
        scanf(" %c %d %c %d", &is_good1, &topping1, &is_good2, &topping2);
        if(topping1 == topping2) {
            int num = number(topping1, is_good1);
            if(is_good1 == is_good2) {
                add_edge(not_exp(num), num);
            }
        } else {
            int num1 = number(topping1, is_good1);
            int num2 = number(topping2, is_good2);
            add_edge(not_exp(num1), num2);
            add_edge(not_exp(num2), num1);
        }
    }

    Kosaraju();

    bool is_able = 1;
    char ans[N];
    for(int i = 1;i <= n;i++) {
        int num_good = number(i, '+');
        int num_bad = number(i, '-');
        if(SCC_id[num_good] == SCC_id[num_bad]) {
            is_able = 0;
        } else if(SCC_id[num_good] < SCC_id[num_bad]) {
            ans[i] = '+';
        } else if(SCC_id[num_good] > SCC_id[num_bad]) {
            ans[i] = '-';
        }
    }

    if(!is_able) {
        printf("IMPOSSIBLE\n");
        return 0;
    }

    for(int i = 1;i <= n;i++) {
        printf("%c ", ans[i]);
    }
    return 0;
}