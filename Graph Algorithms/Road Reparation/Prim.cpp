#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
using pii = pair<int, int>;
const int N = 100005;
bool mark[N];
priority_queue<pii, vector<pii>, greater<pii>> pq; // (weight, node)
vector<pii> adj[N];
int n, m;
ll Prim() {
	pq.push(pii(0, 1));
	ll ans = 0;
	while(!pq.empty()) {
		int x = pq.top().second;
		int weight = pq.top().first;
		pq.pop();
		if(mark[x]) continue;
		mark[x] = true;
		ans += weight;
		for(auto [v, w] : adj[x]) {
			if(!mark[v]) {
				pq.push(pii(w, v));
			}
		}
	}
	for(int i = 1; i <= n; i++) {
		if(!mark[i]) return 0;
	}
	return ans;
}
int main() {
	scanf("%d%d", &n, &m);
	for(int i = 0; i < m; i++) {
		int a, b; ll w;
		scanf("%d %d %lld", &a, &b, &w);
		adj[a].push_back(pii(b, w));
		adj[b].push_back(pii(a, w));
	}

	ll ans = Prim();
	if(ans) printf("%lld\n", ans);
	else printf("IMPOSSIBLE\n");
	return 0;
}