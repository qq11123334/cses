#include <bits/stdc++.h>
#define ll long long int
using namespace std;
const int N = 100005;
struct Edge {
    int a, b;
    ll w;
}edge[2 * N];
int p[N + 5], sz[N + 5];
int find(int x) {
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
void unite(int a,int b) {
    a = find(a); b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    p[a] = p[b];
    sz[b] += sz[a];
}
void init() {
    for(int i = 1;i <= N;i++) {
        sz[i] = 1; p[i] = i;
    }
}
int n, m;
ll Kruskal() {
    init(); // Union-Find init
    // edge[i] = {a, b, w}
    sort(edge, edge + m, [](Edge a, Edge b) { return a.w < b.w; }); 
    int idx = 0;
    ll ans = 0;
    for(int i = 0;i < n - 1;i++) {
        while(idx < m && find(edge[idx].a) == find(edge[idx].b))
            idx++;
        if(idx == m)
            return 0;
        unite(edge[idx].a,edge[idx].b);
        ans = ans + edge[idx].w;
    }
    return ans;
}
int main() {
    init();
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; ll w;
        scanf("%d%d%lld",&a, &b, &w);
        edge[i] = {a, b, w};
    }
    ll ans = Kruskal();
    if(ans) printf("%lld\n", ans);
    else printf("IMPOSSIBLE\n");
}