#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
using namespace std;
vector <int> adj[100005];
vector <int> par[100005];
vector <int> tp_sort;
bool vis[100005];
ll dp[100005];
void DFS(int x)
{
    vis[x] = 1;
    for(auto i : adj[x])
    {
        if(!vis[i]) DFS(i);
    }
    tp_sort.push_back(x);
}
int main()
{
    int n, m;
    scanf("%d %d", &n, &m);
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
        par[b].push_back(a);
    }

    DFS(1);
    reverse(tp_sort.begin(), tp_sort.end());

    dp[1] = 1;
    for(auto cur : tp_sort)
    {
        for(auto las : par[cur])
        {
            dp[cur] += dp[las];
            dp[cur] %= MOD;
        }
    }
    printf("%lld\n", dp[n]);
    return 0;
}