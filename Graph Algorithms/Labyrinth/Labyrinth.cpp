#include <bits/stdc++.h>
#define x first
#define y second
using namespace std;
pair<int,int> parent[1005][1005],start,des;
char grid[1005][1005];
int x_dir[4] = {1,-1,0,0};
int y_dir[4] = {0,0,1,-1};
int n,m;
bool vis[1005][1005];
vector <char> ans;
bool isvaild(int x,int y)
{
    if(x >= 0 && y >= 0 && x < n && y < m && grid[x][y] != '#' &&  !vis[x][y]) return 1;
    return 0;
}
void output(int x,int y)
{
    int x_dif = x - parent[x][y].x;
    int y_dif = y - parent[x][y].y;
    char R = 'R',L = 'L',U = 'U',D = 'D';
    if(x_dif == 1 && y_dif == 0) ans.push_back(D);
    if(x_dif == -1 && y_dif == 0) ans.push_back(U);
    if(x_dif == 0 && y_dif == 1) ans.push_back(R);
    if(x_dif == 0 && y_dif == -1) ans.push_back(L);
}
int main()
{
    //freopen("test_input.txt","r",stdin);
    memset(vis,0,sizeof(vis));
    ans.clear();
    scanf("%d%d",&n,&m);
    for(int i = 0;i < n;i++)
    {
        for(int j = 0;j < m;j++)
        {
            char input;
            scanf(" %c",&input);
            if(input == 'A') start = {i,j};
            if(input == 'B') des = {i,j};
            grid[i][j] = input;
        }
    }

    queue <pair<int,int> > bfs;
    bool flag = 0;

    bfs.push(start);
    vis[start.x][start.y] = 1;

    while(!bfs.empty())
    {
        pair<int,int> front = bfs.front();
        //printf("%d\n",bfs.size());
        bfs.pop();
        for(int i = 0;i < 4;i++)
        {
            int next_x = front.x + x_dir[i];
            int next_y = front.y + y_dir[i];
            if(isvaild(next_x,next_y)) 
            {
                bfs.push({next_x,next_y});
                parent[next_x][next_y] = {front.x,front.y};
                vis[next_x][next_y] = 1;
                if(next_x == des.x && next_y == des.y)
                {
                    flag = 1;
                    break;
                }
            }
        }
        if(flag) break;
    }
    if(flag)
    {
        pair <int,int> coordinate;
        coordinate.x = des.x;
        coordinate.y = des.y;
        while(1)
        {
            int x = coordinate.x;
            int y = coordinate.y;
            if(x == start.x && y == start.y) break;
            output(x,y);
            coordinate = parent[x][y];
        }
        printf("YES\n");
        printf("%d\n",ans.size());

        for(int i = ans.size() - 1;i >= 0;i--)
        {
            printf("%c",ans[i]);
        }
    }
    else
    {
        printf("NO");
    }
    


    return 0;
}