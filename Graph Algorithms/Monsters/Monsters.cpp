#include <bits/stdc++.h>
#define pii pair<int, int>
#define INF 0x3f3f3f3f
#define x first
#define y second
#define U 1
#define D 2
#define L 3
#define R 4
using namespace std;
char grid[1005][1005];
int dir_x[4] = {1, -1, 0, 0};
int dir_y[4] = {0, 0, 1, -1};
int n, m;
int check_relation_location(int x1, int y1, int x2, int y2) // (x2, y2) is base
{
    int x_res = x1 - x2;
    int y_res = y1 - y2;
    if(x_res == 0 && y_res == 1)
        return R;
    if(x_res == 0 && y_res == -1)
        return L;
    if(x_res == 1 && y_res == 0)
        return D;
    if(x_res == -1 && y_res == 0)
        return U;
    return -1;
}
bool is_valid(int x, int y)
{
    if(x <= 0 || x > n || y <= 0 || y > m)
        return 0;
    return 1;
}
void print_table(int A[][1005])
{
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= m;j++)
            printf("%d ", A[i][j]);
        printf("\n");
    }
}
queue<pii> q;
int dis_A[1005][1005];
pii par[1005][1005];
int dis_M[1005][1005];
int main()
{
    memset(dis_A, INF, sizeof(dis_A));
    memset(dis_M, INF, sizeof(dis_M));

    scanf("%d%d", &n, &m);
    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= m;j++)
        {
            scanf(" %c", &grid[i][j]);
            if(grid[i][j] == 'A')
            {
                dis_A[i][j] = 0;
                q.push({i, j});
            }
        }
    }

    while(!q.empty())
    {
        int cur_x = q.front().x;
        int cur_y = q.front().y;

        q.pop();

        for(int i = 0;i < 4;i++)
        {
            int next_x = cur_x + dir_x[i];
            int next_y = cur_y + dir_y[i];

            if(is_valid(next_x, next_y) && grid[next_x][next_y] != '#' && dis_A[next_x][next_y] == INF)
            {
                dis_A[next_x][next_y] = dis_A[cur_x][cur_y] + 1;
                par[next_x][next_y] = {cur_x, cur_y};
                q.push({next_x, next_y});
            }
        }
    }

    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= m;j++)
        {
            if(grid[i][j] == 'M')
            {
                dis_M[i][j] = 0;
                q.push({i, j});
            }
        }
    }

    while(!q.empty())
    {
        int cur_x = q.front().x;
        int cur_y = q.front().y;

        q.pop();

        for(int i = 0;i < 4;i++)
        {
            int next_x = cur_x + dir_x[i];
            int next_y = cur_y + dir_y[i];

            if(is_valid(next_x, next_y) && grid[next_x][next_y] != '#' && dis_M[next_x][next_y] == INF)
            {
                dis_M[next_x][next_y] = dis_M[cur_x][cur_y] + 1;
                q.push({next_x, next_y});
            }
        }
    }

    bool isable = 0;
    int des_x, des_y;

    // print_table(dis_M);
    // print_table(dis_A);
    for(int i = 1;i <= n;i++)
    {
        if(dis_A[i][1] < dis_M[i][1])
        {
            isable = 1;
            des_x = i;
            des_y = 1;
        }
        if(dis_A[i][m] < dis_M[i][m])
        {
            isable = 1;
            des_x = i;
            des_y = m;
        }
    }

    for(int i = 1;i <= m;i++)
    {
        if(dis_A[1][i] < dis_M[1][i])
        {
            isable = 1;
            des_x = 1;
            des_y = i;
        }
        if(dis_A[n][i] < dis_M[n][i])
        {
            isable = 1;
            des_x = n;
            des_y = i;
        }
    }

    if(isable)
    {
        printf("YES\n");
        printf("%d\n", dis_A[des_x][des_y]);
        string ans;
        int cur_x = des_x, cur_y = des_y;
        while(dis_A[cur_x][cur_y] != 0)
        {
            int par_x = par[cur_x][cur_y].x;
            int par_y = par[cur_x][cur_y].y;
            int res = check_relation_location(cur_x, cur_y, par_x, par_y);
            
            if(res == L)
                ans.push_back('L');
            else if(res == R)
                ans.push_back('R');
            else if(res == U)
                ans.push_back('U');
            else if(res == D)
                ans.push_back('D');

            cur_x = par_x;
            cur_y = par_y;
        }
        reverse(ans.begin(), ans.end());
        printf("%s\n", ans.c_str());
    }
    else
    {
        printf("NO\n");
    }
    
    return 0;
}