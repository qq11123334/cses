#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
using namespace std;
typedef pair<ll, ll> Edge;
const int N = 100005;
vector<Edge> adj[N];
ll dis[N];
bool in_que[N];
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        ll w;
        scanf("%d%d%lld", &a, &b, &w);
        adj[a].push_back(make_pair(b, w));
    }

    int s = 1;
    memset(dis, 0x3f, sizeof(dis));
    dis[s] = 0;
    queue<int> que;
    que.push(s);
    while(!que.empty()) {
        int x = que.front();
        in_que[x] = 0; que.pop();
        for(auto [v, w] : adj[x]) {
            if(dis[v] > dis[x] + w) {
                dis[v] = dis[x] + w;
                if(!in_que[v]) {
                    in_que[v] = 1;
                    que.push(v);
                }
            }
        }
    }

    for(int i = 1;i <= n;i++) {
        printf("%lld ", dis[i]);
    }
    return 0;
}