#include <bits/stdc++.h>
#define ll long long int
#define INF 1e18
using namespace std;
typedef pair<ll, ll> Edge;
const int N = 100005;
vector<Edge> adj[N];
ll dis[N];
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; ll w;
        scanf("%d%d%lld", &a, &b, &w);
        adj[a].push_back(Edge(b, w));
    }
    int s = 1;
    priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> pq;
    memset(dis, 0x3f, sizeof(dis));
    dis[s] = 0; pq.push(make_pair(0, s));
    while(!pq.empty()) {
        auto P = pq.top(); pq.pop();
        int x = P.second; ll _dis = P.first;
        if(dis[x] < _dis) continue;
        for(auto &[v, w] : adj[x]) {
            if(dis[v] > dis[x] + w) {
                dis[v] = dis[x] + w;
                pq.push(make_pair(dis[v], v));
            }
        }
    }

    for(int i = 1;i <= n;i++) {
        printf("%lld ", dis[i]);
    }
    return 0;
}
