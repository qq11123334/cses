#include <bits/stdc++.h>
using namespace std;

using ll = long long int; // use "ll" as "long long int"

const int N = 100005;     // max number of nodes
vector<pair<ll, ll>> adj[N]; // adjacency list, store {neighbor_id, weight}
bool mark[N];
ll dis[N]; // distance
priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> pq; // {distance, node_id}

int n, m;
void initialize() {
    int src = 1; // source 
    for(int i = 1; i <= n; i++) {
        dis[i] = 1e18;
    }
    dis[src] = 0; 
    pq.push(make_pair(0, src));
}

void Dijkstra() {
    // remember initialize
    while(!pq.empty()) {
        auto P = pq.top(); pq.pop();
        int v = P.second;
        if(mark[v]) continue;

        for(auto [neighbor, weight] : adj[v]) { // g++17
            if(dis[neighbor] > dis[v] + weight) {
                dis[neighbor] = dis[v] + weight;
                pq.push(make_pair(dis[neighbor], neighbor));
            }
        }
        mark[v] = true;
    }
}
int main() {
    scanf("%d%d", &n, &m);
    for(int i = 0; i < m; i++) {
        int a, b; ll w;
        scanf("%d%d%lld", &a, &b, &w);
        adj[a].push_back({b, w});
    }

    initialize();
    Dijkstra();


    for(int i = 1; i <= n; i++) {
        printf("%lld ", dis[i]);
    }
}
