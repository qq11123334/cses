#include <bits/stdc++.h>
using namespace std; 
using ll = long long int; // use "ll" as "long long int" 
const int N = 100005, M = 200005;     // max number of nodes
ll dis[N]; // distance
struct Edge {
    int from, to;
    ll weight;
};

vector<Edge> edges; // edge list
int n, m;

void initialize() {
    int src = 1;
    for(int i = 0; i <= n; i++) {
        dis[i] = 1e18;
    }
    dis[src] = 0;
}
void Bellman_Ford() {
    // remeber initialize
    for(int i = 1; i <= n; i++) { // n - 1 rounds
        for(auto &[from, to, weight] : edges) {
            if(dis[to] > dis[from] + weight) { // relax
                dis[to] = dis[from] + weight;
                if(i == n) { 
                    // exist negative cycle 
                }
            }
        }
    }
}
int main() {
    scanf("%d%d", &n, &m);
    for(int i = 0; i < m; i++) {
        int a, b; ll w;
        scanf("%d%d%lld", &a, &b, &w);
        edges.push_back({a, b, w});
    }
 
    initialize();
    Bellman_Ford();

    for(int i = 1; i <= n; i++) {
        printf("%lld ", dis[i]);
    }
}
