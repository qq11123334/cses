#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
#define MOD 1000000007
#define ll long long int
using namespace std;
ll dp[(1 << 20)][21]; // visited subset and the last vertrx
vector<int> adj[25];
// int adj[25][25];
int main() {
    // freopen("input.txt", "r", stdin);
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        a--, b--;
        adj[a].push_back(b);
        // assert(!adj[a][b]);
        // adj[a][b]++;
    }

    dp[(1 << 0)][0] = 1;
    for(int s = 0;s < (1 << n);s++) {
        for(int i = 0;i < n;i++) {
            if(s & (1 << n) && i != n - 1)
                continue;
            if(s & (1 << i)) {
                for(auto j : adj[i]) {
                    if(!(s & (1 << j))) {
                        dp[s | (1 << j)][j] += dp[s][i];
                        dp[s | (1 << j)][j] %= MOD;
                    }
                }
                // for(int j = 0;j < n;j++) {
                //     if(!(s & (1 << j)) && adj[i][j]) {
                //         dp[s | (1 << j)][j] += dp[s][i] * adj[i][j];
                //         dp[s | (1 << j)][j] %= MOD;
                //     }
                // }
            }
        }
    }
    
    printf("%lld\n", dp[(1 << n) - 1][n - 1]);
    return 0;
}