#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include <bits/stdc++.h>
#define MOD 1000000007
#define ll long long int
using namespace std;
ll dp[(1 << 20)][21]; // visited subset and the last vertrx
vector<pair<int, int>> adj[25];
int adj_matrix[25][25];
int main() {
    // freopen("input.txt", "r", stdin);
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        a--, b--;
        adj_matrix[a][b]++;
    }

    for(int i = 0;i < n;i++) {
        for(int j = 0;j < n;j++) {
            if(adj_matrix[j][i]) 
                adj[i].push_back(make_pair(j, adj_matrix[j][i]));
        }
    }

    dp[(1 << 0)][0] = 1;
    for(int s = 1;s < (1 << (n - 1));s++) {
        for(int i = 0;i < n;i++) {
            if(s & (1 << i)) {
                int s_without_i = s ^ (1 << i);
                for(auto v : adj[i]) {
                    if(s_without_i & (1 << v.first)) {
                        dp[s][i] += dp[s_without_i][v.first] * v.second;
                        dp[s][i] %= MOD;
                    }
                }
            }
        }
    }
    int s = (1 << n) - 1;
    int s_without_i = s ^ (1 << (n - 1));
    for(auto v : adj[n - 1]) {
        if(s_without_i & (1 << v.first)) {
            dp[s][n - 1] += dp[s_without_i][v.first] * v.second;
            dp[s][n - 1] %= MOD;
        }
    }

    printf("%lld\n", dp[(1 << n) - 1][n - 1]);
    return 0;
}