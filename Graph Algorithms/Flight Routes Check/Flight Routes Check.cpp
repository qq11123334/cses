#include <bits/stdc++.h>
using namespace std;
int ord, comp_cnt, mark[100005];
int idx[100005], low[100005];
bool in_s[100005];
vector <int> adj[100005], s;
void DFS(int x)
{
    idx[x] = ord;
    low[x] = ord++;
    s.push_back(x);
    in_s[x] = 1;
    for(auto i : adj[x])
    {
        if(!idx[i])
        {
            DFS(i);
            low[x] = min(low[x], low[i]);
        }
        else if(in_s[i])
        {
            low[x] = min(low[x], idx[i]);
        }
    }

    if(low[x] == idx[x])
    {
        comp_cnt++;
        while(s.back() != x)
        {
            mark[s.back()] = comp_cnt;
            in_s[s.back()] = 0;
            s.pop_back();
        }
        mark[x] = comp_cnt;
        in_s[x] = 0;
        s.pop_back();
    }
}
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
    }

    ord = 1;
    DFS(1);

    for(int i = 1;i <= n;i++)
    {
        // printf("%d\n", mark[i]);
        if(mark[1] != mark[i])
        {
            printf("NO\n");
            if(mark[i]) printf("%d 1\n", i);
            else printf("1 %d\n", i);
            return 0;
        }
    }

    printf("YES\n");

    return 0;
}