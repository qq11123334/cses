#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>
using namespace std;
vector <int> adj[100005];
int parent[100005];
bool vis[100005];
bool isCyclic = 0;
int start;
int des;
void DFS(int x)
{
    if(isCyclic) return;
    vis[x] = 1;
    for(auto i : adj[x])
    {
        //printf("%d\n",x);
        if(!vis[i])
        {
            parent[i] = x;
            DFS(i);
        }
        else
        {
            if(parent[x] != i) 
            {
                //printf("??");
                start = x;
                des = i;
                isCyclic = 1;
            }
        }
        if(isCyclic) break;
    }
}
int main()
{
    int n,m;
    scanf("%d%d",&n,&m);

    for(int i = 0;i < m;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++)
    {
        if(!vis[i]) DFS(i);
        if(isCyclic) break;
    }
    if(isCyclic)
    {
        vector<int> ans;
        ans.push_back(start);
        int next = start;
        while(next != des)
        {
           // printf("%d\n",next);
            next = parent[next];
            ans.push_back(next);
        }
        ans.push_back(start);
        printf("%d\n",(int)ans.size());

        for(int i = 0;i < (int)ans.size();i++)
        {
            printf("%d ",ans[i]);
        }
    }
    else
    {
        printf("IMPOSSIBLE");
    }
    
    return 0;
}