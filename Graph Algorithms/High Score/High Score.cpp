#include <bits/stdc++.h>
#define ll long long int
#define pll pair<ll, ll>
#define INF 0x3f3f3f3f3f3f3f3f
#define MAX_N 2505
#define MAX_M 5005
using namespace std;
struct Edge {
    int a, b; ll w;
    Edge() {} // important
    Edge(int _a, int _b, ll _w) : a(_a), b(_b), w(_w) {}
}edge[MAX_M];
vector<pll> adj[MAX_N];
vector<ll> check;
bool vis1[MAX_N], vis2[MAX_N];
ll dis[MAX_N];
void DFS_second(int x) {
    vis2[x] = 1;
    for(auto v : adj[x]) {
        ll next = v.first;
        if(!vis2[next]) DFS_second(next);
    }
}
void DFS_first(int x) {
    vis1[x] = 1;
    for(auto v : adj[x]) {
        ll next = v.first;
        if(!vis1[next]) DFS_first(next);
    }
}
int n, m;
void init() {
    memset(dis, 0x3f, sizeof(dis)); memset(vis1, 0, sizeof(vis1));
    memset(vis2, 0, sizeof(vis2));
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b; ll w; scanf("%d%d%lld", &a, &b, &w);
        adj[a].push_back(make_pair(b, -w));
        edge[i] = Edge(a, b, -w);
    }
}
ll Bellman_Ford() {
    int s = 1, t = n;
    dis[s] = 0;
    for(int i = 1;i <= n;i++) {
        for(auto e : edge) {
            ll a = e.a, b = e.b, w = e.w;
            if(dis[b] > dis[a] + w) {
                dis[b] = dis[a] + w;
                if(i == n) {
                    check.push_back(b);
                }
            }
        }
    }

    DFS_first(s);
    for(int i = 0;i < (int)check.size();i++) {
        if(vis1[check[i]]) DFS_second(check[i]);
    }

    // if vis2[t] is True, exist Negative Cycle
    // if dis[t] == INF, can't reach t
    if(vis2[t] || dis[t] == INF) return INF;
    return dis[t];
}
int main() {
    init();
    ll res = Bellman_Ford(); 
    if(res != INF) printf("%lld\n", -res);
    else printf("-1\n");
}