#include <bits/stdc++.h>
using namespace std;
int main() {
	freopen("input.txt", "r", stdin);
	int n, m;
	if(!(cin >> n >> m)) {
		cerr << "n or m missing" << endl;
	}
	if(n < 1 || n > 2500) {
		cout << "n out of range" << endl;
		exit(0);
	}

	if(m < 1 || m > 5000) {
		cout << "m out of range" << endl;
		exit(0);
	}

	for(int i = 0; i < m; i++) {
		int a, b;
		long long w;
		if(!(cin >> a >> b >> w)) {
			cerr << "missing info of edge" << endl;
			exit(0);
		}
		if(a < 1 || a > n || b < 1 || b > n) {
			cerr << "vertex out of range" << endl;
			cerr << "in " << i << "-th edge" << endl;
			cerr << "n = " << n << " a = " << a << " b = " << b << endl;
			exit(0);
		}
		if(w < -(int)1e9 || w > (int)1e9) {
			cerr << "w out of range" << endl;
			cerr << "w = " << w << endl;
			exit(0);
		}
	}

	cerr << "valid test!" << endl;
	return 0;
}
