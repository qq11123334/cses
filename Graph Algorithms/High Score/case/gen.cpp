#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
struct Edge {
	int a, b;
	ll w;
	Edge() {} // important
	Edge(int _a, int _b, ll _w) : a(_a), b(_b), w(_w) {}
};
vector<Edge> edge;
void add_edge(int a, int b, ll w) {
	edge.push_back(Edge(a, b, w));
}
int main() {
	freopen("input.txt", "w", stdout);
	int n = 1000;
	add_edge(1, 2, -1000000000);
	for(int i = n - 2; i >= 2; i--) {
		add_edge(i, i + 1, 1);
	}
	add_edge(n - 2, 2, 1);
	add_edge(n - 2, n - 1, 1);
	add_edge(n - 1, n, 1);

	printf("%d %d\n", n, (int)edge.size());
	for(auto e : edge) {
		printf("%d %d %lld\n", e.a, e.b, e.w);
	}
	return 0;
}
