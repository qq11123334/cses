#include <bits/stdc++.h>
#define pll pair<ll, ll>
using ll = long long int;
const ll INF = 0x3f3f3f3f3f3f3f3f;
const int N = 2505;
const int M = 5005;
using namespace std;
struct Edge {
	int a, b;
	ll w;
	Edge() {} // important
	Edge(int _a, int _b, ll _w) : a(_a), b(_b), w(_w) {}
} edge[M];
ll dis[N];
int n, m;
void init() {
	scanf("%d%d", &n, &m);
	for (int i = 0; i < m; i++) {
		int a, b; ll w;
		scanf("%d%d%lld", &a, &b, &w);
		edge[i] = Edge(a, b, -w);
	}
}
ll Bellman_Ford(int s, int t) {
	memset(dis, 0x3f, sizeof(dis));
	dis[s] = 0;
	int times = 2 * n;
	for (int i = 1; i <= times; i++) {
		for (auto e : edge) {
			int a = e.a, b = e.b; ll w = e.w;
			if (dis[a] == INF) continue;
			if (dis[b] > dis[a] + w) {
				dis[b] = dis[a] + w;
				// printf("i = %d a = %d b = %d %lld %lld\n", i, a, b, dis[a], dis[b]);
				if (i >= n) {
					if(dis[a] < 0) dis[a] = -INF;
					if(dis[b] < 0) dis[b] = -INF;
					if (b == t) return INF;
				}
			}
		}
	}
	return dis[t];
}
int main() {
	init();
	ll res = Bellman_Ford(1, n);
	if (res != INF)
		printf("%lld\n", -res);
	else
		printf("-1\n");
}