#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> Edge;
const int N = 100005, M = 200005;
int indegree[N], outdegree[N];
vector<Edge> adj[N];
bool vis_edge[M];
vector<int> path;
void Euler(int x) {
    while(!adj[x].empty()) {
        auto [v, e] = adj[x].back();
        adj[x].pop_back();
        if(!vis_edge[e]) {
            vis_edge[e] = 1;
            Euler(v);
            path.push_back(v);
        }
    }
}
void add_edge(int a, int b, int i) {
    adj[a].push_back(Edge(b, i));
    outdegree[a]++;
    indegree[b]++;
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        add_edge(a, b, i);
    }
    add_edge(n, 1, m);
    Euler(1);

    for(int i = 1;i <= n;i++) {
        if(indegree[i] != outdegree[i]) {
            printf("IMPOSSIBLE\n");
            exit(0);
        }
    }
    for(int i = 0;i <= m;i++) {
        if(!vis_edge[i]) {
            printf("IMPOSSIBLE\n");
            exit(0);
        }
    }
    reverse(path.begin(), path.end());
    assert(vis_edge[m]);
    int start = -1;
    for(int i = 0;i < (int)path.size();i++) {
        int next_i = (i + 1) % (int)path.size();
        if(path[i] == n && path[next_i] == 1) {
            start = next_i;
        }
    }
    for(int i = 0;i < (int)path.size();i++) {
        printf("%d ", path[(i + start) % path.size()]);
    }
    return 0;
}