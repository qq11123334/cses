#include <bits/stdc++.h>
using namespace std;
vector <int> adj[100005];
vector <int> topological_sort;
bool vis[100005];
int par[100005];
int dp[100005];
void find_path(int x)
{
    if(x == 1) 
    {
        printf("1 ");
        return;
    }
    find_path(par[x]);
    printf("%d ", x);
}
void DFS(int x)
{
    vis[x] = 1;
    for(auto i : adj[x])
    {
        if(!vis[i]) DFS(i);
    }
    topological_sort.push_back(x);
}
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a].push_back(b);
    }

    DFS(1);
    reverse(topological_sort.begin(), topological_sort.end());

    dp[1] = 1;
    for(auto cur : topological_sort)
    {
        for(auto nxt : adj[cur])
        {
            if(dp[nxt] < dp[cur] + 1)
            {
                dp[nxt] = dp[cur] + 1;
                par[nxt] = cur;
            }
        }
    }
    if(dp[n])
    {
        printf("%d\n", dp[n]);
        find_path(n);
        printf("\n");
    }
    else
    {
        printf("IMPOSSIBLE\n");
    }
    return 0;
}