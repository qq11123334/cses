#include <bits/stdc++.h>
#define MAX_N 505
#define INF 0x3f3f3f3f
using namespace std;
struct Edge {
    int to, cap, rev;
    Edge(int _to, int _cap, int _rev): to(_to), cap(_cap), rev(_rev) {}
};
struct Edge2 {
    int from, to;
    Edge2(int _from, int _to): from(_from), to(_to) {}
};
vector<Edge> adj[MAX_N];
vector<Edge2> edge;
int n, m;
int s, t;
int level[MAX_N], iter[MAX_N];
void BFS() {
    memset(level, -1, sizeof(level));
    queue<int> q;
    level[s] = 1;
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();

        for(auto &e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                level[e.to] = level[frt] + 1;
                q.push(e.to);
            }
        }
    }
}
int DFS(int now, int flow) {
    if(now == t) return flow;
    
    for(int &i = iter[now];i < (int)adj[now].size();i++) {
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            int ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
int max_flow() {
    int ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        
        int tmp;
        memset(iter, 0 ,sizeof(iter));
        while((tmp = DFS(s, INF)) > 0) ret += tmp;
    }
    return ret;
}
void add_edge(int from, int to, int cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int main() {
    // freopen("input.txt", "r", stdin);
    cin >> n >> m;
    s = 1, t = n;
    for(int i = 0;i < m;i++) {
        int a, b;
        cin >> a >> b;
        add_edge(a, b, 1);
        add_edge(b, a, 1);
        edge.push_back(Edge2(a, b));
    }

    cout << max_flow() << endl;
    BFS();
    for(auto e : edge) {
        if(level[e.from] * level[e.to] < 0) {
            printf("%d %d\n", e.from, e.to);
        }
    }
    return 0;
}