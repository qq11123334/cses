#include <bits/stdc++.h>
using namespace std;
const int N = 505;
struct Edge {
    int to, cap, rev;
    bool is_real;
    Edge() {}
    Edge(int _to, int _cap, int _rev, bool _is_real) : to(_to), cap(_cap), rev(_rev), is_real(_is_real) {}
};
vector<Edge> adj[N];
int iter[N], level[N];
void add_edge(int a, int b, int cap) {
    adj[a].push_back(Edge(b, cap, (int)adj[b].size(), 1));
    adj[b].push_back(Edge(a, 0, (int)adj[a].size() - 1, 0));
}
int s, t;
void BFS() {
    memset(level, -1, sizeof(level));
    level[s] = 0;
    queue<int> q;
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();
        for(Edge &e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                level[e.to] = level[frt] + 1;
                q.push(e.to);
            }
        }
    }
}
int DFS(int now, int flow) {
    if(now == t) return flow;
    for(int &i = iter[now];i < (int)adj[now].size();i++) {
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            int ret = DFS(e.to, min(e.cap, flow));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
int max_flow() {
    int ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        memset(iter, 0, sizeof(iter));
        int tmp;
        while((tmp = DFS(s, 0x3f3f3f3f)) > 0) {
            ret += tmp;
        }
    }
    return ret;
}
vector<int> path;
void DFS_print(int x) {
    path.push_back(x);
    for(Edge &e : adj[x]) {
        if(e.cap == 0 && e.is_real) {
            e.cap++;
            DFS_print(e.to);
            return;
        }
    }
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        add_edge(a, b, 1);
    }
    s = 1, t = n;
    int ans = max_flow();
    printf("%d\n", ans);
    for(int i = 0;i < ans;i++) {
        path.clear();
        DFS_print(1);
        printf("%d\n", (int)path.size());
        for(auto v : path) {
            printf("%d ", v);
        }
        printf("\n");
    }
    return 0;
}