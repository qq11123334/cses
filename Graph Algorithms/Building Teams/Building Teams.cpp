#include <iostream>
#include <vector>
#include <cstring>
#include <cstdio>
using namespace std;
vector <int> adj[100005];
int team[100005];
bool isBipartite = 1;

void DFS(int x,int id)
{
    team[x] = id;
    for(auto i : adj[x])
    {
        if(!team[i])
        {
            DFS(i, !(id - 1) + 1);
        }
        else
        {
            if(team[i] == id)
            {
                isBipartite = 0;
            }
        }
        
    }
}
int main()
{
    memset(team,0,sizeof(team));
    int n,m;
    scanf("%d%d",&n,&m);

    for(int i = 0;i < m;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i = 1;i <= n;i++)
    {
        if(!team[i])
        {
            DFS(i,1);
        }
    }

    if(isBipartite)
    {
        for(int i = 1;i <= n;i++)
        {
            printf("%d ",team[i]);
        }
    }
    else
    {
        printf("IMPOSSIBLE");
    }
    
    return 0;
}