#include <bits/stdc++.h>
using namespace std;
int dir_x[8] = {1, 1, 2, 2, -1, -1, -2, -2};
int dir_y[8] = {2, -2, 1, -1, 2, -2, 1, -1};
int chess[9][9];
bool is_valid(int x, int y)
{
    if(x > 0 && x < 9 && y > 0 && y < 9)
        return 1;
    return 0;
}
void print_chess()
{
    printf("------\n");
    for(int i = 1;i <= 8;i++)
    {
        for(int j = 1;j <= 8;j++)
            printf("%d ", chess[i][j]);
        printf("\n");
    }
    printf("----\n");
}
void DFS(int x, int y, int level)
{
    // print_chess();
    chess[x][y] = level;
    if(level == 64)
    {
        for(int i = 1;i <= 8;i++)
        {
            for(int j = 1;j <= 8;j++)
                printf("%d ", chess[i][j]);
            printf("\n");
        }
        exit(0);
    }

    for(int i = 0;i < 8;i++)
    {
        int next_x = x + dir_x[i];
        int next_y = y + dir_y[i];
        if(is_valid(next_x, next_y) && !chess[next_x][next_y])
        {
            DFS(next_x, next_y, level + 1);
            chess[next_x][next_y] = 0;
        }
    }
}
int main()
{
    int row, col;
    cin >> col >> row;
    DFS(row, col, 1);
    return 0;
}