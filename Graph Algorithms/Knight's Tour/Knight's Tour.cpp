#include <bits/stdc++.h>
using namespace std;
bool vis[10][10];
bool is_valid(int x, int y) {
    if(!vis[x][y] && x >= 1 && x <= 8 && y >= 1 && y <= 8) return 1;
    else return 0;
}
int dir_x[8] = {1, 1, 2, 2, -1, -1, -2, -2};    
int dir_y[8] = {2, -2, 1, -1, 2, -2, 1, -1};
int get_remain(int x, int  y) {
    int cnt = 0;
    for(int k = 0;k < 8;k++) {
        int nxt_x = x + dir_x[k];
        int nxt_y = y + dir_y[k];
        if(is_valid(nxt_x, nxt_y)) cnt++;
    }
    return cnt;
}
int grid[10][10];
bool DFS(int x, int y, int level) {
    grid[x][y] = level;
    if(level == 64) {
        return true;
    }
    vis[x][y] = 1;
    vector<pair<int, pair<int, int>>> v;
    for(int k = 0;k < 8;k++) {
        int nxt_x = x + dir_x[k];
        int nxt_y = y + dir_y[k];
        if(is_valid(nxt_x, nxt_y)) {
            int res = get_remain(nxt_x, nxt_y);
            v.push_back({res, {nxt_x, nxt_y}});
        }
    }

    sort(v.begin(), v.end());

    for(auto nxt : v) {
        if(DFS(nxt.second.first, nxt.second.second, level + 1))
            return true;
    }

    vis[x][y] = 0;
    return false;
}
void print_grid() {
    for(int i = 1;i <= 8;i++) {
        for(int j = 1;j <= 8;j++) {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}
int main() {
    int x, y;
    scanf("%d%d", &x, &y);
    DFS(y, x, 1);
    print_grid();
    return 0;
}