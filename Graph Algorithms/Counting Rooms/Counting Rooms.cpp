#include <stdio.h>
char grid_map[1005][1005];
int visited[1005][1005];
int n, m;
bool is_valid(int x, int y) {
    return (x >= 0 && x < n && y >= 0 && y < m && grid_map[x][y] != '#');
}
void Explore(int x, int y)
{
    if(!is_valid(x, y)) return;
    if(visited[x][y]) return;
    visited[x][y] = 1;
    Explore(x - 1, y);
    Explore(x + 1, y);
    Explore(x, y - 1);
    Explore(x, y + 1);
}
void prt()
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%d ",visited[i][j]);
        }
        printf("\n");
    }
}
int main()
{
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            scanf(" %c", &grid_map[i][j]);
        }
    }
    int cnt = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (grid_map[i][j] != '#' && !visited[i][j]) {
                //prt();
                //printf("%d %d\n",i,j);
                Explore(i, j);
                cnt++;
            }
        }
    }
    printf("%d", cnt);
    return 0;
}
