#include <bits/stdc++.h>
using namespace std;
vector<int> ans;
vector<int> adj[200005];
int vis[200005];
bool find_ans;
int par[200005];
bool find_path(int x, int sta)
{
    if(x == 0 || vis[x] == 2) return 0;
    if(x == sta) return 1;
    int res = find_path(par[x], sta);
    ans.push_back(x);
    return res;
}
void DFS(int x)
{
    vis[x] = 1;
    for(auto i : adj[x])
    {
        if(find_ans) return;
        if(!vis[i]) 
        {
            par[i] = x;
            DFS(i);
        }
        else if(vis[i] == 1)
        {
            ans.clear();
            ans.push_back(x);
            ans.push_back(i);
            find_ans = find_path(x, i);
        }
    }
    vis[x] = 2;
}
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d %d", &a, &b);
        adj[a].push_back(b);
    }

    for(int i = 1;i <= n;i++)
    {
        if(!vis[i]) DFS(i);
        if(find_ans) break;
    }

    if(find_ans)
    {
        printf("%d\n", (int)ans.size());
        for(auto i : ans)
        {
            printf("%d ", i);
        }
    }
    else
    {
        printf("IMPOSSIBLE\n");
    }
    return 0;
}