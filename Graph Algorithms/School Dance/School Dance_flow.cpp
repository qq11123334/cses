#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define MAX_Node 1005
using namespace std;
const int BOY = 1;
const int GIRL = 2;
struct Edge {
    int to, cap, rev;
    Edge(int _to, int _cap, int _rev): to(_to), cap(_cap), rev(_rev) {}
};
struct Edge2 {
    int from, to, idx;
    Edge2(int _from, int _to, int _idx): from(_from), to(_to), idx(_idx) {}
};
vector<Edge> adj[MAX_Node];
int level[MAX_Node], iter[MAX_Node];
void add_edge(int from, int to, int cap) {
    adj[from].push_back(Edge(to, cap, (int)adj[to].size()));
    adj[to].push_back(Edge(from, 0, (int)adj[from].size() - 1));
}
int s, t;
void BFS() {
    memset(level, -1, sizeof(level));
    queue<int> q;
    level[s] = 0;
    q.push(s);
    while(!q.empty()) {
        int frt = q.front();
        q.pop();
        for(auto e : adj[frt]) {
            if(e.cap > 0 && level[e.to] == -1) {
                q.push(e.to);
                level[e.to] = level[frt] + 1;
            }
        }
    }
}
int DFS(int now, int flow) {
    if(now == t) return flow;
    for(int &i = iter[now];i < (int)adj[now].size();i++) {
        Edge &e = adj[now][i];
        if(e.cap > 0 && level[e.to] == level[now] + 1) {
            int ret = DFS(e.to, min(flow, e.cap));
            if(ret > 0) {
                e.cap -= ret;
                adj[e.to][e.rev].cap += ret;
                return ret;
            }
        }
    }
    return 0;
}
int max_flow() {
    int ret = 0;
    while(1) {
        BFS();
        if(level[t] == -1) break;
        int tmp;
        memset(iter, 0, sizeof(iter));
        while((tmp = DFS(s, INF)) > 0) {
            ret += tmp;
        }
    }
    return ret;
}
vector<Edge2> edge_list;
int n, m, k;
int number(int num, int gender) {
    if(gender == GIRL) return num + n;
    if(gender == BOY) return num;
    assert(0); 
    return 0;
}
int main() {
    // freopen("input.txt", "r", stdin);
    cin >> n >> m >> k;
    s = 0, t = n + m + 1;
    for(int i = 0;i < k;i++) {
        int b, g; cin >> b >> g;
        int num_boy = number(b, BOY);
        int num_girl = number(g, GIRL);
        edge_list.push_back(Edge2(b, g, (int)adj[num_boy].size()));
        add_edge(num_boy, num_girl, 1);
    }
    for(int i = 1;i <= n;i++) {
        add_edge(s, number(i, BOY), 1);
    }
    for(int i = 1;i <= m;i++) {
        add_edge(number(i, GIRL), t, 1);
    }
    cout << max_flow() << endl;
    BFS();
    for(auto e : edge_list) {
        int num_boy = number(e.from, BOY);
        int num_girl = number(e.to, GIRL);
        if(adj[num_boy][e.idx].cap == 0) {
            printf("%d %d\n", e.from, e.to);
        }
    }
    return 0;
}