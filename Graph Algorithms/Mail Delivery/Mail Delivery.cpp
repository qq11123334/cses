#include <bits/stdc++.h>
using namespace std;
const int N = 100005, M = 200005;
typedef pair<int, int> Edge;
vector<Edge> adj[N];
vector<int> Euler_path;
int deg[N];
bool vis_node[N], vis_edge[M];
void DFS(int x) {
    vis_node[x] = 1;
    while(!adj[x].empty()) {
        auto [v, e] = adj[x].back();
        adj[x].pop_back();
        if(!vis_edge[e]) {
            vis_edge[e] = 1;
            DFS(v);
            Euler_path.push_back(v);
        }
    }
}
void Euler() {
    DFS(1);
    Euler_path.push_back(1);
}
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 1;i <= m;i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        deg[a]++, deg[b]++;
        adj[a].push_back(Edge(b, i));
        adj[b].push_back(Edge(a, i));
    }

    Euler();
    for(int i = 1;i <= n;i++) {
        if((!vis_node[i] && deg[i] != 0) || deg[i] % 2 != 0) {
            printf("IMPOSSIBLE\n");
            exit(0);
        }
    }

    for(auto v : Euler_path) {
        printf("%d ", v);
    }
    return 0;
}