#include <bits/stdc++.h>
#define ll long long int
#define INF 0x3f3f3f3f
using namespace std;
struct Edge
{
    int a;
    int b;
    ll w;
}edge[200005];
vector<pair<ll,ll>> adj[100005];
ll dis1[100005];
ll dis2[100005];
bool used[100005];
bool cmp(Edge a,Edge b)
{
    return a.w > b.w;
}
int main()
{
    memset(dis1,INF,sizeof(dis1));
    memset(dis2,INF,sizeof(dis2));
    memset(used,0,sizeof(used));
    int n,m;
    scanf("%d%d",&n,&m);
    for(int i = 0;i < m;i++)
    {
        int a,b,w;
        scanf("%d%d%d",&a,&b,&w);
        adj[a].push_back({b,w});
        edge[i] = {a,b,w};
    }

    sort(edge,edge+m,cmp);

    priority_queue <pair<ll,ll>> dij;
    dij.push({0,1});
    dis1[1] = 0;
    dis2[1] = 0;
    while(!dij.empty())
    {
        int top = dij.top().second;
        dij.pop();
        if(used[top]) continue;
        used[top] = 1;
        for(auto i : adj[top])
        {
            int a = top;
            int b = i.first;
            ll w = i.second;
            if(dis1[b] > dis1[a] + w)
            {
                dis1[b] = dis1[a] + w;
                dij.push({-dis1[b],b});
            }
        }
    }
    
    memset(used,0,sizeof(used));
    dij.push({0,1});
    while(!dij.empty())
    {
        int top = dij.top().second;
        dij.pop();
        if(used[top]) continue;
        used[top] = 1;
        for(auto i : adj[top])
        {
            int a = top;
            int b = i.first;
            ll w = i.second;
            if(dis2[b] > dis2[a] + w || dis2[b] > dis1[a] + w/2)
            {
                dis2[b] = min(dis2[a] + w,dis1[a] + w/2);
                dij.push({-dis2[b],b});
            }
        }
    }
    printf("%lld",dis2[n]);
    return 0;
}