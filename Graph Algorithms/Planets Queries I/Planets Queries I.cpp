#include <bits/stdc++.h>
#define mx_log 31
using namespace std;
int nxt[200005][32];
int n, q;
void build_nxt()
{
    for(int k = 1;k <= mx_log;k++)
    {
        for(int i = 1;i <= n;i++)
        {
            nxt[i][k] = nxt[nxt[i][k - 1]][k - 1];
        }
    }
}
int qry(int x, int k)
{
    for(int i = 0;i <= mx_log;i++)
    {
        if(k & (1 << i))
        {
            x = nxt[x][i];
        }
    }
    return x;
}
int main()
{
    scanf("%d%d", &n, &q);
    for(int i = 1;i <= n;i++)
    {
        scanf("%d", &nxt[i][0]);
    }

    build_nxt();

    while(q--)
    {
        int x, k;
        scanf("%d%d", &x, &k);
        printf("%d\n", qry(x, k));
    }
    return 0;
}