#include <iostream>
#include <queue>
#include <vector>
#include <cstring>
#include <cstdio>
using namespace std;
vector <int>adj[100005];
vector <int>ans;
queue<int> BFS;
int parent[100005];
int dis[100005];
int vis[100005];
int main()
{
    memset(parent,0,sizeof(parent));
    memset(vis,0,sizeof(vis));
    int n,m;
    scanf("%d%d",&n,&m);

    for(int i = 0;i < m;i++)
    {
        int a,b;
        scanf("%d%d",&a,&b);
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    BFS.push(1);
    dis[1] = 1;
    vis[1] = 1;

    while(!BFS.empty())
    {
        int x = BFS.front();
        BFS.pop();
        for(auto i : adj[x])
        {
            if(!vis[i])
            {
                vis[i] = 1;
                dis[i] = dis[x] + 1;
                BFS.push(i);
                parent[i] = x;
            }
        }
    }

    if(vis[n])
    {
        printf("%d\n",dis[n]);
        int x = n;
        ans.push_back(n);
        while(x != 1)
        {
            ans.push_back(parent[x]);
            x = parent[x];
        }

        for(int i = ans.size() - 1;i >= 0 ;i--)
        {
            printf("%d ",ans[i]);
        }
    }
    else
    {
        printf("IMPOSSIBLE");
    }
    
    return 0;
}