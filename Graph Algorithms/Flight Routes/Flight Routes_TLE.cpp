#include <bits/stdc++.h>
#define pll pair<ll, ll>
#define ll long long int
using namespace std;
multiset<ll> s[100005];
ll n, m, k;
priority_queue<pll, vector<pll>, greater<pll>> pq;
vector<pll> adj[100005];
bool my_insert(int x, ll dis)
{
    if((int)s[x].size() < k)
    {
        s[x].insert(dis);
        return 1;
    }
    else
    {
        auto mx = prev(s[x].end());
        if(*mx > dis)
        {
            s[x].erase(mx);
            s[x].insert(dis);
            return 1;
        }
    }
    return 0;
}
int main()
{
    scanf("%lld%lld%lld", &n, &m, &k);
    for(int i = 0;i < m;i++)
    {
        ll src, des, w;
        scanf("%lld%lld%lld", &src, &des, &w);
        adj[src].push_back({des, w});
    }
 
    pq.push({0, 1});
    my_insert(1, 0);
    while(!pq.empty())
    {
        ll src = pq.top().second;
        ll src_dis = pq.top().first;
        pq.pop();
        for(auto i : adj[src])
        {
            ll des = i.first;
            ll w = i.second;
            if(my_insert(des, src_dis + w))
                pq.push({src_dis + w, des});
        }
    }
 
 
    for(auto dis : s[n])
        printf("%lld ", dis);
    return 0;
}