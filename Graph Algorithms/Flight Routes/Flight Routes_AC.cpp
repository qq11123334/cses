#include <bits/stdc++.h>
#define pll pair<ll, ll>
#define ll long long int
using namespace std;
multiset<ll> s[100005];
ll n, m, k;
priority_queue<pll, vector<pll>, greater<pll>> pq;
vector<pll> adj[100005];
vector<ll> dis[100005];
int main()
{
    scanf("%lld%lld%lld", &n, &m, &k);
    for(int i = 0;i < m;i++)
    {
        ll src, des, w;
        scanf("%lld%lld%lld", &src, &des, &w);
        adj[src].push_back({des, w});
    }

    pq.push({0, 1});
    while(!pq.empty())
    {
        ll src = pq.top().second;
        ll src_dis = pq.top().first;
        pq.pop();
        if((int)dis[src].size() >= k)
            continue;
        
        dis[src].push_back(src_dis);
        
        for(auto i : adj[src])
        {
            ll des = i.first;
            ll w = i.second;
            pq.push({src_dis + w, des});
        }
    }

    for(auto distance : dis[n])
        printf("%lld ", distance);
    return 0;
}

/*
2 2 1
1 2 1
1 2 2
*/