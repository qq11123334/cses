#include <bits/stdc++.h>
using namespace std;
int p[100005], sz[100005];
int n, m, mx, cnt;
void init()
{
    for(int i = 0;i <= n;i++)
    {
        p[i] = i;
        sz[i] = 1;
    }
}
int find(int x)
{
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
void unite(int a, int b)
{
    if(find(a) == find(b)) return;
    a = find(a);
    b = find(b);
    cnt--;
    
    if(sz[a] > sz[b]) swap(a, b);

    p[b] = p[a];
    sz[a] += sz[b];
    mx = max(mx, sz[a]);
}
int main()
{
    scanf("%d%d", &n, &m);
    init();
    cnt = n;
    for(int i = 0;i < m;i++)
    {
        int a, b;
        scanf("%d%d", &a, &b);
        unite(a, b);
        printf("%d %d\n", cnt, mx);
    }
    return 0;
}