#include <bits/stdc++.h>
using namespace std;
const int pN = (1 << 15) + 5, N = 20;
vector<int> adj[pN];
int n, pn;
int next_bit(int x, bool plus_one) {
    return ((x << 1) + plus_one) % pn;
}
string ans;
void Euler(int x) {
    while(!adj[x].empty()) {
        auto v = adj[x].back();
        adj[x].pop_back();
        Euler(v);
        if((v & 1) == 1) ans += '1';
        if((v & 1) == 0) ans += '0';
    }
}
int main() {
    cin >> n;
    if(n == 1) {
        cout << "10" << endl; exit(0);
    }
    pn = (1 << (n - 1));
    for(int i = 0;i < pn;i++) {
        adj[i].push_back(next_bit(i, 0));
        adj[i].push_back(next_bit(i, 1));
    }

    Euler(0);
    string zero; zero.resize(n - 1, '0');
    ans += zero;
    cout << ans << endl;
    return 0;
}