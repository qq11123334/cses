#include <bits/stdc++.h>
#define MOD 1000000007
#define INF 0x3f3f3f3f3f3f3f3f
#define ll long long int
#define pii pair<ll, ll>
using namespace std;
vector<pii> adj[100005];
vector<int> dij_order;
ll dis[100005];
bool mark[100005];
ll cnt[100005];
int mi[100005], mx[100005];
int main()
{
    
    int n, m;
    scanf("%d%d", &n, &m);
    for(int i = 0;i <= n;i++)
    {
        dis[i] = INF;
        mi[i] = 0x3f3f3f3f;
        mx[i] = 0;
    }
    
    for(int i = 0;i < m;i++)
    {
        int src, des;
        ll w;
        scanf("%d%d%lld", &src, &des, &w);
        adj[src].push_back({des, w});
    }

    priority_queue<pii, vector<pii>, greater<pii>> pq;
    pq.push({0, 1});
    dis[1] = 0;
    while(!pq.empty())
    {
        int src = pq.top().second;
        pq.pop();

        if(mark[src]) 
            continue;
        mark[src] = 1;
        dij_order.push_back(src);

        for(auto i : adj[src])
        {
            int des = i.first;
            ll w = i.second;
            if(dis[des] > dis[src] + w)
            {
                dis[des] = dis[src] + w;
                pq.push({dis[des], des});
            }
        }
    }

    cnt[1] = 1;
    mx[1] = 0;
    mi[1] = 0;
    for(auto src : dij_order)
    {
        for(auto i : adj[src])
        {
            int des = i.first;
            int w = i.second;
            if(dis[src] + w == dis[des])
            {
                cnt[des] = (cnt[des] + cnt[src]) % MOD;
                mi[des] = min(mi[des], mi[src] + 1);
                mx[des] = max(mx[des], mx[src] + 1);
            }
        }
    }
    printf("%lld %lld %d %d\n", dis[n], cnt[n], mi[n], mx[n]);
    return 0;
}