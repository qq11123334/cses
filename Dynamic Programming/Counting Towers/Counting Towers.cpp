#include <bits/stdc++.h>
#define ll long long int
#define MOD 1000000007
using namespace std;
ll dp[1000005][6];
//  _|_ __  _|   |_   |
void build() {
    dp[1][0] = 1;
    dp[1][1] = 1;
    dp[1][2] = 1;
    dp[1][3] = 1;
    dp[1][4] = 1;
    dp[1][5] = 1;

    for(int i = 2;i <= 1000000 ;i++) {
        dp[i][0] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][2] + dp[i - 1][3] + dp[i - 1][4];
        dp[i][1] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][5];
        dp[i][2] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][2] + dp[i - 1][3] + dp[i - 1][4];
        dp[i][3] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][2] + dp[i - 1][3] + dp[i - 1][4];
        dp[i][4] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][2] + dp[i - 1][3] + dp[i - 1][4];
        dp[i][5] = dp[i - 1][0] + dp[i - 1][1] + dp[i - 1][5];
        for(int j = 0;j < 6;j++)
            dp[i][j] %= MOD;
    }
}
int main()
{
    build();
    int t;
    cin >> t;
    while(t--) {
        int n; cin >> n;
        cout << (dp[n][0] + dp[n][1]) % MOD << endl;
    }
    return 0;
}