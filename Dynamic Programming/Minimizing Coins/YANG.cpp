#include <bits/stdc++.h>
#define ll long long int
#define N 105
#define M ((ll)1e6 + 5)
#define INF 0x3f3f3f3f
using namespace std;
ll coin[N], dp[M];
int n;
/*
x
x - coin[0], x - coin[1] ... x - coin[n - 1]
*/
ll solve(ll money) { // caculate money money need min coin
    if(money < 0) return INF;
    if(money == 0) return 0;
    if(dp[money]) return dp[money];
    ll mi = INF;
    for(int i = 0;i < n;i++) {
        mi = min(mi, solve(money - coin[i]) + 1);
    }
    return dp[money] = mi;
}

/*
dp[0] = 0, dp[1] = 1, dp[2] = 2, dp[3] = 3, dp[4] = 4, dp[5] = 1
*/
int main() {
    ll x;
    cin >> n >> x;
    for(int i = 0;i < n;i++) {
        cin >> coin[i];
    }
    // ll ans = solve(x);
    // printf("%lld\n", ans >= INF ? -1 : ans);
    dp[0] = 0;
    for(int i = 1;i <= x;i++) {
        // caculate dp[i]
        dp[i] = INF;
        for(int j = 0;j < n;j++) {
            if(i - coin[j] < 0) continue;
            dp[i] = min(dp[i], dp[i - coin[j]] + 1);
        }
    }
    printf("%lld\n", dp[x] >= INF ? -1 : dp[x]);
    return 0;
}