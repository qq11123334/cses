#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int coin[105];
int dp[1000005]; // dp[i], i dollar's minmum
int main()
{
    memset(dp,INF,sizeof(dp));
    int n,x;
    scanf("%d%d",&n,&x);
    for (int i = 0;i < n;i++)
    {
        scanf("%d",&coin[i]);
    }
    dp[0] = 0;
    for(int i = 0;i < n;i++)
    {
        for(int j = coin[i]; j <= x;j++)
        {
            
            dp[j] = min(dp[j] , dp[j - coin[i]] + 1);
        }
    }

    if(dp[x] == INF) printf("-1");
    else
    {
        printf("%d",dp[x]);
    }
    
    return 0;
}