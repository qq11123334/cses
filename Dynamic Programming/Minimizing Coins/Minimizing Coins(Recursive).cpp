#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int coin[105], dp[1000005];
int n;
int C(int money)
{
    if(money == 0) return 0;
    if(money < 0) return INF;
    if(dp[money]) return dp[money];
    
    int mi = INF;
    for(int i = 0;i < n;i++)
        mi = min(mi, C(money - coin[i]) + 1);

    return dp[money] = mi;
}
int main()
{
    int x;
    scanf("%d%d", &n, &x);
    for(int i = 0;i < n;i++)
        scanf("%d", &coin[i]);
    
    printf("%d\n", C(x) >= INF ? -1 : C(x));
    return 0;
}
/*
1 2
3
*/