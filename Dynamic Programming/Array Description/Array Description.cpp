#include <bits/stdc++.h>
using namespace std;
int arr[100005];
int dp[100005][105];
const int MOD = 1000000007;
int main()
{
    int n,m;
    scanf("%d%d",&n,&m);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&arr[i]);
    }

    if(arr[0] == 0)
    {
        for(int i = 1;i <= m;i++)
        {
            dp[0][i] = 1;
        }
    }
    else
    {
        dp[0][arr[0]] = 1;
    }
    
    for(int i = 1;i < n;i++)
    {
        if(arr[i] != 0)
        {
            for(int j = arr[i] - 1;j <= arr[i] + 1;j++)
            {
                if(j >= 1 && j <= m) dp[i][arr[i]] = (dp[i][arr[i]] + dp[i - 1][j]) % MOD;
                //printf("%d %d %d\n",i,j,dp[i][j]);
            }
        }
        else
        {
            if(arr[i - 1] == 0)
            {
                for(int j = 1;j <= m;j++)
                {
                    for(int k = j - 1;k <=j + 1;k++)
                    {
                        //printf("%d\n",i);
                        if(k >= 1 && k <= m) dp[i][j] = (dp[i][j] + dp[i - 1][k]) %MOD;
                        //printf("%d %d %d\n",i,j,dp[i][j]);
                    }
                }
            }
            else
            {
                for(int j = arr[i - 1] - 1;j <= arr[i - 1] + 1;j++)
                {
                    if(j >= 1 && j <= m) dp[i][j] = (dp[i][j] + dp[i - 1][arr[i - 1]]) % MOD;
                    //printf("%d %d %d\n",i,j,dp[i][j]);
                }
            }
        }
    }

    int ans = 0;
    for(int i = 1;i <= m;i++)
    {
        ans = (ans + dp[n - 1][i])%MOD;
    }

    printf("%d",ans);
    return 0;
}