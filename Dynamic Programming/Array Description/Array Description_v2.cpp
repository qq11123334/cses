#include <bits/stdc++.h>
#define MOD 1000000007
#define ll long long int
using namespace std;
ll dp[100005][105];
int arr[100005];
int main()
{
    int n, m;
    scanf("%d%d",&n,&m);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }

    if(arr[0] != 0) dp[0][arr[0]] = 1;
    else for(int i = 1;i <= m;i++) dp[0][i] = 1;

    for (int i = 1; i < n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if(arr[i] == j || arr[i] == 0) dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j] + dp[i - 1][j + 1];
            dp[i][j] %= MOD;
        }
    }

    ll ans = 0;
    for(int i = 1;i <= m;i++)
    {
        ans += dp[n - 1][i];
        ans %= MOD;
    }

    printf("%d\n",(int)ans);
    return 0;
}