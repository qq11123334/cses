#include <bits/stdc++.h>
#define ll long long int
#define pii pair<ll,ll>
using namespace std;
vector<pii> project[400005];
map<ll, int> reindex;
ll dp[400005], start[200005], ending[200005], reward[200005];
int main()
{
    int n;
    scanf("%d", &n);

    for(int i = 0;i < n;i++)
    {
        ll a, b, w;
        scanf("%lld%lld%lld", &a, &b, &w);
        b++; // 重要
        start[i] = a, ending[i] = b, reward[i] = w;
        reindex[a], reindex[b];
    }

    int idx = 0;
    for(auto &v : reindex)
    {
        v.second = idx++;
    }

    for(int i = 0;i < n;i++)
    {
        project[reindex[start[i]]].push_back({reindex[ending[i]], reward[i]});
    }

    for(int i = 0;i < idx;i++)
    {
        if(i > 0) dp[i] = max(dp[i], dp[i - 1]);
        for(auto j : project[i])
        {
            dp[j.first] = max(dp[j.first], dp[i] + j.second);
        }
        //printf("%lld\n", dp[i]);
    }

    printf("%lld", dp[idx - 1]);

    return 0;
}