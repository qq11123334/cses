#include <bits/stdc++.h>
using namespace std;
using ll = long long int;
const ll INF = 1e18;
ll sum(ll n) {
    if (n == 0)
        return 0;
    if (n < 0)
        return INF;
    ll tmp = n;
    int max_digit = 0;
    while (tmp > 0) {
        int digit = tmp % 10;
        max_digit = max(digit, max_digit);
        tmp = tmp / 10;
    }
    return sum(n - max_digit) + 1;
}
int main()
{
    ll n;
    scanf("%lld",&n);
    printf("%lld",sum(n));
    return 0;
}
