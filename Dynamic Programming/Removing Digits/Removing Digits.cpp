#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int dp[1000005];
int sum(int n)
{
    //printf("%d\n",n);
    if (n == 0)
        return 0;
    if (n < 0)
        return INF;
    if(dp[n]) return dp[n];
    int tmp = n;
    int ans = INF;
    while (tmp > 0)
    {
        int digit = tmp % 10;
        if(digit != 0 ) ans = min(ans, sum(n - digit) + 1);
        tmp = tmp / 10;
    }
    return dp[n] = ans;
}
int main()
{
    memset(dp,0,sizeof(dp));
    int n;
    scanf("%d",&n);
    printf("%d",sum(n));
    return 0;
}