#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define INF 0x3f3f3f3f
using namespace std;
int dp[505][505];
int sum(int a, int b)
{
    //printf("%d %d\n",a,b);
    if (a == b)
        return 0;
    if (dp[a][b])
        return dp[a][b];
    int ans = INF;

    for (int i = 1; i <= a / 2; i++)
    {
        ans = min(ans, sum(i, b) + sum(a - i, b) + 1);
    }
    for (int i = 1; i <= b / 2; i++)
    {
        ans = min(ans, sum(a, i) + sum(a, b - i) + 1);
    }

    return dp[a][b] = ans;
}
int main()
{
    memset(dp, 0, sizeof(dp));
    int a, b;
    scanf("%d%d", &a, &b);
    printf("%d", sum(a, b));
    return 0;
}