#include <bits/stdc++.h>
#define MOD 1000000007
#define ll long long int
using namespace std;
ll dp[1000005];
ll coin[105];
int main()
{
    int x, n;
    memset(dp, 0, sizeof(dp));
    scanf("%d%d", &n, &x);

    for (int i = 0; i < n; i++)
    {
        scanf("%lld", &coin[i]);
    }

    dp[0] = 1;

    for (int i = 1; i <= x; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if(i - coin[j] >= 0) dp[i] = dp[i] + dp[i-coin[j]];
            dp[i] %= MOD;
        }
    }

    printf("%lld",dp[x]);
    return 0;
}