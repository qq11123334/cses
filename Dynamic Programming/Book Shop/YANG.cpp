#include <bits/stdc++.h>
#define ll long long int
#define MAX_X ((ll)1e5 + 1)
#define MAX_N ((ll)1e3 + 1)
using namespace std;
int dp[MAX_N][MAX_X]; // Use pre i item, the max value of j weights limit
int weight[MAX_N], value[MAX_N];
int main() {
    int n, x;
    scanf("%d%d", &n, &x);
    for(int i = 1;i <= n;i++) scanf("%d", &weight[i]);
    for(int i = 1;i <= n;i++) scanf("%d", &value[i]);

    for(int i = 1;i <= n;i++) {
        // caculate dp[i][0 ~ x]
        for(int j = 0;j <= x;j++) {
            // caculate dp[i][j]
            dp[i][j] = dp[i - 1][j];
            if(j - weight[i] >= 0) dp[i][j] = max(dp[i - 1][j - weight[i]] + value[i], dp[i][j]);
        }
    }
    int mx = 0;
    for(int i = 0;i <= x;i++) {
        mx = max(dp[n][i], mx);
    }
    printf("%lld\n", mx);
    return 0;
}