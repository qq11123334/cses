#include <bits/stdc++.h>
using namespace std;
int weight[1005];
int value[1005];
int dp[100005];
int main()
{
    int n,x;
    memset(dp,0,sizeof(dp));
    scanf("%d%d",&n,&x);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&weight[i]);
    }

    for(int i = 0;i < n;i++)
    {
        scanf("%d",&value[i]);
    }

    for(int i = 0;i < n;i++)
    {
        for(int j = x;j >= weight[i];j--)
        {
            dp[j] = max(dp[j-weight[i]] + value[i],dp[j]);
        }
    }

    printf("%d",dp[x]);
    return 0;
}