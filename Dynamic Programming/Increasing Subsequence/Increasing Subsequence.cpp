#include <bits/stdc++.h>
using namespace std;
const int N = 200005;
vector <int> dp;
int arr[N];
int main() {
    int n;
    scanf("%d",&n);
    for(int i = 0;i < n;i++) {
        scanf("%d", &arr[i]);
    }

    for(int i = 0;i < n;i++) {
        auto it = lower_bound(dp.begin(), dp.end(), arr[i]);
        if(it == dp.end()) dp.push_back(arr[i]);
        else *it = arr[i];
    }

    printf("%d", dp.size());
    return 0;
}