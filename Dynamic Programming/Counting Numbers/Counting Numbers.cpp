#include <bits/stdc++.h>
#define ll long long int
using namespace std;
vector<int> itov(ll d) {
    vector<int> vc;
    while(d) {
        vc.push_back(d % 10);
        d /= 10;
    }
    return vc;
}
vector<int> v;
ll dp[20][12][2][2];
ll DFS(int level, int digit, bool lead_zero, bool limit) {
    if(level == -1) {
        return 1;
    }
    if(dp[level][digit][lead_zero][limit]) {
        return dp[level][digit][lead_zero][limit];
    }

    int up = limit ? v[level] : 9;
    ll res = 0;
    for(int i = 0;i <= up;i++) {
        if(digit != i || (lead_zero && i == 0)) {
            res += DFS(level - 1, i, lead_zero && i == 0, limit && (i == v[level]));
        }
    }
    return dp[level][digit][lead_zero][limit] = res;
}
ll solve(ll d) {
    v.clear();
    v = itov(d);
    memset(dp, 0, sizeof(dp));
    return DFS((int)v.size() - 1, 11, 1, 1);
}
int main() {
    ll a, b;
    scanf("%lld %lld", &a, &b);
    printf("%lld\n", solve(b) - solve(a - 1));
    return 0;
}