#include <iostream>
#include <cstring>
#include <cstdio>
#define MAX 100000
using namespace std;
bool sum[100005];
int coin[105];
int main()
{
    int n;
    memset(sum,0,sizeof(sum));
    scanf("%d",&n);
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&coin[i]);
    }

    sum[0] = 1;

    for(int i = 0;i < n;i++)
    {
        for(int j = MAX; j >= coin[i];j--)
        {
            sum[j] |= sum[j - coin[i]];
        }
    }

    int cnt = 0;
    for(int i = 1;i <= MAX;i++)
    {
        if(sum[i]) cnt++;
    }

    printf("%d\n",cnt);

    for(int i = 1;i <= MAX;i++)
    {
        if(sum[i]) printf("%d ",i);
    }
    return 0;
}