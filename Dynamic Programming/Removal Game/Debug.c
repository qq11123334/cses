 #include<stdio.h>
 #include<stdlib.h>
 
   int min(int a , int b ){
 
       if( a>b ){
           return b;
       }
       if( (a<b) || ( a==b )){
          return a;
      }
  }
  int max(int a , int b ){
 
      if( a>b ){
          return a;
     }
      if( (a<b) || ( a==b )){
          return b;
      }
  }
 
  int list[1000], dp[1000][1000];
 
  int solve(int left, int right){
 
      int ans1, ans2;
 
      if( left > right ){
          return 0;
      }
      if(left == right){
          return list[left];
      }
      if(dp[left][right]){
          return dp[left][right];
      }
      if( left <= right+2){
          ans1 = min(list[left]+solve(left+2 , right),list[left]+solve(left+1,right-1));
      }
      if(left <= right+2){
          ans2 = min( list[right]+solve(left+1, right-1),list[right]+solve(left, right-2));
     }
 
      return dp[left][right] = max(ans1,ans2);
  }
 
  int main (){
 
      int n, i;
 
      while ((scanf("%d",&n) != EOF)){
 
          int total=0, minus=0;
 
          for(i=0 ; i<n ; i++){
              scanf("%d",&list[i]);
              total = total + list[i];
          }
 
          //printf("%d\n",solve(0, n-1));
 
          minus = total - solve(0, n-1);
 
          if( (solve(0, n-1)>minus) || (solve(0, n-1)==minus)){
              printf("Ture\n");
          }
          if( solve(0, n-1)<minus){
              printf("False\n");
          }
      }
 
      return 0 ;
 
  }