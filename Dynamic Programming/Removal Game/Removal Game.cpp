#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define ll long long int
#define INF 0x3f3f3f3f3f
using namespace std;
ll dp[5005][5005];
ll arr[5005];
ll solve(ll l, ll r)  //算出 在只有索引值為l ~ r的數字中 first player可得幾分
{
    ll ans1,ans2;
    if(l > r) return 0;
    if(l == r) return arr[l];
    if(l == r - 1) return max(arr[l], arr[r]);
    if(dp[l][r]) return dp[l][r];
    
    // first player choose leftmost number
    // and consider the second player choose leftmost number and rightmost number after first player choose leftmost number
    ans1 = min(arr[l] + solve(l + 2, r),arr[l] + solve(l + 1, r - 1));


    //first player choose rightmost number
    //and consider the second player choose leftmost number and rightmost number after first player choose rightmost number
    ans2 = min(arr[r] + solve(l + 1, r - 1),arr[r] + solve(l, r - 2));


    //printf("%lld %lld %lld\n",l,r,max(ans1,ans2));

    return dp[l][r] = max(ans1, ans2);
}
int main()
{
    int n;
    scanf("%d",&n);

    for(int i = 0;i < n;i++)
    {
        scanf("%lld",&arr[i]);
    }
    
    //最後first player最高可得的分數
    printf("%lld",solve(0,n - 1));
    return 0;
}