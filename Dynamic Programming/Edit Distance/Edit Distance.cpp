#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
int dp[5005][5005];
int main()
{
    string a, b;
    cin >> a >> b;
    int n = (int)a.size(), m = (int)b.size();
    memset(dp, INF, sizeof(dp));
    dp[0][0] = 0;
    for(int i = 0;i <= n;i++)
    {
        for(int j = 0;j <= m;j++)
        {
            if(i) dp[i][j] = min(dp[i][j], dp[i - 1][j] + 1);
            if(j) dp[i][j] = min(dp[i][j], dp[i][j - 1] + 1);
            if(i && j) dp[i][j] = min(dp[i][j], dp[i - 1][j - 1] + (a[i - 1] != b[j - 1]));
        }
    }

    cout << dp[n][m];


    return 0;
}