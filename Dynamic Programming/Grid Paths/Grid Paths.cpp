#include <bits/stdc++.h>
#define MOD 1000000007
using namespace std;
char grid[1005][1005];
int dp[1005][1005];
int main()
{
    memset(dp,0,sizeof(dp));

    int n;
    scanf("%d\n",&n);

    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            scanf(" %c",&grid[i][j]);
        }
    }

    dp[1][1] = 1;

    for(int i = 1;i <= n;i++)
    {
        for(int j = 1;j <= n;j++)
        {
            if(!(i == 1 && j == 1)) dp[i][j] = (dp[i-1][j] + dp[i][j-1])%MOD;
            if(grid[i][j] == '*') dp[i][j] = 0;
        }
    }

    printf("%d",dp[n][n]);
    return 0;
}