#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
#define ll long long
using namespace std;
pair<ll, ll> dp[1 << 20];
ll weight[25];
int main() {
    int n;
    ll x;
    scanf("%d%lld", &n, &x);
    for(int i = 0;i < n;i++)
        scanf("%lld", &weight[i]);

    dp[0] = {1, 0};
    for(int s = 1;s < (1 << n);s++) {
        dp[s] = {INF, INF};
        for(int i = 0;i < n;i++) {
            if(s & (1 << i)) {
                pair<ll, ll> option = dp[s ^ (1 << i)];
                if(option.second + weight[i] > x) {
                    option.first++;
                    option.second = weight[i];
                } else {
                    option.second += weight[i];
                }
                dp[s] = min(dp[s], option);
            }
        }
    }

    printf("%lld\n", dp[(1 << n) - 1].first);
    return 0;
}