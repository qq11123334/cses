#include <bits/stdc++.h>
#define MOD 1000000007
#define ll long long int
using namespace std;
ll dp[1000005];
ll solve(int n)
{
    if(n < 0) return 0;
    if(n == 0) return 1;
    if(dp[n]) return dp[n];
    ll sum = 0;
    for (int i = 1;i <= 6;i++)
    {
        sum = (sum + solve(n - i)) %MOD;
    }
    return dp[n] = sum % MOD;
}
int main()
{
    int n;
    memset(dp,0,sizeof(dp));
    scanf("%d",&n);
    printf("%lld",solve(n));
    return 0;
}