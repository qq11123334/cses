#include <bits/stdc++.h>
#define MOD (1000000007)
using namespace std;
const int pN = 1025, M = 2001;
int dp[M][pN];
vector<int> last_valid[pN];
int m, n, pn;
// 0 []ㄩ
// 1 ㄇ
void print_bit(int bitmask) {
    for(int bit = 1;bit < pn;bit <<= 1) {
        printf("%d", (bitmask & bit) > 0);
    }
}
inline bool is_valid(int bitmask) { // 就是你的key
    for(int bit = 1;bit < pn;bit <<= 1) {
        if(!(bitmask & bit)) {
            int next_bit = (bit << 1);
            if(next_bit >= pn) return 0;
            if(bitmask & next_bit) return 0;
            bitmask |= bit;
            bitmask |= next_bit;
        }
    }
    return 1;
}
inline void build_valid() {
    for(int b_cur = 0;b_cur < pn;b_cur++) {
        for(int b_last = 0;b_last < pn;b_last++) {
            if(!(b_cur & b_last) && is_valid(b_cur | b_last)) {
                last_valid[b_cur].push_back(b_last);
            }
        }
    }
}
int main() {
    scanf("%d%d", &n, &m);
    pn = (1 << n);
 
    for(int b = 0;b < pn;b++) {
        if(is_valid(b)) {
            dp[1][b]++;
        }
    }
    
    build_valid();

    for(int i = 2;i <= m;i++) {
        for(int b_cur = 0;b_cur < pn;b_cur++) {
            for(auto b_last : last_valid[b_cur]) {
                dp[i][b_cur] += dp[i - 1][b_last];
                dp[i][b_cur] %= MOD;
            }
        }
    }
    printf("%d\n", dp[m][0]);
    return 0;
}