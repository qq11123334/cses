#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define MOD 1000000007
using namespace std;
int dp[505][124755];
int main()
{
    memset(dp,0,sizeof(dp));
    int n;
    scanf("%d",&n);

    dp[0][0] = 1;
    for(int i = 1;i <= n;i++)
    {
        for(int j = 0; j <= n*(n-1)/2;j++)
        {
            dp[i][j] = dp[i - 1][j];
            if(j - i >= 0) dp[i][j] = (dp[i][j] + dp[i - 1][j - i])%MOD;
        }
    }

    if(n % 4 == 0 || (n+1) % 4 == 0)
    {
        printf("%d",dp[n-1][n*(n+1)/4]);
    }
    else
    {
        printf("0");
    }
    
    return 0;
}